<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Device
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
//print_r($_SESSION);exit;
$lang = $language->english('eng');	
?>	

<script>
 $(function() {
		
		$("#creation_date").datepicker();
		$("#expiary_date").datepicker();
	});

</script>
<form method="post" name="company_add" id="company_add" enctype="multipart/form-data">
<center>
		<?php 
			
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
		?>

 <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody>
	<tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left:5px;" ><font color="#FFFFFF"><?php echo $lang['Edit Device']?></font></th>
    </tr>
	<tr>
		<td   colspan="2" style="font-size:10px; color:red;" align="right"  class="fontstyle">*<?php echo $lang['All fields are mandatory']?></td>
	</tr>
	<?php if ($_SESSION['usertype'] == 'super_admin') { ?>
    <tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Related To'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		<select class="form-control" name="company_id">
		<option value="">Please Select</option>
		<?php 
		for($i=0;$i<count($company[0]);$i++)
		{
		?>
			<option value= "<?php echo$company[0][$i]->company_id?>" <?php if($_SESSION['company_id'])echo "selected";?>><?php echo $company[0][$i]->company_name; ?> </option>
		<?php 
		} 
		?>
		</select>
		</div></td>
	</tr>
	<?php } ?> <input type="hidden" class="form-control" name="company_id" value="<?php echo $admin_company->company_id;?>">
    <tr>
		<td class="fontstyle" align="right">
		 <div class="form-group" style="width:63% !important;">
            <label for="username" class="control-label col-xs-10"><?php echo $lang['Device Name'].MANDATORYMARK ?></label></div></td>
		<td align="left">
		<div class="col-xs-4">
		   <input type="text" title="Enter Company Name" class="form-control" id="device_name" name="device_name" value="<?php echo $Row->device_name;?>" onbuler="return  KeywordSearch()" > 
		  </div></td>		
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%">
        <label for="username" class="control-label col-xs-10"><?php echo $lang['Device Location'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		 <select class="form-control" name="location_id" id="location_id" style="width:170px;">
			<option value="" >Please Select</option>
			<?php  
			for($i=0;$i<count($loc[0]);$i++)
			{ ?>
				<option value="<?php echo $loc[0][$i]->location_id;?>" <?php if($Row->location_id == $loc[0][$i]->location_id) { echo "selected"; } ?>><?php echo $loc[0][$i]->location_name;?>
				</option>
			<?php  } 
			?>
			</select> 
			</div> </td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Device Owner'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-4">
		<select class="form-control" name="user_id" style="width:170px;">
		<option value="">Please Select</option>
		<?php 
		for($i=0;$i<count($users[0]);$i++)
		{ ?>
		
		<option value="<?php echo $users[0][$i]->user_id;?>" <?php if($Row->user_id == $users[0][$i]->user_id) { echo "selected"; } ?>><?php echo $users[0][$i]->first_name.' '.$users[0][$i]->last_name;?>
		</option>
		<?php
		}
		?>
		</select>
		</div></td>
	</tr>
        <tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="username" class="control-label col-xs-10"><?php echo $lang['Device Status'].MANDATORYMARK ?></label></td>
		<td align="left">
		<div class="col-xs-6">
		 <select class="form-control" name="device_status" id="device_status" style="width:170px;">
			<option value="" >Please Select</option>
			<option value="Active" <?php if($Row->device_status=='Active') { echo "selected"; }?>>Active </option> &nbsp; &nbsp;&nbsp;&nbsp;
            <option value="Inactive" <?php if($Row->device_status=='Inactive') { echo "selected"; }?>>In-Active</option>
			</select>
		</div> </td>
	</tr>
    
	 <tr>
		<td class="fontstyle" align="right"><label for="description" class="control-label col-xs-10"><?php echo $lang['Device Description'].MANDATORYMARK ?></label></td>
		<td align="left"><div class="col-xs-4">
       <textarea class="form-control" name="device_description"><?php if($_POST['device_description']) echo $_POST['device_description']; else echo $Row->device_description?></textarea> </div></td>
	</tr>
    <tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="update"><?php echo $lang['Update']?></button>
		<!--<button type="reset" class="btn btn-primary" name="Reset"><?php echo $lang['Reset']?></button>-->
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=device');?>'"><?php echo $lang['Back']?></button>
     </div>
	</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b>

	
</form>

</center>
</body>
</html>