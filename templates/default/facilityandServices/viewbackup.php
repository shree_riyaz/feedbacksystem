<?php
    /*======================================
    Developer    -    Neha Pareek
    Module      -  Facility & Services
    SunArc Tech. Pvt. Ltd.
    ======================================        
    ******************************************************/
    

?>

<script type="text/javascript" src="<?php echo ROOTADMINURL?>js/jquery.min.js"></script>
   

<?php 
$lang = $language->english($lang);
?>
<form method="post" name="fault_add" id="fault_add" enctype="multipart/form-data" onsubmit="return validation()">
<center>
    <?php 
            //print_r($company);
        if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                <div class="alert alert-danger alert-dismissable">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                  echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                <div class="alert alert-success alert-dismissable">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['success']);
            }
            ?>

	<table class="table table-striped" style="width:80% !important;">
    
	<tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2"><font color="#FFFFFF"><?php echo $lang['View Faults']?></font>
	  </th>
	   </tr>
	<tr height="36">
	<td align='center'  style="text-align : center;  ">
	<font color="black"><?php echo $lang['Service Name']?></font>
	</td>
	<td align='center'>
	<?php echo ucfirst($service->service_name); ?>
	</td>
	<td></td>
	</tr>
	<tr height="36">
	<td align='center'  style="text-align : center; ">
	<font color="black"><?php echo $lang['Service Description']?></font>
	</td>
	<td align='center'>
	<?php echo ucfirst($service->service_description); ?>
	</td>
	<td></td>
	</tr>
	<tr>
	<td width="20%" align='center'>
		Faults 
	</td>
	<td align="center">

		
		<table class="table" align="center" >
		
		<tr height="20">
			<td  height="20"><?php echo $lang['Fault Name']?>
			<!--<a style="cursor:pointer; " onClick="OrderPage('fault_name');">
		<?php if($frmdata['orderby']=='fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>--></td>
		<td align="center"  > 
		<?php echo $lang['Fault Image']?>&nbsp;&nbsp;
		</td>
		<td height="20"><?php echo $lang['Remove']?></td>
		</tr>
		
<?php
   // echo '<pre>';print_r($Row);
		$srNo=$frmdata['from'];
		$count=count($Row);
		
if($Row)
{	
			
    $objpassEncDec = new passEncDec;
	for($counter=0;$counter<$count;$counter++)
	{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
		$confirmDelete = 'Do you really want to delete this Fault ?';		
		  $obj = new passEncDec;
  ?>
		<tr  >
		<td align='left' ><?php echo ucfirst($Row[$counter]->fault_name); ?>&nbsp; &nbsp;</td>
		<td align='center'><!--<?php echo $Row[$counter]->fault_image; ?>&nbsp;-->
		
					<?php if($Row[$counter]->fault_image)
						{  
						$pos = strrpos($Row[$counter]->image,'/');
						$img = substr($Row[$counter]->image,$pos+1);
						
									if(file_exists( "images/faults_images/".$Row[$counter]->fault_image))
									{	
									?>
										<div align="left">
										<img src="<?php echo "images/faults_images/".$Row[$counter]->fault_image; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
										</div>
								<?php
									}
									else
									{ ?> 
										<div align="left">
										<img src="<?php echo "images/faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
										</div>
									<?php 
									
									}
						}
						else
						{  ?>
							<div align="left">
							<img src="<?php echo "images/faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
							</div>
						<?php 
								
						}
						?>
		
		
		
		</td>&nbsp;
		<td align='right'>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=faults&do=del&id='.$Row[$counter]->fault_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a></td>
		</tr>
	
<?php
$sno++;
	}
?>
		</table>
		</td>
		<td></td>
	</tr>
	<tr>
	<td width="50%" align='center'>
		Add Faults
	</td>
	<td>
		<table class="table table-striped" id="myfields">
			<tbody>
				<tr>
				<td width= "15%">
				   <label> Fault: </label>
				</td>
				<td width="20" height="22">
				   <input type='text' name='mytextfield[]' class="form-control txt"/>
				</td>
				<td width="15%">
				 <label> Image: </label>
				</td>
				<td width="20">
				   <input type='file' name='myFilesfield[]' class="form-control" />
				</td>
				<td width="10%"></td>
				</tr>
				<tfoot>
					<tr class="alt" align="center">
					<td colspan="4" style="text-align: center; padding-top : 10px;" align="center">
					<a href='javascript:addFields()' class="btn btn-primary" id="addmorelink">Add More</a>
					<!--<a href='javascript:removeFields()' class="btn btn-primary" id="removelink">Remove</a>-->
					 <div class="col-xs-offset-2 col-xs-8" style="width:40% !important; ">
					<button type="submit" class="btn btn-primary" name="add"><?php echo $lang['Add']?></button>
					<input type="button" value="<?php echo $lang['Reset']?>" onClick="this.form.reset()" class="btn btn-primary"/>
					<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=facilityandServices&do=view&id=1');?>'"><?php echo $lang['Back']?></button>
					</div>
					</td>
					</tr>
				<tfoot>
			<tbody>
		</table>
	</td>
	<td></td>
	</tr>
	</table>
	
<?php 
}
else
{

$frmdata['message']="Sorry ! user not found for the selected criteria";
		ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php
}
?>

</body>
<script type="text/javascript">
function addFields()
 {
     var table = document.getElementById('myfields');
     var row = table.insertRow(1);
    var cell1 = row.insertCell();
    var cell2 = row.insertCell();
    var cell3 = row.insertCell();
    var cell4 = row.insertCell();
	 var cell5 = row.insertCell();
    cell1.innerHTML = "<b>Fault: </b>";
    cell2.innerHTML = " <input type='text' name='mytextfield[]'  class='form-control txt'/>";
    cell3.innerHTML = "<b>Image:</b> ";
    cell4.innerHTML = "<input type='file' name='myFilesfield[]'  class='form-control'/> ";
	cell5.innerHTML = "<input type='button' value='Remove' class='btn btn-primary' onclick='SomeDeleteRowFunction(this);' />";
    var newTextContent = " <input type='text' name='mytextfield[]'  class='form-control'/>";
    var newFileContent = "<input type='file' name='myFilesfield[]'  class='form-control'/>";
      $("#mytextfield").append(newTextContent); 
      // alert("ok");
      $("#myFilesfield").append(newFileContent);   
 }
 
 function doValidate(fault)
{
	var e = document.getElementsByName(fault_name);
	for(var i = 0; i < e.length; i++)
	{
		if(e[i].value == "")
		{
			alert("Please Enter Fault");
			return;
		}
	}
	alert("all fields have some value");
}


    function validation()
    {
           
		var b=document.getElementsByTagName('input');
		
		var count=0;
			for (i=0;i<b.length;i++)
			{
				var box=b[i];
				if(	box.value=='' && box.type=='text')
				{
					alert('You put something in the box');
					box.focus();
					return false;
				}
			}
	}
    
	function SomeDeleteRowFunction(btndel) {
    if (typeof(btndel) == "object") {
        $(btndel).closest("tr").remove();
    } else {
        return false;
    }
}


</script>
</html>
	
	
	
	
	
	
	<!--Faults -->
<!--
<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
  	
		<tr>
  		<td align="right" height='12'>&nbsp;
		<a title="Add New Faults" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=faults&do=add "><?php echo $lang['Add New Faults']?>
        </a>
		</td>
  		</tr>
</table>
<?php
   // echo '<pre>';print_r($Row);
		$srNo=$frmdata['from'];
		$count=count($Row);
		?>
		<table width="90%" align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped" >
		<tr class="trwhite"> 
			<td colspan="2" align='left'>
					<?php	
						if($totalCount > 0)
						   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; 
						else
                           print "Showing Results:0-0 of 0";
						?>
						</td>		
						<td align="right" colspan="6"  ><?php echo getPageRecords();?></td>
					</tr>
		</tr>
	<tr class="tblheading">
		<th class="anth" height="36" ><?php echo $lang['S.No.']?></th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer; color:#FFF;" onClick="OrderPage('fault_name');"><?php echo $lang['Fault Name']?>
		<?php if($frmdata['orderby']=='fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth"> 
		<a  style="color:#FFF;"><?php echo $lang['Fault Image']?></a>
		</th>
		<th class="anth"><?php echo $lang['Remove']?></th>
	</tr>	
<?php
if($Row)
{	
			
    $objpassEncDec = new passEncDec;
	for($counter=0;$counter<$count;$counter++)
	{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
		$confirmDelete = 'Do you really want to delete this Fault ?';		
		  $obj = new passEncDec;
		  ?>
		
	<tr>
		<td align='center'><?php echo $srNo; ?></td>
		<td align='center'><?php echo ucfirst($Row[$counter]->fault_name); ?>&nbsp;</td>
		<td align='center'><?php echo $Row[$counter]->fault_image; ?>&nbsp;</td>
		<td align='center'>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=faults&do=del&id='.$Row[$counter]->fault_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		</td>
		
	</tr>
	<?php
		$sno++;
	}
?>
</table>
   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	</div>
<?php 
}
else
{

$frmdata['message']="Sorry ! user not found for the selected criteria";
		ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php
}
?>-->
	
	
	
	
	