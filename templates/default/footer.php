</div>

</div>
</div>
</div>
  <!-- end #footer -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Are you sure?</h4>
            </div>
            <div class="modal-body text-center">
                <p class="modal-message-p">Do you really want to delete these records?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<!--                <button type="button"  class="btn btn-danger" data-dismiss="modal">Delete</button>-->
                <a href="#" id="confirm_delete_action" class="btn btn-danger delete-href">Delete</a>
            </div>
        </div>

    </div>
</div>

</body>
</html>

<script type="text/javascript">
    $('document').ready(function() {
        $('#action_list_id').on("change",function() {
            $('#myModal').modal('show');

        });
    });
</script>

<script type="text/javascript">
    $('document').ready(function() {
        $('#confirm_delete_action').on("click",function() {
//            alert(6456);
            document.frmlist.submit();
//            $('#frmlist').form.submit();
            $('#myModal').modal('hide');
        });
    });
</script>

<script>
    $('.delete-modal').click(function () {
        var get_link = $(this).attr("delete-link");
        var get_delete_msg = $(this).attr("data-message");
//        alert(get_delete_msg);
        $('.delete-href').attr("href",get_link);
        $('.modal-message-p').text(get_delete_msg);

    });
</script>
<script type="text/javascript">

//Make focus to first element of the form.
for(var i = 0; document.forms[0].elements[i].type == 'hidden' || document.forms[0].elements[i].disabled; i++)
{continue;}

document.forms[0].elements[i].focus();



</script>
<script type="text/javascript" src="<?php echo ADMINSCRIPTROOT ?>/js/app.js"></script>
<script type="text/javascript">

    $('.demo123').daterangepicker({
        "showDropdowns": true,
        "linkedCalendars": false,
        "alwaysShowCalendars": false,
    }, function(start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });
</script>

<script type="text/javascript">

//    console.log(4566);
//    console.log(<?php //echo $_SESSION['maya'] ; ?>//);

$(document).on('click','applyBtn',function () {
    var selected_span_text = $('.span_range').text();
    var selected_text_box = $('#selected_date_range_hidden').val();

    var vall =  $('#reportrange .span_range').text($('#selected_date_range_hidden').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))) ;
    $('#data_of_custom').val(vall);

});

    $(function() {

        console.log(<?php echo $_SESSION['start_date_by_interval'] ; ?>)
        console.log(<?php echo $_SESSION['end_date_by_interval'] ; ?>)

        var start = <?php echo isset($_SESSION['start_date_by_interval']) ? $_SESSION['start_date_by_interval'] : "moment().subtract(30, 'days')"?>;
        var end = <?php echo  isset($_SESSION['end_date_by_interval']) ? $_SESSION['end_date_by_interval'] : "moment()" ?>;

//        console.log(start);
//        console.log(end);

        function cb(start,end,label) {

//            alert(start);
            $('#reportrange .span_range').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#selected_date_range_hidden').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
//            $('.ranges ul li').data('data-range-key',label);
            $('#selected_date_range_type_hidden').val(label);

//            $(document).on('click','div.ranges>ul>li',function () {
//                var selected_li = $(this).text();
//                $('#selected_date_range_type_hidden').val(selected_li.trim());
//                $('div.ranges>ul>li').val(label);
//            });
//

            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        }

        $('#reportrange').daterangepicker({
            "showDropdowns": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(30, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
//                'Custom Range': [start, end]
            },
            startDate: start,
            endDate: end,
            opens: 'left'
        }, cb);

        cb(start, end);

    });


        $('.navbar-collapse a.collapsed').click(function (e) {
            $('.list-tabs').animate({
                scrollTop: $(this).offset().top - 50
            }, 500);


        });

        $(document).ready(function (e) {
            if( $('li.active-tab').length >= 1 ) {
                var offset = $('li.active-tab').offset().top;
                $('.list-tabs').animate({
                    scrollTop: offset - 50
                }, 500);
            }
        });

        $('.device-link').click(function() {
            if ($('#collapseThree').hasClass('in'))
            {
                $('#collapseThree').removeClass('in');
                $('#collapseThree').addClass('collapse');
                $('a.report-link').addClass('collapsed');

            }



        });
        $('.report-link').click(function() {
            if ($('#collapseTwo').hasClass('in'))
            {
                $('#collapseTwo').removeClass('in');
                $('#collapseTwo').addClass('collapse');
                $('a.device-link').addClass('collapsed');
            }


        });


</script>
