<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Role
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	

?>
<script>
 $(function() {
		
		$("#from").datepicker();
		$("#to").datepicker();
	});
</script>
<table width="90%" align='center'>
	<tr>
		<td class="mainhead">
			Assign Role
		</td>
	</tr>
</table>		
<br>	
		<br>	
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
  	
		<tr>
  		<td class="mainhead">&nbsp;
		
		
		<?php
		include_once('nav.php');
			?>
			</td>
  		</tr>
</table><br/>	
<form method="post" name="frmlist" id="frmlist" enctype="multipart/form-data">
<center>
	<?php 
		// print_r($roles);print_r($users);		
			if(isset($_SESSION['error']))
			{
					echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsgnew">';
				echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center"><div class="successmsg">';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			
			}
			?>

 <table width="60%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablecss">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="3" style="padding-left: 5px;"><font color="#FFFFFF">Assign New Role To User</font></th>
    </tr>
	<tr>
		<td colspan="3"  style="font-size:9px"align="right"  class="fontstyle">All fields are mandatory</td>
	</tr>
	
	<tr> 
		<td align="right" class="fontstyle" width="30%">Role name</td>
		<td align="left" colspan="2"><select name="role_id">
		<?php 
		$count = count($roles[0]);
		for($i=0;$i<$count;$i++)
		{
			if($users[0][0]->role_id == $roles[0][$i]->role_id)
			{
				$selected = ' selected';
			
			}
			echo '<option value="'.$roles[0][$i]->role_id.'" '.$selected.'>'.$roles[0][$i]->role_name.'</option>';
		}
		?>
		
		</select> </td>
	</tr>
	<tr> 
		<td align="right" class="fontstyle" width="30%">User Name</td>
		<td align="left" colspan="2"><select name="user_id">
		
		<?php 
		
		$users_count = count($users[0]);
		for($i=0;$i<$users_count;$i++)
		{
			if($users[0][0]->user_id == $users[0][$i]->id)
			{
				$selected = ' selected';
			
			}
			echo '<option value="'.$users[0][$i]->id.'" '.$selected.'>'.$users[0][$i]->first_name.$users[0][$i]->last_name.'</option>';
		}
		?>
		
		</select></td>
	</tr>
	<tr>
				<td>
					<div align="right">Date:</div>
				</td>
			<td align="left">From :<input title="Enter Creation Date" style="text-transform: capitalize;" type="text" name="fromDate" id="from" size=25 value="<?php echo $_POST['from'];?>"> </td>
	</tr>
	<tr>
	<td></td>
		<td align="left">To :<input title="Enter Expiary Date" style="text-transform: capitalize;" type="text" name="toDate" id="to" size=25 value="<?php echo $_POST['to'];?>"> </td>
	</tr>
		
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left"><input type="submit"
			value="Assign Role" name="assignrole" id="assignrole" /> <input type="reset"
			value=" Reset " name="clear" id="clear" />
	  <input type="button" name="back" id="back" value="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=role');?>'"/>			</td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

	
</form>

</center>
</body>
</html>
