<?php
	/*======================================
	Developer	-	JAishree Sahal
	Module      -   Role 
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
	

?>
<script>
function Clear()
{
	document.getElementById('role_name').value='';
	var checkboxes = document.getElementsByTagName('input');
	for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type === 'checkbox') {
               checkboxes[i].checked = false;
			   
          }
		
} }
</script>
<form method="post" name="frmlist" id="frmlist" enctype="multipart/form-data">
<center>
	<?php 
			
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="3" style="padding-left: 5px;"><font color="#FFFFFF">Add New Role</font></th>
    </tr>
	<tr>
		<td   colspan="3"  style="font-size:10px; color:red;" align="right"  class="fontstyle">*All fields are mandatory</td>
	</tr>
	
	<tr> 
		<td align="right" class="fontstyle" width="30%"><label for="role_name" class="control-label col-xs-10"><?php echo $lang['Role Name'].MANDATORYMARK?></label></td>
		<td align="left" colspan="2" style="padding-left:5px;"><input title="Enter Role Name" type="text" name="role_name" id="role_name" size=25 value="<?php echo $_POST['role_name'];?>" class="form-control"> </td>
	</tr>
	</tbody></table>
<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
  
		<td class="fontstyle" align="left">
		<label for="role_name" class="control-label col-xs-10"><?php echo $lang['Module Access']?></label>
		<td align="right"><a style="cursor:pointer;font-weight:bold" onclick="selectAllModule()">Select All</a> &nbsp;&nbsp;&nbsp;
		<a style="cursor:pointer;font-weight:bold" onclick="unselectAllModule()">
		Unselect All</a> </span>
		</td>
	</tr>
	<tr> 
	<?php
			//print_r($modules);
			$mdcnt=count($modules[0]); 			
			for($counter =0; $counter < $mdcnt; $counter++)
			{
				//echo $counter;
				if ($modules[0][$counter]->module_id != '')
				{	
					echo $modules[$counter]->module_id;
					($counter == ($mdcnt -1)) ? $colspan='2' : $colspan ='';	
					$checkeda = '';
					$checkede = '';
					$checkedd = '';
					$checkedr ='';
					$checkeded = '';
					$checkedl='';
					$check = '';
					$display = 'display:none';
												
						echo ($counter % 2 == 0) ? '<tr>' : '';								
						echo '<td colspan="'.$colspan.'" width="50%">
						<input type="checkbox" '.$check.' name="modules['.$modules[0][$counter]->module_id.']" id="'.$modules[0][$counter]->module_id.'" value="'.$modules[0][$counter]->module_id.'" onclick="showhidePer(this,this.id)">
						<label for="'.$modules[0][$counter]->module_id.'">&nbsp;&nbsp;'.ucfirst($modules[0][$counter]->module_name).'</label>';
						echo ' <div style="margin-left:25px;'.$display.'" id="div:'.$modules[0][$counter]->module_id.'" class="chk">';
						if($modules[0][$counter]->module_name!='dashboard' && $modules[0][$counter]->module_name!='help')
						{
						echo '			
						<input type="checkbox" '.$checkeda.' name="modules['.$modules[0][$counter]->module_id.'][]" value="A" id="'.$modules[0][$counter]->module_id.':A" >&nbsp;&nbsp;Add
						<input type="checkbox" '.$checkede.' name="modules['.$modules[0][$counter]->module_id.'][]" value="E" id="'.$modules[0][$counter]->module_id.':E">&nbsp;&nbsp;Edit
						<input type="checkbox" '.$checkedd.' name="modules['.$modules[0][$counter]->module_id.'][]" value="D" id="'.$modules[0][$counter]->module_id.':D">&nbsp;&nbsp;Delete
						<input type="checkbox" '.$checkedr.' name="modules['.$modules[0][$counter]->module_id.'][]" value="R" id="'.$modules[0][$counter]->module_id.':R" >&nbsp;&nbsp;View
						<input type="checkbox" '.$checkedl.' name="modules['.$modules[0][$counter]->module_id.'][]" value="L" id="'.$modules[0][$counter]->module_id.':L" >&nbsp;&nbsp;List';
						}
						if (in_array($modules[0][$counter]->module_name, array('question_master','backup','report','candidate_master','stream_master')))
						{
							echo '<input type="checkbox" '.$checkeded.' name="modules['.$modules[0][$counter]->module_id.'][]" value="ED" id="'.$modules[0][$counter]->module_id.':ED" >Export Data';
						}
					
					echo   '</div>
						 </td>';				
					echo ($counter % 2 != 0) ? '</tr>' : '';
					
				}	 
			}
			?>
	</tr>
	<tr class="alt">
		<td colspan=2 style="text-align: center;" align="left">
        	<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		<button type="submit" class="btn btn-primary" name="addrole"><?php echo $lang['Add']?></button>
		<button type="reset" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=role');?>'"><?php echo $lang['Back']?></button>
     </div>
     </td>
	</tr>
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>

	
</form>

</center>
</body>
<script>
function showhidePer(module, id)
{
	div = 'div:'+id;
	
	if (module.checked)
	{	
		//alert(div);
		//$("#div").show();
		document.getElementById(div).style.display='';
	}
	else
	{
		document.getElementById(id+':A').checked=false;
		document.getElementById(id+':E').checked=false;
		document.getElementById(div).style.display='none';
		if (document.getElementById(id+':ED'))
		{
			document.getElementById(id+':ED').checked = false;
		}
	}	
}
</script>
</html>
