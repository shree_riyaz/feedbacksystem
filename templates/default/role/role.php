<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Online Examination - Manage Role</title>

</head>

<?php
include_once(ROOT."/incajax.php");
$xajax->printJavascript(); 
?>
<script>
function CheckallDelete()
{
			count = document.getElementsByName('delchk[]').length;
			
			for(counter=0;counter<count;counter++)
			{
					document.getElementById('delchk'+counter).checked = true;
			}
		
}
function UnCheckallDelete()
{
			count = document.getElementsByName('delchk[]').length;
			
			for(counter=0;counter<count;counter++)
			{
					document.getElementById('delchk'+counter).checked = false;
			}
		
}
function ifdeleteVideo(val)
{
	 if(confirm("Do you really want to delete Role(s)?"))
	{
		document.getElementById('id').value=val;
		document.forms[0].submit();
		return true;
	}
	else
	{
		return false;
	}
}

function Check(RecordCount,Field,Type)
	{
	 
	var Flag = 0; //keeps track that atleast one record is selected to change the status
	 var Counter = 0; 
	 var Field = document.getElementsByName('delchk[]');
	 if(RecordCount == 1)
	 {
	     if(Field[Counter].checked == true)
		 {
		    Flag = 1;
		  }
		 else
		 {

		    Flag = 0;
		  } 
	 }	  	
	 else
	 {
	  	for (Counter=0; Counter < RecordCount; Counter++) 
		{
		  if(Field[Counter].checked == true)
			{
			   Flag = 1;
			   break;
			 }    
		   else
		   {
			 Flag = 0;
			}
		}  	
	}
		
     if(Flag == 0)
	 {

		if(Type == "deletetrade")
		{
			alert("Please select atleast one Role to delete.");
		}
		return false;
	 }
	 else
	 	return true;	
}

</script>
<body >

<div id="outerwrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="980" id="tbl_outer">
	  <tr>
		<td>
			<?php 
			include_once(CURRENTTEMP."/"."header.php"); ?>
		</td>
	  </tr>
	  <tr>
		<td>
			<div id="content">
			
					
	<div id="main">
		<div id="contents">
			<form action="" method="post" name="frmlist" id="frmlist">
			<?php
			if ($_SESSION['admin_user_name'] == $admin_user->user_name ||  $auth->isModuleAccessible('role',$user_id)):			
				if ($mod == 'role' || $mod == 'assignrole' || $mod == 'users')
				{
					echo '<div align="left" style="width:90%;text-align:center;"><span id="secondary"><a href="'.CreateURL('index.php','mod=role').'">Manage Role</a></span>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span><a href="index.php?mod=role&do=add">Add Role</a></span>
							&nbsp;&nbsp;&nbsp;&nbsp;<span><a href="">Assign Role</a></span>
							&nbsp;&nbsp;&nbsp;&nbsp;<span><a href="">Admin Users</a></span>';
								$admin_user = $DB->SelectRecord('admin_users');
	
									if (($_SESSION['admin_user_name'] == $admin_user->user_name)){								
									echo '&nbsp;&nbsp;&nbsp;&nbsp;<span><a href="'.CreateURL('index.php','mod=users&do=cpass&nameID='.$admin_info->id).'">Change Admin Password</a></span>';
								}	
					echo '<div>';
				}
				else
				{
					echo '<span><a href="'.CreateURL('index.php','mod=role').'">Role</a></span>';
				}
			endif;
			?>
			<?php 
			if (isset($_SESSION['ActionAccess']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['ActionAccess'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['ActionAccess']);
			}
			// Show particular Messages
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:red;">';
				echo $_SESSION['error'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="40%" style="border:2px #CCCCCC solid;margin-top:5px;"><tbody><tr><td align="center" style="padding:3px 3px 3px 3px;color:green;">';
				echo $_SESSION['success'];
				echo '</td></tr></tbody></table>';
				unset($_SESSION['success']);
			}
			
			//print_r($frmdata);
			?>	
		
		<table width="40%" border="0" cellspacing="1"  style="margin-top:12px" cellpadding="0" align="center" class="tableccss">
		<tr >
		  <th height="30" colspan="7" align="center" style="font-size:14px">Role</th>
		</tr> 
		<tr class="trwhite">			
			<td align="right" class="fontstyle">Role Name:</td>
			<td align="left"><input name="role" type="text" size="30" class="rounded" id="role" maxlength="20"   value="<?php echo $frmdata['role']; ?>" /></td>
		</tr>
	 	<tr class="trwhite">	 
	 		<td height="36" colspan="4" align="center">
				<input class="buttons rounded" type="submit" name="searchrole" id = "searchrole"  value="Search Role"  title="Click to search">
				&nbsp;&nbsp;
		        <input class="buttons rounded" type="submit" name="clearsearch" value="Clear Search"   title="Click to clear">
			</td>
		</tr>		
	  </table>
	<br>	
	  <?php 
		if($rolelist)
		{		   
		 $srNo=$frmdata['from'];
		 $count=count($rolelist);	 	   
		?>
		<div id="celebs">		  
	  <table width="70%" align="center" cellpadding="1"  cellspacing="1" border="0" bgcolor="#e1e1e1" style="margin-top:15px;">
		<tr class="trwhite"> 
			<td colspan="10">
				<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td width="454" align="left" valign="top">
						<?php	
							print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
						?>
						</td>		
						<td width="205" align="right" valign="top"><?php echo getPageRecords();?></td>
					</tr>
				</table>
			 </td>
		</tr> 
		</tr>
		<tr class="tblheading">
		  <td width="33" align="center" height="36" >S.No</td>
		  <td width="118" align="center" ><a  style="color:#AF5403; text-decoration:underline;cursor:pointer" onClick="OrderPage('roleName');">Role
			<?php if($frmdata['orderby']=='roleName') {print '<img src="'.ROOTURL.'/images/asc.gif">';} elseif($frmdata['orderby']=='roleName desc'){print '<img src="'.ROOTURL.'/images/desc.gif">';}?>
			</a>
			</td>
		 
		  <td width="100" align="center" >Action</td>
		  <td width="20%"><a href="javascript:CheckallDelete()"  style="color:#AF5403;" >Select All</a> / <a href="javascript:UnCheckallDelete()" style="color:#AF5403;" >Unselect All</a></td>
		</tr>
		
		<?php 	
			for($counter=0;$counter<$count;$counter++)
			{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
			
		?>
		<tr class='<?= $trClass; ?>'>
		   <td align="center" ><?php  print $srNo ?></td>		   
		   <td align="center" class="fontstyle" <?php echo $style ?>>&nbsp;<?php echo highlightSubString($frmdata['role'],$rolelist[$counter]->roleName); ?></td>		 
		   <td align="center" <?php echo $style ?>>
		  		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=role&do=edit&nameID='.$rolelist[$counter]->roleID);?>' title="Edit Role" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
				<a title="Delete"  href='<?php print CreateURL('index.php','mod=role&do=del&nameID='.$rolelist[$counter]->roleID);?>' onclick="return confirm('Do you really want to delete this role?')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		   </td>
		   <td>
		  		
								 <input name="delchk[]" type="checkbox" id="delchk<?php echo $counter;?>" value="<?php print $rolelist[$counter]->roleID;?>" />
				
		   </td>
		</tr>		
		<?php
			} 
		?>
	</table>
	<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">		
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	 <table width="100%">
				<tr>
					<td align="center" >
			               <input class="input" type="submit" value="Delete Selected" name="deletetrade" onClick="if(Check('<?php print $count;?>',document.frmlist.delchk,'deletetrade')){ return ifdeleteVideo('<?php echo $count;?>') }else {return false;} "></td>
				</tr>
				</table>
	</div>
	  <?php
	  }
	  else
	  { 
		$frmdata['message']['text']="Sorry ! Role not found for the selected criteria";
		ShowMessage();
	   }
	  
	  ?>
	  <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php print $frmdata['orderby']?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" />
	</form>	
		
		</div><!--Div Contents closed-->
				</div><!--Div main closed-->

			</div><!--Content div closed-->
		</td>
	  </tr>
	</table>	
</div><!--Outer wrapper closed--> 
</body>
</html>
