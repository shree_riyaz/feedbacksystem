<?php
    /*======================================
    Developer    -    Neha Pareek
    Module      -  Facility & Services
    SunArc Tech. Pvt. Ltd.
    ======================================        
    ******************************************************/
    

?>

<script type="text/javascript" src="<?php echo ROOTADMINURL?>js/jquery.min.js"></script>
   

<?php 
$lang = $language->english($lang);
?>
<form method="post" name="frmlist" id="add_faults" enctype="multipart/form-data" onsubmit="return validation()" >
<center>
    <?php 
          
        if(isset($_SESSION['error']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                <div class="alert alert-danger alert-dismissable">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['error'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['error']);
            }
            if(isset($_SESSION['success']))
            {
                echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
                <div class="alert alert-success alert-dismissable">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo $_SESSION['success'];
                echo '</div></td></tr></tbody></table><br>';
                unset($_SESSION['success']);
            }
    ?>

	<table class="table table-striped" cellpadding="0" style="width:80% !important; margin-bottom: 10px; ">
    
		<tr valign="middle" align="center"> 
		  <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['View Faults']?></font>
		  </th>
		</tr>
		<tr height="36">
		<td align='center'  style="text-align : center;  ">
		<font color="black"><?php echo $lang['Service Name']?></font>
		</td>
		<td align='left'>
		<?php echo ucfirst($service->service_name); ?>
		</td>
		
		</tr>
		<tr height="36">
		<td align='center'  style="text-align : center; ">
		<font color="black"><?php echo $lang['Service Description']?></font>
		</td>
		<td align='left'>
		<?php echo ucfirst($service->service_description); ?>
		</td>
		
		</tr>
		<tr>
		<td align="center" colspan="2">
		<?php 
		$srNo=$frmdata['from'];
		$count=count($Row);
		
		if($Row)
		{
		?>
			<table class="table-striped" align="center" width="100%" id="fault_list" >
			<tbody>
			<tr>
			<td height="20" align="center" ><?php echo $lang['S.No.']?></td>
			<td  height="20" align="center">
			<a style="cursor:pointer; color: #000; " onClick="OrderPageField('fault_name');"><?php echo $lang['Fault Name']?>
			<?php if($frmdata['orderby']=='fault_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='fault_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
			</a>
			</td>
			<td align="center"  > 
			<?php echo $lang['Fault Image']?>&nbsp;&nbsp;
			</td>
			<td height="20" align="center" ><?php echo $lang['Remove / Add']?></td>
			</tr>
			
	<?php
  			
			static $n;	
			$objpassEncDec = new passEncDec;
			for($counter=0;$counter<$count;$counter++)
			{
						$srNo++;
						if(($counter%2)==0)
						{
							$trClass="tdbggrey";
						}
						else
						{
							$trClass="tdbgwhite";
						}
						
				$confirmDelete = 'Do you really want to delete this Fault ?';		
				  $obj = new passEncDec;
	?>
		<tr>
		<td align='center'><?php echo $srNo; ?></td>
		<td align="center" style="padding-right : 8px;" ><?php echo ucfirst($Row[$counter]->fault_name); ?>&nbsp; &nbsp;</td>
		<td align='center'>
		
					<?php if($Row[$counter]->fault_image)
						{  
						$pos = strrpos($Row[$counter]->image,'/');
						$img = substr($Row[$counter]->image,$pos+1);
						
									if(file_exists( "images/faults_images/".$Row[$counter]->fault_image))
									{	
									?>
										<div align="center" >
										<img src="<?php echo "images/faults_images/".$Row[$counter]->fault_image; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
										</div>
								<?php
									}
									else
									{ ?> 
										<div align="left">
										<img src="<?php echo "images/faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
										</div>
									<?php 
									
									}
						}
						else
						{  ?>
							<div align="left">
							<img src="<?php echo "images/faults_images/no_image.gif"; ?>" alt="Fault Image"  style="height :50px; width:50px;"/>
							</div>
						<?php 
								
						}
						?>
		
		
		
		</td>&nbsp;
		<td align='center'>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=faults&do=del&id='.$Row[$counter]->fault_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a></td>
		</tr>
	
	<?php
		$sno++;
		$n = $sno++;
		}
	?>	
	<?php 
		}
		else
		{

			$frmdata['message']="Sorry ! Faults not found.";
			ShowMessage();
	?>
	<style> .tblheading,.trwhite{ display:none; }

	</style>
	<?php
		}
	?>
			</tbody>
			</table>
	</td>
	</tr>
	<tr>
	<td align="center" colspan="2">
		<table class="table-striped" align="center" width="80%" id="myfields">
		<tbody>
		
			<tr class="txt">
			<td align="center" style="font-size:16px; font-weight: bold;">*</td>
			<td align="center" style="padding-left:32px;">
				<input type='text'placeholder="Enter Fault Here" name='mytextfield[]' class="form-control txt"/>
			</td>
			<td align="center" style="padding-left:38px;">
				<input type='file' name='myFilesfield[]' class="form-control " style="margin-right:20px; "/>
			</td>
			<td align="center">	</td>
			<td> </td>
			</tr>
			
			<tfoot>
					<tr>
					<td></td>
					<td></td>
					
					<td colspan="2" align="right"><a href='javascript:addFields()' style="font-size:14px;" id="addmorelink">Add More Faults</a>
					</td>
					<td> </td>
					</tr>
					
					<tr class="alt" align="center">
					<td></td>
					<td></td>
					<td  style="text-align: center;" >
					<button type="submit" class="btn btn-primary" name="add"><?php echo $lang['Add']?></button>
					<input type="button" value="<?php echo $lang['Reset']?>" onClick="this.form.reset()" class="btn btn-primary"/>
					<!--<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=facilityandServices&do=view&id=1');?>'"><?php echo $lang['Back']?></button>-->
					</td>
					</tr>
			</tfoot>
			
		</tbody>
		</table>
	</td>
	</tr>
	</table>
	
	
<style type="text/css">
.frmt
{
padding-left : 39px;
}
.frmtbtn
{	align: left;
	text-align:center;
}
.fault_rw
{

}
</style>
<script type="text/javascript">
	function addFaults()
	{
		<?php
			if(isset('fault_list'))
			{
		?>
			document.getElementById('txt').style.display="none";
			document.getElementById('alt').style.display="none";
		<?php
			}
			else
			{	
				if(isset('fault_rw'))
				{
		?>			
					document.getElementById('txt').style.display="none";
					document.getElementById('alt').style.display="block";
		<?php
				}
		
			}
		?>
	}
	function addFields()
	{
     var table = document.getElementById('myfields');
	 var lastRow = table.rows.length;
     var row = table.insertRow(lastRow-3);
	 row.className="frmtbtn fault_rw";
     var cell1 = row.insertCell();
     var cell2 = row.insertCell();
     var cell3 = row.insertCell();
     var cell4 = row.insertCell();
	
	
    cell1.innerHTML = "<span style='font-size:16px; text-align: center; font-weight:bold; padding-left:0px;'>*</span>";
	
    cell2.innerHTML = " <input type='text' placeholder='Enter Other Fault Here' name='mytextfield[]'  style='text-align: center;' class='form-control txt'/>";
	cell2.className = "frmt";
   
    cell3.innerHTML = "<input type='file' name='myFilesfield[]' style='text-align: center;' class='form-control'/> ";
	cell3.className = "frmt";
	cell4.innerHTML = "<input type='button' value='Remove' class='btn btn-primary' style='text-align: center; ' onclick='SomeDeleteRowFunction(this);'  />";
	cell4.className = "frmtbtn1";
    var newTextContent = " <input type='text' name='mytextfield[]'  class='form-control'/>";
    var newFileContent = "<input type='file' name='myFilesfield[]'  class='form-control'/>";
      $("#mytextfield").append(newTextContent); 
  
      $("#myFilesfield").append(newFileContent);   
 	}
 
	 function doValidate(fault)
	{
		var e = document.getElementsByName(fault_name);
		for(var i = 0; i < e.length; i++)
		{
			if(e[i].value == "")
			{
				alert("Please Enter Fault.");
				return;
			}
		}
		alert("all fault fields should have some value.");
	}


    function validation()
    {
           
		var b=document.getElementsByTagName('input');
		
		var count=0;
			for (i=0;i<b.length;i++)
			{
				var box=b[i];
				if(	box.value=='' && box.type=='text')
				{
					alert('Enter Any Fault in Textbox.');
					box.focus();
					return false;
				}
			}
	}
    
	function SomeDeleteRowFunction(btndel)
	{
	    if (typeof(btndel) == "object")
	    {
	        $(btndel).closest("tr").remove();
	    }
	    else
	    {
	        return false;
	    }
	}


</script>
	  <!--<input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >-->
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'fault_name';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" />
</center>
</form>