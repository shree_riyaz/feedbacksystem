<?php
 //echo "<pre>";
// print_r($_SESSION);
// print_r($Row);
 // print_r($users);
 // print_r($cleaner);exit;

?>
<script>
function deleteConfirm()
{
	if(confirm("Are you really want to delete ? "))
		return true;
	else
		return false;
}
</script>

<center>
<br />
	

<br />	
<form method="post" name="feedback_edit" id="feedback_edit" enctype="multipart/form-data">
<center>
	<?php 
			// print_r($Row); exit;
			$lang = $language->english($lang);
			if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>

 <table width="60%"  border="0" align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
    <tbody><tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Update Feedback']?></font></th>
    </tr>
	<tr>
		<td   colspan="2"  style="font-size:10px; color:red;" align="right"  class="fontstyle">*<?php echo $lang['All fields are mandatory']?></td>
	</tr>
	<?php if ($_SESSION['usertype'] == 'admin') { ?>
    <tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Related To'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		
		<select class="form-control" name="company_id" style="width:170px;">
		<option value="">Please Select</option>
		<?php 
		/*if($_SESSION['company_id'])	
		{
			$selected = 'selected';
		}*/
		
		for($i=0;$i<count($company[0]);$i++)
		{
			// if($Row[0][0]->company_id == $company[0][$i]->company_id)	
			// {
				// $selected = 'selected';
			// }
		?>
		<option value="<?php echo $company[0][$i]->company_id;?>" <?php if($Row[0][0]->company_id == $company[0][$i]->company_id) { echo "selected"; } ?>><?php echo $company[0][$i]->company_name;?>
		</option>
		<?php
		}
		?>
		</select>
		</div></td>
		</tr>
		<?php// } else {?>
		<input type="hidden" class="form-control" name="company_id" value="<?php echo $Row[0][0]->company_id;?>">
		<?php } ?>
	 <tr> 
		<td align="right" class="fontstyle" width="30%"> <label for="subscription_plan" class="control-label col-xs-10"><?php echo $lang['Feedback'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
		<input type="text" name="service_id" class="form-control" id="service_id" readonly="readonly" value="<?php echo ucfirst($services[0][0]->service_name) ?>">
		</div></td>
	</tr>
	<tr>
		<td class="fontstyle" align="right"><label for="feedback" class="control-label col-xs-10"><?php echo $lang['Fault'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
            <select class="form-control" name="fault_id" id="fault_id" style="width:170px;">
			<option value="">Please Select</option>
			<?php 
			//replace $faults to $faults_res By : Neha Pareek. Dated : 04 Nov 2015
			for($i=0;$i<count($faults_res[0]);$i++)
			{?>
				<option value="<?php echo $faults_res[0][$i]->fault_id;?>" <?php if($Row[0][0]->fault_id == $faults_res[0][$i]->fault_id) { echo "selected"; } ?>><?php echo $faults_res[0][$i]->fault_name;?>
		</option>
			<?php 
			}
			?>
			</select> </div>
		</td>
	</tr>
		<tr>
		<td class="fontstyle" align="right"><label for="location" class="control-label col-xs-10"><?php echo $lang['Device'].MANDATORYMARK?></label></td>
		<td align="left"><div class="col-xs-4">
        <select class="form-control" name="device_id" id="device_id" style="width:170px;">
			<option value="">Please Select</option>
			<?php 
			for($i=0;$i<count($location[0]);$i++)
			{ ?>
			<option value="<?php echo $location[0][$i]->device_id;?>" <?php if($Row[0][0]->device_id == $location[0][$i]->device_id) { echo "selected"; } ?>><?php echo $location[0][$i]->device_name;?>
			</option>
			<?php 
			}
			?>
		</select> </div></td>
	</tr>
	<tr>
		<td class="fontstyle" align="right"><label for="rating" class="control-label col-xs-10"><?php echo $lang['Rating'].MANDATORYMARK?> </label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="rating" id="rating" style="width:170px;">
			<option value="" <?php if ($Row[0][0]->rating == '' || $Row->rating == '0') {  echo "selected"; } ?>>Please Select</option>
		<!--	<option value="">Please Select</option>-->
			<option value="Excellent" <?php if($Row[0][0]->rating=='Excellent'){ echo "selected";}?>>Excellent</option>
			<option value="Good" <?php if($Row[0][0]->rating=='Good'){ echo "selected"; }?>>Good</option>
			<option value="Average" <?php if($Row[0][0]->rating=='Average'){ echo "selected"; }?>>Average</option>
			<option value="Poor" <?php if($Row[0][0]->rating=='Poor'){ echo "selected"; }?>>Poor</option>
			<option value="Very Poor" <?php if($Row[0][0]->rating=='Very Poor'){ echo "selected"; }?>>Very Poor</option>
		</select></div></td>
	</tr>
	 <tr>
		<td class="fontstyle" align="right"><label for="location" class="control-label col-xs-10"><?php echo $lang['Device Status'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="device_status" id="device_status" style="width:170px;">
			<option value="" <?php if ($Row[0][0]->device_status == '' || $Row->device_status == '0') {echo "selected";} ?>>Please Select</option>
			<option value="Active" <?php if($Row[0][0]->device_status=='Active'){ echo "selected"; }?>>Active</option>
			<option value="Inactive" <?php if($Row[0][0]->device_status=='Inactive'){echo "selected"; }?>>In-Active</option>
		</select></div>
		</td>
	</tr>
	<tr>
		<td class="fontstyle" align="right"><label for="rating" class="control-label col-xs-10"><?php echo $lang['Status'].MANDATORYMARK?> </label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="status" id="status" style="width:170px;">
			<option value="" <?php if ($Row[0][0]->feedback_status == '' || $Row[0][0]->feedback_status == '0') { echo "selected"; } ?>>Please Select</option>
			<option value="1" <?php if($Row[0][0]->feedback_status=='1'){ echo "selected";}?>>Completed</option>
			<option value="0" <?php if($Row[0][0]->feedback_status=='0'){ echo "selected";}?>>Pending</option>
		</select> </div></td>
	</tr>
    <?php if($_SESSION['role_id']!='3')  { ?>
	<tr>
		<td class="fontstyle" align="right"><label for="users" class="control-label col-xs-10"><?php echo $lang['Assigned To'].MANDATORYMARK?></label></td>
		<td align="left">
		<div class="col-xs-4">
        <select class="form-control" name="assigned_to" id="assigned_to" style="width:170px;">
			<option value="" >Please Select</option>
			<?php 
			for($i=0; $i<count($users[0]); $i++) //loop for supervisors
			{ 	
				$usr = $users[0][$i]->user_id; 
			?>
				<!-- create optgroup of supervisers -->
				<optgroup label="<?php echo ucfirst($users[0][$i]->first_name.' '.$users[0][$i]->last_name);?>">
			<?php 
				for($j = 0; $j<count($cleaner[0]); $j++)//loop for cleaners
				{		
					if($cleaner[0][$j]->assigned_to == $usr)
					{ //create options list of cleaners
			?>
					<option value="<?php echo $cleaner[0][$j]->user_id ;?>" <?php if($Row[0][0]->assigned_to == $cleaner[0][$j]->user_id) { echo "selected"; } ?>><?php echo ucfirst($cleaner[0][$j]->first_name.' '.$cleaner[0][$j]->last_name); ?></option>
			<?php 
			}
				}
				?>
				</optgroup>
				<?php
			}
			?>
		</select>
		</div>
		</td>
	</tr>
   
    <?php } else { ?> <input type="hidden" name="assigned_to" value="<?php echo $Row[0][0]->assigned_to;?>" /><?php } ?>
        <input type="hidden" name="user_id" value="<?php echo $Row[0][0]->user_id;?>" />
     <tr> 
		<td align="right" class="fontstyle" width="30%"><label for="active" class="control-label col-xs-10"><?php echo $lang['Active'].MANDATORYMARK?></label></td>
		
		<td align="left"><div class="col-xs-6">
			<input type="radio" name="is_active" value="Y" <?php if($Row[0][0]->is_active=='Y') {echo "checked";} ?> /><?php echo $lang['Active']?>  &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="is_active" value="N" <?php if($Row[0][0]->is_active=='N') {echo "checked";} ?>/><?php echo $lang['In-Active']?>
			</div>
		</td>
	</tr>
	<tr class="alt">
	 <td colspan=2 style="text-align: center;" align="left">
		<div class="col-xs-offset-2 col-xs-10" style="width:50% !important; margin-left: 24.6667%;">
		
		<button type="submit" class="btn btn-primary" name="update" id="update"><?php echo $lang['Update']?></button>
	<!--	<button type="reset" class="btn btn-primary" name="Reset" name="clear" id="clear"><?php echo $lang['Reset']?></button>-->
		<button type="reset" class="btn btn-primary" name="Back" onClick="window.location.href='<?php print CreateURL('index.php','mod=feedback');?>'"><?php echo $lang['Back']?></button>
     </div>
		</td>
		
	</tr>
	
</table>
<b class="xbottom"><b class="xb4"></b><b class="xb3"></b><b class="xb2"></b><b
	class="xb1"></b></b></div>
</form>
</center>