<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Anonymous Feedback</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Anonymous Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                    <span style="font-weight: bold; color: darkolivegreen">
                                        Count - <?php echo 'Genuine('.$count_data[1]->total_genuine.') / Anonymous('.$count_data[0]->total_anonymous.')'; ?>
                                    </span>
                                <br>

                                <!--<span style="font-weight: bold; color: darkolivegreen">
                                Ratio  -  --><?php //echo getRatio($count_data[1]->total_genuine,$count_data[0]->total_anonymous); ?>
                                <!--</span>-->
                                <br>
                                <br>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Number of feedback</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($Row[0] as $row_list) { ?>

                                            <tr>
                                                <td><?php echo date('d-m-Y',strtotime($row_list->created_at)); ?></td>
                                                <td><?php echo $row_list->anonymous_total_feedback; ?></td>

                                            </tr>
                                        <?php } //} ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</section>




<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

