<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Location Wise Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> Location Based Feedback
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Location Name</th>
                                            <th>Number of feedbacks</th>
                                            <th>Average</th>
                                            <th>Excellent</th>
                                            <th>Good</th>
                                            <th>Poor</th>
                                            <th>Very Poor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        foreach ($Row as $row_list) {

//                                                echo "<pre>"; print_r($row_list );
//                                                echo "<pre>"; print_r($row_list['location_name']);
                                            ?>


                                            <tr>
                                            <td><?php echo $row_list['location_name']; ?></td>
                                            <!--                                                    <td class="total_feedback">--><?php //echo $row_list['total_feedback']; ?><!--</td>-->
                                            <?php
                                            foreach ($row_list['feedback_detail'] as $feedback_detail_key => $feedback_detail){

                                                if ($feedback_detail_key == $row_list['location_id']) {

                                                    $avg = isset($feedback_detail[$feedback_detail_key]['Average']) ? substr($feedback_detail[$feedback_detail_key]['Average'],8) : 0 ;
                                                    $excellent = isset($feedback_detail[$feedback_detail_key]['Excellent']) ? substr($feedback_detail[$feedback_detail_key]['Excellent'],10) : 0 ;
                                                    $good =    isset($feedback_detail[$feedback_detail_key]['Good']) ? substr($feedback_detail[$feedback_detail_key]['Good'],5) : 0 ;
                                                    $poor =    isset($feedback_detail[$feedback_detail_key]['Poor']) ? substr($feedback_detail[$feedback_detail_key]['Poor'],5) : 0 ;
                                                    $very_poor =    isset($feedback_detail[$feedback_detail_key]['Very Poor']) ? substr($feedback_detail[$feedback_detail_key]['Very Poor'],10) : 0 ;
                                                    $total = $avg+$excellent+$good+$poor+$very_poor;
                                                    ?>
                                                    <td class="total_feedback"><?php echo $total ?></td>

                                                    <td class="avg"><?php echo $avg ?></td>
                                                    <td class="excellent"><?php echo $excellent ?></td>
                                                    <td class="good"><?php echo $good ?></td>
                                                    <td class="poor"><?php echo $poor  ?></td>
                                                    <td class="very_poor"><?php echo $very_poor ?></td>
                                                    <?php //} //} ?>


                                                    </tr>
                                                <?php } } } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }   ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown by Location Wise
                        </div>
                        <!-- /.panel-heading -->
                        <?php if (count($get_data_for_pie_data_location)){ ?>

                            <div class="panel-body">
                                <div class="">

                                    <script type="text/javascript">

                                        Highcharts.setOptions({
                                            colors: ['#CD327D','#99B5FA','#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4','#FFA07A','#52FFF3','#C5D33D','#F30F2F','#F1DD41','#F0974E','#D2A88E','#F7D562','#F88DDF','#B35A8B','#A4743F','#E0E96E','#360C30','#DDFAF8','#FDED04','#F59AAB','#FE4F4F','#FAC58A','#F4D4DA']
                                        });

                                        $(function () {


                                            $('#location_based_graph_chart').highcharts({

                                                chart: {
                                                    renderTo: 'container',
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: 'Feedback graph on basis of Location Type'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.y:.f}</b>'
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        borderColor: '#000000',
                                                        innerSize: '80%',
                                                    }
                                                },

                                                series: [{
                                                    type: 'pie',
                                                    name: 'Feedback',
                                                    size: '100%',
                                                    innerSize: '60%',
                                                    data: <?php echo json_encode($get_data_for_pie_data_location);?>,
                                                    showInLegend: true,
                                                    dataLabels: {
                                                        enabled: false
                                                    }
                                                }]
                                            });

                                        });

                                    </script>

                                    <div id="location_based_graph_chart"></div>

                                </div>
                            </div>
                        <?php } else { ?>

                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show location based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>




<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var avg = $('.avg').text();
        var excellent = $('.excellent').val();
        var good = $('.good').val();
        var poor = $('.poor').val();
        var very_poor = $('.very_poor').val();

        $('.total_feedback').val(avg + excellent + good + poor + very_poor);

    } );
</script>

