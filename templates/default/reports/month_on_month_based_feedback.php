<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));

//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//echo "<pre>"; print_r($Row); exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<section>
    <div class="col-sm-9 drop-shadow nopadding">
        <div class="user-heading">
            <span>Month On Month Report</span>
            <?php
            include_once 'user_profile.php';
            ?>
        </div>
        <div class="userbg">

            <?php if(($_SESSION['usertype']) == 'company_admin' ) { ?>

                <div class="row">
                    <div class="col-sm-12 demo">
                        <?php
                        include_once(CURRENTTEMP."/"."date_picker.php");
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default report-panel">
                            <div class="panel-heading">
                                <i class="fa fa-calendar" aria-hidden="true"></i> Month on Month Feedback Report
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-bordered interval-feedback table-striped table-hover" id="example">
                                        <thead>
                                        <tr>
                                            <th>Month</th>
                                            <th>Number of Feedback</th>
                                            <th>Excellent</th>
                                            <th>Good</th>
                                            <th>Average</th>
                                            <th>Poor</th>
                                            <th>Very Poor</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $replace_with= " ' ";
                                        foreach ($Row[0] as $row_list) {
                                            ?>

                                            <tr>

                                                <td><?php echo isset($row_list->month_name) ? $row_list->month_name  : 0 ?></td>
                                                <td><?php echo isset($row_list->number_of_feedback) ? $row_list->number_of_feedback  : 0 ?></td>
                                                <td><?php echo isset($row_list->excellent) ? $row_list->excellent  : 0 ?></td>
                                                <td><?php echo isset($row_list->good) ? $row_list->good  : 0 ?></td>
                                                <td><?php echo isset($row_list->avergae) ? $row_list->avergae  : 0 ?></td>
                                                <td><?php echo isset($row_list->poor) ? $row_list->poor  : 0 ?></td>
                                                <td><?php echo isset($row_list->very_poor) ? $row_list->very_poor  : 0 ?></td>


                                            </tr>
                                        <?php  }  ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Month on Month
                        </div>
                        <!-- /.panel-heading -->
                        <?php if (count($month_on_array) ){ ?>
                            <div class="panel-body">
                                <div class="overall-feedback-div">

                                    <script type="text/javascript">

                                        $(function () {
                                            Highcharts.setOptions({
                                                colors: ['#CD327D','#99B5FA','#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4','#FFA07A','#52FFF3','#C5D33D','#F30F2F','#F1DD41','#F0974E','#D2A88E','#F7D562','#F88DDF','#B35A8B','#A4743F','#E0E96E','#360C30','#DDFAF8','#FDED04','#F59AAB','#FE4F4F','#FAC58A','#F4D4DA']
                                            });
//                                                Highcharts.chart('container', {
                                            var chart = Highcharts.chart('container_fault_month_on', {

                                                chart: {
                                                    type: 'column'
                                                },

                                                title: {
                                                    text: 'Month on Month Feedback Report'
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    type: 'category',
                                                    labels: {
                                                        rotation: -45,
                                                        style: {
                                                            fontSize: '13px',
                                                            fontFamily: 'Verdana, sans-serif'
                                                        }
                                                    }
                                                },
                                                yAxis: {
                                                    min: 0,
                                                    title: {
                                                        text: 'Number of Feedback '
                                                    }
                                                },
                                                legend: {
                                                    enabled: false
                                                },
                                                tooltip: {
                                                    pointFormat: 'Total Feedback : <b>{point.y:.f}</b>'
                                                },
                                                series: [{
                                                    name: 'Month on Month Feedback',

                                                    data: [
                                                        <?php
                                                        foreach ($month_on_array as $month_on_array_list) {
                                                            echo $month_on_array_list.',';
                                                        }
                                                        ?>
                                                    ],
                                                    dataLabels: {
                                                        enabled: true,
                                                        rotation: -90,
                                                        color: '#FFFFFF',
                                                        align: 'right',
                                                        format: '{point.y:.f}', // one decimal
                                                        y: 10, // 10 pixels down from the top
                                                        style: {
                                                            fontSize: '12px',
                                                            fontFamily: 'Verdana, sans-serif'
                                                        }
                                                    }
                                                }]
                                            });
                                        });

                                    </script>

                                    <div id="container_fault_month_on"></div>

                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="panel-body">
                                <br>
                                <div class="text-center">
                                    <span  style="font-size: 15px;">Oops! No data available to show month on month based feedback graph chart.</span>
                                </div>
                                <br>
                            </div>

                        <?php } ?>
                        <!-- /.panel-body -->
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>




<script type="text/javascript">

    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>

