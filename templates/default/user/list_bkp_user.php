<?php 
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   USer
	SunArc Tech. Pvt. Ltd.
	======================================		
	******************************************************/
?>

<script>

function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
	 
	if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i);
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

function showpassdiv(id){
$("#password_"+id).css("display", "block");
}
function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=user&do=list";
	return false;
}
</script>
		
<br />
<?php 
$lang = $language->english($lang);
?>
<form method="post" name="frmlist" id="frmlist" >
<?php
//echo count($Row);
	if (isset($_SESSION['ActionAccess']))
	{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="90%" ><tbody><tr><td colspan="6"  align="center"><div class="errormsg">';
				echo $_SESSION['ActionAccess'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['ActionAccess']);
			
	}
	if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
    if(isset($_SESSION['success']))
    {
        echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
        <div class="alert alert-success alert-dismissable">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        echo $_SESSION['success'];
        echo '</div></td></tr></tbody></table><br>';
        unset($_SESSION['success']);
    }
?>
	
	
<div style=" border: 1px #ddd solid; width:50% !important; display: table; text-align : center;">		
	<table class="table" style="width:100% !important;">
    <tbody>
	<tr valign="middle" align="center"> 
      <th height="30" class="thColor" colspan="2" style="padding-left: 5px;"><font color="#FFFFFF"><?php echo $lang['Search User']?></font></th>
    </tr>
	</table>
	 <div class="form-group" style="width: 100% !important;">
            <label for="username" class="control-label col-xs-4"><?php echo $lang['Search User']?></label>
		<!--<div class="col-xs-7">-->
		   <input type="text" class="form-control" id="keyword" name="keyword" value="<?php echo (isset($frmdata['keyword'])?$frmdata['keyword']:'');?>" onbuler="return  KeywordSearch()" >
		   <span style="font-size:11px">
		   <?php if(($_SESSION['usertype'])=='super_admin')
		   { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Name ,Role,Phone No,Email Id,Company Name <?php }
		   else
		   { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Name , Role, Phone No, Email Id, Assigned To<?php } ?>
		   </span>
        <!-- </div>-->
     </div>
	 <!--<br/><br/><br/>-->
	 <div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left: 0px; padding-bottom:10px;">
		
		<button type="submit" class="btn btn-primary" name="search"  onclick="return Check()"><?php echo $lang['Search']?></button>
		<button type="reset" class="btn btn-primary" name="clearsearch" id="clear" onclick="return Clear()"><?php echo $lang['Reset']?></button>
     </div>
</div>
	<br/>
	<br/>

	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
  	
		<tr>
  		<td align="right" height='12'>&nbsp;
  		<?php
		if(($_SESSION['usertype'])=='company_admin' )
		{
		?>
		<a title="Add New User" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=user&do=add "><?php echo $lang['Add New User']?>
        </a>
        <?php
		}
		?>
		</td>
  		</tr>
</table>

<?php
   // echo '<pre>';print_r($Row);
		$srNo=$frmdata['from'];
		$count=count($Row);
		?>
		<table width="90%" align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped" >
		<tr class="trwhite"> 
				
                <td align="left" style="padding-left:3px;"><?php
				if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
				{
				?>
                <select name="actions" onchange="this.form.submit()" class="form-control">
                 <option value="">Select Action</option>
                <option value="Delete">Delete</option>
                <option value="Active">Active</option>
                <option value="Inactive">In-Active</option>
                </select> <?php
           		}
           		?>
               </td>
              		<td colspan="6" align='right' style="padding-right:12px;">
					<?php	
						if($totalCount > 0)
						   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount; 
						else
                           print "Showing Results:0-0 of 0";
						?>
					</td>		
					
					<td align="right" width="125" >
					<span style="float: left; margin-top: 9px;">Show :</span><?php echo getPageRecords();?></td>
				
		<!--</tr>-->

		</tr>
	<tr class="tblheading">
		<th class="anth"height="36" >
		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
		{
		?>
		<input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)"/>&nbsp;
		<?php
		}
		echo $lang['S.No.'];
		?> 
		</th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('first_name');"><?php echo $lang['Name']?>
		<?php if($frmdata['orderby']=='first_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='first_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<?php  if($_SESSION['usertype']=='super_admin') { ?><th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('company_name');"><?php echo $lang['Company Name']?>
		<?php if($frmdata['orderby']=='company_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='company_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th><?php } ?>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('role_name');"><?php echo $lang['Role']?>
		<?php if($frmdata['orderby']=='role_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='role_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
        <th  align="center" class="anth">
            <a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('o.company_email');"><?php echo $lang['Email Id']?>
                <?php if($frmdata['orderby']=='o.company_email') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.company_email desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
            </a>
        </th>

		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('is_active');"><?php echo $lang['Status']?>
		<?php if($frmdata['orderby']=='is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth"> 
		<a  style="cursor:pointer;color:#FFF;" onClick="OrderPage('user_phone');"><?php echo $lang['Phone Number']?>
		<?php if($frmdata['orderby']=='user_phone') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='user_phone desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>

		<th class="anth"><?php if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin') echo $lang['Edit / Delete'] ; else echo $lang['Edit'];?></th>
	</tr>	
<?php
if($Row)
{			
    $objpassEncDec = new passEncDec;
	for($counter=0;$counter<$count;$counter++)
	{
				$srNo++;
				if(($counter%2)==0)
				{
					$trClass="tdbggrey";
				}
				else
				{
					$trClass="tdbgwhite";
				}
				
		
		  		
          $confirmDelete = 'All the related data of this user will be deleted .Do you really want to delete this User ?';		
		  $obj = new passEncDec;
		  ?>
		
	<tr>
		<td align="center">
		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin' )
		{
		?>
		<input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->user_id;?>"/>&nbsp;&nbsp;
		<?php
		}
		echo $srNo;
		?>
		</td>
		<td align='center'><?php echo ucwords($Row[$counter]->first_name.' '.$Row[$counter]->last_name); ?>&nbsp;</td>
		<?php if($_SESSION['usertype']=='super_admin') {?><td align='center'><?php echo ucwords($Row[$counter]->company_name); ?>&nbsp;</td><?php } ?>
		<td align='center'><?php echo $Row[$counter]->role_name; ?>&nbsp;</td>
		<td align='center'><?php if($Row[$counter]->is_active == 'N')echo "In-Active"; else echo "Active"; ?>&nbsp;</td>
		<td align='center'><?php echo $Row[$counter]->user_phone; ?>&nbsp;</td>
		<td align='center'><?php echo $Row[$counter]->user_email; ?></td>
        <?php if($_SESSION['usertype']!='super_admin') {?><td align='center'><?php echo ucfirst($Row[$counter]->aFname.' '.$Row[$counter]->aLname); ?></td><?php } ?>
		<td align='center'>
		<?php
		if(($_SESSION['usertype'])=='super_admin' ||($_SESSION['usertype'])=='company_admin')
		{
		?>
		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=user&do=edit&id='.$Row[$counter]->user_id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
		<a title="Delete"  href='<?php print CreateURL('index.php','mod=user&do=del&id='.$Row[$counter]->user_id);?>' onclick="return confirm('<?php echo $confirmDelete ?>')"><img src="<?php print IMAGEURL ?>/b_drop.png"></a>
		<?php } 
		else
		{
		?>
		<a class="fontstyle" href='<?php print CreateURL('index.php','mod=user&do=edit&id='.$Row[$counter]->user_id);?>' title="Edit" ><img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 /></a>
		<?php } ?>
		</td>
		
	</tr>
	<?php
		$sno++;
	}
?>
</table>

<table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
 </table>

<?php 
}
elseif($_SESSION['keywords'] == 'Y')
{
	$frmdata['message']="Sorry ! user not found for the selected criteria";
	unset ($_SESSION['keywords']);
	ShowMessage();
}
else
{
	$frmdata['message']="Sorry ! No users. Please add user first.";
	ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php
}
?>


      <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'u.first_name';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" />
	  </form>	
