<?php
	/*======================================
	Developer	-	Jaishree Sahal
	Module      -   Order
	SunArc Tech. Pvt. Ltd.
	======================================
	******************************************************/
$lang = $language->english('eng');
?>

<script>
 function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');

	if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

function Check()
{
	if(document.getElementById('keyword').value=='')
	{
		alert('Please enter any value for search.');
		return false;
	}
	else
	{
		return true;
	}
}
function Clear()
{
	document.getElementById('keyword').value='';
	location.href="index.php?mod=company&do=list";
	return false;
}

</script>

<br />
<?php
//print_r($_SESSION);
//echo "<pre>"; print_r($Row);
?>
    <form method="post" name="frmlist" id="frmlist" class="form-horizontal" >
<?php
//echo count($Row);
	if(isset($_SESSION['error']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-danger alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				  echo $_SESSION['error'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['error']);
			}
			if(isset($_SESSION['success']))
			{
				echo'<table cellspacing="0" cellpadding="0" border="0" align="center" width="60%" ><tbody><tr><td colspan="6"  align="center">
				<div class="alert alert-success alert-dismissable">
				   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
				echo $_SESSION['success'];
				echo '</div></td></tr></tbody></table><br>';
				unset($_SESSION['success']);
			}
			?>
<center>

<div style=" border: 1px #ddd solid; width:50% !important; display: table; text-align : center;">
	<table class="table" style="width:100% !important;">
    <tbody>
	<tr valign="middle" align="center">
      <th height="30" class="thColor" colspan="2" style="padding-left:5px;"><font color="#FFFFFF"><?php echo $lang['Search Company']?></font></th>
    </tr></table>
	 <div class="form-group" style="width:100% !important;">
            <label for="username" style="padding-right:20px;" class="control-label col-xs-4"><?php echo $lang['Search Company']?></label>
		<!--<div class="col-xs-7">-->
		   <input type="text" style="margin-left:10px;" class="form-control" title="Search by company name , subscription plan,email and user name" id="keyword" name="keyword" value="<?php echo (isset($frmdata['keyword'])?$frmdata['keyword']:'');?>" onblur="return  KeywordSearch()" >
		   <span style="font-size:11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Search By Company Name ,Plan,Contact Person,Email Id</span>
		   <!--</div>-->
     </div>
	 <div class="col-xs-offset-2 col-xs-10" style="width:100% !important; margin-left:0px; padding-bottom:10px;">

		<button type="submit" class="btn btn-primary" name="search" onclick="return Check()"><?php echo $lang['Search']?></button>
		<button type="submit" class="btn btn-primary" name="clearsearch" onclick="return Clear()"><?php echo $lang['Reset']?></button>
     </div>
</div>
	<br/>
	<br/>
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center"  >
		<tr>
  		<td align="right" height='12'>&nbsp;
		<a title="Add New Company" style=" font-family:Arial, Helvetica, sans-serif;cursor:hand;font-size:14px" href="index.php?mod=company&do=add "><?php echo $lang['Add New Company']?>
        </a>
		</td>
  		</tr>
</table>
<?php
	$srNo=$frmdata['from'];
	$count=count($Row);
?>
		<table align="center" cellpadding="0"  cellspacing="0" border="0" class="table table-striped" style="font-size:14px;width:90%">
		</tr>
			<tr class="trwhite">
			<td align="left" style="padding-left : 10px;">
                <select name="actions" onchange="this.form.submit()" class="form-control">
                 <option value="">Select Action</option>
                <option value="Delete">Delete</option>
                <option value="Active">Active</option>
                <option value="Inactive">In-Active</option>
                </select>
               </td>
			<td colspan="5" align='right' style="padding-right : 3px;">
			<?php
				if($totalCount > 0)
				   print "Showing Results:".($frmdata['from']+1).'-'.($frmdata['from']+$count)." of ".$totalCount;
				else
				   print "Showing Results:0-0 of 0";
            ?>
			</td>

              <td colspan="2" align="right" width="125"><span style="float: left; margin-top: 9px; ">&nbsp;&nbsp;&nbsp;Show :</span><?php echo getPageRecords();?></td>

			</tr>
		</tr>
	<tr class="tblheading">
    <th class="anth"height="36" ><input type="checkbox" name="chkAll[]" id="chkAll"  onchange="checkAll(this)"/>&nbsp;<?php echo $lang['S.No.']?></th>
    <th  align="center" class="anth">
		<a  style="color:#ffffff; cursor:pointer" onClick="OrderPage('o.company_name');"><?php echo $lang['Company Name']?>
		<?php if($frmdata['orderby']=='o.company_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.company_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a   style="color:#ffffff;  cursor:pointer" onClick=
		"OrderPage('o.is_active');"><?php echo $lang['Company Status']?>
		<?php if($frmdata['orderby']=='o.is_active') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.is_active desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a   style="color:#ffffff;  cursor:pointer" onClick=
		"OrderPage('p.plan_name');"><?php echo $lang['Subscription Plan']?>
		<?php if($frmdata['orderby']=='p.plan_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='p.plan_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a   style="color:#ffffff;  cursor:pointer" onClick=
		"OrderPage('o.company_email');"><?php echo $lang['Email Id']?>
		<?php if($frmdata['orderby']=='o.company_email') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.company_email desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th>
		<th  align="center" class="anth">
		<a   style="color:#ffffff;  cursor:pointer" onClick=
		"OrderPage('o.user_name');"><?php echo $lang['Contact Person']?>
		<?php if($frmdata['orderby']=='o.user_name') {print '<img src="'.IMAGEURL.'/asc.gif">';} elseif($frmdata['orderby']=='o.user_name desc'){print '<img src="'.IMAGEURL.'/desc.gif">';}?>
		</a>
		</th >
		<th align="center" class="anth">Members</th>
		<!--<th class="anth">View</th>-->
		<th class="anth"><?php echo $lang['Edit / Delete']?></th>
	</tr>
<?php
if($Row)
{
	for($counter=0;$counter<$count;$counter++)
	{
		$srNo++;
		if(($counter%2)==0)
		{
			$trClass="tdbggrey";
		}
		else
		{
			$trClass="tdbgwhite";
		}
		$confirmDelete = 'All the related data of this company will be deleted. Do you really want to delete this Company ?';
		//added By : Neha Pareek. Dated : 10-11-2015, for getting all users of particular company.
		$users = $DBFilter->SelectRecords('users','is_deleted = "N" and company_id='.$Row[$counter]->company_id);
		$cleaners = $DBFilter->SelectRecords('users','is_deleted = "N" and role_id ="3" and company_id='.$Row[$counter]->company_id);
		$company_admin = $DBFilter->SelectRecords('users','is_deleted = "N" and role_id ="2" and company_id='.$Row[$counter]->company_id);
		$supervisor = $DBFilter->SelectRecords('users','is_deleted = "N" and role_id ="4" and company_id='.$Row[$counter]->company_id);
		//echo count($users[0]);
		//echo $Row[$counter]->company_id;
?>

	<tr>
    <td align="center"><input type="checkbox" name="chkbox[]" id="chkbox" value="<?php echo $Row[$counter]->company_id;?>"/>&nbsp;&nbsp;<?php echo $srNo; ?></td>
	<td align='center'><?php echo ucwords($Row[$counter]->company_name); ?>&nbsp;</td>
	<td align='center'><?php if($Row[$counter]->is_active != 'Y'){echo ucfirst('in-Active');} else {echo ucfirst('active');}?>&nbsp;</td>
	<td align='center'><?php echo ucfirst($Row[$counter]->plan_name); ?></td>
	<td align='center'><?php echo $Row[$counter]->company_email; ?></td>
	<td align='center'><?php echo $Row[$counter]->user_name; ?></td>
	<td align='center' title="CompanyAdmin : <?php echo count($company_admin[0]);?>&nbsp;&nbsp; Supervisor : <?php echo count($supervisor[0]);?> &nbsp;&nbsp;Cleaners: <?php echo count($cleaners[0]);?>">
	<!--<a style="color : black; cursor:pointer" href="#" title="CompanyAdmin : <?php echo count($company_admin[0]);?>&nbsp;&nbsp; Supervisor : <?php echo count($supervisor[0]);?> &nbsp;&nbsp;Cleaners: <?php echo count($cleaners[0]);?>"><?php echo count($users[0]); ?></a>-->
	<?php echo count($users[0]); ?>
	</td>
	<td align='center'>
	 <a class="fontstyle"  href='<?php print CreateURL('index.php','mod=company&do=edit&id='.$Row[$counter]->company_id);?>' title="Edit" >
	 <img src="<?php echo IMAGEURL."/b_edit.png" ?>" border=0 />
	 </a>
	 <a title="Delete"
		href='<?php print
		CreateURL('index.php','mod=company&do=del&id='.$Row[$counter]->company_id);?>'
		onclick="return confirm('<?php echo $confirmDelete ?>')">
		<img src="<?php print IMAGEURL ?>/b_drop.png">
	 </a>
	</td>
</tr>
<?php
	$sno++;
	}
?>
</table>
   <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
		 <tr>
			  <td align="center" colspan="12">
				  <?php
					 PaginationDisplay($totalCount);
				   ?>
			  </td>
		</tr>
	 </table>
	<!--</div>-->
<?php
}
elseif($_SESSION['keywords'] == 'Y')
{
	$frmdata['message']="Sorry ! Company not found for the selected criteria";
	unset ($_SESSION['keywords']);
	ShowMessage();
}
else
{

	$frmdata['message']="Sorry ! No company found. Please add company first.";
	ShowMessage();
?>
<style> .tblheading,.trwhite{ display:none; }

</style>
<?php

}?>


      <input name="pageNumber" type="hidden" value="<?php print $frmdata['pageNumber']?>" >
	  <input name="orderby" type="hidden" value="<?php if($frmdata['orderby']!='')  echo $frmdata['orderby']; else echo 'company_name';?>" >
	  <input name="order" type="hidden" value="<?php print $frmdata['order']?>" >
	  <input name="actUserID" type="hidden" value="" /></form>

</center>