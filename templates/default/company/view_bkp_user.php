<?php

$lang = $language->english($lang);
$DBFilter = New $DBFilter();
//print_r($_SESSION);
//echo "Hello";
//print_r($Row);
$id = $_SESSION['user_id'];
$date= date("d-M-Y");
$expiary_date = date("d-M-Y", strtotime($Row->expiary_date));
//echo "<pre>";
// print_r($Row);
//print_r($feedbk);
//print_r( $DBFilter->SelectRecord('faults'));
//echo count($feedbk[0]);
//exit;
?>
<style>text.highcharts-credits {
        display: none;
    }</style>

<?php

if ($_SESSION['usertype'] != 'company_admin') {
    ?>
    <center>
        <br/>
        <table align='center' class="table table-striped" width="90%">
            <tr>
                <td class="thColor" style="text-align:center;font-size:14px;color:#FFF;font-weight:bold;">
                    <?php echo '' . ucfirst($Row->company_name); ?>
                </td>
            </tr>

            <tr>
                <td align="right"><?php echo $date; ?></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
                           class="table table-striped">

                        <tr valign="top">
                            <td height="36" width="20%" align="center"><?php echo $lang['Company Name'] ?></td>
                            <td width="30%" colspan="2"><?php echo ucfirst($Row->company_name); ?></td>
                            <td></td>

                            <td rowspan="5" colspan="3" width="90%" align="center">
                                <?php if ($feedbk) { ?>
                                    <span style="color: #000;"> Recent Faults</span>
                                    <table class="table table-striped" width="100%" align="center">
                                    <tr>
                                        <th height="25"
                                            style="background-color:inherit;"><?php echo $lang['S.No.'] ?></th>
                                        <th height="25"
                                            style="background-color:inherit;"><?php echo $lang['Feedback'] ?></th>
                                        <th height="25"
                                            style="background-color:inherit;"><?php echo $lang['Device'] ?></th>
                                    </tr>
                                    <?php
                                    $count = 1;
                                    $rows = count($feedbk[0]);
                                    for ($i = 0; $i < $rows; $i++) {
                                        $fault = $DBFilter->SelectRecord('faults', "fault_id='" . $feedbk[0][$i]->fault_id . "'");
                                        $device = $DBFilter->SelectRecord('device', "device_id='" . $feedbk[0][$i]->device_id . "'");
                                        //print_r($f->fault_name);
                                        //exit;

                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo ucwords($fault->fault_name); ?></td>
                                            <td><?php echo $device->device_name; ?></td>
                                        </tr>

                                        <?php
                                        $count++;
                                    }
                                    ?>

                                    </table><?php } else { ?>
                                    <span style="color: #000;"> No Faults Found</span>

                                <?php } ?>

                            </td>

                        </tr>
                        <tr>
                            <td height="36" width="20%" align="center"><?php echo $lang['Subscription Plan'] ?></td>
                            <td colspan="2"><?php echo ucfirst($plan->plan_name); ?></td>
                            <td></td>
                        </tr>
                        <!--	<tr>
			<td height="36"  align="center"><?php echo $lang['Administrator'] ?></td>
			<td><?php echo $Row->user_name; ?></td>
		</tr>-->
                        <tr>
                            <td height="36" width="20%" align="center"><?php echo $lang['Email Id'] ?></td>
                            <td colspan="2"><?php echo $Row->company_email; ?></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td height="36" width="20%" align="center"><?php echo $lang['Creation Date'] ?></td>
                            <td colspan="2"><?php echo ucfirst($Row->creation_date); ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td height="36" width="20%" align="center"><?php echo $lang['Expiry Date'] ?></td>
                            <td colspan="2"><?php echo ucfirst($Row->expiary_date); ?></td>
                            <td></td>
                        </tr>
                        <!--Added By Shivender-->
                        <tr>
                            <td height="36" width="20%" align="center"><?php echo 'Left Days'; ?></td>
                            <td colspan="2">
                                <?php
                                $exp_days = str_replace("-", "", $Row->expiary_date);
                                $create_date = date("Y-m-d");
                                $create_date = str_replace("-", "", $create_date);
                                $diff = $exp_days - $create_date;
                                echo $diff . ' days';
                                ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td height="36" width="20%" align="center"><?php echo $lang['Users'] ?></td>
                            <td align="center" colspan="0">
                                <input type="text" class="form-control" disabled="disabled" name="cleaner_count"
                                       readonly="readonly" style=" text-align:center; width:110px;"
                                       value="<?php echo "Cleaners : " . count($cleaner[0]); ?>"></td>
                            <td><input type="text" disabled="disabled" class="form-control " name="superviser_count"
                                       readonly="readonly" style=" text-align:center; width:150px;"
                                       value="<?php echo "Supervisor : " . count($superviser[0]); ?>"></td>
                            <td><input type="text" disabled="disabled" class="form-control" name="comp_admin_count"
                                       readonly="readonly" style=" text-align:center; width:150px;"
                                       value="<?php echo "Company Admin : " . count($comp_admin[0]); ?>"></td>
                            <td><input type="text" disabled="disabled" name="device_count" class="form-control"
                                       readonly="readonly" style=" text-align:center; width:125px;"
                                       value="<?php echo "Devices : " . count($devices[0]); ?>">
                            </td>
                            <!--
			<td height="36" width="20%" align="center"><?php echo $lang['Users'] ?></td>
			<td  align="left" width="12%" colspan="2"><?php echo $lang['Cleaner'] ?>&nbsp;&nbsp;<input type="text" class="form-control" disabled="disabled" name="cleaner_count"  readonly="readonly" style=" text-align:center; width:110px;" value="<?php echo "Cleaners : " . count($cleaner[0]); ?>">
			&nbsp;&nbsp;<?php echo $lang['Superviser'] ?>&nbsp;&nbsp;<input type="text" disabled="disabled" class="form-control" name="superviser_count"  readonly="readonly" style=" text-align:center; width:35px;" value="<?php echo count($superviser[0]); ?>">
			&nbsp;&nbsp;<?php echo $lang['Company Admin'] ?>&nbsp;&nbsp;<input type="text" disabled="disabled" class = "form-control" name="comp_admin_count"  readonly="readonly" style=" text-align:center; width:35px;" value="<?php echo count($comp_admin[0]); ?>">
			&nbsp;&nbsp;<?php echo $lang['Devices'] ?>&nbsp;&nbsp;<input type="text" disabled="disabled" name="device_count" class="form-control" readonly="readonly" style=" text-align:center; width:35px;" value="<?php echo count($devices[0]); ?>"></td>
			-->
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br/>

    </center>
    <?php
}
?>
<!--Start dashboard-->
<?php
if ($_SESSION['usertype'] == 'company_admin') {
    ?>
    <div id="page-wrappe" style="min-height: 347px;">
        <!--  <div class="row">
              <div class="col-lg-4 pull-left">
                  <form method="post">
                      <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 1px; border: 1px solid #ccc; width: 100%">
                          <i class="fa fa-calendar"></i>&nbsp;
                          <span></span> <i class="fa fa-caret-down"></i>
                          <input type="hidden" id="selected_date_range" name="selected_date_range" value="">
                      </div>
                      <input type="submit" name="show_chart_submit" value="Show">
                  </form>
              </div>
          </div>
          <br>-->
        <!-- /.row -->

        <div class="row">
            <ul class="nav navbar-top-links navbar-right" style="position: relative; right: 2%;">
                <form method="post">
                    <li class="dropdown">
                        <div id="reportrange" class="range_part" style="margin-top: 10px; box-shadow: 1px 1px 3px 0px darkgray;background: #fff; cursor: pointer; padding: 5px 15px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span class="span_range"></span>

                            <i class="fa fa-caret-down"></i>
                            <input type="hidden" id="selected_date_range_hidden" name="selected_date_range" value="">
                            <input type="hidden" id="selected_date_range_type_hidden" name="selected_date_range_type_hidden" value="">
                        </div>
                    </li>
                    <li>
                        <input type="submit"  class="btn btn-default btn-sm" name="show_chart_submit" value="Show" style="
    padding-top: 3px;
    padding-bottom: 2px;    height: 30px;
    font-weight: bold;
">

                    </li>
                </form>

            </ul><br/>
        </div>

        <hr>

        <div class="row">

            <div class="col-lg-4 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo count($cleaner[0]); ?></div>
                                <div>Cleaners</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-user fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo count($superviser[0]); ?></div>
                                <div>Supervisors</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-desktop fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge"><?php echo count($devices[0]); ?></div>
                                <div>Devices</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Daily Feedback
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="overall-feedback-div">

                            <div id="container_overall"></div>


                            <script type="text/javascript">


                                // create the chart
                                var chart = Highcharts.chart('container_overall', {
                                    chart: {
                                        events: {
                                            addSeries: function () {
                                                var label = this.renderer.label('A series was added, about to redraw chart', 100, 120)
                                                    .attr({
                                                        fill: Highcharts.getOptions().colors[0],
                                                        padding: 10,
                                                        r: 5,
                                                        zIndex: 8
                                                    })
                                                    .css({
                                                        color: '#FFFFFF'
                                                    })
                                                    .add();

                                                setTimeout(function () {
                                                    label.fadeOut();
                                                }, 1000);
                                            }
                                        }
                                    },
                                    title: {
                                        text: ''
                                    },
                                    xAxis: {
                                        categories: <?php echo $overall_feedback_by_date; ?>
                                    },

                                    series: [{
                                        data: <?php echo $overall_feedback_count; ?>,
                                        name: "Daily Feedback Record"

                                    }]
                                });

                            </script>


                        </div>
                    </div>
                </div>





                <!--<div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Chart for Fault Feedback
                </div>
                <div class="panel-body">
                    <div class="monthly-feedback-div" style="border:  1px solid black;">
                        <h2 style="text-align: left;">Monthly Feedback -</h2>

                        <script type="text/javascript">

                            $(function () {

                                var monthly_data_click = <?php /*echo $monthly_feedback_count; */ ?>;
                                var monthly_data_creation_date = <?php /*echo $monthly_feedback_by_date; */ ?>;

                                $('#containers_monthly').highcharts({
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: 'Monthly feedback Ratio'
                                    },
                                    xAxis: {
                                        categories: monthly_data_creation_date
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Number of Monthly feedback'
                                        }
                                    },
                                    series: [{
                                        name: 'Monthly Feedback',
                                        data: monthly_data_click
                                    }]
                                });
                            });

                        </script>

                        <div class="row">
                            <div class="col-md-10">
                                <div id="containers_monthly"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>-->

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Feedback Type
                    </div>
                    <div class="panel-body">
                        <div class="poor-feedback-div">
                            <!--                        https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/line-basic/-->
                            <script type="text/javascript">
                                $(function () {
                                    $('#containers_poor').highcharts({

                                        chart: {
                                            plotBackgroundColor: null,
                                            plotBorderWidth: 0,//null,
                                            plotShadow: false
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                allowPointSelect: true,
                                                cursor: 'pointer',

                                                dataLabels: {
                                                    enabled: true,
                                                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                                    style: {
                                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                    }
                                                },
                                                showInLegend: true
                                            }
                                        },
                                        series: [{
                                            type: 'pie',
                                            name: 'Feedback',
                                            size: '90%',
                                            data: <?php echo json_encode($get_data_for_pie_feedback_type);?>
                                        }]
                                    });
                                });

                            </script>

                            <div id="containers_poor"></div>
                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Breakdown By Fault Type
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="overall-feedback-div">

                            <script type="text/javascript">

                                $(function () {


                                    $('#container_fault').highcharts({

                                        chart: {
                                            renderTo: 'container',
                                            type: 'pie'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        tooltip: {
                                            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                        },
                                        plotOptions: {
                                            pie: {
                                                borderColor: '#000000',
                                                innerSize: '80%',
                                            }
                                        },

                                        series: [{
                                            type: 'pie',
                                            name: 'Feedback',
                                            size: '100%',
                                            innerSize: '55%',
                                            data: <?php echo json_encode($get_data_for_pie_data_list_fault);?>,
                                            showInLegend: true,
                                            dataLabels: {
                                                enabled: false
                                            }
                                        }]
                                    });

                                });

                            </script>

                            <div id="container_fault"></div>

                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
    </div>

    <!--End dashboard-->
    <?php
}
?>


<script type="text/javascript">

    $(document).on('click','div.ranges>ul>li',function () {

        var ar = <?php echo json_encode($_SESSION['all_time_list']) ;?>;

        var selected_li = $(this).text();
//            console.log(selected_li);
//        console.log(ar);
//        alert(selected_li);

        $.each(ar, function( index, value ){
            if(index == selected_li.trim()){

                var str_array = value.split(';');
//                console.log('hai');
//                console.log(index);
//                console.log(value);
//                console.log(str_array);



//                cb(str_array[0],str_array[1]);
            }
        });

    });



    $(function() {



//        var start = moment().subtract(30, 'days');
//        var end = moment();
        console.log(<?php echo $_SESSION['start_date_by_interval'] ; ?>)
        console.log(<?php echo $_SESSION['end_date_by_interval'] ; ?>)

        var start = <?php echo isset($_SESSION['start_date_by_interval']) ? $_SESSION['start_date_by_interval'] : "moment().subtract(30, 'days')"?>;
        var end = <?php echo  isset($_SESSION['end_date_by_interval']) ? $_SESSION['end_date_by_interval'] : "moment()" ?>;

//        console.log(start);
//        console.log(end);

        function cb(start, end) {

            $('#reportrange .span_range').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#selected_date_range_hidden').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            $(document).on('click','div.ranges>ul>li',function () {
                var selected_li = $(this).text();
                $('#selected_date_range_type_hidden').val(selected_li.trim());
            });
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(30, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            opens: 'left'
        }, cb);

        cb(start, end);

    });

</script>

