<?php
/**
 * MySQL DB. All data is stored in data_pdo_mysql database
 * Create an empty MySQL database and set the dbname, username
 * and password below
 * 
 * This class will create the table with sample data
 * automatically on first `get` or `get($id)` request
 */
 
 session_start();
  include_once("../config.php");
 include 'basicfunction.php';
 include_once("dbclass.php");

class DB_PDO_MySQL
{
    private $db;

   function __construct ()
    {
		//set the db /id/pass
        try {
//update the dbname username and password to suit your server
            $this->db = new PDO(
            'mysql:host='.HOST.';dbname='.DATABASE, USER,PASSWORD);
			//'mysql:host=localhost;dbname=admin_iboks', 'admin_iboks', 'T28TV0efUn');
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$this->db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
        } catch (PDOException $e) {
            throw new RestException(501, 'MySQL: ' . $e->getMessage());
        } 
    }
	
function Response($table,$email)
{
	$data = $this->SelectRecords('users',"email='$email'","id");
	if($table === 'true') 
		return '1, success, '.$data[0][0]->id;
}

	function CheckSession()
	{
		//print_r($_SESSION['login']);
			unset($_SESSION['login']);
				session_destroy();
			//print_r($_SESSION['login']);
			
			echo "<script>alert('Successfully Loged Out');
			 location.href= 'admin_login.php';</script>	";
			exit;
	
	
	}


function InsertRecord($table,$data)
{ 
	$DB= new DBConnect();
	 if($table=='')
		{
			return false;
		}	
	else
	{  
		if($data=='')
		{
			return false;
		}
		else
		{
	
			//$conn = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbuser,$dbpass);
			//$sql = "SELECT title FROM books ORDER BY title";
			//$query = $conn->query($sql);
						
			//$this->stmt = $this->db->query($query);
		 $sql = "SHOW COLUMNS FROM ".$table;
		   	$resource = $this->db->query($sql);
			 if($resource)
			{ 
				$fields="";
				$values="";
				
			 $resource->setFetchMode(PDO::FETCH_BOTH);
			//while($result = $query->fetch()){
				//$data= $resource->fetch(PDO::FETCH_BOTH);
				while($row = $resource->fetch())
				{	 
					
					if(array_key_exists($row['Field'],$data))
					{  
						 $fields.=$row['Field'].",";
						if(is_array($data[$row['Field']]))
						{
							
						   //$data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							// ($data[$row['Field']]);
							$data[$row['Field']]=implode(",",$data[$row['Field']]);
						}	
						$values.="'".$data[$row['Field']]."',";
					}
				}
				
				if($fields)
				{	
					$fields=substr($fields,0,strlen($fields)-1);
					$values=substr($values,0,strlen($values)-1);					
				  	$query="Insert into ".$table."(".$fields.") values(".$values.")";
			   // echo $query;
                     $this->db->query($query);
				     $id= $this->db->lastInsertId();                          					   
						//echo $id;
						if($id){  
              	             return $id;
						}
						else
						return true;
					
					return false;
				}
				
			}
			
			
		}
	}
	return false;			
}

function Sresponse($request_data){
    $id = $this->InsertRecord('users',$request_data);
    return '1, Success, '.$id;
    
}


function InsertedRelatedRecord($table, $data)
{
	$DB= new DBConnect();
	if($table == '')
	{
		return false;
	}
	else
	{
		if($data == '')
		{
			return false;
		}
		else
		{
			//$resource=mysql_query("SHOW COLUMNS FROM".$table);
			$sql = "SHOW COLUMNS FROM".$table;
				$resource = $conn->query($sql);
			 if($resource)
			{
				
				
				$fields="";
				$values="";
				
				$resource->setFetchMode(PDO::FETCH_ASSOC);    
				while($row = $resource->fetch())
				{	 
					
					if(array_key_exists($row['Field'],$data))
					{  
						 $fields.=$row['Field'].",";
						
						 
						
						$values.="'".$data[$row['Field']]."',";
					}
				}
				
				if($fields)
				{	
					$fields=substr($fields,0,strlen($fields)-1);
					$values=substr($values,0,strlen($values)-1);					
				  	$query="Insert into ".PREFIX.$table."(".$fields.") values(".$values.")";
			
			
					if($DB->Select($query))
					{
						if($id=$this->db->lastInsertId())
						return $id;
						else
						return true;
					}
					return false;
				}
				
			}
			
		}
		
	}
	return false;
}
	function UpdateRecord($table,$data,$cond='')
{

	$DB= new DBConnect();
	if($table=='')
	{  
		return false;
	}	
	else
	{
		if($data=='')
		{  
			return false;
		}
		else
		{ 
			$resource= $DB->Select("SHOW COLUMNS FROM ".$table);
			
			if($resource)
			{
				$sql="";
				$resource->setFetchMode(PDO::FETCH_ASSOC);    
				
				while($row = $resource->fetch())
				{
						
					if(array_key_exists($row['Field'],$data))
					{
					     if(is_array($data[$row['Field']]))
						 {
						     $data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							 $data[$row['Field']]=implode(",",$data[$row['Field']]);
						  } 
					 $sql.=$row['Field']."='".$data[$row['Field']]."',";
					
					}
				}
				
				if($sql)
				{
					$sql=substr($sql,0,strlen($sql)-1);
										
					$query="update ".$table." set ".$sql;
					
					if($cond)
					{
						if(!is_array($cond))
						{
							$query.=" where ".$cond;
						}
						else
						{
								$count=count($cond);
								if($count)
								{
									$fields=@array_keys($cond);
									$query.=" where ";
									for($counter=0;$counter<$count;$counter++)
									{
										
										if($fields[$counter]=='or' || $fields[$counter]=='and')
										{
												 $query.=" ".$fields[$counter]." "; 
										}
										else
										{
										
												 $query.=" ".$fields[$counter]."='".$cond[$fields[$counter]]."'"; 
										}		
									}
								}
							}		
					}
	//echo  $query;
					
			//exit;
			
					if($DB->Update($query))
					{
					
						return true;
					}
					
					return false;
				}
				
			}
			
		}
	}
	return false;			
}


/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to delte reocrd
	@Description : this function is used to delte record

*/
function DeleteRecord($table,$cond='')
{
 	$DB= new DBConnect();
	if($table)
	{
		$query="delete from ".$table;
		
		if($cond)
		{
			 $query.=" where ".$cond;
		}
		//echo $query ; exit;
		if($DB->Delete($query))
		{
			$arr = array('result' => 'Record deleted Successfully', 'success' => 1);
				return $arr;
		}
	}
	return false;
}


/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	@backqury : It wil be include in end of query like order by statement
	
	@Description : this function is used to select multiple records

*/
function SelectRecords($table,$cond='',$fields='*',$backquery='')
{
	global $frmdata;
	$DB=new DBConnect();
	
	if($table)
	{
		if($fields)
		{		
				if(is_array($fields))
				{
					$fields=implode(",",$fields);
				}
				
				$query="select ".$fields." from ".$table." ";
				
				if($cond)
				{
					 $query.="where ".$cond." ";
				}
				
				if($backquery)
				{
					    $query.=$backquery;
				}
		//echo  $query;
		//exit;
			
			  	//$resource = $conn->query($sql);
				
				if($result=$DB->Select($query))
				{ 
					if($result->rowCount() > 0)
					{
						$arr=array();
				  		while($row= $result->fetchAll(PDO::FETCH_OBJ)) 
				  		{	
				         $arr[]=$row;
					    }
						return $arr;
					}		
				 	 
				}
				
		}
	}
	//$arr = array('Error Code' => 400, 'status' => 'Bad Request', 'Message' => 'Login Failed');
		//echo json_encode($arr);
	return false;
}


/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	@backqury : It wil be include in end of query like order by statement
	@totalcount : to count of records
	@Description : this function is used to select multiple records with pagination work
			
*/
function  SelectRecordsWithPagination($table,$cond='',$fields='*',$backquery='',$totalCount)
{
	global $frmdata;
	$DB=new DBConnect();
	if($table)
	{
		if($fields)
		{		
				if(is_array($fields))
				{
					$fields=implode(",",$fields);
				}
				
				$query="select ".$fields." from ".$table." ";
				
				if($cond)
				{
					 $query.="where ".$cond." ";
				}
				
				if($backquery)
				{
					    $query.=$backquery;
				}
		
		//echo $query;
 
				if($result=$DB->Select($query))
				{
					echo $totalCount= $result->rowCount();
					 //echo $query;
					if( $totalCount > 0)
					{
						
						if($frmdata['from'] && $frmdata['from']<$totalCount)
						//mysql_data_seek($result,$frmdata['from']);
						$result->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_ABS,$frmdata['from']);
						$countRows=1;
						
						$arr=array();
						$result->setFetchMode(PDO::FETCH_OBJ);     
				  		while($row=$result->fetch()) 
				  		{	
				         	
							if($countRows <= $frmdata['to'])
						 		$arr[]=$row;
							$countRows++;
							
					    }
						return $arr[0];
					}		
				 	 
				}
				
		}
	}
	return false;
}
/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	
	
	@Description : this function is used to select single record

*/
function SelectRecord($table,$cond='',$fields='*')
{
	
	global $frmdata;
	$DB=new DBConnect();
	if($table)
	{ 
		if($fields)
		{		
				if(is_array($fields))
				{
					 $fields=implode(",",$fields);
				}
				
				$query="select ".$fields." from ".$table." ";
				if($cond)
				{
					 $query.=" where ".$cond;
				}
				$result=$DB->Select($query);
				
				if($result=$DB->Select($query))
				{
					if($result->rowCount() > 0)
					{
						$result->setFetchMode(PDO::FETCH_OBJ)  ;
				  		$row= $result->fetchAll(); 
				  		
						return $row[0];
						
					}		
				 	 
				}
				
		}
	}
	return false;
}

/*
	@para
	query : query statement
	@Description : this function is used to run join or other complex quries

*/
function RunSelectQuery($query)
{
	//echo $query; exit;
	$DB=new DBConnect;
	if($result=$DB->Select($query))
				{ 
					if($result->rowCount() > 0)
					{
						$arr=array();
				  		while($row= $result->fetchAll(PDO::FETCH_OBJ)) 
				  		{	
				         $arr[]=$row;
					    }
						return $arr;
					}		
				 
				}
	return false;
}


function RunSelectQueryWithPagination($query,&$totalCount)
{
	global $frmdata;
	$DB=new DBConnect;
	
	if($result=$DB->Select($query))
	{	
			//echo $query;
	 $totalCount = $result->rowCount();
		if($totalCount > 0)
		
		{
			
				
				$res = $DB->Select($query);
				
				$res->execute();
				
				$countRows=1;
				
				$arr=array();
				
				$row=$res->fetchAll(PDO::FETCH_OBJ);
				
			 $totalCount = count($row);
				foreach($row as $key => $value){
				if($key >=$frmdata['from'])
				{
					$arry[]=$value;
					//print_r($arry);
				}
				
				}
				
			
				for($i=0; $i<count($arry) ;$i++)
				
				{
					if(!isset($frmdata['record']) || (isset($frmdata['record']) && $frmdata['record']!='All') )
					{
							if($countRows <= $frmdata['to'])
							{
									$arr[]=$arry[$i];
							} 
					}
					else
					{
							$arr[]=$arry[$i];
					}	
					$countRows++;
				
				}
			
			return $arr;
		}		
				 	 
	}
	
	return false;
}


 //=============================================================================================
    // function pagination 
    //======================================================================================       
    function PaginationWork($option = '')
    {
       
	   $DB=new DBConnect;
	    global $frmdata;
        //print_r($frmdata);
        //echo $frmdata['record'];
        $recordPerPage = $DB->RecordPerPage($frmdata['record']);
    // echo $recordPerPage;
        echo $frmdata['pageNumber'];
        //print_r($frmdata[0]->recordPerPage);
        //if page number not set or search done
        if (!isset($frmdata['pageNumber']))
        {
            $frmdata['pageNumber'] = 1;
        } //!isset($frmdata['pageNumber'])
        if ($recordPerPage != 'All')
        {
            //at first page 
            if ($frmdata['pageNumber'] == 1)
            {
                $frmdata['from'] = 0;
                $frmdata['to']   = $recordPerPage;
            } //$frmdata['pageNumber'] == 1
            //for next pages
            else
            {
                if ($frmdata['pageNumber'] <= 0)
                {
                    $frmdata['pageNumber'] = 1;
                    $frmdata['from']       = 0;
                    $frmdata['to']         = $recordPerPage;
                } //$frmdata['pageNumber'] <= 0
                else
                {
                    $frmdata['from'] = $recordPerPage * (((int) $frmdata['pageNumber']) - 1);
                    $frmdata['to']   = $recordPerPage;
                }
                
                
                //echo $frmdata['pageNumber']."<br>";
                
            }
            
        } //$recordPerPage != 'All'
        else
        {
            //$frmdata['from']=0;
            //$frmdata['to']=9999999999999999999999999999999;
        }
		//Commented by ANU R // echo $frmdata['from'];
    }
//=================================================================================================================			==========pagination dispaly======================================================================================================================================================//

	
				function PaginationDisplay($totalCount, $option = '')
    {
        //echo $totalCount;
        // echo $option='';
		$DB=new DBConnect;
        global $frmdata;
       //	print_r($frmdata);
        //	echo $totalCount;
        $recordPerPage = $DB->RecordPerPage($frmdata['record']);
        // echo $recordPerPage;
        if ($recordPerPage != 'All')
        {
            if ($totalCount > $recordPerPage)
            {
                echo '<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="fontstyle" >
				  <tr valign="bottom" > 
					<td width="70" height="5" align="left">';
                //===============================================	============================
                //LINK FOR PREVIOUS PAGE.....
                //===========================================================================
                //===========================================================================
                if ($frmdata['from'] > 0)
                {
                    echo '<a href="javascript:PrePage();" ><img border="0" src="' . IMAGEURL . '/previous.gif"></a>';
                    //Added by richa verma on 12 may 2011 to show first data
                    
                    /*echo'</td><td width="70" height="5" align="right" valign="bottom"><a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,1);" ><img border="0" src="'.IMAGEURL.'/first.gif"></a>';	*/
                    
                } //$frmdata['from'] > 0
                
                echo '</td>
							<td>&nbsp;</td>';
                
                //==============================================================================
                //INDIVIDUAL PAGE LINKS.....
                //==============================================================================
                //==============================================================================
                
                $i = 1;
                $j = $frmdata['pageNumber'];
                if ($j >= 10)
                    $i = $j - 9;
                if ($totalCount > 2 * $recordPerPage)
                {
                    //=========================================================================================================================
                    //=========================================================================================================================
                    for (; $i <= 10 + $frmdata['pageNumber'] && ($totalCount > ($i - 1) * $recordPerPage); $i++)
                    {
                        if ($i == $frmdata['pageNumber'])
                        {
                            echo '<a onmouseover="this.style.cursor=\'hand\'" >
							<td height="20" width="23" align="center" valign="middle" onmouseover="this.style.cursor=\'hand\'" style="color:#50BFC9" class="fontstyle">';
                            
                            echo ($i);
                            echo '</td> </a> <td  width="1" valign="middle" >|</td>';
                            
                        } //$i == $frmdata['pageNumber']
                        else
                        {
                            echo '<td height="10" width="23"  valign="middle" align="center"class="fontstyle">';
                            echo '<a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,' . $i . ');">';
                            
                            echo ($i);
                            
                            echo '</a></td><td   width="1" valign="middle">|</td>';
                            
                        }
                    } //; $i <= 10 + $frmdata['pageNumber'] && ($totalCount > ($i - 1) * $recordPerPage); $i++
                    //=========================================================================================================================
                    //=========================================================================================================================
                } //$totalCount > 2 * $recordPerPage
                $frmdata['pageNumber'] = $j;
                //Commented by ANU R//echo $frmdata['pageNumber'];
                echo '<td>&nbsp;</td>';
                echo '<td  width="50" height="5" align="right" valign="top">&nbsp; ';
                
                //==============================================================================
                //NEXT PAGE LINK.....
                //==============================================================================
                //==============================================================================
                //print_r($frmdata);	
                if ($totalCount > ($frmdata['from'] + $frmdata['to']))
                {
                    //Added by Richa Verma on 12 may 2011 to show last data
                    
                    /*
                    $count=$totalCount/$recordPerPage;
                    if($pos = strpos($count, '.'))// if return value in float
                    {
                    $pagelast=explode('.',$count);
                    
                    if ($pagelast[0]<$count)
                    {
                    $plast=$pagelast[0]+1;
                    }
                    }	
                    else 
                    {	
                    $plast=$count;
                    }	
                    echo '<a href="javascript:setvalue(document.frmlist,document.frmlist.pageNumber,'.$plast.')" ><img border="0" src="'.IMAGEURL.'/last.gif"></a></td><td width="50" height="5" align="right" >';
                    */
                    
                    echo '<a href="javascript:NextPage();" ><img border="0" src="' . IMAGEURL . '/next.gif"></a>';
                    
                    
                } //$totalCount > ($frmdata['from'] + $frmdata['to'])
                
                echo '</td>
				  </tr>
				</table>';
                
            } //$totalCount > $recordPerPage
        } //$recordPerPage != 'All'
        
    }
	

/*
	@para
	qury : query statement
	
	@Description : this function is used to select multiple records .will return only single record

*/
function RunSelectQuerySigle($query)
{
	
	$DB=new DBConnect;
	if($result=$DB->Select($query))
	{
			$result-> setFetchMode(PDO::FETCH_OBJ);
		$row=$result->fetch();
		if($row)
			return $row;
	}
	return false;
} 


/*
	@para
	@$table : String( Name of table)
	@$value  : single value
	@$cond  : condition for updation
	@Description : this function is used to take data as formof array and gerates a update status query.

*/
function UpdateStatus($table,$value,$cond,$field='statusval')
{
	$DB= new DBConnect();
	if($table=='')
	{
		return false;
	}	
	else
	{
		if($value=='')
		{
			return false;
		}
		else
		{
				$query=" update ".$table." set ".$field."='".$value."'";
				if($cond)
				{
					 $query.=" where  ".$cond;
				}
		   echo  $query;// exit;
				if($DB->Update($query))
				{
					return true;
				}
		}
	}
	
 return false;
} 	
/*
	Added by Neha Kaushik
	@para
	@$table : String(Name of table)
	@$data : Array of data that needed to be enter in database
	@Descriptiion : this function is used to take data as form of array and generates a insert query.
*/

function insert_array($table, $data)
 {
$DB= new DBConnect();
	
foreach ($data as $field=>$value) 
  {
		$fields[] = '`' . $field . '`';
		
		if ($field) 
		{
			$values[] = "'" . mysql_real_escape_string($value) . "'";
		}
	}
	$field_list = join(',', $fields);
	$value_list = join(', ', $values);
	
	$query ="INSERT INTO `".$table ."`(" . $field_list .")VALUES(".$value_list.")";
	//  echo $query;exit;
	return $query;

			
}
/*
	@added by Neha Kaushik
	@para
	@$table : String( Name of table)
	@$data  : Array of data that needed to be enter in database
	@Description : this function is used to take data as formof array and genrates a insert query.

*/		
function InsertarrayRecords($table,$data)
{

	$DB= new DBConnect();
	
	if($table=='')
	{
		return false;
	}	
	else
	{
		if($data=='')
		{
			return false;
		}
		else
		{
			
			$resource= mysql_query("SHOW COLUMNS FROM ".$table);
			
			if($resource)
			{
				$fields="";
				$values="";
				
				while($row = mysql_fetch_array($resource))
				{
					
					if(array_key_exists($row['Field'],$data))
					{
						 $fields.=$row['Field'].",";
						if(is_array($data[$row['Field']]))
						{
						   // $data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							// ($data[$row['Field']]);
							$data[$row['Field']]=implode(",",$data[$row['Field']]);
						}	
						$values.="'".$data[$row['Field']]."',";
					}
				}
				
				if($fields)
				{
					$fields=substr($fields,0,strlen($fields)-1);	
					$values=substr($values,0,strlen($values)-1);				
					$query="Insert into ".PREFIX.$table."(".$fields.") values(".$values.")";
				
		
					if($DB->Select($query))
					{
						return mysql_insert_id();
					}
					return false;
				}
				
			}
			
		}
	}
	return false;			
}

/*
	@para
	@$table : String( Name of table)
	@$cond  : condtion to select recorde reocrd
	@$fields : name of fields data are needed from query
	@backqury : It wil be include in end of query like order by statement
	
	@Description : this function is used to select multiple records

*/
function SelectRecords1($table,$cond='',$fields='*',$backquery='')
{
	global $frmdata;
	$DB=new DBConnect();
	
	if($table)
	{
		if($fields)
		{		
				if(is_array($fields))
				{
					$fields=implode(",",$fields);
				}
				
				$query="select distinct ".$fields." from ".$table." ";
				
				if($cond)
				{
					 $query.="where ".$cond." ";
				}
				
				if($backquery)
				{
					    $query.=$backquery;
				}
		
				if($result=$DB->Select($query))
				{
					if(mysql_num_rows($result) > 0)
					{
						$arr=array();
				  		while($row=mysql_fetch_object($result)) 
				  		{	
				         $arr[]=$row;
					    }
						return $arr;
					}		
				 	 
				}
				
		}
	}
	return false;
}

/*
	@added by Neha Kaushik
	@para
	@$table : String( Name of table)
	@$data  : Array of data that needed to be enter in database
	@Description : this function is used to take data as formof array and genrates a insert query.

*/		
function InsertarrayRecords1($table,$data,$cond='')
{
	$DB= new DBConnect();
	if($table=='')
	{  
		return false;
	}	
	else
	{
		if($data=='')
		{  
			return false;
		}
		else
		{
			$resource= $DB->Select("SHOW COLUMNS FROM ".$table);
			if($resource)
			{
				$sql="";
				
				while($row = mysql_fetch_array($resource))
				{ 
				
					if(array_key_exists($row['Field'],$data))
					{
					     if(is_array($data[$row['Field']]))
						 {
						     $data[$row['Field']]=array_values(array_filter($data[$row['Field']]));
							 $data[$row['Field']]=implode(",",$data[$row['Field']]);
						  } 
					 $sql.=$row['Field']."='".$data[$row['Field']]."',";
					
					}
				}
			
				
				if($sql)
				{
					$sql=substr($sql,0,strlen($sql)-1);
										
					$query="update ".PREFIX.$table." set ".$sql;
					if($cond)
					{
						if(!is_array($cond))
						{
							$query.=" where ".$cond;
						}
						else
						{
								$count=count($cond);
								if($count)
								{
									$fields=@array_keys($cond);
									$query.=" where ";
									for($counter=0;$counter<$count;$counter++)
									{
										
										if($fields[$counter]=='or' || $fields[$counter]=='and')
										{
												 $query.=" ".$fields[$counter]." "; 
										}
										else
										{
										
												 $query.=" ".$fields[$counter]."='".$cond[$fields[$counter]]."'"; 
										}		
									}
								}
							}		
					}
		
					if($DB->Update($query))
					{
						return true;
					}
					return false;
				}
				
			}
			
		}
	}
	return false;			
}


/*
	@added by Jaishree & Nidhishanker
	@$Query : String( Query to Execute )
	@$line  : The label to execute the exact function
	@Description : THIS FUNCTION IS TO FIRE TRANSACTION QUERIES.

*/	

//========================================================================
//THIS FUNCTION IS TO FIRE TRANSACTION QUERIES....
// RETURNS ID OF LAST INSERT OR IF DIES RETURNS ZERO...
//========================================================================
	function TransId($Query,$line='TransId')
    {
       if(!($this->db->query($Query)))
			return 0;

		if(!($Id=$this->db->lastInsertId()))
			return 0;
		return $Id;	
    }

//========================================================================
//THIS FUNCTION IS TO FIRE BEGIN TRANSACTION QUERIES....
//========================================================================
//========================================================================

	function TransBegin($Query='BEGIN',$line='TransBegin')
	{
		if(!($this->db->query($Query)))
			return 0; 
		return 1;	
	}

//========================================================================

//========================================================================
//THIS FUNCTION IS TO FIRE COMMIT TRANSACTION QUERIES....
//========================================================================
//========================================================================

	function TransCommit($Query='COMMIT',$line='TransCommit')
	{
	
		if(!($this->db->query($Query)))
			return 0; 
		return 1;	
	}

//========================================================================

//========================================================================
//THIS FUNCTION IS TO FIRE ROLLBACK TRANSACTION QUERIES....
//========================================================================
//========================================================================
	function TransRollback($Query='ROLLBACK',$line='TransRollback')
	{
		$this->db->query($Query);
		$this->ErrorHandler($line,$module);	
	}

//========================================================================

//========================================================================
//THIS FUNCTION IS TO FIRE TRANSACTION QUERIES....
// ROLLBACK IF ANY QUERY FAILS OTHERWISE COMMIT...
//========================================================================
//========================================================================	

	function Trans($TransQuery,$line='Trans')
    {
		//echo "<pre>"; print_r($TransQuery); exit;
		foreach($TransQuery as $Query)
		{ 
			if(!($this->db->query($Query))) 
			{
				return 0;			
			}
		}
		return 1;				
    } 

function DrawFromDB($table,$field,$type,$data,$sort="yes",$title="")
{
//connect to DB;
	$DB= new DBConnect();
$query= $DB->Select("SHOW COLUMNS FROM ".$table) or die (mysql_error()); 
if(mysql_fetch_array($query)>0)
{
$row= mysql_fetch_array($query);
$count=count($row);
//$options=explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$row[1]));

if ($sort =="yes")
sort ($options);
$ARay = explode(",",$data);
}
switch ($type)
{ 
case "select":
$text="\n\n<select name='". $field ."'>\n";
if ($title > "")
$text.="<option value=\"\">" . $title . "</option>\n";
for ($i=0;$i<count($options);$i++)
{ 
$selected = NULL;
if ($data == $options[$i])
$selected ="SELECTED ";
$text.="<option " . $selected .
"value=\"".$options[$i]."\">".ucfirst($options[$i])."</option>\n";
}
$text.="</select>\n\n";
break;

default:
$text="\n";
for ($i=0;$i<count($options);$i++)
{
$checked=NULL;
for ($j=0;$j<count($ARay);$j++)
{ 
if ($ARay[$j] == $options[$i])
$checked=" CHECKED ";
}
$text.="<INPUT TYPE='checkbox'" . $checked . " NAME='" . $field."[]'
VALUE='". $options[$i] . "'>".ucfirst($options[$i])." \n";
}
$text.="\n";
break;
}
return $text; 
}
	
}