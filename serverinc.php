<?php
error_reporting(E_ALL);
ini_set("display_errors", 0); 
session_name('admin');
@session_start();
@include_once("start.php");
$DB= new DBFilter();

$link = mysql_connect(HOST,USER,PASSWORD);
if ($link)
{
	Print "";
} 
else
{
	Print "No connection to the database";
}
if (!mysql_select_db(DATABASE))
{
	Print "Couldn't connect database";
} 

//For select grade according to stream 
function showgrade($catID,$table,$width, $html_field_id='')
{
	unset($_SESSION['total']);
	$objResponse = new xajaxResponse();
	$output='';
	//$objResponse->addAlert($catID);
	$sql ="select * from ".$table." where stream_id = '".$catID."' order by grade_title";
	if ($result=mysql_query($sql))
	{
		if($html_field_id!='')
		{
			$field_id = "grade_id_manage";
		}
		else
		{
			$field_id = "grade_id";
		}
		
		$output='<select style="width:107px;" name="'.$field_id.'" id="'.$field_id.'" class="rounded" ><option value="" >Select Grade</option>';
	    while($row=mysql_fetch_array($result))
		{ 
			$selected='';
			if ($row['id'] == $_SESSION[$field_id])
			{
				$selected='selected';
			}
			$output.='<option value="'.$row['id'].'"'.$selected.'>'.stripslashes($row['grade_title']).'</option>';
		}
			$output.='</select>';
		if(isset($_SESSION['grade_id']))
		{
			unset($_SESSION['grade_id']);
		}		
	}
	//$objResponse->addAlert($output);
	$objResponse->addAssign('subCate','innerHTML',$output);
	//$objResponse->addAssign('total_mark','innerHTML','0');
	unset($_SESSION['grade_id']);
	
	return $objResponse;
}
function showgradedone($catID,$table,$width, $html_field_id='')
{
	unset($_SESSION['total']);
	$objResponse = new xajaxResponse();
	$output='';
	//$objResponse->addAlert($catID);
	$sql ="select * from ".$table." where stream_id = '".$catID."' order by grade_title";
	if ($result=mysql_query($sql))
	{
		if($html_field_id!='')
		{
			$field_id = $html_field_id;
		}
		else
		{
			$field_id = "grade";
		}
		
		$output='<select style="width:107px;" name="'.$field_id.'" id="'.$field_id.'" class="rounded" ><option value="" >Select Grade</option>';
	    while($row=mysql_fetch_array($result))
		{ 
			$selected='';
			if ($row['id'] == $_SESSION[$field_id])
			{
				$selected='selected';
			}
			$output.='<option value="'.$row['id'].'"'.$selected.'>'.stripslashes($row['grade_title']).'</option>';
		}
			$output.='</select>';
		if(isset($_SESSION['grade_id']))
		{
			unset($_SESSION['grade_id']);
		}		
	}
	//$objResponse->addAlert($output);
	$objResponse->addAssign('subCategrade','innerHTML',$output);
	//$objResponse->addAssign('total_mark','innerHTML','0');
	unset($_SESSION['grade_id']);
	
	return $objResponse;
}
function showgradeupgradingdone($catID,$table,$width, $html_field_id='')
{
	unset($_SESSION['total']);
	$objResponse = new xajaxResponse();
	$output='';
	//$objResponse->addAlert($catID);
	$sql ="select * from ".$table." where stream_id = '".$catID."' order by grade_title";
	if ($result=mysql_query($sql))
	{
		if($html_field_id!='')
		{
			$field_id = "grade_id_manage";
		}
		else
		{
			$field_id = "upgrading_grade";
		}
		
		$output='<select style="width:107px;" name="'.$field_id.'" id="'.$field_id.'" class="rounded" ><option value="" >Select Grade</option>';
	    while($row=mysql_fetch_array($result))
		{ 
			$selected='';
			if ($row['id'] == $_SESSION[$field_id])
			{
				$selected='selected';
			}
			$output.='<option value="'.$row['id'].'"'.$selected.'>'.stripslashes($row['grade_title']).'</option>';
		}
			$output.='</select>';
		if(isset($_SESSION['grade_id']))
		{
			unset($_SESSION['grade_id']);
		}		
	}
	//$objResponse->addAlert($output);
	$objResponse->addAssign('subCate','innerHTML',$output);
	//$objResponse->addAssign('total_mark','innerHTML','0');
	unset($_SESSION['grade_id']);
	
	return $objResponse;
}

// Show how many option going to display when user select type of question
function showoption($type, $no_of_images='', $no_of_options='', $qid='')
{
	$objResponse = new xajaxResponse();
	$output='';
	//$objResponse->addAlert($qid);
	if($type=='T')
	{
		/*if(isset($_SESSION['correct_ans']))
		{
			$checked_ans=$_SESSION['correct_ans'];
			//$objResponse->addAlert($checked_ans);
			if($checked_ans=='1')
			{
				$first_checked='checked';
			}
			elseif($checked_ans=='2')
			{
				$second_checked='checked';
			}
			unset($_SESSION['correct_ans']);
	
		}*/
		$output='<table id="tbl_dashboard" style=""><tr>
						    <td width="150" valign="top"><div align="right">Enter Options:<span class="red">*</span></div></td>
						    <td colspan="3" valign="top">&nbsp;</td>
						  </tr>';
		
		/*$output.='<tr><td valign="top"><div align="right">1. </div></td> 
					<td width="335" valign="top">
								<input type="text" name ="option_1" class="rounded required textfield " size="30" Value="True" />
								<input name="correct_ans" type="radio" value="1" '.$first_checked.'/></td>
								</tr>';
		$output.='<tr><td valign="top"><div align="right">2. </div></td> 
					<td width="335" valign="top">
								<input type="text" name ="option_2" class="rounded required textfield" size="30" Value="False" />
								<input name="correct_ans" type="radio" value="2" '.$second_checked.'/></td>
								</tr>';*/
		
		for($counter=1;$counter<=2;$counter++)
		{
			if (isset($_SESSION['option_'.$counter]))
			{
				$value=$_SESSION['option_'.$counter];
				unset($_SESSION['option_'.$counter]);
			}
			elseif (isset($_SESSION['pre_option_'.$counter]))
			{
				$value=$_SESSION['pre_option_'.$counter];
				unset($_SESSION['pre_option_'.$counter]);
				
			}
			
			if($value=='' && $counter==1)
			{
				$value='True';
			}
			elseif($value=='' && $counter==2)
			{
				$value='False';
			}
			
			$output.='	<tr><td valign="top">
								<div align="right">'.$counter.'</div>
							</td> 
							<td width="" valign="top">
								<input type="text" name ="option_'.$counter.'" class="rounded textfield required" size="30" value="'.htmlentities($value, null, "Windows-1252", false).'" />
								<input name="correct_ans" type="radio" value="'.$counter.'"  ';
			
			if(isset($_SESSION['correct_ans']))
			{
				if($_SESSION['correct_ans'] == $counter)
				{
					$output .= ' checked ';
					unset($_SESSION['correct_ans']);
				}
			}
			
			$output .= ' /></td></tr>';
			$value='';
		}
		$output.='	<tr><td valign="top">&nbsp;</td>
						<td valign="top" colspan="3"><div align="left" id="mandatory"><strong>Note:</strong> Select radio button for correct answer. </div></td>
					</tr></table>';
								
	}
	elseif($type=='M')
	{
		$output='<table id="tbl_dashboard" style=""><tr>
						    <td width="150" valign="top"><div align="right">Enter Options:<span class="red">*</span></div></td>
						    <td colspan="3" valign="top">&nbsp;</td>
						  </tr>';
		for($counter=1;$counter<=4;$counter++)
		{
			if (isset($_SESSION['option_'.$counter]))
			{
				$value=$_SESSION['option_'.$counter];
				unset($_SESSION['option_'.$counter]);
			}
			elseif (isset($_SESSION['pre_option_'.$counter]))
			{
				$value=$_SESSION['pre_option_'.$counter];
				unset($_SESSION['pre_option_'.$counter]);
			}
			
			$output.='<tr><td valign="top"><div align="right">'.$counter.'</div></td> 
					<td width="" valign="top">
								<input type="text" name ="option_'.$counter.'" class="rounded textfield required" size="30" value="'.htmlentities($value, null, "Windows-1252", false).'" />
								<input name="correct_ans" type="radio" value="'.$counter.'" ';
			
			if(isset($_SESSION['correct_ans']))
			{
				if($_SESSION['correct_ans'] == $counter)
				{
					$output .= ' checked ';
					unset($_SESSION['correct_ans']);
				}
			}
			$output .= '/></td></tr>';
		}
		$output.='<tr>
						 <td valign="top">&nbsp;</td>
						 <td valign="top" colspan="3"><div align="left" id="mandatory"><strong>Note:</strong> Select radio button for correct answer. </div></td>
				   </tr></table>';
	}
	elseif($type == 'I')
	{
		$no_of_images = (($no_of_images != '') && ($no_of_images >= 3)) ? $no_of_images : 3;
		$no_of_options = ($no_of_options != '') ? $no_of_options : 4;
		
		$output='<input type="hidden" name="image_type" id="image_type" value=""/>';
		//$output.='<iframe id="ifrm" name="ifrm" src="" width="0" heigth="0" border="0" style="display:none;"></iframe>';
		
		$output.='<table id="image_tab" style="" border="0">
					<tr>
					 	<td valign="top" width="150">
					 		<div align="right">
					 			Upload Images:
					 		</div>
					 	</td>
						<td colspan="3" valign="top">
							&nbsp;
						</td>
					</tr>';
				//print_r('mjbvm');
		for($counter=1;$counter<=$no_of_images;$counter++)
		{

			if(isset($_SESSION['fileObject_image_'.$counter]))
			{
				$_SESSION['image_'.$counter] = $_SESSION['fileObject_image_'.$counter]['final_path'];
			}
			$value = '';
			
			$output.='<tr id="tr_image_'.$counter.'">
						<td valign="top">
							<div align="right">
								'.$counter.'
							</div>
						</td> 
						<td valign="top">
							
								<input type="file" name ="image_'.$counter.'" id="image_'.$counter.'" class="rounded textfield required" size="30" onchange="uploadTempFile(this.id);"/>&nbsp;';
			
			$output .='<input name="question_mark" type="radio" value="'.$counter.'" ';
			
			if(isset($_SESSION["question_mark"]))
			{
				if($_SESSION["question_mark"] == $counter)
				{
					$output .= ' checked ';
					//unset($_SESSION["question_mark"]);
				}
			}
			$output .= '/>&nbsp;';
			
			$output .='<span style="max-width:25px;max-height:25px;" id="img_image_'.$counter.'">';
			
			if(isset($_SESSION['image_'.$counter]))
			{
				$img = '/uploadfiles/' . $_SESSION['image_'.$counter];
				
				if(file_exists(ROOT . $img))
				$output.=	'<img style="width:20px;heigth:20px;" src = ' . ROOTURL . $img . ' />';
				
				unset($_SESSION['image_'.$counter]);
			}
			elseif(isset($_SESSION['pre_image_'.$counter]))
			{
				$img = '/uploadfiles/' . $_SESSION['pre_image_'.$counter];
				
				if(file_exists(ROOT . $img))
				$output.=	'<img style="width:20px;heigth:20px;" src = ' . ROOTURL . $img . ' />';
				
				//unset($_SESSION['pre_image_'.$counter]);
			}
			
			$output .=		'</span>
							</td>
						</tr>';
			
		}
		
		$output.='<tr id="image_add_more" >
					<td valign="top">&nbsp;</td>
					<td valign="top"><a href="javascript:void(0);" onclick="addMoreImage();" >Add More</a>&nbsp;&nbsp;&nbsp
					<span id="remove_image" valign="top">';
		
		if($no_of_images > 3)
		{			
			$output .='<a href="javascript:void(0);" onclick="removeLastImage();" >Remove Last</a>';
		} 
		
		$output .= '</span></td></tr>';
		
		$output.='<tr id="question_note">	 
						<td valign="top">&nbsp;</td>
						 <td valign="top" colspan="3">
						 	<div align="left" id="mandatory">
						 		<strong>Note:</strong> Select radio button to set image as question mark. &nbsp;
						 		<span><a href="javascript:void(0);" onclick="resetQMark();">Reset Question Mark</a></span><br>
						 		Please upload image in .JPG, .GIF, .PNG and .JPEG format.<br> 
						 		Image size should not be more than 2MB.
						 	</div>
						 </td>
				   </tr></table>';

		$output.='<table style="" border="0">
					<tr>
					 	<td width="150" valign="top">
					 		<div align="right">
					 			Upload Options:<span class="red">*</span>
					 		</div>
					 	</td>
						<td colspan="3" valign="top">
							&nbsp;
						</td>
					</tr>';
					
		for($counter=1;$counter<=$no_of_options;$counter++)
		{
		
			if(isset($_SESSION['fileObject_option_'.$counter]))
			{
				$_SESSION['option_'.$counter] = $_SESSION['fileObject_option_'.$counter]['final_path'];
			}
			$value="";
			
			$output.='<tr id="tr_option_'.$counter.'">
						<td valign="top">
							<div align="right">
								'.$counter.'
							</div>
						</td> 
						<td width="">
							<input type="file" name ="option_'.$counter.'" id="option_'.$counter.'" class="rounded textfield required" size="30" onchange="uploadTempFile(this.id);"/>';
							
			$output .='<input name="correct_ans" type="radio" value="'.$counter.'" ';
			
			if(isset($_SESSION['correct_ans']))
			{
				if($_SESSION['correct_ans'] == $counter)
				{
					$output .= ' checked ';
					unset($_SESSION['correct_ans']);
				}
			}
			$output .= '/>&nbsp;<span style="max-width:25px;max-height:25px;" id="img_option_'.$counter.'">';
			
			if(isset($_SESSION['option_'.$counter]))
			{
				$img = '/uploadfiles/' . $_SESSION['option_'.$counter];
				
				if(file_exists(ROOT . $img))
				$output.='<img style="width:20px;heigth:20px;" src = ' . ROOTURL . $img . ' />';
				
				unset($_SESSION['option_'.$counter]);
			}
			elseif(isset($_SESSION['initial_answer_'.$counter]))
			{
				$img = '/uploadfiles/' . $_SESSION['initial_answer_'.$counter];
				
				if(file_exists(ROOT . $img))
				$output.='<img style="width:20px;heigth:20px;" src = ' . ROOTURL . $img . ' />';
				
				//unset($_SESSION['initial_answer_'.$counter]);
				unset($_SESSION['pre_option_'.$counter]);
			}
			$output .= '</span>';
			$output .= '</td></tr>';
		}
		
		$output.='<tr id="option_add_more" >
					<td valign="top">&nbsp;</td>
					<td valign="top" colspan="3"><a href="javascript:void(0);" onclick="addMoreOption();" >Add More</a>&nbsp;&nbsp;&nbsp
					<span id="remove_option" valign="top">';
		
		if($no_of_options > 4)
		{
			$output .= '<a href="javascript:void(0);" onclick="removeLastOption();" >Remove Last</a>';
		}
		
		$output.= '</span></td></tr>';
		
		$output.='<tr id="option_note">
						<td valign="top">&nbsp;</td>
						<td valign="top" colspan="3"><div align="left" id="mandatory"><strong>Note:</strong> Select radio button for correct answer. <br>Please upload image in .JPG, .GIF, .PNG and .JPEG format.<br> Image size should not be more than 2MB.</div></td>
				   </tr></table>';
	}

	elseif($type == 'S')
	{ 
	//$objResponse->addAlert($_SESSION['subjective']);
		
		$sql ="select subjective from question_answer where question_id = '".$qid."'";//$objResponse->addAlert($sql);
		$result=mysql_query($sql);//$objResponse->addAlert($qid);
		$count=mysql_fetch_array($result);//$objResponse->addAlert($count['subjective']);
		//$objResponse->addAssign('subjective','innerHTML',$count);
		$output = '<p style = "font-size:14px; color:black">Answer<span class="red"> *</span></p>';
		$output .= '<td width="150" valign="top" align="center"  colspan="1"><textarea rows = "10" cols = "20" name = "subjective" style="width:450px"> '.$count['subjective'].' </textarea>';		    
		$output .='</td>';
		
	}
	
	
	$objResponse->addAssign('opt','innerHTML',$output);
	return $objResponse;
}

// Display total inline iwhen user select no of question 
function showtotal($gid,$sid)
{
	$objResponse = new xajaxResponse();
	$output='';
	
	$beg_sql ="select count(*)as count from question where FIND_IN_SET($sid, stream_id) and grade_id ='".$gid."' and question_level= 'B' order by id";
	$beg_result=mysql_query($beg_sql);
	$beg_count=mysql_fetch_array($beg_result);
	$beg_level=$beg_count['count'];
	$objResponse->addAssign('beg_level','innerHTML',$beg_level);
	
	$int_sql ="select count(*)as count from question where FIND_IN_SET($sid, stream_id) and grade_id ='".$gid."' and question_level= 'I' order by id";
	$int_result=mysql_query($int_sql);
	$int_count=mysql_fetch_array($int_result);
	$int_level=$int_count['count'];
	$objResponse->addAssign('int_level','innerHTML',$int_level);
	
	$high_sql ="select count(*)as count from question where FIND_IN_SET($sid, stream_id) and grade_id ='".$gid."' and question_level= 'H' order by id";
	$high_result=mysql_query($high_sql);
	$high_count=mysql_fetch_array($high_result);
	$high_level=$high_count['count'];
	$objResponse->addAssign('high_level','innerHTML',$high_level);
	
	return $objResponse;
}

// Display how many questions according to their marks available in bank
function getquestion($sid, $cust_que_set, $subject_id='', $post_value='', $precourse_subject_id='', $equipment_id = '', $qtyp = '')
{//$qtyp = 'N';

	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert($qtyp);
	
	//return $objResponse;
	//$objResponse->addAlert($sid.'='.$cust_que_set.'='. $subject_id.'='.$post_value.'='. $precourse_subject_id.'='.$equipment_id.'='.$qtyp);
	//return $objResponse;
	//print_r($_SESSION['total']);exit;
	if(isset($_SESSION['custom_total_marks']))
	{
		//$objResponse->addAssign('total_mark','innerHTML','');
		//unset($_SESSION['custom_total_marks']);
		//unset($_SESSION['question']);
	}
	
	if(isset($_SESSION['total_paper_set']))
	{
		unset($_SESSION['total_paper_set']);
	}
	if(isset($_SESSION['beg']))
	{
		unset($_SESSION['beg']);
	}
	if(isset($_SESSION['int']))
	{
		unset($_SESSION['int']);
	}
	if(isset($_SESSION['high']))
	{
		unset($_SESSION['high']);
	}
	//print_r($_SESSION);exit;
	//Fetch Beginner level Question
	$boutput='';
		if($cust_que_set!='')
			$que_set_condition = " and id not in (".$cust_que_set.")";
		else
			$que_set_condition="";
		
		if($subject_id!='')
		{
			$common_query_cond = " ( FIND_IN_SET($subject_id, subject_id) ";
			$subject_info = $DB->SelectRecord('subject',"id=$subject_id");
			
			if($subject_info->stream_id!='' && $subject_info->stream_id!=0)
			{
				$common_query_cond.= "or FIND_IN_SET($subject_info->stream_id, stream_id) ";
			}
			$common_query_cond.=") ";
			
			$html_onchange_function = 'subjectQuestionRestrict(this.value,this.name)';
		}
		
		elseif($sid!='' && $sid!=0)
		{
			//$common_query_cond = "stream_id = '".$sid."'";
			$common_query_cond = " FIND_IN_SET($sid, stream_id) ";
			$html_onchange_function = 'questionRestrict(this.value,this.name)';
		}
		else
		{
			$common_query_cond = " FIND_IN_SET($precourse_subject_id, subject_id) ";
			$html_onchange_function = 'questionRestrict(this.value,this.name)';
		}
			
	$join = '';
	if($equipment_id != '' && $equipment_id != 0) {
		$join = ' JOIN question_equipment qe ON qe.question_id = question.id ';
		$common_query_cond .= " AND qe.equipment_id = '$equipment_id' ";
	}
	if($qtyp=='Y')
	{
	$beg_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'B'".$que_set_condition." and question_type = 'S'";//$objResponse->addAlert($beg_marks_sql);
	}
	else
	{
	$beg_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'B'".$que_set_condition;
}
	$beg_mark_result=mysql_query($beg_marks_sql);
	$is_exist=0;
	$boutput.='<fieldset class="rounded"><legend>Must Know</legend><table align="center"><tr><td></td></tr>';
	while($beg_mark=mysql_fetch_array($beg_mark_result))
	{
		if($beg_mark!='')
		{
			
			$marks=	$beg_mark['marks'];
			if($qtyp=='Y'){
			$beg_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'B' and marks=".$marks.$que_set_condition." and question_type = 'S'";
			}
			else
			{
			$beg_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'B' and marks=".$marks.$que_set_condition;
			}
			
			//$objResponse->addAlert($beg_tmarks_sql);
			$beg_tmark_result=mysql_query($beg_tmarks_sql);
			$beg_tmark=mysql_fetch_array($beg_tmark_result);
			$total_beg=$beg_tmark['count'];
			$_SESSION['beg'][$marks]=$total_beg;
			//makeBeginnerQuestionSession();
			//$objResponse->addAlert($marks.','.$_SESSION['total']["beg_".$marks.'_question']);
			if($post_value!='')
			{
				for($counter=0; $counter<count($post_value);  $counter++)
				{
					if(substr($post_value[$counter],0,strpos($post_value[$counter],'='))=='beg_'.$marks.'_'.$total_beg)
					{
						$_SESSION['total']["beg_".$marks] = substr($post_value[$counter],strpos($post_value[$counter],'=')+1);
						$_SESSION['total']["beg_".$marks.'_question']+=$_SESSION['total']["beg_".$marks];
					}
				}
			}
			$boutput.='<tr><td>'.$marks.' x <input type="text" maxlength="2" value="'.$_SESSION['total']["beg_".$marks].'" size="1" id="beg_'.$marks.'_'.$total_beg.'" name="beg_'.$marks.'_'.$total_beg.'"  onchange="'.$html_onchange_function.';" onkeyup="isNum(this.value,this.id);"> / '.$total_beg.'</td></tr>';
			unset($_SESSION['beg-'.$marks]);
			$is_exist=1;
		}
		
	}
	
	$sub_id = $subject_id ? $subject_id : '-1';
	
	if($is_exist==0)
	{
		$boutput.="<tr><td>No Question available.</td></tr>";	
	}
	else
	{ if($qtyp != 'Y')
	{
		$boutput.="<tr><td><br><fieldset class='rounded'>";
//		$boutput.="<label for='beg_question_type'><input type='checkbox' name='beg_question_type' id='beg_question_type'>Select Question Type</label>";
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['M'].'" size="1" name="B_question_type_M" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Multiple Type</label>';
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['T'].'" size="1" name="B_question_type_T" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% True/false</label>';
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['I'].'" size="1" name="B_question_type_I" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Reasoning</label>';
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['S'].'" size="1" name="B_question_type_S" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Subjective</label>';
		$boutput.="</fieldset></td></tr>";
	}}
	$boutput.='</table></fieldset>';
	
	//Fetech Intermediate level Question
	$ioutput='';
	if($qtyp=='Y')
	{$int_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'I'".$que_set_condition."and question_type = 'S'";
			
	}
	else
	{
	$int_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'I'".$que_set_condition;
	}
	//$objResponse->addAlert($int_marks_sql);
	$int_mark_result=mysql_query($int_marks_sql);
	$is_exist=0;
	$ioutput.='<fieldset class="rounded"><legend>Should Know</legend><table align="center"><tr><td></td></tr>';
	while($int_mark=mysql_fetch_array($int_mark_result))
	{	
		//$objResponse->addAlert($int_mark);
		$marks=	$int_mark['marks'];
		if($qtyp=='Y')
		{
		$int_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'I' and marks=".$marks.$que_set_condition." and question_type ='S'";
		}
		else
		{
		$int_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'I' and marks=".$marks.$que_set_condition;
		}
		$int_tmark_result=mysql_query($int_tmarks_sql);
		$int_tmark=mysql_fetch_array($int_tmark_result);
		$total_int=$int_tmark['count'];
		$_SESSION['int'][$marks]=$total_int;
		
		//makeInterQuestionSession();
		if($post_value!='')
		{
			for($counter=0; $counter<count($post_value);  $counter++)
			{
				if(substr($post_value[$counter],0,strpos($post_value[$counter],'='))=='int_'.$marks.'_'.$total_int)
				{
					$_SESSION['total']["int_".$marks] = substr($post_value[$counter],strpos($post_value[$counter],'=')+1);
					$_SESSION['total']["int_".$marks.'_question']+=$_SESSION['total']["int_".$marks];
				}
			}
		}
		//$objResponse->addAlert($_SESSION['total']["int_2_question"]);
		$ioutput.='<tr><td>'.$marks.' x <input type="text" maxlength="2" size="1" value="'.$_SESSION['total']["int_".$marks].'" id="int_'.$marks.'_'.$total_int.'" name="int_'.$marks.'_'.$total_int.'"  onchange="'.$html_onchange_function.';" onkeyup="isNum(this.value,this.id);"> / '.$total_int.'</td></tr>';
		unset($_SESSION['int-'.$marks]);
		$is_exist=1;
	}
	if($is_exist==0)
	{
		$ioutput.="<tr><td>No Question available.</td></tr>";	
	}
	else
	{if($qtyp != 'Y')
	{
		$ioutput.="<tr><td><br><fieldset class='rounded'>";
//		$ioutput.="<label for='int_question_type'><input type='checkbox' name='int_question_type' id='int_question_type'>Select Question Type</label>";
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['M'].'" size="1" name="I_question_type_M" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Multiple Type</label>';
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['T'].'" size="1" name="I_question_type_T" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% True/false</label>';
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['I'].'" size="1" name="I_question_type_I" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Reasoning</label>';
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['S'].'" size="1" name="I_question_type_S" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Subjective</label>';
		$ioutput.="</fieldset></td></tr>";
	}}
	$ioutput.='</table></fieldset>';
	
	//Fetech Higher level Question
	$houtput='';
	if($qtyp =='Y')
	{$high_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'H'".$que_set_condition."and question_type = 'S'";}
	else {
	$high_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'H'".$que_set_condition;
	}
	$high_mark_result=mysql_query($high_marks_sql);
	$is_exist=0;
	$houtput.='<fieldset class="rounded"><legend>Could Know</legend><table align="center"><tr><td></td></tr>';
	while($high_mark=mysql_fetch_array($high_mark_result))
	{
		$marks=	$high_mark['marks'];
		if($qtyp =='Y'){
		$high_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'H' and marks=".$marks.$que_set_condition." and question_type = 'S'";
		}
		else
		{
		$high_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'H' and marks=".$marks.$que_set_condition;
		}
		$high_tmark_result=mysql_query($high_tmarks_sql);
		$high_tmark=mysql_fetch_array($high_tmark_result);
		
		$total_high=$high_tmark['count'];
		//$objResponse->addAlert($high_tmarks_sql);
		$_SESSION['high'][$marks]=$total_high;
		//makeHigherQuestionSession();
		if($post_value!='')
		{
			for($counter=0; $counter<count($post_value);  $counter++)
			{
				if(substr($post_value[$counter],0,strpos($post_value[$counter],'='))=='high_'.$marks.'_'.$total_high)
				{
					$_SESSION['total']["high_".$marks] = substr($post_value[$counter],strpos($post_value[$counter],'=')+1);
					$_SESSION['total']["high_".$marks.'_question']+=$_SESSION['total']["high_".$marks];
				}
			}
		}
		$houtput.='<tr><td>'.$marks.' x <input type="text" maxlength="2" size="1" value="'.$_SESSION['total']["high_".$marks].'" id="high_'.$marks.'_'.$total_high.'" name="high_'.$marks.'_'.$total_high.'"  onchange="'.$html_onchange_function.'" onkeyup="isNum(this.value,this.id);"> / '.$total_high.'</td></tr>';
		unset($_SESSION['high-'.$marks]);
		$is_exist=1;
	}
	if($is_exist==0)
	{
		$houtput.="<tr><td>No Question available.</td></tr>";	
	}
	else
	{if($qtyp != 'Y')
	{
		$houtput.="<tr><td><br><fieldset class='rounded'>";
//		$houtput.="<label for='high_question_type'><input type='checkbox' name='high_question_type' id='high_question_type'>Select Question Type</label>";
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['M'].'" size="1" name="H_question_type_M" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Multiple Type</label>';
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['T'].'" size="1" name="H_question_type_T" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% True/false</label>';
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['I'].'" size="1" name="H_question_type_I" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Reasoning</label>';
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['S'].'" size="1" name="H_question_type_S" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Subjective</label>';
		$houtput.="</fieldset></td></tr>";
	}}
	$houtput.='</table></fieldset>';
	
	$message='<span style="color:red;font-size:10px">Marks x No. of question? / Question available</span>';
	//$objResponse->addAssign('showques','innerHTML','');
	if($subject_id!='')
	{
		$objResponse->addAssign('subject_message','innerHTML',$message);
		$objResponse->addAssign('subject_beg_level','innerHTML',$boutput);
		$objResponse->addAssign('subject_int_level','innerHTML',$ioutput);
		$objResponse->addAssign('subject_high_level','innerHTML',$houtput);
	}
	elseif($sid!='' && $qtyp!='')
	{
		$objResponse->addAssign('subject_message','innerHTML',$message);
		$objResponse->addAssign('subject_beg_level','innerHTML',$boutput);
		$objResponse->addAssign('subject_int_level','innerHTML',$ioutput);
		$objResponse->addAssign('subject_high_level','innerHTML',$houtput);

	}
	else 
	{
		$objResponse->addAssign('message','innerHTML',$message);
		$objResponse->addAssign('beg_level','innerHTML',$boutput);
		$objResponse->addAssign('int_level','innerHTML',$ioutput);
		$objResponse->addAssign('high_level','innerHTML',$houtput);
	}
	//$objResponse->addAlert($_SESSION['total']['int_2_totalQuestion']);
	return $objResponse;
	
}

function saveQuestionTypeCount($type, $count, $subject_id=-1)
{
	$objResponse = new xajaxResponse();
	
	$type = explode('_', $type);
	$_SESSION['question_type_count'][$subject_id][$type[0]][$type[3]] = $count;
	return $objResponse;
}

function getquestionedit($sid,$testid, $type='', $precourse_subject_id='', $equipment_id = '')
{
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert('SDG');
	
	if(isset($_SESSION['total_paper_set']))
	{
		unset($_SESSION['total_paper_set']);
	}
	if(isset($_SESSION['beg']))
	{
		unset($_SESSION['beg']);
	}
	if(isset($_SESSION['int']))
	{
		unset($_SESSION['int']);
	}
	if(isset($_SESSION['high']))
	{
		unset($_SESSION['high']);
	}
	$total=0;
	if($sid!='' && $sid!=0)
	{
		$common_query_cond = " FIND_IN_SET($sid, stream_id) ";
	}
	elseif($precourse_subject_id != '')
	{
		$common_query_cond = " FIND_IN_SET($precourse_subject_id, subject_id) ";
	}
	
	$join = '';
	if($equipment_id != '' && $equipment_id != 0) {
		$join = ' JOIN question_equipment qe ON qe.question_id = question.id ';
		$common_query_cond .= " AND qe.equipment_id = '$equipment_id' ";
	}
	
	// Fetech Beginner level Question
	$boutput='';
	$beg_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'B'";
	$beg_mark_result=mysql_query($beg_marks_sql);
	$is_exist=0;
	$custom_que_set_query = '';
	if($type=='R,C')
	{
		$custom_que_set_query = " and id not in (select question_id from test_questions where test_id=$testid and question_selection='C')";
	}
	
	$boutput.='<fieldset class="rounded"><legend>Must Know</legend><table align="center"><tr><td></td></tr>';
	while($beg_mark=mysql_fetch_array($beg_mark_result))
	{
		if($beg_mark!='')
		{
			$marks=	$beg_mark['marks'];
			unset($_SESSION['beg-'.$marks]);
			$beg_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'B' and marks=".$marks.$custom_que_set_query."";
			
			$beg_tmark_result=mysql_query($beg_tmarks_sql);
			$beg_tmark=mysql_fetch_array($beg_tmark_result);
			$total_beg=$beg_tmark['count'];
			
			//$objResponse->addAlert($total_beg);
			//return $objResponse;
			
			if($total_beg!=0)
			{
				$beg_num ="select question_total from test_question_selection where test_id=".$testid." and question_marks =".$marks." and question_level= 'B'";
				$beg_num_result=mysql_query($beg_num);
				$num=mysql_fetch_array($beg_num_result);
				if($num['question_total']!='')
				{
					$total=$total+($marks*$num['question_total']);
					$_SESSION['beg-'.$marks]=$num['question_total'];
					$_SESSION['total']["beg_".$marks.'_question']+=$num['question_total'];
					$_SESSION['total']["beg_".$marks] = $num['question_total'];
				}
				$boutput.='<tr><td>'.$marks.' x <input type="text" maxlength="2" size="1" id="beg_'.$marks.'_'.$total_beg.'" name="beg_'.$marks.'_'.$total_beg.'"  value="'.$num['question_total'].'" onchange="questionRestrict(this.value,this.name)" onkeyup="isNum(this.value,this.id);"> / '.$total_beg.'</td></tr>';
				$_SESSION['beg'][$marks]=$total_beg;
			//	unset($_SESSION['beg-'.$marks]);
				$is_exist=1;
			}
		}
		
	}
	if($is_exist==0)
	{
		$boutput.="<tr><td>No Question are available.</td></tr>";	
	}
	else
	{	
		$boutput.="<tr><td><br><fieldset class='rounded'>";
//		$boutput.="<label for='beg_question_type'><input type='checkbox' name='beg_question_type' id='beg_question_type'>Select Question Type</label>";
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['M'].'" size="1" name="B_question_type_M" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Multiple Type</label>';
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['T'].'" size="1" name="B_question_type_T" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% True/false</label>';
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['I'].'" size="1" name="B_question_type_I" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Reasoning</label>';
		$boutput.='<label class="beg_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['B']['S'].'" size="1" name="B_question_type_S" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Subjective</label>';
		$boutput.="</fieldset></td></tr>";
	}
	$boutput.='</table></fieldset>';
	//$objResponse->addAlert($boutput);
	
	//Fetch Intermediate level Question
	$ioutput='';
	$int_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'I'";
	$int_mark_result=mysql_query($int_marks_sql);
	$is_exist=0;
	$ioutput.='<fieldset class="rounded"><legend>Should Know</legend><table align="center"><tr><td></td></tr>';
	while($int_mark=mysql_fetch_array($int_mark_result))
	{	
		$marks=	$int_mark['marks'];
		unset($_SESSION['int-'.$marks]);
		$int_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'I' and marks=".$marks.$custom_que_set_query."";
		$int_tmark_result=mysql_query($int_tmarks_sql);
		$int_tmark=mysql_fetch_array($int_tmark_result);
		$total_int=$int_tmark['count'];
		
		if($total_int!=0)
		{
			$int_num ="select question_total from test_question_selection where test_id = '".$testid."' and question_marks ='".$marks."' and question_level= 'I'";
			$int_num_result=mysql_query($int_num);
			$num=mysql_fetch_array($int_num_result);
			//$objResponse->addAlert($int_num);
			if($num['question_total']!='')
			{
				$total=$total+($marks*$num['question_total']);
				$_SESSION['int-'.$marks]=$num['question_total'];
				$_SESSION['total']["int_".$marks.'_question']+=$num['question_total'];
				$_SESSION['total']["int_".$marks] = $num['question_total'];
			}
			$ioutput.='<tr><td>'.$marks.' x <input type="text" maxlength="2" size="1" id="int_'.$marks.'_'.$total_int.'" name="int_'.$marks.'_'.$total_int.'"  value="'.$num['question_total'].'"onchange="questionRestrict(this.value,this.name)" onkeyup="isNum(this.value,this.id);"> / '.$total_int.'</td></tr>';
			$_SESSION['int'][$marks]=$total_int;
			//
			$is_exist=1;
		}
	}
	if($is_exist==0)
	{
		$ioutput.="<tr><td>No Question available.</td></tr>";	
	}
	else
	{
		$ioutput.="<tr><td><br><fieldset class='rounded'>";
//		$ioutput.="<label for='int_question_type'><input type='checkbox' name='int_question_type' id='int_question_type'>Select Question Type</label>";
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['M'].'" size="1" name="I_question_type_M" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Multiple Type</label>';
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['T'].'" size="1" name="I_question_type_T" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% True/false</label>';
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['I'].'" size="1" name="I_question_type_I" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Reasoning</label>';
		$ioutput.='<label class="int_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['I']['S'].'" size="1" name="I_question_type_S" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Subjective </label>';
		$ioutput.="</fieldset></td></tr>";
	}
	$ioutput.='</table></fieldset>';
	
	//Fetech Higher level Question
	$houtput='';
	$high_marks_sql ="select distinct(marks) from question $join where ".$common_query_cond." and question_level= 'H'";
	$high_mark_result=mysql_query($high_marks_sql);
	$is_exist=0;
	$houtput.='<fieldset class="rounded"><legend>Could Know</legend><table align="center"><tr><td></td></tr>';
	while($high_mark=mysql_fetch_array($high_mark_result))
	{
		$marks=	$high_mark['marks'];
		unset($_SESSION['high-'.$marks]);
		$high_tmarks_sql ="select count(DISTINCT question.id) as count from question $join where ".$common_query_cond." and question_level= 'H' and marks=".$marks.$custom_que_set_query."";
		
		$high_tmark_result=mysql_query($high_tmarks_sql);
		$high_tmark=mysql_fetch_array($high_tmark_result);
		//$objResponse->addAlert($beg_tmarks_sql);
		$total_high=$high_tmark['count'];
		
		if($total_high!=0)
		{
			$high_num ="select question_total from test_question_selection where test_id = '".$testid."' and question_marks ='".$marks."' and question_level= 'H'";
			$high_num_result=mysql_query($high_num);
			$num=mysql_fetch_array($high_num_result);
			if($num['question_total']!='')
			{
				$total=$total+($marks*$num['question_total']);
				$_SESSION['high-'.$marks]=$num['question_total'];
				$_SESSION['total']["high_".$marks.'_question']+=$num['question_total'];
				$_SESSION['total']["high_".$marks] = $num['question_total'];
			}
			$houtput.='<tr><td>'.$marks.' x <input type="text" maxlength="2" size="1" id="high_'.$marks.'_'.$total_high.'" name="high_'.$marks.'_'.$total_high.'"  value="'.$num['question_total'].'" onchange="questionRestrict(this.value,this.name)" onkeyup="isNum(this.value,this.id);"> / '.$total_high.'</td></tr>';
			$_SESSION['high'][$marks]=$total_high;
			//
			$is_exist=1;
		}
	}
	if($is_exist==0)
	{
		$houtput.="<tr><td>No Question available.</td></tr>";	
	}
	else
	{
		$houtput.="<tr><td><br><fieldset class='rounded'>";
//		$houtput.="<label for='high_question_type'><input type='checkbox' name='high_question_type' id='high_question_type'>Select Question Type</label>";
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['M'].'" size="1" name="H_question_type_M" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Multiple Type</label>';
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['T'].'" size="1" name="H_question_type_T" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% True/false</label>';
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['I'].'" size="1" name="H_question_type_I" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Reasoning</label>';
		$houtput.='<label class="high_types"><br><input type="text" maxlength="2" value="'.$_SESSION['question_type_count'][$sub_id]['H']['S'].'" size="1" name="H_question_type_S" onkeyup="isNum(this.value,this.id);" onchange="xajax_saveQuestionTypeCount(this.name, this.value, \''.$sub_id.'\');">% Subjective</label>';
		$houtput.="</fieldset></td></tr>";
	}
	//$houtput.="<input type='text' name='high_total_question' value=''>";
	$houtput.='</table></fieldset>';
	$message='<span style="color:red;font-size:10px">Marks x No. of question? / Question available</span>';
	//$objResponse->addAssign('showques','innerHTML','');
	$objResponse->addAssign('message','innerHTML',$message);
	$objResponse->addAssign('beg_level','innerHTML',$boutput);
	$objResponse->addAssign('int_level','innerHTML',$ioutput);
	$objResponse->addAssign('high_level','innerHTML',$houtput);
	//$objResponse->addAlert($type);
	if($total!=0)
	{
		//$objResponse->addAlert($total);
		//$_SESSION['total_paper_set']=$total;
		$objResponse->addScriptCall('xajax_gettotalmarks','editDefault',$total,'');
	}
	if($type=='R,C')
	{
		//$objResponse->addAlert("ok");
		$_SESSION['random_total_value']=$total;
		$objResponse->addScriptCall('xajax_getcustomquestionedit',$sid,$testid,$precourse_subject_id, $equipment_id);
	}
	
	//print_r($_SESSION);exit;
	return $objResponse;
	
}

// Calculate Total marks when user select no. of question to be given in paper
function showtotalmarks($required,$mark,$totalques,$level)
{
	
	$objResponse = new xajaxResponse();
	$diff=0;
	//$objResponse->addAlert($level.", ".$mark.", ".$_SESSION[$level.'-'.$mark]);
	$set=$_SESSION[$level.'-'.$mark];
	
	if (!isset($_SESSION[$level.'-'.$mark]))
	{
		$_SESSION[$level.'-'.$mark]=$required;
		if(isset($_SESSION['total_paper_set']) || $_SESSION['total_paper_set']!='')
		{
			$_SESSION['total_paper_set']=($required*$mark)+($_SESSION['total_paper_set']);
			$sfs=$_SESSION['total_paper_set'];
			
		}
		else
		{	
			$_SESSION['total_paper_set']=($required*$mark);
		}
		//$display_mark_set=$_SESSION['total_paper_set'];
		
	}
	else
	{
		$diff=($_SESSION[$level.'-'.$mark]*$mark)-($required*$mark);
		
		if($diff>0)
		{
			$_SESSION['total_paper_set']=($_SESSION['total_paper_set']-$diff);
			
		}
		else
		{
			$_SESSION['total_paper_set']=($_SESSION['total_paper_set']-$diff);
			
		}
		$_SESSION[$level.'-'.$mark]=$required;
	}
		//$display_mark_set=$_SESSION['total_paper_set'];
	
	if($_SESSION['custom_total_marks']!='')
	{
		$display_mark_set = $_SESSION['total_paper_set']+$_SESSION['custom_total_marks'];
	}
	else
	{
		$display_mark_set=$_SESSION['total_paper_set'];
	}	
	
	$objResponse->addAssign('total_mark','innerHTML',$display_mark_set);
	return $objResponse;
}

// Show all questions belongs to particular stream and grade for custom
function getcustomquestion($stream_id, $subject_id='', $precourse_subject_id='', $equipment_id = '',$qtyp='')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert($stream_id);
	//$objResponse->addAlert($qtyp);
	//return $objResponse;
	if(isset($_SESSION['total_paper_set']))
	{
		$objResponse->addAssign('total_mark','innerHTML',0);
		unset($_SESSION['total_paper_set']);
	}
	
		if($subject_id!='')
		{
			$common_query_cond = " (FIND_IN_SET($subject_id, subject_id) ";
			$subject_info = $DB->SelectRecord('subject',"id=$subject_id");
			
			if($subject_info->stream_id!='' && $subject_info->stream_id!=0)
			{
				$common_query_cond.= "or FIND_IN_SET($subject_info->stream_id, stream_id) ";
			}
			$common_query_cond.=") ";
			
			$html_onclick_function = 'showSubjectCustomTotal(this)';
		}
		elseif($stream_id!='' && $stream_id!=0)
		{
			//$common_query_cond = "stream_id = '".$stream_id."'";
			$common_query_cond = " FIND_IN_SET($stream_id, stream_id) ";
			$html_onclick_function = 'showcustomtotal(this)';
		}
		else
		{
			//$common_query_cond = "stream_id = '".$stream_id."'";
			$common_query_cond = " FIND_IN_SET($precourse_subject_id, subject_id) ";
			$html_onclick_function = 'showcustomtotal(this)';
		}
	
	$join = '';
	if($equipment_id != '' && $equipment_id != 0) {
		$join = ' JOIN question_equipment qe ON qe.question_id = question.id ';
		$common_query_cond .= " AND qe.equipment_id = '$equipment_id' ";
	}
	
	$output='';
	//$question_sql ="select * from question where stream_id = '".$stream_id."' and grade_id ='".$grade_id."'";
	if($qtyp=='Y'){
	$question_sql ="select DISTINCT question.* from question $join where ".$common_query_cond." and question_type='S'";
	}
	else
	{
	$question_sql ="select DISTINCT question.* from question $join where ".$common_query_cond;
	}
	//$objResponse->addAlert($question_sql);
	
	$question_sqlresult=mysql_query($question_sql);
	
	$output.='<div style="max-height:300px;overflow:auto;"><table class="border" align="center"><tr><td class="border" style="color:#AF5403">Select</td><td class="border" style="color:#AF5403">Question</td><td class="border" style="color:#AF5403">Marks</td><td class="border" style="color:#AF5403;text-align:center" >Level</td></tr>';
	$sub_output = '';
	
	//$objResponse->addAlert($_SESSION['custom_question_id_set']);
	//echo count($question_sqlresult);exit;

	while($get_quest=mysql_fetch_array($question_sqlresult))
	{
		$marks=$get_quest['marks'];
		$question=utf8_encode(html_entity_decode(stripslashes($get_quest['question_title'])));
		//$question=$get_quest['question_title'];
		$qid=$get_quest['id'];
		$level=$get_quest['question_level'];
		if($level=='B')
		{
			$level="Beginner";
			$que_level = "beg";    //=====this variable is for session which is only used for count selected question
		//================ to get total question of beginner level=====================================================
			if(isset($_SESSION['total']['beg_'.$marks.'_totalQuestion']) && $_SESSION['total']['beg_'.$marks.'_totalQuestion']>0)
				$_SESSION['total']['beg_'.$marks.'_totalQuestion']++;
			else
				$_SESSION['total']['beg_'.$marks.'_totalQuestion']=1;
		}
		if($level=='I')
		{
			$level="Intermediate";
			$que_level = "int";  
		//================ to get total question of Intermediate level=====================================================
			if(isset($_SESSION['total']['int_'.$marks.'_totalQuestion']) && $_SESSION['total']['int_'.$marks.'_totalQuestion']>0)
				$_SESSION['total']['int_'.$marks.'_totalQuestion']++;
			else
				$_SESSION['total']['int_'.$marks.'_totalQuestion']=1;
				
			//$objResponse->addAlert($marks.', '.$_SESSION['total']['int_'.$marks.'_totalQuestion']);
		}
		if($level=='H')
		{
			$level="Higher";
			$que_level = "high";  
		//================ to get total question of Higher level=====================================================
			if(isset($_SESSION['total']['high_'.$marks.'_totalQuestion']) && $_SESSION['total']['high_'.$marks.'_totalQuestion']>0)
				$_SESSION['total']['high_'.$marks.'_totalQuestion']++;
			else
				$_SESSION['total']['high_'.$marks.'_totalQuestion']=1;
		}
		$checked='';
		if (isset($_SESSION['custom_question_id_set'])) 
		{
			$custom_que_id_array = explode(',',$_SESSION['custom_question_id_set']);
			if (in_array($qid,$custom_que_id_array))
			{
				$checked='checked';
				$_SESSION['total'][$que_level."_".$marks.'_question']++;
				$_SESSION['total'][$qid]=1;
			}
		}
		//$objResponse->addAlert($_SESSION['total']["int_2_question"]);
		//$sub_output.='<tr><td class="border" style="text-align:center"><input type="checkbox" name="question_'.$marks.'_'.$qid.'" value="'.$qid.'_'.$marks.'" onclick="showcustomtotal(this.value)"></td><td class="border">'.$question.'</td><td class="border" style="text-align:center">'.$marks.'</td><td class="border" style="text-align:center">'.$level.'</td></tr>';
		$sub_output.='<tr><td class="border" style="text-align:center"><input type="checkbox" '.$checked.' name="custom_question[]" id="custom_question" value="'.$qid.'_'.$marks.'_'.$level.'" onclick="'.$html_onclick_function.';"></td><td class="border">'.$question.'</td><td class="border" style="text-align:center">'.$marks.'</td><td class="border" style="text-align:center">'.$level.'</td></tr>';
	}
	
	if($sub_output == '')
	{
		$sub_output = "<tr><td colspan=4>No Question available.</td></tr>";
	}
	$output.= $sub_output . '</table></div>';
	//echo $output;exit;
	//print_r($_SESSION['total']);exit;
	//$objResponse->addAlert($output);
	if($subject_id!='')
	{
		$objResponse->addAssign('subject_showques','innerHTML',$output);
	}
	elseif($stream_id!='' && $qtyp!='')
	{
		$objResponse->addAssign('subject_showques','innerHTML',$output);
	}
	else
	{
		$objResponse->addAssign('showques','innerHTML',$output);
	}
	
	//$objResponse->addAssign('message','innerHTML','');
	//$objResponse->addAssign('beg_level','innerHTML','');
	//$objResponse->addAssign('int_level','innerHTML','');
	//$objResponse->addAssign('high_level','innerHTML','');
	
	return $objResponse;
}

function getcustomquestionedit($stream_id, $testid, $precourse_subject_id='', $equipment_id = '')
{
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert($stream_id);
	//$objResponse->addAlert($grade_id);
	$output='';
	$total=0;
	if($stream_id!='' && $stream_id!=0)
	{
		$common_query_cond = " FIND_IN_SET($stream_id, stream_id) ";
	}
	elseif($precourse_subject_id!='')
	{
		$common_query_cond = " FIND_IN_SET($precourse_subject_id, subject_id) ";
	}
	
	$join = '';
	if($equipment_id != '' && $equipment_id != 0) {
		$join = ' JOIN question_equipment qe ON qe.question_id = question.id ';
		$common_query_cond .= " AND qe.equipment_id = '$equipment_id' ";
	}
	
	//$question_sql ="select * from question where stream_id = '".$stream_id."' and grade_id ='".$grade_id."' ";
	if($qtyp=='N')
	{
	$question_sql ="select DISTINCT question.* from question $join where ".$common_query_cond;
	}
	else{
	$question_sql ="select DISTINCT question.* from question $join where ".$common_query_cond." and question_type = 'S'";}
	$question_sqlresult=mysql_query($question_sql);
	
	$output.='<div style="width:500px;height:300px;overflow:scroll;"><table class="border" align="center"><tr><td class="border" style="color:#AF5403">Select</td><td class="border" style="color:#AF5403">Question</td><td class="border" style="color:#AF5403">Marks</td><td class="border" style="color:#AF5403;text-align:center" >Level</td></tr>';
	//$objResponse->addAlert($testid);
	while($get_quest=mysql_fetch_array($question_sqlresult))
	{
		$marks=$get_quest['marks'];
		$question=utf8_encode(html_entity_decode(stripslashes($get_quest['question_title'])));
		$qid=$get_quest['id'];
		$level=$get_quest['question_level'];
		if($level=='B')
		{
			$level="Beginner";
			$sess_level_name = "beg";
		//================ to get total question of beginner level=====================================================
			if(isset($_SESSION['total']['beg_'.$marks.'_totalQuestion']) && $_SESSION['total']['beg_'.$marks.'_totalQuestion']>0)
				$_SESSION['total']['beg_'.$marks.'_totalQuestion']++;
			else
				$_SESSION['total']['beg_'.$marks.'_totalQuestion']=1;
				
		}
		if($level=='I')
		{
			$level="Intermediate";
			$sess_level_name = "int";
		//================ to get total question of Intermediate level=====================================================
			if(isset($_SESSION['total']['int_'.$marks.'_totalQuestion']) && $_SESSION['total']['int_'.$marks.'_totalQuestion']>0)
				$_SESSION['total']['int_'.$marks.'_totalQuestion']++;
			else
				$_SESSION['total']['int_'.$marks.'_totalQuestion']=1;
		}
		if($level=='H')
		{
			$level="Higher";
			$sess_level_name = "high";
		//================ to get total question of Higher level=====================================================
			if(isset($_SESSION['total']['high_'.$marks.'_totalQuestion']) && $_SESSION['total']['high_'.$marks.'_totalQuestion']>0)
				$_SESSION['total']['high_'.$marks.'_totalQuestion']++;
			else
				$_SESSION['total']['high_'.$marks.'_totalQuestion']=1;
		}
		$test_sql ="select id from test_questions where question_id = '".$qid."' and test_id ='".$testid."' and question_selection='C'";
		//$objResponse->addAlert($test_sql);
		$test_sqlresult=mysql_query($test_sql);
		$res=mysql_fetch_array($test_sqlresult);
		//$objResponse->addAlert($res);
		if($res!='')
		{
			$checked='checked';
			$_SESSION['question'][$qid]=$marks;
			$total=$total+$marks;
			if (isset($_SESSION['total'][$sess_level_name."_".$marks.'_question']) && $_SESSION['total'][$sess_level_name."_".$marks.'_question']>0)
			{
				$_SESSION['total'][$sess_level_name."_".$marks.'_question']++;
				$_SESSION['total'][$qid]=1;
			}
			else
			{
				$_SESSION['total'][$sess_level_name."_".$marks.'_question']=1;
				$_SESSION['total'][$qid]=1;
			}
		}
		else
		{
			$checked='';
		}
		$output.='<tr><td class="border" style="text-align:center"><input type="checkbox" id="custom_question" name="custom_question[]" value="'.$qid.'_'.$marks.'_'.$level.'" onclick="showcustomtotal(this)" '.$checked.'></td><td class="border">'.$question.'</td><td class="border" style="text-align:center">'.$marks.'</td><td class="border" style="text-align:center">'.$level.'</td></tr>';
	}
	
	$output.='</table></div>';
	
	//$objResponse->addAlert($output);
	$objResponse->addAssign('showques','innerHTML',$output);
	/*$objResponse->addAssign('message','innerHTML','');
	$objResponse->addAssign('beg_level','innerHTML','');
	$objResponse->addAssign('int_level','innerHTML','');
	$objResponse->addAssign('high_level','innerHTML','');*/
	if($total!=0 )
	{
		//$objResponse->addAlert($_SESSION['random_total_value']);
		/*if(isset($_SESSION['total_paper_set']) && $_SESSION['total_paper_set']!='')
		{
			//$objResponse->addAlert($_SESSION['total_paper_set']);
			$prev_value = $_SESSION['total_paper_set'];
			$total = $total+$prev_value;
			unset($_SESSION['total_paper_set']);
		}
		$objResponse->addAssign('total_mark','innerHTML',$total);*/
		//$_SESSION['custom_total_marks']=$total;
		if(isset($_SESSION['random_total_value']) && $_SESSION['random_total_value']!=='')
		{
			//$objResponse->addAlert($total);
			$ramdom_value = $_SESSION['random_total_value'];
			unset($_SESSION['random_total_value']);
			$objResponse->addScriptCall('xajax_gettotalmarks','editDefault',$ramdom_value,$total);
		}
	}
	
	return $objResponse;
}
// Show custome mark calculation
function customMarks($value)
{
	$objResponse = new xajaxResponse();
	$marks_id=explode('_',$value);
	$qid=$marks_id[0];
	$marks=$marks_id[1];
	$total=0;
	$output='';
	if(isset($_SESSION['question'][$qid]))
	{
		$_SESSION['custom_total_marks']=$_SESSION['custom_total_marks']-$marks;
		unset($_SESSION['question'][$qid]);
		
	}
	else
	{
		$_SESSION['custom_total_marks']=$_SESSION['custom_total_marks']+$marks;
		$_SESSION['question'][$qid]=$marks;
	}
	//$total=$_SESSION['custom_total_marks'];
	
	if($_SESSION['total_paper_set']!='')
	{
		$total = $_SESSION['custom_total_marks']+$_SESSION['total_paper_set'];
	}
	else
	{
		$total=$_SESSION['custom_total_marks'];
	}
	 //$objResponse->addAlert($qid);
	//$objResponse->addAlert($marks);
	$objResponse->addAssign('total_mark','innerHTML',$total);
	return $objResponse;
	
}

function gettotalmarks($mode, $current_marks, $value, $random_mark='', $totalques='', $level='', $subject_id='', $qtyp='')
{
	$objResponse = new xajaxResponse();
	$total=0;
	//$objResponse->addAlert($subject_id);
	//$objResponse->addAlert($current_marks.','.$value.','.$level.','.$random_mark.','.$totalques);
	//return $objResponse;
	//print_r($_SESSION['total']);exit;
	if($mode=='add')
	{
		//======================condition when enter custom marks======================================
		if($level=='')
		{
			$marks_id=explode('_',$value);
			$qid=$marks_id[0];
			$marks=$marks_id[1];
			$level=$marks_id[2];
			if($level=='Beginner')
			{
				$level="beg";
				//$_SESSION[$level."_".$marks]=$_SESSION[$level."_".$random_mark];
				/*if(isset($_SESSION['total'][$level."_".$marks]) && $_SESSION['total'][$level."_".$marks]!='')
				{
					//$diff_value = $marks*$_SESSION['total_marks'][$level."_".$marks];
					unset($_SESSION['total_marks'][$level."_".$marks]);
				}*/
			//$objResponse->addAlert($level.'_'.$marks);
			}
			elseif($level=='Intermediate')
			{
				$level="int";
				/*if(isset($_SESSION['total'][$level."_".$marks]) && $_SESSION['total'][$level."_".$marks]!='')
				{
					//$diff_value = $marks*$_SESSION['total_marks'][$level."_".$marks];
					unset($_SESSION['total_marks'][$level."_".$marks]);
				}*/
			}
			elseif($level=='Higher')
			{
				$level="high";
				/*if(isset($_SESSION['total'][$level."_".$marks]) && $_SESSION['total'][$level."_".$marks]!='')
				{
					//$diff_value = $marks*$_SESSION['total_marks'][$level."_".$marks];
					unset($_SESSION['total_marks'][$level."_".$marks]);
				}*/
			}
			
			$ischecked=$marks_id[3];
			if($ischecked=='check')
			{
				//$objResponse->addAlert($_SESSION['total'][$level."_".$marks."_totalQuestion"]);
				//$objResponse->addAlert($_SESSION['total'][$level."_".$marks.'_question']);
				if($_SESSION['total'][$level."_".$marks."_totalQuestion"]!='' && $_SESSION['total'][$level."_".$marks.'_question']!='' && $_SESSION['total'][$level."_".$marks."_totalQuestion"]<=$_SESSION['total'][$level."_".$marks.'_question'])
				{
					$objResponse->addAlert("Your question limit has been completed.");
					//$objResponse->addScript('unselectElement(val)');
					$objResponse->addScriptCall('unselectElement',$marks_id[0].'_'.$marks_id[1].'_'.$marks_id[2]);
					return $objResponse;
				}
				elseif (isset($_SESSION['total'][$level."_".$marks.'_question']) && $_SESSION['total'][$level."_".$marks.'_question']>0)
				{
					$_SESSION['total'][$level."_".$marks.'_question']++;
					$_SESSION['total'][$qid]=1;
				}
				else 
				{
					$_SESSION['total'][$level."_".$marks.'_question']=1;
					$_SESSION['total'][$qid]=1;
				}
				$total = ($current_marks+$marks);
			}
			else
			{
				if(isset($_SESSION['total']['custom_total_que']) && $_SESSION['total']['custom_total_que']>0)
				{
					$_SESSION['total']['custom_total_que']--;
				}
				
				if(isset($_SESSION['total'][$qid]) && $_SESSION['total'][$qid]==1)
				{
					$_SESSION['total'][$level."_".$marks.'_question']--;
					unset($_SESSION['total'][$qid]);
				}
				$total = $current_marks-$marks;
			}
			if($subject_id)
			{
				$objResponse->addScriptCall('findSubjectNumofQues','','');
			}
			else
			{
				$objResponse->addScriptCall('findNumofQues','','');
			}
		}
		elseif($_SESSION['total'][$level."_".$random_mark]!='')
		{
			//$objResponse->addAlert($value.", ".$_SESSION[$level][$random_mark].", ".$_SESSION['total'][$level."_".$random_mark]);
			//$objResponse->addAlert($_SESSION['total'][$level."_".$random_mark.'_question']);
			$total = $current_marks-$random_mark*$_SESSION['total'][$level."_".$random_mark];
			
			
			//$objResponse->addAlert($total.", ".$value.", ".$_SESSION[$level][$random_mark]);
			if($value<=$_SESSION[$level][$random_mark])
			{
				$_SESSION['total'][$level."_".$random_mark] = $value;
				$total = $total+$random_mark*$value;
				if(isset($_SESSION['total'][$level."_".$random_mark.'_question']))
					$_SESSION['total'][$level."_".$random_mark.'_question']-=$_SESSION[$level][$random_mark]-$value;
				//$objResponse->addAlert($total);
			}
			else
			{
				//$objResponse->addAlert($total);
				if(isset($_SESSION['total'][$level."_".$random_mark.'_question']))
					$_SESSION['total'][$level."_".$random_mark.'_question']-=$_SESSION['total'][$level."_".$random_mark];
				$_SESSION['total'][$level."_".$random_mark] = '';
			}
		}
		elseif($value!='' && $level!='' && $value<=$_SESSION[$level][$random_mark])		//======================condition when enter random marks======================================
		{
			
			$total = $current_marks+$random_mark*$value;
			$_SESSION['total'][$level."_".$random_mark]=$value;
		
			if(isset($_SESSION['total'][$level."_".$random_mark.'_question']))
			{
				$_SESSION['total'][$level."_".$random_mark.'_question']+=$value;
			}
			else
			{
				$_SESSION['total'][$level."_".$random_mark.'_question']=$value;
			}
			//$objResponse->addAlert($_SESSION['total'][$level."_".$random_mark.'_question']);
		}
		else
		{
			$total = $current_marks;
		}
		
		if($subject_id!='')
		{
			$objResponse->addAssign('subject_total_mark','innerHTML',$total);
		}
		
		else
		{
		 $objResponse->addAssign('total_mark','innerHTML',$total);
		}
		 //$objResponse->addScript('document.getElementById("'.$level.'_'.$random_mark.'_'.$totalques.'").focus();');
	}
	/*elseif ($mode=='edit')
	{
		//$objResponse->addAlert($current_marks.','.$value.','.$level.','.$random_mark);
		//$objResponse->addAlert($_SESSION[$level][$random_mark]);
		if($value!='' && $level!='')
		{
			$total = $current_marks+$random_mark*$value;
			//$_SESSION[$level][$random_mark]=$value;
			$_SESSION['total'][$level."_".$random_mark]=$value;
			if(isset($_SESSION['total'][$level."_".$random_mark.'_question']))
			{
				$_SESSION['total'][$level."_".$random_mark.'_question']+=$value;
			}
			else
			{
				$_SESSION['total'][$level."_".$random_mark.'_question']=$value;
			}
		}
		elseif($_SESSION['total'][$level."_".$random_mark]!='')
		{
			$total = $current_marks-$random_mark*$_SESSION['total'][$level."_".$random_mark];
			$_SESSION['total'][$level."_".$random_mark.'_question']-=$_SESSION['total'][$level."_".$random_mark];
			unset($_SESSION['total'][$level."_".$random_mark]);
		}
		elseif(isset($_SESSION[$level][$random_mark]) && $_SESSION[$level][$random_mark]!='')
		{
			$total = $current_marks-$random_mark*$_SESSION[$level][$random_mark];
			$_SESSION['total'][$level."_".$random_mark.'_question']-=$_SESSION['total'][$level."_".$random_mark];
			unset($_SESSION['total'][$level."_".$random_mark]);
			unset($_SESSION[$level][$random_mark]);
		}
		else
		{
			$total = $current_marks;
		}
		
		 $objResponse->addAssign('total_mark','innerHTML',$total);
		 //$objResponse->addScript('document.getElementById("'.$level.'_'.$random_mark.'_'.$totalques.'").focus();');
	}*/
	elseif ($mode=='editDefault')
	{
		//$objResponse->addAlert($value);
		if($value!='')
		{
			$total_marks = $current_marks+$value;
		}
		else
		{
			$total_marks = $current_marks;
		}
		
		if($subject_id!='')
		{
			$objResponse->addAssign('subject_total_mark','innerHTML',$total_marks);
		}
		else
		{
			$objResponse->addAssign('total_mark','innerHTML',$total_marks);
		}
	}
	elseif ($mode=='random_reset')
	{
		if(isset($_SESSION['total']))
		{
			unset($_SESSION['total']);
		}
		
		$value_arr=explode(',',$value);
		for($counter=0; $counter<count($value_arr); $counter++)
		{
			$marks_id=explode('_',$value_arr[$counter]);
			$qid=$marks_id[0];
			$marks=$marks_id[1];
			$level=$marks_id[2];
			$ischecked=$marks_id[3];
			if($level=='Beginner')
			{
				$level="beg";
				if(isset($_SESSION['total']['beg_'.$marks.'_totalQuestion']) && $_SESSION['total']['beg_'.$marks.'_totalQuestion']>0)
					$_SESSION['total']['beg_'.$marks.'_totalQuestion']++;
				else
					$_SESSION['total']['beg_'.$marks.'_totalQuestion']=1;
				
			}
			elseif($level=='Intermediate')
			{
				$level="int";
				if(isset($_SESSION['total']['int_'.$marks.'_totalQuestion']) && $_SESSION['total']['int_'.$marks.'_totalQuestion']>0)
					$_SESSION['total']['int_'.$marks.'_totalQuestion']++;
				else
					$_SESSION['total']['int_'.$marks.'_totalQuestion']=1;
				
			}
			elseif($level=='Higher')
			{
				$level="high";
				if(isset($_SESSION['total']['high_'.$marks.'_totalQuestion']) && $_SESSION['total']['high_'.$marks.'_totalQuestion']>0)
					$_SESSION['total']['high_'.$marks.'_totalQuestion']++;
				else
					$_SESSION['total']['high_'.$marks.'_totalQuestion']=1;
				
			}
			
			if($ischecked=='check')
			{
				if (isset($_SESSION['total'][$level."_".$marks.'_question']) && $_SESSION['total'][$level."_".$marks.'_question']>0)
				{
					$_SESSION['total'][$level."_".$marks.'_question']++;
					$_SESSION['total'][$qid]=1;
				}
				else 
				{
					$_SESSION['total'][$level."_".$marks.'_question']=1;
					$_SESSION['total'][$qid]=1;
				}
				$total = $total+$marks;
			}
		}
		
		if($subject_id!='')
		{
			$objResponse->addAssign('subject_total_mark','innerHTML',$total);
		}
		else
		{
		 $objResponse->addAssign('total_mark','innerHTML',$total);
		}
	}
	elseif ($mode=='custom_reset')
	{
		
		if(isset($_SESSION['beg']))
		{
			foreach($_SESSION['beg'] as $key=>$val)
			{
				if(isset($_SESSION['total']['beg_'.$key.'_totalQuestion']))
				{
					unset($_SESSION['total']['beg_'.$key.'_totalQuestion']);
				}
				
				if(isset($_SESSION['total']['beg_'.$key.'_question']))
				{
					unset($_SESSION['total']['beg_'.$key.'_question']);
					$_SESSION['total']["beg_".$key.'_question']=$_SESSION['total']["beg_".$key];
				}
				
				$total+= $_SESSION['total']["beg_".$key]*$key;
			}
		}
		if(isset($_SESSION['int']))
		{
			foreach($_SESSION['int'] as $key=>$val)
			{
				if(isset($_SESSION['total']['int_'.$key.'_totalQuestion']))
				{
					unset($_SESSION['total']['int_'.$key.'_totalQuestion']);
				}
				
				if(isset($_SESSION['total']['int_'.$key.'_question']))
				{
					unset($_SESSION['total']['int_'.$key.'_question']);
					$_SESSION['total']["int_".$key.'_question']=$_SESSION['total']["int_".$key];
				}
				
				$total+= $_SESSION['total']["int_".$key]*$key;
			}
		}
		
		if(isset($_SESSION['high']))
		{
			foreach($_SESSION['high'] as $key=>$val)
			{
				if(isset($_SESSION['total']['high_'.$key.'_totalQuestion']))
				{
					unset($_SESSION['total']['high_'.$key.'_totalQuestion']);
				}
				
				if(isset($_SESSION['total']['high_'.$key.'_question']))
				{
					unset($_SESSION['total']['high_'.$key.'_question']);
					$_SESSION['total']["high_".$key.'_question']=$_SESSION['total']["high_".$key];
				}
				
				$total+= $_SESSION['total']["high_".$key]*$key;
			}
		}
		
		if($subject_id!='')
		{
			$objResponse->addAssign('subject_total_mark','innerHTML',$total);
			unset($_SESSION['custom_question_id_set']);
		}
		else
		{
			$objResponse->addAssign('total_mark','innerHTML',$total);
			unset($_SESSION['custom_question_id_set']);
		}
	}
	 return $objResponse;
}

function deletePermission($id, $module)
{

	global $DB;
	$objResponse = new xajaxResponse();
	$msg="";
	$msgno=0;
	//$objResponse->addAlert($id.", ".$module);
	if($module=='subject_master')
	{
		$module_entity="subject";
		$subject_history = $DB->SelectRecords('candidate_test_subject_history','subject_id='.$id);
		//print_r($subject_history);exit;
		
		if($subject_history)
		{
			$count_subject_history = count($subject_history);
			if($count_subject_history>1)
				$msg = "Permission denied. ".$count_subject_history." tests have been conducted";
			elseif($count_subject_history==1)
				$msg = "Permission denied. ".$count_subject_history." test has been conducted";
			
			$msgno++;
		}

		$future_exam= $DB->SelectRecords('exam_subjects','subject_id='.$id, 'distinct exam_id');
		if($future_exam)
		{
			$count_future_exam = count($future_exam);
			
			if($count_future_exam>1)
				$backstring = " exams are attached";
			elseif ($count_future_exam==1)
				$backstring = " exam is attached";
			
			if($msgno>0)
			{
				$msg.=" \nand ".$count_future_exam.$backstring;
			}
			else 
			{
				$msg="Permission denied. ".$count_future_exam.$backstring;
			}
		}
		
		if($msg!='')
		{
			$msg.=" with this subject.";
		}
	}
	elseif ($module=='examination')
	{
		$module_entity="exam";
		$test_history = $DB->SelectRecords('test','exam_id='.$id);
		if($test_history)
		{
			$count_test_history = count($test_history);
			if($count_test_history>1)
				$msg = "Permission denied. ".$count_test_history." tests have been scheduled with this exam.";
			elseif($count_test_history==1)
				$msg = "Permission denied. ".$count_test_history." test has been scheduled with this exam.";
			
			$msgno++;
		}
	}
	elseif ($module=='test_master')
	{
		$module_entity="test";
		$test_history = $DB->SelectRecord('test','id='.$id);
		if($test_history)
		{
			$test_time = strtotime($test_history->test_date);
			$current_time = strtotime('now');
			//$objResponse->addAlert($test_time.", ".$current_time);
			
			if($test_time<=$current_time)
			{
				$msg = "Permission denied. Test is already conducted.";
			}
			$msgno++;
		}
	}
	elseif ($module=='stream_master')
	{
		$module_entity="trade";
		$exam_history = $DB->SelectRecords('exam_filter','stream_id='.$id);
		if($exam_history)
		{
			//$test_time = strtotime($test_history->test_date);
			//$current_time = strtotime('now');
			//$objResponse->addAlert($test_time.", ".$current_time);
			
			/*if($test_time<=$current_time)
			{
				$msg = "Permission denied. Related test is already conducted.";
			}*/
			$count_exam_history = count($exam_history);
			if($count_exam_history>1)
				$msg = "Permission denied. ".$count_exam_history." exams";
			elseif($count_exam_history==1)
				$msg = "Permission denied. ".$count_exam_history." exam";
			
			$msgno++;
		}
		
		$candidate_info = $DB->SelectRecords('candidate','stream_id='.$id);
		if($candidate_info)
		{
			$count_candidate_info = count($candidate_info);
			
			if($count_candidate_info>1)
				$backstring = " candidates";
			elseif ($count_candidate_info==1)
				$backstring = " candidate";
			
			if($msgno>0)
			{
				$msg.=" and ".$count_candidate_info.$backstring;
			}
			else 
			{
				$msg="Permission denied. ".$count_candidate_info.$backstring;
			}
		}
		
		if($msg!='')
		{
			if($count_exam_history>1 || $count_candidate_info>1)
			{
				$msg.=" are attached with this trade.";
			}
			elseif($count_exam_history==1 || $count_candidate_info==1)
			{
				$msg.=" is attached with this trade.";
			}
		}
	}
	elseif ($module=='grade_master')
	{
		$module_entity="grade";
		$test_history = $DB->SelectRecord('test','grade_id='.$id);
		if($test_history)
		{
			//$test_time = strtotime($test_history->test_date);
			//$current_time = strtotime('now');
			//$objResponse->addAlert($test_time.", ".$current_time);
			
			/*if($test_time<=$current_time)
			{
				$msg = "Permission denied. Related test is already conducted.";
			}*/
			
			$count_test_history = count($test_history);
				if($count_test_history>1)
					$msg = "Permission denied. ".$count_test_history." tests";
				elseif($count_test_history==1)
					$msg = "Permission denied. ".$count_test_history." test";
				
				$msgno++;
			}
			
			$candidate_info = $DB->SelectRecords('candidate','grade_id='.$id);
			if($candidate_info)
			{
				$count_candidate_info = count($candidate_info);
				
				if($count_candidate_info>1)
					$backstring = " candidates";
				elseif ($count_candidate_info==1)
					$backstring = " candidate";
				
				if($msgno>0)
				{
					$msg.=" and ".$count_candidate_info.$backstring;
				}
				else 
				{
					$msg="Permission denied. ".$count_candidate_info.$backstring;
				}
			}
			
			if($msg!='')
			{
				if($count_test_history>1 || $count_candidate_info>1)
				{
					$msg.=" are attached with this grade.";
				}
				elseif($count_test_history==1 || $count_candidate_info==1)
				{
					$msg.=" is attached with this grade.";
				}
			}
			
	}
	elseif ($module=='rank_master')
	{
		$module_entity="rank";
		$candidate_info = $DB->SelectRecords('candidate','rank_id='.$id);
		if($candidate_info)
		{
			$count = count($candidate_info);
			if($count==1)
			{
				$msg = "Permission denied. ".$count." candidate is related with this rank.";
			}
			else
			{
				$msg = "Permission denied. ".$count." candidates are related with this rank.";
			}	
			$msgno++;
		}
	}
	elseif ($module=='question_master')
	{
		$module_entity="question";
		$candidate_question_history = $DB->SelectRecords('candidate_question_history','question_id='.$id, '*', 'group by test_id');
		if($candidate_question_history)
		{
			$count = count($candidate_question_history);
			if($count==1)
			{
				$msg = "Permission denied. ".$count." test is conducted with this question.";
			}
			else
			{
				$msg = "Permission denied. ".$count." tests are conducted with this question.";
			}	
			$msgno++;
		}
	}
	elseif ($module=='question_masters')
	{
		$module_entity="question";
		$candidate_question_history = $DB->SelectRecords('candidate_question_history','question_id='.$id, '*', 'group by test_id');
		if($candidate_question_history)
		{
			$count = count($candidate_question_history);
			if($count==1)
			{
				$msg = "Permission denied. ".$count." test is conducted with this question.";
			}
			else
			{
				$msg = "Permission denied. ".$count." tests are conducted with this question.";
			}	
			$msgno++;
		}
	}
	else if ($module == 'equipment')
	{
		$module_entity="equipment";
		$subequipment = $DB->SelectRecord('sub_equipment','equipment_id='.$id);
		$questionAttached=$DB->SelectRecord('question_equipment','equipment_id='.$id);
		$candidateAttached=$DB->SelectRecord('candidate','equipment_id='.$id);
		if($subequipment)
		{	
			$msg = "Permission denied. This equipment is attached with one or more sub-trade.";			
			$msgno++;
		}
		else if ($questionAttached)
		{
			$msg = "Permission denied. This equipment is attached with one or more questions.";		
		}
		else if ($candidateAttached)
		{
			$msg = "Permission denied. This equipment is attached with one or more candidates.";		
		}
	}
	else if ($module == 'subequipment')
	{
		$questionAttached=$DB->SelectRecord('question_equipment','sub_equipment_id='.$id);
		$candidateAttached=$DB->SelectRecord('candidate','sub_equipment_id='.$id);
		if ($questionAttached)
		{
			$msg = "Permission denied. This sub-trade is attached with one or more questions.";		
		}
		else if ($candidateAttached)
		{
			$msg = "Permission denied. This sub-trade is attached with one or more candidates.";		
		}
	}
	else if ($module == 'unit')
	{
		$module_entity="unit";
		$subunit = $DB->SelectRecord('subunit','unit_id='.$id);
		$candidateAttached=$DB->SelectRecord('candidate','unit_id='.$id);
		if ($subunit)
		{
			$msg = "Permission denied. This unit is attached with one or more sub-unit.";		
		}
		else if ($candidateAttached)
		{
			$msg = "Permission denied. This unit is attached with one or more candidates.";		
		}
	}
	else if ($module == 'subunit')
	{
		$candidateAttached=$DB->SelectRecord('candidate','subunit_id='.$id);
		if ($candidateAttached)
		{
			$msg = "Permission denied. This sub-unit is attached with one or more candidates.";		
		}
	}
	else if ($module == 'arm_master')
	{
		$module_entity="arm";
		$command = $DB->SelectRecord('command','arm_id='.$id);
		if ($command)
		{
			$msg = "Permission denied. This arm is attached with one or more command.";		
		}
	}
	else if ($module == 'command_master')
	{
		$module_entity="command";
		$division = $DB->SelectRecord('division','command_id='.$id);
		if ($division)
		{
			$msg = "Permission denied. This command is attached with one or more division.";		
		}
	}
	else if ($module == 'division_master')
	{
		$module_entity="division";
		$brigade = $DB->SelectRecord('brigade','division_id='.$id);
		if ($brigade)
		{
			$msg = "Permission denied. This division is attached with one or more brigade.";		
		}
	}
	else if ($module == 'brigade_master')
	{
		$module_entity="brigade";
		$unit = $DB->SelectRecord('unit','brigade_id='.$id);
		if ($unit)
		{
			$msg = "Permission denied. This brigade is attached with one or more unit.";		
		}
	}
	if($msg=='')
	{
		$objResponse->addScriptCall('deleterecord',$id, ROOTURL."/index.php?mod=".$module."&do=delete&nameID=".$id,"Are you sure you want to delete this ".$module_entity."?");
	}
	else 
	{
		$objResponse->addAlert($msg);
	}
	return $objResponse;
}

function getExamTypeIdByExamId($exam_id,$subject_selected_value='',$module='',$candidate_id='', $mode='', $is_clear_session='yes')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert($exam_id);
	//return $objResponse;
	if($exam_id!='')
	{
		$exam_max_marks = $DB->SelectRecords('exam_subjects', 'exam_id='.$exam_id, 'sum(subject_max_mark) as exam_total_marks', 'group by exam_id');
		$objResponse->addAssign('exam_max_marks', 'innerHTML', $exam_max_marks[0]->exam_total_marks);
		if(isset($_SESSION['total_paper_set']))
		{
			unset($_SESSION['total_paper_set']);
		}
		//echo $is_clear_session;exit;
		if($is_clear_session=='yes')
		{
			//$objResponse->addAlert($is_clear_session);
			if(isset($_SESSION['beg']))
			{
				unset($_SESSION['beg']);
			}
			if(isset($_SESSION['int']))
			{
				unset($_SESSION['int']);
			}
			if(isset($_SESSION['high']))
			{
				unset($_SESSION['high']);
			}
			if(isset($_SESSION['total']))
			{
				unset($_SESSION['total']);
			}
			$_SESSION['question_type_count'] = array();
		}
		
		if($mode=='edit')
		{
			$exam_type = $DB->SelectRecord('examination','id='.$exam_id);
			//print_r($exam_type);exit;
			
			if($exam_type->exam_type_id==2)
			{
				$isDetailShow='no';
			}
			else
			{
				$isDetailShow='yes';
				/*
				$exam_filter = $DB->SelectRecords('exam_filter','exam_id='.$exam_id);
				$stream_set = '';
				
				//print_r($exam_filter);exit;
				 if($exam_filter)
				 {
					 for($counter = 0; $counter<count($exam_filter); $counter++)
					 {
					 	$stream_set.= $exam_filter[$counter]->stream_id.",";
					 }
					 $stream_set = substr($stream_set,0,strlen($stream_set)-1);	
				 }
				 */
				$stream_set = $exam_type->stream_id;
				$objResponse->addAssign('stream_id', 'value', $stream_set);
				$objResponse->addAssign('precourse_subject_id', 'value', $exam_type->subject_id);
				$objResponse->addAssign('equipment_id', 'value', $exam_type->equipment_id);
				
				//$objResponse->addAlert("server");
				$objResponse->addScriptCall('findNumofQues', $stream_set);
				$objResponse->addScriptCall('ShowCusQues', $stream_set);		
			}
				
			
			//$objResponse->addAlert($exam_id);
			$objResponse->addScriptCall('showGeneralExamDetail',$isDetailShow);
			//if($isLevelShow=='no')	
			$objResponse->addScriptCall('xajax_getExamSubject', $exam_id, $subject_selected_value, $module);
			$objResponse->addScriptCall('xajax_getExamPaper', $exam_id);
		}
		else                  //========if mode is "add" or anything else===============
		{
			if($candidate_id!='')
				 $candidate_test_subject_history = getCandidateTestSubjectHistory($exam_id, $candidate_id);
			//print_r($candidate_test_subject_history);exit;
			if($candidate_test_subject_history)
			{
				$objResponse->addAlert("This candidate's exam's information is already added.");
				$objResponse->addAssign('exam_id', 'value', '');
			}
			else
			{
				$exam_type = $DB->SelectRecord('examination','id='.$exam_id);
				
				if($exam_type->exam_type_id==2)
				{
					$isDetailShow='no';
				}
				else
				{
					$isDetailShow='yes';
					/*
					$exam_filter = $DB->SelectRecords('exam_filter','exam_id='.$exam_id);
					$stream_set = '';
					//print_r($exam_filter);exit;
					 if($exam_filter)
					 {
						 for($counter = 0; $counter<count($exam_filter); $counter++)
						 {
						 	$stream_set.= $exam_filter[$counter]->stream_id.",";
						 }
						 $stream_set = substr($stream_set,0,strlen($stream_set)-1);	
					 }
					 */
					
					$stream_set = $exam_type->stream_id;
					$objResponse->addAssign('stream_id', 'value', $stream_set);	
					$objResponse->addAssign('precourse_subject_id', 'value', $exam_type->subject_id);
					$objResponse->addAssign('equipment_id', 'value', $exam_type->equipment_id);
					//$objResponse->addAlert("server");
					$objResponse->addScriptCall('findNumofQues', $stream_set);
					$objResponse->addScriptCall('ShowCusQues', $stream_set);	
					//$objResponse->addAssign('stream_id', 'value', $exam_type->stream_id);				
				}
					
				
				//$objResponse->addAlert($isDetailShow);
				$objResponse->addScriptCall('showGeneralExamDetail',$isDetailShow);
				//if($isLevelShow=='no')	
				$objResponse->addScriptCall('xajax_getExamSubject', $exam_id, $subject_selected_value, $module);
				$objResponse->addScriptCall('xajax_getExamPaper', $exam_id);
			}
		}
		
	}
	else
	{
		$objResponse->addScriptCall('showGeneralExamDetail','');
	}
		
	return $objResponse;
}

function getExamSubject($exam_id, $subject_selected_value='', $module='', $row_no='')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//print_r($frmdata);//exit;
	//$objResponse->addAlert($exam_id);
	if($exam_id!='')
	{
		$subject = getSubjectInfoByExamId($exam_id);
		//print_r($subject);exit;
		$output='';
		$clearAll = false;
		if($subject)
		{
			if($module=='candidate_master')
			{
				if($row_no=='')
				{
					$output.='<select id="subject_id" name="subject_id" class="rounded" onchange="candidate_mark_show(this);"><option value="">Select Subject</option>';
				}
				else 
				{
					$output.='<select id="subject_id_'.$row_no.'" name="subject_id_'.$row_no.'" class="rounded" onchange="candidate_mark_show(this);">';
				}
				
			}
			else
			{
				$output.='<select id="select_subject_id" name="select_subject_id" class="rounded" onchange="subjectquestionform(this);"><option value="">Select Subject</option>';
			}
			//$output.='';
			for($counter=0; $counter<count($subject); $counter++)
			{
				//$objResponse->addAlert($subject[$counter]->id.", ".$subject_selected_value);
				if($subject[$counter]->subject_id == -1) {
					$clearAll = true;
				}
				$selected='';
				if ($subject[$counter]->subject_id == $subject_selected_value)
				{
					$selected='selected';
				}
				$output.='<option value="'.$subject[$counter]->subject_id.'" '.$selected.' >'.stripslashes($subject[$counter]->subject_name).'</option>';
			}
			$output.='</select>';
		
			if($clearAll) {
				$output = '';
			}
		
		if($row_no=='')
		{
			$objResponse->addAssign('select_subject_td','innerHTML',$output);
		}
		else
		{
			$objResponse->addAssign('subject_nameTd_'.$row_no,'innerHTML',$output);
		}
		}
		
		//$objResponse->addAlert($output);
	}
	
	return $objResponse;
}

function getExamSubjectMulti($exam_id, $subject_selected_value='', $module='', $row_no='')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//print_r($frmdata);//exit;
	//$objResponse->addAlert($exam_id);
	if($exam_id!='')
	{
		$subject = getSubjectInfoByExamId($exam_id);
		//print_r($subject);exit;
		$output='';
		$clearAll = false;
		if($subject)
		{
			if($module=='candidate_master')
			{
				if($row_no=='')
				{
					$output.='<select id="subject_id" name="subject_id" class="rounded" onchange="candidate_mark_show(this);"><option value="">Select Subject</option>';
				}
				else 
				{
					$output.='<select id="subject_id_'.$row_no.'" name="subject_id_'.$row_no.'" class="rounded" onchange="candidate_mark_show(this);">';
				}
				
			}
			else
			{
				$output.='<select id="select_subject_id[]" name="select_subject_id[]" class="rounded" multiple onchange="subjectquestionform(this);"><option value="">Select Subject</option>';
			}
			//$output.='';
			for($counter=0; $counter<count($subject); $counter++)
			{
				//$objResponse->addAlert($subject[$counter]->id.", ".$subject_selected_value);
				if($subject[$counter]->subject_id == -1) {
					$clearAll = true;
				}
				$selected='';
				if ($subject[$counter]->subject_id == $subject_selected_value)
				{
					$selected='selected';
				}
				$output.='<option value="'.$subject[$counter]->subject_id.'" '.$selected.' >'.stripslashes($subject[$counter]->subject_name).'</option>';
			}
			$output.='</select>';
		
			if($clearAll) {
				$output = '';
			}
			//$objResponse->addAlert($output);
		if($row_no=='')
		{
			$objResponse->addAssign('select_subject_td','innerHTML',$output);
		}
		else
		{
			$objResponse->addAssign('subject_nameTd_'.$row_no,'innerHTML',$output);
		}
		}
		
		//$objResponse->addAlert($output);
	}
	
	return $objResponse;
}
function changeMaxMarksByPaper($pid)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	$marks = 0;
	
	if(!in_array($pid, $_SESSION['paperIdForCurrentExam']))
	{
		$message = "This paper is not identical with selected exam.";
		$message .= "\n\nA paper is identical with an exam only if both paper and exam have same subject with equal maximum marks.";
		$objResponse->addAlert($message);
		$objResponse->addScript('$("#paper_id").val(" ");');
	}
	
	if($pid != '')
	{
		$paper = $DB->SelectRecord('paper',"id = '$pid'");
		$marks = $paper->maximum_marks;
	}
	
	$objResponse->addAssign('total_mark','innerHTML',$marks);
	
	return $objResponse;
}

function getExamPaper($exam_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	if($exam_id!='')
	{
		$subject = getSubjectInfoByExamId($exam_id);
		
		$sub_ids = array();
		if($subject)
		{
			for($counter=0; $counter<count($subject); $counter++)
			{
				$sub_ids[] = $subject[$counter]->subject_id;
			}
			
			$query = "SELECT DISTINCT paper.id
						FROM paper
						JOIN paper_subject AS ps ON paper.id = ps.paper_id
						JOIN exam_subjects AS es ON 
								( 
									( es.subject_id = ps.subject_id ) AND 
									( es.subject_max_mark = ps.subject_total_marks ) 
								) WHERE es.exam_id = '$exam_id'";
			
			$result = $DB->RunSelectQuery($query);
			
			$_SESSION['paperIdForCurrentExam'] = array();
			if($result != '')
			{
				foreach($result as $r)
				{
					$paper_id = $r->id;
					$query = "SELECT COUNT(*) as sub_count FROM paper_subject WHERE paper_id = '$paper_id' GROUP BY paper_id; ";

					$sub_count = $DB->RunSelectQuery($query);
					$sub_count = $sub_count[0]->sub_count;
					
					if($sub_count == count($sub_ids))
					{
						$_SESSION['paperIdForCurrentExam'][] = $paper_id;
					}
				}
			}
			

		}
	}
	
	$query = "SELECT DISTINCT paper.*, 
				GROUP_CONCAT(DISTINCT CONCAT_WS(':', subject_name, CONCAT(subject_total_marks, 'MM')) ORDER BY subject_name ASC SEPARATOR ', ') as subjects
					FROM paper
					JOIN paper_subject AS ps ON paper.id = ps.paper_id
					JOIN subject ON subject.id = ps.subject_id
					GROUP BY paper_id";
			
	$result = $DB->RunSelectQuery($query);
	
	$output.='<select id="paper_id" name="paper_id" class="rounded" onchange="xajax_changeMaxMarksByPaper(this.value);"><option value="">Select Paper</option>';
	if($result != '')
	{
		foreach($result as $r)
		{
			$paper_id = $r->id;
			$paper_title = $r->paper_title;
			$subjects = $r->subjects;
			
			$selected = '';
			if($_SESSION['selected_paper_id'] == $paper_id)
			{
				$selected = 'selected';
				unset($_SESSION['selected_paper_id']);
			}
				
			$output .= "<option value = '$paper_id' $selected>$paper_title ($subjects)</option>";
		}
	}
	
	$output.='</select>';
	$objResponse->addAssign('select_paper_td','innerHTML',$output);
	
	return $objResponse;
}

function getSubjectQuestion($subject_id, $custom_que_id_set, $random_que_set, $subject_total_mark, $tableRowCount, $mode, $subject_diff_question='N')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAssign("test","innerHTML",$subject_id);
	$totalQuestion = 0;
	$random_que_set_array=explode(',',$random_que_set);
	//print_r($random_que_set_array);exit;
	$random_input_array = array();
	for($counter=0; $counter<count($random_que_set_array); $counter++)
	{
		$random_que_set_value_array=explode('_',$random_que_set_array[$counter]);
		$random_que_level = $random_que_set_value_array[0];
		$random_que_marks = $random_que_set_value_array[1];
		$random_que_available = $random_que_set_value_array[2];
		$random_que_input_value = $random_que_set_value_array[3];
		$random_input_array[$random_que_level."_".$random_que_marks."_".$random_que_available] = $random_que_input_value;
	}
	if($custom_que_id_set!='')
	{
		$cust_que_set_condition = " and id not in ($custom_que_id_set)";
		$custom_que_id_array = explode(',',$custom_que_id_set);
		$totalQuestion = count($custom_que_id_array);
	}
	
	//if($subject_id!='')
	{
		$common_query_cond = " (FIND_IN_SET($subject_id, subject_id) ";
		$subject_info = $DB->SelectRecord('subject',"id=$subject_id");
		
		$subject_name = $subject_info->subject_name;
		if($subject_info->stream_id!='' && $subject_info->stream_id!=0)
		{
			$common_query_cond.= "or FIND_IN_SET($subject_info->stream_id, stream_id) ";
		}
		$common_query_cond.=") ";
	}
	
	$question_type_count = $_SESSION['question_type_count'];

	$random_que_id_set='';
	if(isset($_SESSION['beg']))
	{
		foreach($_SESSION['beg'] as $key=>$val) // key=marks val==no of question in database
		{
			$test_question_selection['question_total']=$random_input_array['beg_'.$key.'_'.$val];
			$selected=$test_question_selection['question_total'];
			if($selected!=0)
			{
				if(is_array($question_type_count[$subject_id]['B']))
				{
					$totalRemainingQuestions = $selected;
					$selectedQuestions = array();
					
					foreach($question_type_count[$subject_id]['B'] as $question_type => $question_count)
					{
						$type_count = floor($question_count * $selected / 100);

						$questionDetails = $DB->SelectRecords('question',"question_level='B' and question_type='$question_type' and marks=$key and ".$common_query_cond.$cust_que_set_condition,'*',"ORDER BY RAND() limit $type_count");
						$count=count($questionDetails);
						
						if($questionDetails && count($questionDetails) > 0)
						{
							for($counter=0;$counter<$count;$counter++)
							{
								if($totalRemainingQuestions <= 0)
								{
									break;
								}
								
								$selectedQuestions[] = $questionDetails[$counter]->id;
								$random_que_id_set.=$questionDetails[$counter]->id.",";
								$totalRemainingQuestions--;
								$totalQuestion++;
							}
							
							if($totalRemainingQuestions <= 0)
							{
								break;
							}
						}
					}
					
					if(count($selectedQuestions) > 0)
					{
						$selectedQuestions = implode(',', $selectedQuestions);
						$excludeQuestionCond = " AND id NOT IN ($selectedQuestions) ";
						$selected = $totalRemainingQuestions;
					}
				}
				
				$questionDetails=$DB->SelectRecords('question',"question_level='B' and marks=$key and ".$common_query_cond.$cust_que_set_condition.$excludeQuestionCond,'*',"limit $selected");
				$count=count($questionDetails);
				 
				for($counter=0;$counter<$count;$counter++)
				{
					$random_que_id_set.=$questionDetails[$counter]->id.",";
					$totalQuestion++;
				}
				
			}
		}
	}
	unset($_SESSION['beg']);
	
	if(isset($_SESSION['int']))
	{
		foreach($_SESSION['int'] as $key=>$val)
		{
			$test_question_selection['question_total']=$random_input_array['int_'.$key.'_'.$val];
			$selected=$test_question_selection['question_total'];
			if($selected!=0)
			{
				if(is_array($question_type_count[$subject_id]['I']))
				{
					$totalRemainingQuestions = $selected;
					$selectedQuestions = array();
					
					foreach($question_type_count[$subject_id]['I'] as $question_type => $question_count)
					{
						$type_count = floor($question_count * $selected / 100);
						
						$questionDetails = $DB->SelectRecords('question',"question_level='I' and question_type='$question_type' and marks=$key and ".$common_query_cond.$cust_que_set_condition,'*',"ORDER BY RAND() limit $type_count");
						$count=count($questionDetails);
						
						if($questionDetails && count($questionDetails) > 0)
						{
							for($counter=0;$counter<$count;$counter++)
							{
								if($totalRemainingQuestions <= 0)
								{
									break;
								}
								
								$selectedQuestions[] = $questionDetails[$counter]->id;
								$random_que_id_set.=$questionDetails[$counter]->id.",";
								$totalRemainingQuestions--;
								$totalQuestion++;
							}
							
							if($totalRemainingQuestions <= 0)
							{
								break;
							}
						}
					}
					
					if(count($selectedQuestions) > 0)
					{
						$selectedQuestions = implode(',', $selectedQuestions);
						$excludeQuestionCond = " AND id NOT IN ($selectedQuestions) ";
						$selected = $totalRemainingQuestions;
					}
				}
				
				$questionDetails=$DB->SelectRecords('question',"question_level='I' and marks=$key and ".$common_query_cond.$cust_que_set_condition.$excludeQuestionCond,'*',"limit $selected");
				$count=count($questionDetails);
				for($counter=0;$counter<$count;$counter++)
				{
					$random_que_id_set.=$questionDetails[$counter]->id.",";
					$totalQuestion++;
				}
			}
		}
		unset($_SESSION['int']);
	}
	
	if(isset($_SESSION['high']))
	{
		foreach($_SESSION['high'] as $key=>$val)
		{
			$test_question_selection['question_total']=$random_input_array['high_'.$key.'_'.$val];
			$selected=$test_question_selection['question_total'];
			if($selected!=0)
			{
				if(is_array($question_type_count[$subject_id]['H']))
				{
					$totalRemainingQuestions = $selected;
					$selectedQuestions = array();
					
					foreach($question_type_count[$subject_id]['H'] as $question_type => $question_count)
					{
						$type_count = floor($question_count * $selected / 100);
						
						$questionDetails = $DB->SelectRecords('question',"question_level='H' and question_type='$question_type' and marks=$key and ".$common_query_cond.$cust_que_set_condition,'*',"ORDER BY RAND() limit $type_count");
						$count=count($questionDetails);
						
						if($questionDetails && count($questionDetails) > 0)
						{
							for($counter=0;$counter<$count;$counter++)
							{
								if($totalRemainingQuestions <= 0)
								{
									break;
								}
								
								$selectedQuestions[] = $questionDetails[$counter]->id;
								$random_que_id_set.=$questionDetails[$counter]->id.",";
								$totalRemainingQuestions--;
								$totalQuestion++;
							}
							
							if($totalRemainingQuestions <= 0)
							{
								break;
							}
						}
					}
					
					if(count($selectedQuestions) > 0)
					{
						$selectedQuestions = implode(',', $selectedQuestions);
						$excludeQuestionCond = " AND id NOT IN ($selectedQuestions) ";
						$selected = $totalRemainingQuestions;
					}
				}
				
				$questionDetails=$DB->SelectRecords('question',"question_level='H' and marks=$key and ".$common_query_cond.$cust_que_set_condition.$excludeQuestionCond,'*',"limit $selected");
				$count=count($questionDetails);
				for($counter=0;$counter<$count;$counter++)
				{
					$random_que_id_set.=$questionDetails[$counter]->id.",";
					$totalQuestion++;
				}
			}
		}
		unset($_SESSION['high']);
	}	
	$random_que_id_set = substr($random_que_id_set,0,strlen($random_que_id_set)-1);

	$subject_td_value = ((stripcslashes($subject_name))).' <input type="hidden" name="subject_id_'.($tableRowCount).'" id="subject_id_'.($tableRowCount).'" value="'.$subject_id.'" />';
	$totalQuestion_td_value = $totalQuestion.' <input type="hidden" name="total_question_'.($tableRowCount).'" id="total_question_'.($tableRowCount).'" value="'.$totalQuestion.'" /><input type="hidden" name="random_que_id_'.($tableRowCount).'" id="random_que_id_'.($tableRowCount).'" value="'.$random_que_id_set.'" /><input type="hidden" name="custom_que_id_'.($tableRowCount).'" id="custom_que_id_'.($tableRowCount).'" value="'.$custom_que_id_set.'" />';
	$total_mark_td_value = $subject_total_mark.' <input type="hidden" name="subject_total_mark_'.($tableRowCount).'" id="subject_total_mark_'.($tableRowCount).'" value="'.$subject_total_mark.'" />';
	
	if($subject_diff_question=='Y')
	{
		$subject_diff_question_td_value = 'Yes <input type="hidden" name="subject_diff_question_'.($tableRowCount).'" id="subject_diff_question_'.($tableRowCount).'" value="'.$subject_diff_question.'" />';
	}
	else
	{
		$subject_diff_question_td_value = 'No <input type="hidden" name="subject_diff_question_'.($tableRowCount).'" id="subject_diff_question_'.($tableRowCount).'" value="'.$subject_diff_question.'" />';
	}
	
	$objResponse->addAssign("subject_name","value",$subject_td_value);
	$objResponse->addAssign("subject_total_question","value",$totalQuestion_td_value);
	$objResponse->addAssign("subject_paper_mark","value",$total_mark_td_value);
	$objResponse->addAssign("subject_diff_question","value",$subject_diff_question_td_value);
	
	if($mode=='add')
	{
		$objResponse->addScriptCall("addRow");
	}
	else 
	{
		$objResponse->addScriptCall("editRow",'update',$tableRowCount);
	}
	
	return $objResponse;
}

function getTradeQuestion($subject_id, $custom_que_id_set, $random_que_set, $subject_total_mark, $tableRowCount, $mode, $subject_diff_question='N')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAssign("test","innerHTML",$subject_id);
	$totalQuestion = 0;
	$random_que_set_array=explode(',',$random_que_set);
	//print_r($random_que_set_array);exit;
	$random_input_array = array();
	for($counter=0; $counter<count($random_que_set_array); $counter++)
	{
		$random_que_set_value_array=explode('_',$random_que_set_array[$counter]);
		$random_que_level = $random_que_set_value_array[0];
		$random_que_marks = $random_que_set_value_array[1];
		$random_que_available = $random_que_set_value_array[2];
		$random_que_input_value = $random_que_set_value_array[3];
		$random_input_array[$random_que_level."_".$random_que_marks."_".$random_que_available] = $random_que_input_value;
	}
	if($custom_que_id_set!='')
	{
		$cust_que_set_condition = " and id not in ($custom_que_id_set)";
		$custom_que_id_array = explode(',',$custom_que_id_set);
		$totalQuestion = count($custom_que_id_array);
	}
	
	//if($subject_id!='')
	{
		$common_query_cond = " (FIND_IN_SET($subject_id, subject_id) ";
		$subject_info = $DB->SelectRecord('stream',"id=$subject_id");
		
		$subject_name = $subject_info->stream_title;
		if($subject_info->id!='' && $subject_info->id!=0)
		{
			$common_query_cond.= "or FIND_IN_SET($subject_info->id, stream_id) ";
		}
		$common_query_cond.=") ";
	}
	
	$question_type_count = $_SESSION['question_type_count'];

	$random_que_id_set='';
	if(isset($_SESSION['beg']))
	{
		foreach($_SESSION['beg'] as $key=>$val) // key=marks val==no of question in database
		{
			$test_question_selection['question_total']=$random_input_array['beg_'.$key.'_'.$val];
			$selected=$test_question_selection['question_total'];
			if($selected!=0)
			{
				if(is_array($question_type_count[$subject_id]['B']))
				{
					$totalRemainingQuestions = $selected;
					$selectedQuestions = array();
					
					foreach($question_type_count[$subject_id]['B'] as $question_type => $question_count)
					{
						$type_count = floor($question_count * $selected / 100);

						$questionDetails = $DB->SelectRecords('question',"question_level='B' and question_type='$question_type' and marks=$key and ".$common_query_cond.$cust_que_set_condition,'*',"ORDER BY RAND() limit $type_count");
						$count=count($questionDetails);
						
						if($questionDetails && count($questionDetails) > 0)
						{
							for($counter=0;$counter<$count;$counter++)
							{
								if($totalRemainingQuestions <= 0)
								{
									break;
								}
								
								$selectedQuestions[] = $questionDetails[$counter]->id;
								$random_que_id_set.=$questionDetails[$counter]->id.",";
								$totalRemainingQuestions--;
								$totalQuestion++;
							}
							
							if($totalRemainingQuestions <= 0)
							{
								break;
							}
						}
					}
					
					if(count($selectedQuestions) > 0)
					{
						$selectedQuestions = implode(',', $selectedQuestions);
						$excludeQuestionCond = " AND id NOT IN ($selectedQuestions) ";
						$selected = $totalRemainingQuestions;
					}
				}
				
				$questionDetails=$DB->SelectRecords('question',"question_level='B' and marks=$key and ".$common_query_cond.$cust_que_set_condition.$excludeQuestionCond,'*',"limit $selected");
				$count=count($questionDetails);
				 
				for($counter=0;$counter<$count;$counter++)
				{
					$random_que_id_set.=$questionDetails[$counter]->id.",";
					$totalQuestion++;
				}
				
			}
		}
	}
	unset($_SESSION['beg']);
	
	if(isset($_SESSION['int']))
	{
		foreach($_SESSION['int'] as $key=>$val)
		{
			$test_question_selection['question_total']=$random_input_array['int_'.$key.'_'.$val];
			$selected=$test_question_selection['question_total'];
			if($selected!=0)
			{
				if(is_array($question_type_count[$subject_id]['I']))
				{
					$totalRemainingQuestions = $selected;
					$selectedQuestions = array();
					
					foreach($question_type_count[$subject_id]['I'] as $question_type => $question_count)
					{
						$type_count = floor($question_count * $selected / 100);
						
						$questionDetails = $DB->SelectRecords('question',"question_level='I' and question_type='$question_type' and marks=$key and ".$common_query_cond.$cust_que_set_condition,'*',"ORDER BY RAND() limit $type_count");
						$count=count($questionDetails);
						
						if($questionDetails && count($questionDetails) > 0)
						{
							for($counter=0;$counter<$count;$counter++)
							{
								if($totalRemainingQuestions <= 0)
								{
									break;
								}
								
								$selectedQuestions[] = $questionDetails[$counter]->id;
								$random_que_id_set.=$questionDetails[$counter]->id.",";
								$totalRemainingQuestions--;
								$totalQuestion++;
							}
							
							if($totalRemainingQuestions <= 0)
							{
								break;
							}
						}
					}
					
					if(count($selectedQuestions) > 0)
					{
						$selectedQuestions = implode(',', $selectedQuestions);
						$excludeQuestionCond = " AND id NOT IN ($selectedQuestions) ";
						$selected = $totalRemainingQuestions;
					}
				}
				
				$questionDetails=$DB->SelectRecords('question',"question_level='I' and marks=$key and ".$common_query_cond.$cust_que_set_condition.$excludeQuestionCond,'*',"limit $selected");
				$count=count($questionDetails);
				for($counter=0;$counter<$count;$counter++)
				{
					$random_que_id_set.=$questionDetails[$counter]->id.",";
					$totalQuestion++;
				}
			}
		}
		unset($_SESSION['int']);
	}
	
	if(isset($_SESSION['high']))
	{
		foreach($_SESSION['high'] as $key=>$val)
		{
			$test_question_selection['question_total']=$random_input_array['high_'.$key.'_'.$val];
			$selected=$test_question_selection['question_total'];
			if($selected!=0)
			{
				if(is_array($question_type_count[$subject_id]['H']))
				{
					$totalRemainingQuestions = $selected;
					$selectedQuestions = array();
					
					foreach($question_type_count[$subject_id]['H'] as $question_type => $question_count)
					{
						$type_count = floor($question_count * $selected / 100);
						
						$questionDetails = $DB->SelectRecords('question',"question_level='H' and question_type='$question_type' and marks=$key and ".$common_query_cond.$cust_que_set_condition,'*',"ORDER BY RAND() limit $type_count");
						$count=count($questionDetails);
						
						if($questionDetails && count($questionDetails) > 0)
						{
							for($counter=0;$counter<$count;$counter++)
							{
								if($totalRemainingQuestions <= 0)
								{
									break;
								}
								
								$selectedQuestions[] = $questionDetails[$counter]->id;
								$random_que_id_set.=$questionDetails[$counter]->id.",";
								$totalRemainingQuestions--;
								$totalQuestion++;
							}
							
							if($totalRemainingQuestions <= 0)
							{
								break;
							}
						}
					}
					
					if(count($selectedQuestions) > 0)
					{
						$selectedQuestions = implode(',', $selectedQuestions);
						$excludeQuestionCond = " AND id NOT IN ($selectedQuestions) ";
						$selected = $totalRemainingQuestions;
					}
				}
				
				$questionDetails=$DB->SelectRecords('question',"question_level='H' and marks=$key and ".$common_query_cond.$cust_que_set_condition.$excludeQuestionCond,'*',"limit $selected");
				$count=count($questionDetails);
				for($counter=0;$counter<$count;$counter++)
				{
					$random_que_id_set.=$questionDetails[$counter]->id.",";
					$totalQuestion++;
				}
			}
		}
		unset($_SESSION['high']);
	}	
	$random_que_id_set = substr($random_que_id_set,0,strlen($random_que_id_set)-1);

	$subject_td_value = ((stripcslashes($subject_name))).' <input type="hidden" name="subject_id_'.($tableRowCount).'" id="subject_id_'.($tableRowCount).'" value="'.$subject_id.'" />';
	$totalQuestion_td_value = $totalQuestion.' <input type="hidden" name="total_question_'.($tableRowCount).'" id="total_question_'.($tableRowCount).'" value="'.$totalQuestion.'" /><input type="hidden" name="random_que_id_'.($tableRowCount).'" id="random_que_id_'.($tableRowCount).'" value="'.$random_que_id_set.'" /><input type="hidden" name="custom_que_id_'.($tableRowCount).'" id="custom_que_id_'.($tableRowCount).'" value="'.$custom_que_id_set.'" />';
	$total_mark_td_value = $subject_total_mark.' <input type="hidden" name="subject_total_mark_'.($tableRowCount).'" id="subject_total_mark_'.($tableRowCount).'" value="'.$subject_total_mark.'" />';
	
	if($subject_diff_question=='Y')
	{
		$subject_diff_question_td_value = 'Yes <input type="hidden" name="subject_diff_question_'.($tableRowCount).'" id="subject_diff_question_'.($tableRowCount).'" value="'.$subject_diff_question.'" />';
	}
	else
	{
		$subject_diff_question_td_value = 'No <input type="hidden" name="subject_diff_question_'.($tableRowCount).'" id="subject_diff_question_'.($tableRowCount).'" value="'.$subject_diff_question.'" />';
	}
	
	$objResponse->addAssign("subject_name","value",$subject_td_value);
	$objResponse->addAssign("subject_total_question","value",$totalQuestion_td_value);
	$objResponse->addAssign("subject_paper_mark","value",$total_mark_td_value);
	$objResponse->addAssign("subject_diff_question","value",$subject_diff_question_td_value);
	
	if($mode=='add')
	{
		$objResponse->addScriptCall("addRow");
	}
	else 
	{
		$objResponse->addScriptCall("editRow",'update',$tableRowCount);
	}
	
	return $objResponse;
}

function getSubjectQuestion_intell($subject_id, $subject_total_mark,$tableRowCount,$mode,$subject_existing_total_mark)
{
	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert($subject_total_mark);
	$subject_info = $DB->SelectRecord('subject',"id=$subject_id");
	$subject_name = $subject_info->subject_name;
	$subject_td_value = ((stripcslashes($subject_name))).' <input type="hidden" name="subject_id_'.($tableRowCount).'" id="subject_id_'.($tableRowCount).'" value="'.$subject_id.'" />';
	$totalQuestion_td_value = $subject_total_mark.' <input type="hidden" name="subject_total_mark_'.($tableRowCount).'" id="subject_total_mark_'.($tableRowCount).'" value="'.$subject_total_mark.'" /><input type="hidden" name="random_que_id_'.($tableRowCount).'" id="random_que_id_'.($tableRowCount).'" value="'.$random_que_id_set.'" /><input type="hidden" name="custom_que_id_'.($tableRowCount).'" id="custom_que_id_'.($tableRowCount).'" value="'.$custom_que_id_set.'" />';
	$objResponse->addAssign("subject_name","value",$subject_td_value);
	$objResponse->addAssign("subject_total_question","value",$totalQuestion_td_value);
	$objResponse->addAssign("subject_paper_mark","value",'N/A');
	$objResponse->addAssign("subject_diff_question","value",'N/A');

	
	if($mode=='add')
	{
		$objResponse->addScriptCall("addRow");
		$objResponse->addScriptCall("sumfinalquestions",$subject_total_mark,$subject_existing_total_mark);
	}
	else 
	{
		$objResponse->addScriptCall("editRow",'update',$tableRowCount);
		$objResponse->addScriptCall("sumfinalquestions",$subject_total_mark,$subject_existing_total_mark);
	}
	
	return $objResponse;
}


function getCandidateExamDetail($exam_id, $candidate_id, $subject_id='', $row_no='', $mode='')
{
	global $DB;
	$objResponse = new xajaxResponse();
	//$candidate_test_subject_history = $DB->SelectRecord('candidate_test_subject_history',"candidate_id=$candidate_id and exam_id=$exam_id");
	if($subject_id=='')
	{
		$subject_id=-1;
		$objResponse->addAssign('tbl_body', 'innerHTML', '');
	}
	//$objResponse->addAlert($subject_id);
	
	//=============get candidate marks from history======================================
		$candidate_test_subject_history = getCandidateTestSubjectHistory($exam_id, $candidate_id, $subject_id);
		//print_r($candidate_test_subject_history);exit;
		if($candidate_test_subject_history)
		{
			//$candidate_test_history = $DB->SelectRecord('candidate_test_history', "exam_id=$exam_id and candidate_id=$candidate_id");
			if($subject_id==-1)
			{
				if($candidate_test_subject_history[0]->is_outer_candidate=='N')
				{
					//$objResponse->addScript("document.getElementById('candidate_general_marks').innerHTML=".$candidate_test_subject_history[0]->marks_obtained);
					$objResponse->addAssign('candidate_general_marks', 'style.display', '');
					$objResponse->addAssign('candidate_general_marks', 'innerHTML', $candidate_test_subject_history[0]->marks_obtained);
					$objResponse->addAssign('candidate_mark', 'style.display', 'none');
				}
				else 
				{
					$objResponse->addAssign('candidate_general_marks', 'style.display', '');
					//$objResponse->addAssign('candidate_general_marks', 'innerHTML', $candidate_test_subject_history[0]->marks_obtained);
					$objResponse->addAssign('candidate_mark', 'style.display', '');
					$objResponse->addAssign('candidate_mark', 'value', $candidate_test_subject_history[0]->marks_obtained);
				}
			}
			else
			{
				if($row_no=='')
				{
					if($candidate_test_subject_history[0]->is_outer_candidate=='N')
					{
					//$objResponse->addScript("document.getElementById('candidate_subject_marks').innerHTML=".$candidate_test_subject_history[0]->marks_obtained);
						$objResponse->addAssign('candidate_subject_marks', 'style.display', '');
						$objResponse->addAssign('candidate_subject_marks', 'innerHTML', $candidate_test_subject_history[0]->marks_obtained);
						$objResponse->addAssign('subject_mark_obtained', 'style.display', 'none');
					}
					else 
					{
						$objResponse->addAssign('candidate_subject_marks', 'style.display', 'none');
						//$objResponse->addAssign('candidate_subject_marks', 'innerHTML', $candidate_test_subject_history[0]->marks_obtained);
						$objResponse->addAssign('subject_mark_obtained', 'style.display', '');
						$objResponse->addAssign('subject_mark_obtained', 'value', $candidate_test_subject_history[0]->marks_obtained);
					}
				}
				else
				{
					$objResponse->addAssign('subject_markTd_'.$row_no, 'innerHTML', $candidate_test_subject_history[0]->marks_obtained);
				}
			}
		}
		else 
		{
			if($row_no=='')
			{
				$objResponse->addAssign('candidate_mark', 'style.display', '');
				$objResponse->addAssign('subject_mark_obtained', 'style.display', '');
				$objResponse->addAssign('subject_mark_obtained', 'value', '');
				$objResponse->addAssign('candidate_general_marks', 'style.display', 'none');
				$objResponse->addAssign('candidate_subject_marks', 'style.display', 'none');
				$objResponse->addAssign('candidate_general_marks', 'innerHTML', '');
				$objResponse->addAssign('candidate_subject_marks', 'innerHTML', '');
			}
			else 
			{
				$subject_mark_textfield = '<input type="text" value="" class="rounded textfield" size="4" id="subject_mark_'.$row_no.'" name="subject_mark_'.$row_no.'">';
				$objResponse->addAssign('subject_markTd_'.$row_no, 'innerHTML', $subject_mark_textfield);
			}
		}
	
	//===================get practical info of a subject==================================
		$exam_subject_info = $DB->SelectRecord('exam_subjects', "exam_id=".$exam_id." and subject_id=".$subject_id);
		
		if($subject_id==-1)
		{
			if($exam_subject_info->subjective=='N')
			{
				$objResponse->addAssign('subjective_mark','style.display','none');
				$objResponse->addAssign('subjective_mark','value','');
				
				$objResponse->addAssign('general_subjective_mark','style.display','none');
				$objResponse->addAssign('general_subjective_mark','value','');
				
				$objResponse->addAssign('general_subjective_td','style.display','none');
				$objResponse->addAssign('general_subjective_mark_td','style.display','none');
			}
			else 
			{
				$candidate_subjective_info = $DB->SelectRecord('candidate_subjective_history', "exam_id=".$exam_id." and candidate_id=".$candidate_id." and subject_id=".$subject_id);
		
				if($candidate_subjective_info)
				{
					$objResponse->addAssign('subjective_mark','style.display','');
					$objResponse->addAssign('general_subjective_mark','style.display','');
					
					$objResponse->addAssign('subjective_mark','value',$candidate_subjective_info->marks_obtained);
					$objResponse->addAssign('general_subjective_mark','value',$candidate_subjective_info->marks_obtained);
					
					$objResponse->addAssign('general_subjective_td','style.display','');
					$objResponse->addAssign('general_subjective_mark_td','style.display','');
				}
				else 
				{
					$objResponse->addAssign('subjective_mark','style.display','');
					$objResponse->addAssign('general_subjective_mark','style.display','');
					
					$objResponse->addAssign('general_subjective_td','style.display','');
					$objResponse->addAssign('general_subjective_mark_td','style.display','');
				}
				
			}
			
			if($exam_subject_info->practical=='N')
			{
				$objResponse->addAssign('practical_mark','style.display','none');
				$objResponse->addAssign('practical_mark','value','');
				$objResponse->addAssign('general_practical_td','style.display','none');
			}
			else 
			{
				$candidate_practical_info = $DB->SelectRecord('candidate_practical_history', "exam_id=".$exam_id." and candidate_id=".$candidate_id." and subject_id=".$subject_id);
		
				if($candidate_practical_info)
				{
					$objResponse->addAssign('practical_mark','style.display','');
					$objResponse->addAssign('practical_mark','value',$candidate_practical_info->marks_obtained);
					$objResponse->addAssign('general_practical_td','style.display','');
				}
				else 
				{
					$objResponse->addAssign('practical_mark','style.display','');
					$objResponse->addAssign('general_practical_td','style.display','');
				}
				
			}
		}
		else 
		{
			if($exam_subject_info->subjective=='N')
			{
				if($row_no=='')
				{
					$objResponse->addAssign('subject_subjective_tr','style.display','none');
					$objResponse->addAssign('subject_subjective','checked',false);
					$objResponse->addAssign('subject_subjective_mark','value','');
				}
				else 
				{
					$objResponse->addAssign('subjective_td_'.$row_no,'innerHTML','-');
					$objResponse->addAssign('subjective_markTd_'.$row_no,'innerHTML','-');
				}
				
			}
			else 
			{
				$candidate_subjective_info = $DB->SelectRecord('candidate_subjective_history', "exam_id=".$exam_id." and candidate_id=".$candidate_id." and subject_id=".$subject_id);
				
				if($row_no=='')
				{
					if($candidate_subjective_info)
					{
						$objResponse->addAssign('subject_subjective_tr','style.display','');
						$objResponse->addAssign('subject_subjective','checked',true);
						$objResponse->addAssign('subject_subjective_mark_td','style.display','');
						$objResponse->addAssign('subject_subjective_option_td','style.display','');
						$objResponse->addAssign('subject_subjective_mark','value',$candidate_subjective_info->marks_obtained);
					}
					else 
					{
						$objResponse->addAssign('subject_subjective_tr','style.display','');
					}
				}
				else 
				{
					$checked='';
					$subjective_value='';
					if($candidate_subjective_info)
					{
						$checked='checked';
						$subjective_value=$candidate_subjective_info->marks_obtained;
					}
					
					$subjective_checkbox = '<input type="checkbox" onclick="removemarks(this);" value="Y" '.$checked.' id="subjective_checkbox_value_'.$row_no.'" name="subjective_checkbox_value_'.$row_no.'" />';
					$subjective_marks_textfield = '<input type="text" value="'.$subjective_value.'" class="rounded textfield" size="4" id="subjective_mark_'.$row_no.'" name="subjective_mark_'.$row_no.'">';
				
					$objResponse->addAssign('subjective_td_'.$row_no,'innerHTML',$subjective_checkbox);
					$objResponse->addAssign('subjective_markTd_'.$row_no,'innerHTML',$subjective_marks_textfield);
				
				}
			}
			
			if($exam_subject_info->practical=='N')
			{
				if($row_no=='')
				{
					$objResponse->addAssign('subject_practical_tr','style.display','none');
					$objResponse->addAssign('subject_practical','checked',false);
					$objResponse->addAssign('subject_practical_mark','value','');
					//$objResponse->addAssign('general_practical_td','style.display','none');
				}
				else 
				{
					$objResponse->addAssign('practical_td_'.$row_no,'innerHTML','-');
					$objResponse->addAssign('practical_markTd_'.$row_no,'innerHTML','-');
				}
				
			}
			else 
			{
				$candidate_practical_info = $DB->SelectRecord('candidate_practical_history', "exam_id=".$exam_id." and candidate_id=".$candidate_id." and subject_id=".$subject_id);
				
				if($row_no=='')
				{
					if($candidate_practical_info)
					{
						$objResponse->addAssign('subject_practical_tr','style.display','');
						$objResponse->addAssign('subject_practical','checked',true);
						$objResponse->addAssign('subject_practical_mark_td','style.display','');
						$objResponse->addAssign('subject_practical_option_td','style.display','');
						$objResponse->addAssign('subject_practical_mark','value',$candidate_practical_info->marks_obtained);
					}
					else 
					{
						$objResponse->addAssign('subject_practical_tr','style.display','');
					}
				}
				else 
				{
					$checked='';
					$practical_value='';
					if($candidate_practical_info)
					{
						$checked='checked';
						$practical_value=$candidate_practical_info->marks_obtained;
					}
					
					$practical_checkbox = '<input type="checkbox" onclick="removemarks(this);" value="Y" '.$checked.' id="practical_checkbox_value_'.$row_no.'" name="practical_checkbox_value_'.$row_no.'" />';
					$practical_marks_textfield = '<input type="text" value="'.$practical_value.'" class="rounded textfield" size="4" id="practical_mark_'.$row_no.'" name="practical_mark_'.$row_no.'">';
				
					$objResponse->addAssign('practical_td_'.$row_no,'innerHTML',$practical_checkbox);
					$objResponse->addAssign('practical_markTd_'.$row_no,'innerHTML',$practical_marks_textfield);
				
				}
			}
		}
		
	//==========get subjective info of a exam=====================================
		$examination_info = $DB->SelectRecord('examination', 'id='.$exam_id);
		//$objResponse->addAlert($examination_info->subjective);
		if($examination_info->subjective=='N')
		{
			$objResponse->addAssign('subjective_mark','style.display','none');
			$objResponse->addAssign('subjective_mark','value','');
			$objResponse->addAssign('subjective_mark_td','style.display','none');
		}
		else
		{
			
			$candidate_subjective_info = $DB->SelectRecord('candidate_subjective_history', "exam_id=".$exam_id." and candidate_id=".$candidate_id);
			//print_r($candidate_subjective_info);exit;
			if($candidate_subjective_info)
			{
				$objResponse->addAssign('subjective_mark','style.display','');
				$objResponse->addAssign('subjective_mark_td','style.display','');
				$objResponse->addAssign('subjective_mark','value',$candidate_subjective_info->marks_obtained);
			}
			else 
			{
				$objResponse->addAssign('subjective_mark','style.display','');
				$objResponse->addAssign('subjective_mark_td','style.display','');
				$objResponse->addAssign('subjective_mark','value', '');
			}
			
			/*if ($mode=='add')
			{
				$objResponse->addAssign('subjective_mark','style.display','');
				$objResponse->addAssign('subjective_mark_td','style.display','');
			}*/
		}
	
	return $objResponse;
}

function getSubjectMarksTable($exam_id, $candidate_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	$candidate_subject_info = getCandidateSubjectInfo($exam_id, $candidate_id);
	//print_r($candidate_subject_info);exit;
	if($candidate_subject_info)
	{
		$subject_count = count($candidate_subject_info);
		
		$tableOutput = '<table border="0" cellspacing="1" cellpadding="4" id="tbl_reports" width="100%" bgcolor="#e1e1e1" class="data">
								<thead>
					  				<tr class="tblheading">
										<td width="1%">#</td>
										<td width="25%">Subject</td>
										<td width="15%">Marks Obtained In Subject</td>
										<td width="10%">Subjective</td>
										<td width="15%">Marks Obtained In Subjective</td>
										<td width="10%">Practical</td>
										<td width="15%">Marks Obtained In Practical</td>
										<td width="9%">Action</td>
									</tr>
								</thead>
								<tbody id="tbl_body">';
		
		for($counter=0; $counter<$subject_count; $counter++)
		{
			$subject_marks_hidden_textfield = '';
			if($candidate_subject_info[$counter]->is_outer_candidate=='Y')
			{
				$subject_marks_hidden_textfield = '<input type="hidden" name="subject_mark_'.($counter+1).'" id="subject_mark_'.($counter+1).'" value="'.$candidate_subject_info[$counter]->marks_obtained.'">';
			}
			
			$exam_subject_info = $DB->SelectRecord('exam_subjects', "exam_id=".$exam_id." and subject_id=".$candidate_subject_info[$counter]->subject_id);
			
			if($exam_subject_info->subjective=='N')
			{
				$subjective_td_html = '-';
				$subjective_marks_td_html = '-';
			}
			else
			{
				if($candidate_subject_info[$counter]->subjective=='Y')
				{
					$candidate_subjective_info = $DB->SelectRecord('candidate_subjective_history', "exam_id=$exam_id and subject_id=".$candidate_subject_info[$counter]->subject_id." and candidate_id=$candidate_id");
					$subjective_td_html = 'Yes <input type="hidden" name="subjective_checkbox_value_'.($counter+1).'" id="subjective_checkbox_value_'.($counter+1).'" value="'.$candidate_subject_info[$counter]->subjective.'">';
					$subjective_marks_td_html = $candidate_subjective_info->marks_obtained.'<input type="hidden" name="subjective_mark_'.($counter+1).'" id="subjective_mark_'.($counter+1).'" value="'.$candidate_subjective_info->marks_obtained.'">';
				}
				else 
				{
					$subjective_td_html = 'No <input type="hidden" name="subjective_checkbox_value_'.($counter+1).'" id="subjective_checkbox_value_'.($counter+1).'" value="'.$candidate_subject_info[$counter]->subjective.'">';
					$subjective_marks_td_html = '-'.'<input type="hidden" name="subjective_mark_'.($counter+1).'" id="subjective_mark_'.($counter+1).'" value="">';
				}
			}
			
			if($exam_subject_info->practical=='N')
			{
				$practical_td_html = '-';
				$practical_marks_td_html = '-';
			}
			else
			{
				if($candidate_subject_info[$counter]->practical=='Y')
				{
					$candidate_practical_info = $DB->SelectRecord('candidate_practical_history', "exam_id=$exam_id and subject_id=".$candidate_subject_info[$counter]->subject_id." and candidate_id=$candidate_id");
					$practical_td_html = 'Yes <input type="hidden" name="practical_checkbox_value_'.($counter+1).'" id="practical_checkbox_value_'.($counter+1).'" value="'.$candidate_subject_info[$counter]->practical.'">';
					$practical_marks_td_html = $candidate_practical_info->marks_obtained.'<input type="hidden" name="practical_mark_'.($counter+1).'" id="practical_mark_'.($counter+1).'" value="'.$candidate_practical_info->marks_obtained.'">';
				}
				else 
				{
					$practical_td_html = 'No <input type="hidden" name="practical_checkbox_value_'.($counter+1).'" id="practical_checkbox_value_'.($counter+1).'" value="'.$candidate_subject_info[$counter]->practical.'">';
					$practical_marks_td_html = '-'.'<input type="hidden" name="practical_mark_'.($counter+1).'" id="practical_mark_'.($counter+1).'" value="">';
				}
			}
			
			$tableOutput.='<tr class="tdbggrey">
								<td>'.($counter+1).'</td>
								<td id="subject_nameTd_'.($counter+1).'">'.((stripslashes($candidate_subject_info[$counter]->subject_name))).' <input type="hidden" name="subject_id_'.($counter+1).'" id="subject_id_'.($counter+1).'" value="'.$candidate_subject_info[$counter]->subject_id.'"></td>
								<td id="subject_markTd_'.($counter+1).'">'.$candidate_subject_info[$counter]->marks_obtained.$subject_marks_hidden_textfield.'</td>
								<td id="subjective_td_'.($counter+1).'">'.$subjective_td_html.'</td>
								<td id="subjective_markTd_'.($counter+1).'">'.$subjective_marks_td_html.'</td>
								<td id="practical_td_'.($counter+1).'">'.$practical_td_html.'</td>
								<td id="practical_markTd_'.($counter+1).'">'.$practical_marks_td_html.'</td>
								<td>
									<input type="button" class="buttons" value="Edit" id="edit_'.($counter+1).'" name="edit_'.($counter+1).'" onclick="editRow(this);"/> 
									<img onclick="deleteRow(this);" title="Delete" style="cursor: pointer;" src="'.IMAGEURL.'/b_drop.png" /> 
								</td>
							</tr>';
		}
		
		$tableOutput.='</tbody></table>';
		//$objResponse->addAlert($tableOutput);
		$objResponse->addAssign('all_subject_tr', 'style.display', '');
		$objResponse->addAssign('all_subject_td', 'innerHTML', $tableOutput);
		$objResponse->addAssign('total_subject', 'value', $subject_count);
	}
	
	return $objResponse;
}

//===============this function make a drop-down list of test's name related to selected exam=========
function makeTestListByExamId($exam_id, $subject_selected_value='')
{
	global $DB;
	$objResponse = new xajaxResponse();
	if($exam_id!='')
	{
		
		$test = $DB->SelectRecords('test','exam_id='.$exam_id,'id, test_name');
		//print_r($test);exit;
		$output='';
			$output.='<select style="width:100px;" id="test_id" name="test_id" class="rounded"><option value="">Select Test</option>';
		if($test)
		{
			
			
			//$output.='';
			for($counter=0; $counter<count($test); $counter++)
			{
				//$objResponse->addAlert($subject[$counter]->id.", ".$subject_selected_value);
				$selected='';
				if ($test[$counter]->id == $subject_selected_value)
				{
					$selected='selected';
				}
				$output.='<option value="'.$test[$counter]->id.'" '.$selected.' >'. (stripslashes($test[$counter]->test_name)).'</option>';
			}
		}
			$output.='</select>';
			$objResponse->addAssign('test_list_span','innerHTML',$output);
	}
	return $objResponse;
}

function isExamConducted($exam_id, $test_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	$msg='';
	
	if($exam_id!='')
	{
		$exam_history = $DB->SelectRecord('candidate_test_history', "exam_id=$exam_id");
		
		if($exam_history)
		{
			$msg = 'Permission denied. This exam is already conducted.';
		}
	}
	elseif($test_id!='')
	{
		$test_history = $DB->SelectRecord('candidate_test_history', "test_id=$test_id");
		
		if($test_history)
		{
			$msg = 'Permission denied. This test is already conducted.';
		}
	}
	
	if($msg=='')
	{
		//$objResponse->addScript('document.editfrm.submit();');
		return true;
	}
	else
	{
		$objResponse->addAlert($msg);
		return false;
	}
	
	return $objResponse;
}

function resetTotalMarks($stream_id, $exam_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	if($stream_id!='' && $exam_id!='')
	{
		$exam_type = getExamTypeNameByExamId($exam_id);
		if($exam_type->exam_type_name!='Promotion Exam')
		{
			if(isset($_SESSION['total']))
			{
				unset($_SESSION['total']);
			}
			$objResponse->addAssign('total_mark','innerHTML',0);
		}
	}
	
	return $objResponse;
}

function checkMaxMarks($candidate_marks, $exam_id, $marks_module, $textbox_id='', $subject_id='')
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	if($marks_module=='subjective')
	{
		$subjective_exam_info = $DB->SelectRecord('subjective_exam', "exam_id=$exam_id and subject_id=$subject_id");
		if($candidate_marks>$subjective_exam_info->max_marks)
		{
			$objResponse->addAlert('Please enter candidate subjective marks less than or equal to '.$subjective_exam_info->max_marks.'.');
			$objResponse->addAssign($textbox_id,'value','');
		}
	}
	
	if($marks_module=='practical')
	{
		$practical_exam_info = $DB->SelectRecord('practical_exam', "exam_id=$exam_id and subject_id=$subject_id");
		if($candidate_marks>$practical_exam_info->max_marks)
		{
			$objResponse->addAlert('Please enter candidate practical marks less than or equal to '.$practical_exam_info->max_marks.'.');
			$objResponse->addAssign($textbox_id,'value','');
		}
	}
	
	if($marks_module=='subject')
	{
		$exam_subject_info = $DB->SelectRecord('exam_subjects', "exam_id=$exam_id and subject_id=$subject_id");
		if($candidate_marks>$exam_subject_info->subject_max_mark)
		{
			$objResponse->addAlert('Please enter candidate marks less than or equal to '.$exam_subject_info->subject_max_mark.'.');
			$objResponse->addAssign($textbox_id,'value','');
		}
	}
	
	return $objResponse;
}

function enableTest($test_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	//$objResponse->addAlert($value);
	$test_data = array();
	if($test_id)
	{
		$test_info = $DB->SelectRecord('test',"id=$test_id", 'enable_test');
		if($test_info->enable_test=='Y')
		{
			$test_data['enable_test'] = 'N';
			$DB->UpdateRecord('test', $test_data, "id=$test_id");
			$objResponse->addAssign('enableTest_'.$test_id, 'src',IMAGEURL.'/right.gif');
			$objResponse->addAssign('enableTest_'.$test_id, 'title','Enable test');
			$objResponse->addAlert('Test has been disabled successfully.');
		}
		else
		{
			$test_data['enable_test'] = 'Y';
			$DB->UpdateRecord('test', $test_data, "id=$test_id");
			$objResponse->addAssign('enableTest_'.$test_id, 'src',IMAGEURL.'/disable.gif');
			$objResponse->addAssign('enableTest_'.$test_id, 'title','Disable test');
			$objResponse->addAlert('Test has been enabled successfully.');
		}
		
		
	}
	
	return $objResponse;
}
//---------------------------------


function enable_candidate($cand_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	//$objResponse->addAlert($cand_id);
	$cand_data = array();
	if($cand_id)
	{
		$candi_info = $DB->SelectRecord('candidate',"id=$cand_id", 'candidate_is_active');
		if($candi_info->candidate_is_active=='Y')
		{
			$cand_data['candidate_is_active'] = 'N';
			$DB->UpdateRecord('candidate', $cand_data, "id=$cand_id");
			$objResponse->addAssign('candidate_is_active_'.$cand_id, 'src',IMAGEURL.'/right.gif');
			$objResponse->addAssign('candidate_is_active_'.$cand_id, 'title','candidate active');
			//$objResponse->addAlert('candidate has been unactivated successfully.');
		}
		else
		{
			$cand_data['candidate_is_active'] = 'Y';
			$DB->UpdateRecord('candidate', $cand_data, "id=$cand_id");
			$objResponse->addAssign('candidate_is_active_'.$cand_data, 'src',IMAGEURL.'/disable.gif');
			$objResponse->addAssign('candidate_is_active_'.$cand_data, 'title','Disable candidate');
			//$objResponse->addAlert('Candidate has been activated successfully.');
		}
		
		
	}
	
	return $objResponse;
}

//-----------------------------------

function archiveTest($test_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	
	//$objResponse->addAlert($value);
	$test_data = array();
	if($test_id)
	{
		$test_info = $DB->SelectRecord('test',"id=$test_id", 'is_archive');
		if($test_info->is_archive=='Y')
		{
			$test_data['is_archive'] = 'N';
			$DB->UpdateRecord('test', $test_data, "id=$test_id");
			//$objResponse->addAssign('row_'.$test_id, 'innerHTML','');
			$_SESSION['success'] = 'Test has been activated successfully.';
		}
		else
		{
			$test_data['is_archive'] = 'Y';
			$DB->UpdateRecord('test', $test_data, "id=$test_id");
			//$objResponse->addAssign('row_'.$test_id, 'innerHTML','');
			$_SESSION['success'] = 'Test has been archived successfully.';
		}
	}
	return $objResponse;
}

function loadsubcat($equp_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	$objResponse->addAlert($equp_id);
	if ($equp_id)
	{
		if(is_array($equp_id)) $equp_id = implode(',', $equp_id);
		$ese = $DB->SelectRecord('equipment_subequipment',"equipment_id IN ($equp_id)", 'GROUP_CONCAT(DISTINCT sub_equipment_id) as sub_equipment_id');
		$sub_equipment_info = $DB->SelectRecords('sub_equipment',"id IN ($ese->sub_equipment_id)");
		$count = count($sub_equipment_info);
		for($counter =0 ; $counter < $count; $counter++)
		{
			if ($sub_equipment_info[$counter]->sub_equipment_name !='')
			{
				$objResponse->addScript("addOption('sub_equipment_id','".$sub_equipment_info[$counter]->sub_equipment_name."','".$sub_equipment_info[$counter]->id."')");
			}
		}	
	}
	
	return $objResponse;
}

function loadsubunit($unit_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($unit_id)
	{
		$sub_unit_info = $DB->SelectRecords('subunit',"unit_id=$unit_id");
		
		$count = count($sub_unit_info);
		$objResponse->addScript("addOption('subunit_id','Select Subunit','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			
			if ($sub_unit_info[$counter]->sub_unit_name !='')
			{
				$objResponse->addScript("addOption('subunit_id','".$sub_unit_info[$counter]->sub_unit_name."','".$sub_unit_info[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}
function loadsubunitupgrading($unit_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($unit_id)
	{
		$sub_unit_info = $DB->SelectRecords('subunit',"unit_id=$unit_id");
		
		$count = count($sub_unit_info);
		$objResponse->addScript("addOption('subunit_id_upgrading','Select Subunit','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			
			if ($sub_unit_info[$counter]->sub_unit_name !='')
			{
				$objResponse->addScript("addOption('subunit_id_upgrading','".$sub_unit_info[$counter]->sub_unit_name."','".$sub_unit_info[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}
function loadsubunitdone($unit_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($unit_id)
	{
		$sub_unit_info = $DB->SelectRecords('subunit',"unit_id=$unit_id");
		
		$count = count($sub_unit_info);
		$objResponse->addScript("addOption('subunit_id_done','Select Subunit','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			
			if ($sub_unit_info[$counter]->sub_unit_name !='')
			{
				$objResponse->addScript("addOption('subunit_id_done','".$sub_unit_info[$counter]->sub_unit_name."','".$sub_unit_info[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}
function loadsubunitupgradingdone($unit_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($unit_id)
	{
		$sub_unit_info = $DB->SelectRecords('subunit',"unit_id=$unit_id");
		
		$count = count($sub_unit_info);
		$objResponse->addScript("addOption('subunit_id_upgrading_done','Select Subunit','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			
			if ($sub_unit_info[$counter]->sub_unit_name !='')
			{
				$objResponse->addScript("addOption('subunit_id_upgrading_done','".$sub_unit_info[$counter]->sub_unit_name."','".$sub_unit_info[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}
/************************************************************************************
 * 		Added By : Ashwini Agarwal
 * 		Date	 : April 10, 2012
 */

function loadunit($brigade_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($brigade_id)
	{
		$unit_info = $DB->SelectRecords('unit',"brigade_id=$brigade_id");
		$count = count($unit_info);
		$objResponse->addScript("addOption('unit_id','Select Unit','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			if ($unit_info[$counter]->unit_name !='')
			{
				$objResponse->addScript("addOption('unit_id','".$unit_info[$counter]->unit_name."','".$unit_info[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}

function loadbrigade($division_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($division_id)
	{
		$brigade = $DB->SelectRecords('brigade',"division_id=$division_id");
		$count = count($brigade);
		
		$objResponse->addScript("addOption('brigade_id','Select Brigade','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			if ($brigade[$counter]->brigade_name !='')
			{
				$objResponse->addScript("addOption('brigade_id','".$brigade[$counter]->brigade_name."','".$brigade[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}

function loaddivision($command_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($command_id)
	{
		$division = $DB->SelectRecords('division',"command_id=$command_id");
		$count = count($division);
		
		$objResponse->addScript("addOption('division_id','Select Division','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			if ($division[$counter]->division_name !='')
			{
				$objResponse->addScript("addOption('division_id','".$division[$counter]->division_name."','".$division[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}

function loadcommand($arm_id)
{

	global $DB;
	$objResponse = new xajaxResponse();
	//$objResponse->addAlert($arm_id);
	if ($arm_id)
	{
		$command = $DB->SelectRecords('command_arm',"arm_id=$arm_id");
		$count = count($command);
	//	$objResponse->addAlert($count);
		$objResponse->addScript("addOption('command_id','Select Command','')");
		for($counter=0; $counter<$count; $counter++)
		{
			$commands = $DB->SelectRecord("command","id='".$command[$counter]->command_id."'");
			if ($commands->command_name !='')
			{
			
				$objResponse->addScript("addOption('command_id','".$commands->command_name."','".$commands->id."')");
			}
		}	
	}	
	return $objResponse;
}

function loadcorps($arm_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($arm_id)
	{
		$corps = $DB->SelectRecords('corps',"arm_id=$arm_id");
		$count = count($corps);
		
		$objResponse->addScript("addOption('corps_id','Select Corps','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			if ($corps[$counter]->corps_name !='')
			{
				$objResponse->addScript("addOption('corps_id','".$corps[$counter]->corps_name."','".$corps[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}

/***********************************************************************************************
 * 				Author		:	Ashwini Agarwal
 * 				Date		:	March 20, 2012
 * 				Description	:	To temporarily upload images for image type questions.
 */

function uploadTempFile($val)
{
	global $frmdata;
	$objResponse = new xajaxResponse();
	
	$output="";
	
	if(isset($_SESSION['fileUploadError_'.$val]))
	{
		$output = '<font color="red">'.$_SESSION['fileUploadError'].'</font>';
		unset($_SESSION['fileUploadError']);
	}
	else
	{
		$image_path = $_SESSION['fileObject_'.$val]['final_path'];
		if($image_path != "")
		{
			$img = ROOTURL . '/uploadfiles/' . $image_path;
			$output = '<img style="heigth:20px;width:20px;" src = "' . $img . '" />';
		}
		else
		{
			$output = 'Preview not available.';
		}
	}
	$div = "img_" . $val;
	$objResponse->addAssign($div,'innerHTML',$output);
	$objResponse->addAssign('image_type','value','');
	return $objResponse;
}

/**************************************************************************************************
 * 		Author		:	Ashwini Agarwal
 * 		Date		:	March 28, 2012
 * 		Description	:	To get years of appearience of a candidate in any test.
 */

function showYear($candidate_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	$query = "SELECT DISTINCT YEAR(cqh.created_date) AS year
				FROM candidate AS c
				JOIN candidate_question_history AS cqh ON c.id = cqh.candidate_id
				WHERE c.candidate_id = '$candidate_id'
				ORDER BY year";
	
	$result = $DB->RunSelectQuery($query);
	$count = count($result);
	
	$y = isset($_SESSION['performance_year']) ? $_SESSION['performance_year'] : '';
	
	$output = 'Year: ';
	$output .= "<select name='year' id='year' class='rounded'>";
	$output .= "<option value=''>Select Year</option>";
	
	for($counter = 0; $counter < $count; $counter++)
	{
		if($result[$counter]->year != '')
		{
			$output .= "<option value='".$result[$counter]->year."' ";
			if($y == $result[$counter]->year)
			{
				$output .= " selected='selected' ";
			}
			$output .= " >".$result[$counter]->year."</option>";
		}
	}
	
	$output .= "</select>";
	
	if(isset($_SESSION['performance_year']))
		unset($_SESSION['performance_year']);
	
	$objResponse->addAssign('year_list','innerHTML',$output);
	return $objResponse;
}
//=================================== END ==========================================================//

function unsetSession($index)
{
	$objResponse = new xajaxResponse();
	if(isset($_SESSION[$index]) && $_SESSION[$index] != "")
	{
		$image_path = ROOT . '/uploadfiles/' . $_SESSION[$index];
		@unlink($image_path);
		unset($_SESSION[$index]);
		unset($_SESSION['fileObject_'.$index]);
	}
	return $objResponse;
}
//================================== END ==============================================================//

/********************************************************************************************
 * 			Author			:	Ashwini Agarwal
 * 			Date			:	April 6, 2012
 * 			Description		:	To get list of brigades.
 */

function showBrigade($division_id)
{
	$objResponse = new xajaxResponse();
	global $DB;
	
	$query = "	SELECT b.id, b.brigade_name 
				FROM brigade AS b
				JOIN division AS d ON d.id = b.division_id 
				WHERE d.id = '$division_id' 
				ORDER BY b.brigade_name ";
	
	$brigade = $DB->RunSelectQuery($query);
	$count = count($brigade);

	if(isset($_SESSION['brigade_id']))
	{
		$brigade_id = $_SESSION['brigade_id'];
		unset($_SESSION['brigade_id']);
	}
	
	$output = "<select name='brigade_id' id='brigade_id' class='rounded'>";
	$output .= "<option value=''>Please Select Brigade</option>";
	
	for($counter = 0; $counter < $count; $counter++)
	{
		if($brigade[$counter]->brigade_name != '')
		{
			$output .= "<option value='".$brigade[$counter]->id."' ";
			if($brigade_id == $brigade[$counter]->id)
			{
				$output .= " selected='selected' ";
			}
			$output .= " >".$brigade[$counter]->brigade_name."</option>";
		}
	}
	
	$output .= "</select>";
	
	$objResponse->addAssign('td_brigade','innerHTML',$output);
	return $objResponse;
}

/************************************************************************************************
 * 		Added By	:		Ashwini Agarwal
 * 		Date		:		April 11, 2012
 */

function showCommand($arm_id)
{

	$objResponse = new xajaxResponse();
	global $DB;
	//$objResponse->addAlert($arm_id);
	if(is_array($arm_id)== true)
	{
	$arm_id = implode(",", $arm_id);
	}
	//$objResponse->addAlert($arm_id);
	
	$query = "	SELECT DISTINCT c.command_name,c.id
				FROM command AS c
				left JOIN command_arm AS a ON c.id = a.command_id 
				WHERE a.arm_id IN($arm_id) 
				order by c.command_name";
	
	//$objResponse->addAlert($query);
	$command = $DB->RunSelectQuery($query);
	$count = count($command);
	
	
	
	if(isset($_SESSION['command_id']))
	{
		$command_id = $_SESSION['command_id'];
		unset($_SESSION['command_id']);
	}
	
	$output = "<select name='command_id' id='command_id' class='rounded' onchange='showDivision(this.value);'>";
	$output .= "<option value=''>Please Select Command</option>";
	
	for($counter = 0; $counter < $count; $counter++)
	{
		if($command[$counter]->command_name != '')
		{
			$output .= "<option value='".$command[$counter]->id."' ";
			if($command_id == $command[$counter]->id)
			{
				$output .= " selected='selected' ";
			}
			$output .= " >".$command[$counter]->command_name."</option>";
		}
	}
	
	$output .= "</select>";
	
	$objResponse->addAssign('td_command','innerHTML',$output);
	
	return $objResponse;
}

function showDivision($command_id)
{
	$objResponse = new xajaxResponse();
	global $DB;
	
	$query = "	SELECT d.id, d.division_name 
				FROM division AS d
				JOIN command AS c ON d.command_id = c.id 
				WHERE c.id = '$command_id' 
				ORDER BY d.division_name ";
	
	$division = $DB->RunSelectQuery($query);
	$count = count($division);
	
	if(isset($_SESSION['division_id']))
	{
		$division_id = $_SESSION['division_id'];
		unset($_SESSION['division_id']);
	}
	
	$output = "<select name='division_id' id='division_id' class='rounded' onchange='xajax_showBrigade(this.value)'>";
	$output .= "<option value=''>Please Select Division</option>";
	
	for($counter = 0; $counter < $count; $counter++)
	{
		if($division[$counter]->division_name != '')
		{
			$output .= "<option value='".$division[$counter]->id."' ";
			if($division_id == $division[$counter]->id)
			{
				$output .= " selected='selected' ";
			}
			$output .= " >".$division[$counter]->division_name."</option>";
		}
	}
	
	$output .= "</select>";
	
	$objResponse->addAssign('td_division','innerHTML',$output);
	return $objResponse;
}

function testcaptchaWork($frmname)
{
    
	$objResponse = new xajaxResponse();
  if($frmname == 'true')
  {
	$stringa = '';
		$cifre = 5;
		for($i=1;$i<=$cifre;$i++){
			$letteraOnumero = rand(1,2);
			if($letteraOnumero == 1){
				// lettera
				$lettere = 'ABEFHKMNRVWX';
				$x = rand(1,11);
				$lettera = substr($lettere,$x,1);
				$stringa .= $lettera;
			} else {
				$numero = rand(3,7);
				$stringa .= $numero;
			}
		}
	  // $_SESSION['captcha'] = $stringa;
		$inner = $stringa;
		
		$objResponse->AddAssign("showpass","innerHTML",$inner);
		$objResponse->AddAssign("passwords","value",$inner);
		$objResponse->AddAssign("pass","value",$inner);
		$objResponse->AddAssign("repass","value",$inner);
		$objResponse->AddAssign("pass","readOnly",'true');
		$objResponse->AddAssign("repass","readOnly",'true');
		}
	else
	 {
	    $objResponse->AddAssign("showpass","innerHTML",$inner);
		 $objResponse->AddAssign("pass","value",'');
		$objResponse->AddAssign("repass","value",'');
		$objResponse->AddAssign("pass","readOnly",'');
		$objResponse->AddAssign("repass","readOnly",'');
	 }
		return $objResponse;
}
function loadExamSubcategory($category_id, $selected = null)
{
	$objResponse = new xajaxResponse();
	global $DB;
	
	$query = "select * from sub_promotion_cadre where category_id = " . $category_id ;
	
	$subcategories = $DB->RunSelectQuery($query);	
	$count = count($subcategories);
	
	$output = '<select name="exam_sub_category" id="exam_sub_category" >' ;
	$output .= '<option value="">Please Select Sub Promotional Cadre </option>' ; 
	
									
	for($counter = 0; $counter < $count; $counter++)
	{
		if($subcategories[$counter]->subcategory != '')
		{
			$output .= "<option value='".$subcategories[$counter]->id."' ";
			if($selected == $subcategories[$counter]->id)
			{
				$output .= " selected='selected' ";
			}
			$output .= " >".$subcategories[$counter]->subcategory."</option>";
		}
	}
	
	$output .= "</select>";
	
	$objResponse->addAssign('subcat_load','innerHTML', $output);
	return $objResponse;
}

function hideNotification($id)
{
	$objResponse = new xajaxResponse();
	global $DB;
	
	$DB->UpdateRecord('exam_pass_notification', array('is_read'=>1), "id = '$id'");
	
	$objResponse->addScript("$('#ep-$id').hide();");
	return $objResponse;
}

function loadCourse($institute_id, $selected = null)
{
	global $DB;
	$objResponse = new xajaxResponse();
	$output = '<select class="rounded" name="course_id" id="course_id">';
	$output .= '<option value="">Select Course</option>' ; 
	
	if ($institute_id)
	{
		$course = $DB->SelectRecords('course',"institute_id='$institute_id'");
		$count = count($course);
		
		for($counter = 0; $counter < $count; $counter++)
		{
			if($course[$counter]->course_name != '')
			{
				$output .= "<option value='".$course[$counter]->id."'";
				if($selected == $course[$counter]->id)
				{
					$output .= " selected='selected' ";
				}
				$output .= ">".$course[$counter]->course_name."</option>";
			}
		}
	}
	
	$output .= "</select>";
	$objResponse->addAssign('course_id_td','innerHTML', $output);
	return $objResponse;
}
function TdnUpgrading($unit_id)
{
	global $DB;
	$objResponse = new xajaxResponse();
	if ($unit_id)
	{
		$sub_unit_info = $DB->SelectRecords('subunit',"unit_id=$unit_id");
		
		$count = count($sub_unit_info);
		$objResponse->addScript("addOption('subunit_id','Select Subunit','')");
		for($counter =0 ; $counter < $count; $counter++)
		{
			
			if ($sub_unit_info[$counter]->sub_unit_name !='')
			{
				$objResponse->addScript("addOption('subunit_id','".$sub_unit_info[$counter]->sub_unit_name."','".$sub_unit_info[$counter]->id."')");
			}
		}	
	}	
	return $objResponse;
}

require("incajax.php");
$xajax->processRequests();
?>