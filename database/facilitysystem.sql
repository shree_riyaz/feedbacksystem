-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 12, 2016 at 10:30 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `facilitysystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_detail`
--

CREATE TABLE IF NOT EXISTS `company_detail` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(250) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `user_name` varchar(200) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `creation_date` date DEFAULT NULL,
  `expiary_date` date DEFAULT NULL,
  `company_email` varchar(250) DEFAULT '\r\n',
  `password` varchar(50) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `company_logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `company_detail`
--

INSERT INTO `company_detail` (`company_id`, `company_name`, `plan_id`, `user_name`, `is_deleted`, `creation_date`, `expiary_date`, `company_email`, `password`, `is_active`, `company_logo`) VALUES
(1, 'CFS', 1, 'Zoro Admin', 'N', '2015-08-19', '2016-08-31', 'super_admin@gmail.com', 'nrm0r+iUZZc=', 'Y', '1440760826_logo.jpg'),
(2, 'Technocraft Ltd', 2, 'admin', 'Y', '2015-08-01', '2015-08-25', 'techno@gmail.com', 'obq0tauVZg==', 'N', NULL),
(3, 'Jet Airways', 3, 'admin', 'Y', '2015-07-29', '2016-01-01', 'jet@gmail.com', 'obq0tauVZg==', 'N', NULL),
(4, 'Branding Company', 1, 'admin', 'Y', '2015-08-06', '2015-08-31', 'branding@gmail.com', 'obq0tauVZg==', 'N', NULL),
(10, 'Spice Jet Airways', 3, 'spicejet_admin', 'N', '2015-08-06', '2015-08-31', 'spicejet@gmail.com', 'obq0tauVZg==', 'Y', '1441107579_avg.png       '),
(11, 'sunarc', 3, 'sun', 'Y', '2015-08-11', '2015-08-04', 'rajneesh.vyas@gmail.com', 'wp5Mg7', 'Y', NULL),
(12, 'testing', 2, 'testuser', 'Y', '2015-08-11', '2015-08-31', 'branding@gmail.com', 'rLe4du7EqLrQyqqP', 'Y', NULL),
(13, 'Sunarc Techno', 3, 'sunarc', 'Y', '2015-08-19', '2017-08-31', 'sun@gmail.com', 'obq0tauVZg==', 'N', NULL),
(14, 'IT company', 1, 'John Cena', 'Y', '2015-08-25', '2016-02-29', 'johnc@gmail.com', 'obq0tauVZg==', 'N', NULL),
(15, 'Techno Mall', 2, 'Kevin John', 'Y', '2015-08-26', '2016-04-30', 'kevin@gmail.com', 'XNDyWT', 'N', NULL),
(16, 'Digital Media', 3, 'Mack Thomas', 'Y', '2015-08-26', '2016-07-29', 'mack@gmail.com', 'NLafTo', 'N', NULL),
(17, 'Paras', 1, 'sunny', 'Y', '2015-08-27', '2015-12-26', 'sunny@vmail.com', '7rYPLQ', 'Y', NULL),
(18, 'Paras', 2, 'rony', 'Y', '2015-08-27', '2015-10-05', 'rony@vmail.com', 'E3UXm4', 'Y', NULL),
(19, 'Paras', 3, 'rony', 'Y', '2015-08-27', '2015-12-26', 'rony@vmail.com', 'LIPPOJ', 'N', NULL),
(20, 'Cyber', 1, 'jony', 'Y', '2015-08-27', '2015-08-31', 'cyber@gmail.com', 'fuAFjX', 'N', '1440753923_Chrysanthemum.jpg'),
(21, 'Testing for company deletion', 2, 'Tect COmpany', 'Y', '2015-08-28', '2016-04-29', 'testcompany@gmail.com', 'tzXzCE', 'Y', NULL),
(22, 'zumbino', 3, 'zumbino', 'Y', '2015-08-28', '2015-09-26', 'zumbino@gmail.com', 'XZYQWj', 'N', '1441191628_Cleaner Feedback System_1.png'),
(23, 'Sunarc Technology', 2, 'Sunarc Admin', 'Y', '2015-08-31', '2016-11-30', 'sunarc@gmail.com', 'fpFvTX', 'N', '1440997282_sunarc_logo.png'),
(24, 'Pearl', 2, 'Sunny', 'Y', '2015-09-04', '2015-09-18', 'sunny@vmail.com', 'ntrCid', 'N', '1441369104_Winter.jpg'),
(25, 'Pearl', 2, 'Rony', 'Y', '2015-09-04', '2015-09-18', 'rony@vmail.com', 'XEqoW1', 'N', '1441369563_Winter.jpg'),
(26, 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW', 3, 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW', 'Y', '2015-09-04', '1970-01-01', 'sunny@vmail.com', 'tXHs7v', 'N', '1441369829_1_1234.jpg'),
(27, 'Cyber', 1, 'Jony', 'Y', '2015-09-04', '2015-09-11', 'jony@vmail.com', 'IINIpX', 'N', '1441369926_1_1234.jpg'),
(28, 'Diamond', 2, 'Farhan', 'Y', '2015-09-04', '2005-09-03', 'farhan@vmail.com', 'tnrEvX', 'N', '1441370033_2_234.jpg'),
(29, 'Ruby', 2, 'Vinod', 'Y', '2015-09-04', '2015-09-30', 'vinod@vmail.com', 'E7ZtMt', 'N', '1441370847_Winter.jpg   '),
(30, 'Sunarc Technology', 2, 'Sunarc Admin', 'Y', '2015-09-05', '2016-09-05', 'sunarc@gmail.com', 'nXSXAS', 'N', '1441436706_sunarc_logo_6_4.png'),
(31, 'Cyber', 2, 'jony', 'Y', '2015-09-05', '2038-01-01', 'jony@vmail.com', 'I7MBNo', 'N', '1441439635_Picture 006.png    '),
(32, 'Pearl', 1, 'Preet', 'Y', '2015-09-07', '2015-09-30', 'preet@vmail.com', 'EEwBFX', 'N', '1441608843_Picture 005.png'),
(33, 'Heritage', 2, 'Stephen', 'N', '2015-09-07', '2015-12-31', 'stephen@vmail.com', 'IIPABg', 'N', '1441612530_1_1234.jpg '),
(34, 'Magic', 1, 'Grace', 'Y', '2015-09-07', '2015-09-30', 'grace@vmail.com', 'N7CMcc', 'N', '1441618840_Winter.jpg '),
(35, 'VSR', 2, 'venu', 'Y', '2015-09-08', '2015-10-31', 'venu@vmail.com', 'yNHkvO', 'N', '1441686961_Blue hills.jpg '),
(36, 'Heritage', 2, 'mark', 'Y', '2015-09-16', '2015-09-30', 'mark@vmail.com', '7AI4FC', 'N', '1442380346_Tulips.jpg'),
(37, 'Super', 3, 'goldy', 'Y', '2015-09-16', '2015-09-30', 'goldy@vmail.com', 'yUdBAj', 'N', '1442380670_Penguins.jpg  '),
(38, 'Super', 1, 'super company', 'N', '2015-09-16', '2015-09-30', 'super@vmail.com', 'N5IBJw', 'N', '1442381347_Hydrangeas.jpg '),
(39, 'Dev', 1, 'Dev', 'Y', '2015-09-16', '2015-09-30', 'dev@vmail.com', 'IjNtcF', 'N', '1442381898_Lighthouse.jpg'),
(40, 'Magnifier', 15, 'Mack', 'Y', '2015-09-16', '2015-09-30', 'mack@vmail.com', 'k3qaBX', 'N', '1442388907_Winter.jpg'),
(41, 'Digital Media', 1, 'John Cena', 'Y', '2015-09-16', '2015-09-30', 'johnc@gmail.com', 'LfFMvL', 'N', '1442406555_sunarc_logo.png'),
(42, 'Digital Media', 1, 'John Cena', 'Y', '2015-09-16', '2015-09-30', 'johnc@gmail.com', 'N3EOoj', 'N', '1442406591_sunarc_logo.png'),
(43, 'Heritage', 5, 'Preet', 'Y', '2015-09-21', '2015-09-22', 'preet@vmail.com', 't5Pnmj', 'N', '1442822806_1_1234.jpg     '),
(44, 'Heritage', 15, 'Preet', 'Y', '2015-09-21', '2015-09-22', 'preet@vmail.com', 'yljFif', 'N', '1442823499_1_1234.jpg'),
(45, 'Heritage', 9, 'Preet', 'Y', '2015-09-21', '2015-09-22', 'preet@vmail.com', 'yAWFhm', 'N', '1442823652_1_1234.jpg        '),
(46, 'Heritage', 15, 'Preet', 'N', '2015-09-21', '2015-09-22', 'preet@vmail.com', 'kyhepi', 'Y', '1442823807_1_1234.jpg'),
(47, 'Royal', 2, 'rex', 'N', '2015-09-21', '2015-09-25', 'rex@vmail.com', 'tfWtQf', 'Y', '1442827429_2_234.jpg'),
(48, 'new1', 1, 'Sarah', 'N', '2015-09-22', '2015-09-30', 'new1@xyz.com', 'Nj5hMi', 'N', '1442907892_100.jpg'),
(49, 'CFS ', 15, 'Daniel', 'Y', '2015-11-03', '2016-07-31', 'cfs@gmail.com', '7HyyHZ', 'N', '1446543498_excel-active.png'),
(50, 'mindtree', 2, 'Robert', 'N', '2015-11-03', '2016-09-30', 'mindtree@gmail.com', 'tF7MIs', 'Y', '1446543987_avg-active.png'),
(51, 'Dhrma Production', 15, 'Yash', 'Y', '2015-11-05', '2016-01-22', 'dhrma@gmail.com', 'FMQFB7', 'N', '1446721981_good-active.png'),
(52, 'Yash Raj Production', 2, 'Yash', 'Y', '2015-11-05', '2016-01-08', 'dhrma@gmail.com', 'tFzu8C', 'N', '1446727399_good-active.png'),
(53, 'Sunarc Technologies', 1, 'Dhruv', 'N', '2015-11-05', '2016-11-15', 'sunarc@gmail.com', 'IMpSh1', 'Y', '1446728733_Step1-Add-Class.png  '),
(54, 'TCS', 1, 'Virat Singh', 'N', '2015-11-09', '2015-11-30', 'tcs@gmail.com', 'obq0tauVZg==', 'Y', '1450266150_1440760826_logo.jpg'),
(55, 'Friendzone', 2, 'dyan', 'Y', '2015-12-01', '2016-01-31', 'friendzone@gmail.com', 'dLZ6nsCo', 'N', '1448960240_dream.jpg'),
(56, 'FZI', 2, 'dyan', 'Y', '2015-12-01', '2016-12-31', 'fzi@gmail.com', 'tqyVnsev', 'N', '1448960410_dream.jpg'),
(57, 'Nageroo', 15, 'Dion', 'N', '2015-12-02', '2016-12-02', 'dion@gmail.com', 'gqObtcu8', 'Y', '1449040261_dream.jpg'),
(58, 'FZI', 2, 'Joy', 'N', '2015-12-02', '2015-12-31', 'fzi@gmail.com', 'scCuqsza', 'N', '1449055747_dream.jpg'),
(59, 'test cmp', 1, 'test admin', 'Y', '2015-12-02', '2015-12-31', 'test@gmail.com', 'icqUkcOm', 'N', '1449061127_dream.jpg'),
(60, 'Gulmohar Grand', 2, 'Aman', 'N', '2015-12-08', '2015-12-31', 'gulmohar@gmail.com', 'i6LAnea4', 'Y', '1449554222_image4.jpg'),
(61, 'techno it', 1, 'vasim', 'Y', '2016-01-01', '2029-11-30', 'newtype@xyz.com', 'qKahkMa3', 'N', '1450079277_101_1.jpg '),
(62, 'Grand', 2, 'Jaydeep', 'N', '2015-12-14', '2016-02-29', 'grand@abc.com', 'c5d5l+TM', 'Y', '1450081298_1441783535_Winter.jpg'),
(63, 'dream world', 3, 'smith', 'N', '2015-12-15', '2016-02-29', 'smith@xyz.com', 'q8C7j8bN', 'Y', '1450176028_101.jpg'),
(64, 'NQSecurities', 1, 'Joy', 'N', '2015-12-16', '2016-01-24', 'nq@gmail.com', 'o6mLneiY', 'Y', '1450260220_1440760826_logo.jpg'),
(65, 'ITC', 15, 'Daniel', 'N', '2015-12-16', '2016-01-31', 'itc@gmail.com', 'qMGgmr3W', 'Y', '1450262131_1440760826_logo.jpg'),
(66, 'SunShine P Ltd ', 3, 'Mike Silvester', 'N', '2015-12-18', '2016-12-17', 'vineet.jain@sunarctechnologies.com', 'bod6eq+Z', 'Y', '1450436775_sunshine.jpg'),
(67, 'Travels', 2, 'Roddyrexx', 'N', '2016-01-08', '2016-01-31', 'roddy@gmail.com', 't863m8vZ', 'Y', '1452248401_01.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(200) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `device_status` varchar(200) DEFAULT 'Active',
  `user_id` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `device_description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  KEY `device_fk` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`device_id`, `device_name`, `location_id`, `company_id`, `is_deleted`, `device_status`, `user_id`, `is_active`, `device_description`) VALUES
(1, 'DEV01', 4, 10, 'Y', 'Active', 8, 'N', NULL),
(2, 'DEV02', 7, 10, 'Y', 'Active', 8, 'N', NULL),
(3, 'DEV13', 4, 10, 'Y', 'Active', 8, 'N', NULL),
(4, 'DEV-103', 7, 10, 'Y', 'Inactive', 8, 'N', NULL),
(5, 'DEV104', 7, 10, 'Y', 'Active', 8, 'N', NULL),
(6, 'DEV105', 4, 10, 'Y', 'Active', 8, 'N', 'description here'),
(8, 'DEV-106', 4, 10, 'Y', 'Active', 8, 'N', NULL),
(9, 'DEV_201', 10, 10, 'Y', '', 8, 'N', NULL),
(10, 'DEV_201', NULL, 10, 'Y', NULL, 8, 'N', NULL),
(11, 'DEV_201', NULL, 10, 'Y', NULL, 8, 'N', NULL),
(12, 'DEV_201', NULL, 10, 'Y', NULL, 8, 'N', NULL),
(13, 'DEV_202', NULL, 10, 'Y', NULL, 8, 'N', NULL),
(14, 'DEV_201', 7, 10, 'Y', 'Inactive', 8, 'N', NULL),
(15, 'DEV_001', NULL, 10, 'Y', NULL, 8, 'N', NULL),
(16, 'dev-101', 12, 12, 'Y', 'Active', 19, 'N', NULL),
(17, 'dev-102', 13, 12, 'Y', 'Active', 19, 'N', NULL),
(18, 'dev-103', 14, 12, 'Y', 'Active', 19, 'N', NULL),
(19, 'DEV-103', 7, 10, 'Y', 'Active', 8, 'N', 'Test complete'),
(20, '001-Dev', NULL, 22, 'N', 'Active', 28, 'Y', NULL),
(21, 'DEV_006', NULL, 10, 'Y', 'Active', 29, 'N', NULL),
(22, 'DEV_007', 0, 10, 'Y', 'Active', 29, 'N', NULL),
(23, 'Test', NULL, 10, 'Y', 'Active', 29, 'N', NULL),
(24, 'DEV_007', 4, 10, 'Y', 'Active', 29, 'N', 'Testing'),
(25, 'DEV_006', 4, 10, 'Y', 'Active', 29, 'N', NULL),
(26, 'Test', 7, 10, 'Y', 'Active', 8, 'N', NULL),
(27, 'Test Device', 7, 10, 'Y', 'Active', 8, 'N', 'Test'),
(28, 'DEV_007', 9, 10, 'Y', 'Active', 33, 'N', 'device test for dev_007'),
(29, 'DEV_007', 9, 10, 'Y', 'Active', 29, 'N', 'test'),
(30, '', 0, 10, 'Y', 'Active', 4, 'N', NULL),
(31, '359616044836047', 4, 10, 'Y', 'Active', 4, 'N', NULL),
(32, '000000000000000', 4, 10, 'Y', 'Active', 4, 'N', NULL),
(33, '000000000000000', 4, 10, 'Y', 'Active', 4, 'N', NULL),
(34, 'A-1', 0, 33, 'Y', '', 78, 'N', 'A'),
(35, 'A-1', 0, 33, 'Y', 'Active', 78, 'N', 'A'),
(36, 'A-1', 0, 33, 'N', 'Active', 78, 'Y', 'A'),
(37, '359616044836047', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(38, 'A-1', 18, 34, 'Y', 'Active', 80, 'N', 'A'),
(39, 'A-1', 18, 34, 'N', 'Active', 80, 'N', 'A'),
(40, 'A-1', 18, 34, 'N', 'Inactive', 84, 'Y', 'A'),
(41, 'B-1', 18, 34, 'Y', 'Active', 80, 'N', 'B'),
(42, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(43, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(44, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(45, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(46, 'DEV-20', 24, 10, 'Y', 'Active', 29, 'N', 'demo1'),
(47, 'DEV-21', 24, 10, 'Y', 'Active', 8, 'N', 'demo2'),
(48, 'B-1', 18, 34, 'N', 'Inactive', 84, 'Y', 'B'),
(49, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(50, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(51, 'DEV-23', 9, 10, 'Y', 'Inactive', 29, 'N', 'test demo dev1'),
(52, '', 0, 32, 'N', 'Active', 61, 'Y', NULL),
(53, 'sdsd', 3, 10, 'Y', 'Active', 11, 'N', NULL),
(54, '000000000000000', 4, 22, 'N', 'Active', 53, 'Y', NULL),
(55, '000000000000000', 4, 22, 'N', 'Active', 53, 'Y', NULL),
(56, '000000000000000', 4, 22, 'N', 'Active', 53, 'Y', NULL),
(57, '000000000000000', 4, 22, 'N', 'Active', 53, 'Y', NULL),
(58, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(59, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(60, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(61, 'android1', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(62, 'android1', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(63, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(64, 'A-2', 0, 32, 'N', 'Active', 61, 'Y', 'A'),
(65, 'A-2', 0, 32, 'N', 'Active', 61, 'Y', 'A'),
(66, 'A-2', 26, 32, 'N', 'Active', 61, 'Y', 'A'),
(67, 'A-2', 26, 32, 'N', 'Active', 61, 'Y', 'A'),
(68, 'A-2', 26, 32, 'N', 'Active', 61, 'Y', 'A'),
(69, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(70, '000000000000000', 4, 10, 'Y', 'Active', 11, 'N', NULL),
(71, 'B-1', 18, 32, 'N', 'Inactive', 84, 'Y', 'Test-B'),
(72, 'I 1', 0, 39, 'N', 'Active', 110, 'Y', 'test'),
(73, 'qwe', 7, 10, 'Y', 'Active', 8, 'N', NULL),
(74, '000000000000000', 4, 10, 'Y', 'Active', 8, 'N', NULL),
(75, 'J 1', 31, 39, 'N', 'Active', 110, 'Y', 'demo'),
(76, '359616044836047', 9, 10, 'Y', 'Active', 8, 'N', 'test'),
(77, 'k1', 33, 39, 'N', 'Active', 110, 'Y', '1'),
(78, 'L 3', 33, 39, 'N', 'Active', 110, 'Y', '2'),
(79, 'L 1', 31, 39, 'N', 'Active', 110, 'Y', 'Active'),
(80, 'Z1', 35, 39, 'Y', 'Active', 110, 'N', 'B'),
(81, 'I 1', 26, 32, 'N', 'Active', 56, 'Y', 'A'),
(82, '000000000000000', 4, 10, 'Y', 'Active', 8, 'N', NULL),
(83, '000000000000000', 24, 10, 'Y', 'Active', 8, 'N', NULL),
(87, 'DEV01', 39, 10, 'N', 'Active', 8, 'Y', 'Device 001 for Device Loc DEV-002'),
(88, 'DEV02', 38, 10, 'N', 'Active', 8, 'Y', 'DEV02 for device location Dev001'),
(89, 'DEV 03', 40, 10, 'N', 'Active', 133, 'Y', 'dev 003'),
(90, 'DEV-D1', 41, 50, 'N', 'Active', 136, 'Y', 'device 01'),
(91, 'DEV-D2', 41, 50, 'N', 'Active', 136, 'Y', 'device 2'),
(92, 'DEV-D3', 42, 50, 'N', 'Active', 138, 'Y', 'device 3'),
(93, 'DEV-D4', 42, 50, 'N', 'Active', 138, 'Y', 'device 4'),
(94, 'ipad-987', 44, 1, 'N', 'Active', 145, 'Y', 'ipad-987'),
(95, 'DEV-TCS-01', 45, 54, 'N', 'Active', 148, 'Y', 'device tcs 01'),
(96, 'DEV-TCS-02', 46, 54, 'N', 'Active', 148, 'Y', 'device tcs 02'),
(97, 'DEV-01', 47, 60, 'Y', 'Active', 173, 'N', 'device 1'),
(98, 'DEV-02', 48, 60, 'N', 'Active', 174, 'Y', 'device 2'),
(99, 'DEV-03', 47, 60, 'Y', 'Active', 181, 'N', 'device 3'),
(100, 'DEV-04', 49, 60, 'Y', 'Active', 181, 'N', 'device 4'),
(101, '354101061566855', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(102, '354101061566855', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(103, '354101061566855', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(104, '354101061566855', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(105, '354101061566855', 46, 54, 'N', 'Active', 148, 'Y', NULL),
(106, '354101061566855', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(107, '000000000000000', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(108, '000000000000000', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(109, '000000000000000', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(110, '000000000000000', 45, 54, 'N', 'Active', 148, 'Y', NULL),
(111, 'Mark-1', 50, 66, 'N', 'Active', 195, 'Y', 'Device owner is candice alva'),
(112, '000000000000000', 45, 54, 'N', 'Active', 148, 'Y', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `device_locations`
--

CREATE TABLE IF NOT EXISTS `device_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `location_name` varchar(200) DEFAULT NULL,
  `is_deleted` enum('N','Y') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `location_description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `device_locations`
--

INSERT INTO `device_locations` (`location_id`, `company_id`, `location_name`, `is_deleted`, `is_active`, `location_description`) VALUES
(1, NULL, 'Bikaner', '', 'Y', NULL),
(2, NULL, 'Ajmer', '', 'Y', NULL),
(3, NULL, 'jaipur', '', 'Y', NULL),
(4, 10, 'DEV02-102', 'Y', 'N', 'Testing'),
(5, NULL, 'jaipur', '', 'Y', NULL),
(6, NULL, 'Ajmer', '', 'Y', NULL),
(7, 10, 'DEV01-101', 'Y', 'N', 'Testing'),
(8, NULL, 'jaipur', '', 'Y', NULL),
(9, 10, 'DEV-07', 'Y', 'N', 'test device'),
(10, 10, 'DEV-06', 'Y', 'N', NULL),
(11, 10, 'DEV-05', 'Y', 'N', NULL),
(12, 12, 'DH-101', '', 'N', NULL),
(13, 12, 'DH-102', '', 'N', NULL),
(14, 12, 'DH-103', '', 'N', NULL),
(15, 22, 'my first location', 'N', 'Y', NULL),
(16, 10, 'DEV-01', 'Y', 'N', 'tested'),
(17, 33, 'ABC', 'Y', 'Y', 'XYZ'),
(18, 34, 'DEF', 'N', 'Y', 'A Testing Device'),
(19, 34, 'ABC', 'Y', 'Y', 'B'),
(20, 33, 'Test', 'Y', 'Y', 'Demo'),
(21, 34, 'gautam', 'Y', 'Y', ''),
(22, 29, 'DEV-00', 'N', 'Y', ' device added by vinod'),
(23, 10, 'DEV-0', 'Y', 'N', 'device location for dev-0'),
(24, 10, 'DEV-001', 'Y', 'N', 'demo1'),
(25, 10, 'DEV-002', 'Y', 'N', 'demo2'),
(26, 32, 'ABC', 'N', 'Y', 'A'),
(27, 32, 'DEF', 'N', 'Y', 'D'),
(28, 34, 'DF2', 'N', 'N', 'F2'),
(29, 34, 'DF3', 'N', 'N', 'F3'),
(30, 39, 'tower-1', 'Y', 'N', 'demo'),
(31, 39, 'tower-2', 'N', 'N', 'dummy'),
(32, 39, 'tower-2', 'Y', 'N', 'ground'),
(33, 39, 'tower-3', 'N', 'Y', 'terrace'),
(34, 39, 'tower-4', 'N', 'N', 'B'),
(35, 39, 'tower-4', 'Y', 'N', 'B'),
(36, 32, 'tower-1', 'N', 'N', 'abc'),
(37, 48, 'Central mall', 'N', 'Y', 'testing'),
(38, 10, 'DEV-001', 'N', 'Y', 'Device Location 001'),
(39, 10, 'DEV-002', 'N', 'Y', 'Device Location 002'),
(40, 10, 'DEV-003', 'N', 'Y', 'dev 003 location'),
(41, 50, 'D-LOC-01', 'N', 'Y', 'device loc 1'),
(42, 50, 'D-LOC-02', 'N', 'Y', 'device loc 02'),
(43, 50, 'D-LOC-03', 'N', 'Y', 'device loc 03'),
(44, 1, 'Top Stairs', 'N', 'Y', 'Top Stairs'),
(45, 54, 'DEV-LOC-01', 'N', 'Y', 'location 01'),
(46, 54, 'DEV-LOC-02', 'N', 'Y', 'location 02'),
(47, 60, 'DEV-LOC-01', 'N', 'Y', 'gulmohar device location 1'),
(48, 60, 'DEV-LOC-02', 'N', 'Y', 'Gulmohar device location 2'),
(49, 60, 'DEV-LOC-03', 'Y', 'N', 'device loc 3'),
(50, 66, 'North West', 'N', 'Y', 'Toilet');

-- --------------------------------------------------------

--
-- Table structure for table `faults`
--

CREATE TABLE IF NOT EXISTS `faults` (
  `fault_id` int(11) NOT NULL AUTO_INCREMENT,
  `fault_name` varchar(250) DEFAULT NULL,
  `fault_image` varchar(200) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`fault_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `faults`
--

INSERT INTO `faults` (`fault_id`, `fault_name`, `fault_image`, `service_id`, `is_deleted`, `is_active`) VALUES
(1, 'Dirty Basin', '1441086538_dirty-basin.png', 1, 'Y', 'N'),
(2, 'Dirty Floor', '1441086538_dirty-floor.png', 1, 'Y', 'N'),
(3, 'Fault Equipment', '1441086538_fault-equip.png', 1, 'Y', 'N'),
(4, 'Bad Odor', '1441086538_bad-odor.png', 1, 'Y', 'N'),
(8, 'test', '1441096871_bad-odor.png', 1, 'Y', 'N'),
(9, 'Dirty Toilet', '1441096871_dirty-toilet.png', 1, 'Y', 'N'),
(11, 'Dirty Toilet2', '1441097071_dirty-toilet-active.png', 1, 'Y', 'N'),
(12, 'Dirty Toilet1', '1441097072_dirty-toilet.png', 1, 'Y', 'N'),
(14, 'Dirty Toilet2', '1441097101_dirty-toilet-active.png', 1, 'Y', 'N'),
(15, 'Dirty Toilet1', '1441097101_dirty-toilet.png', 1, 'Y', 'N'),
(17, 'Dirty Toilet2', '1441097185_dirty-toilet-active.png', 1, 'Y', 'N'),
(18, 'Dirty Toilet1', '1441097186_dirty-toilet.png', 1, 'Y', 'N'),
(20, 'Dirty Toilet2', '1441097277_dirty-toilet-active.png', 1, 'Y', 'N'),
(21, 'Dirty Toilet1', '1441097277_dirty-toilet.png', 1, 'Y', 'N'),
(23, 'Dirty Toilet2', '1441097328_dirty-toilet-active.png', 1, 'Y', 'N'),
(24, 'Dirty Toilet1', '1441097328_dirty-toilet.png', 1, 'Y', 'N'),
(26, 'test2', '1441098861_bad-odor-active.png', 1, 'Y', 'N'),
(27, 'test', '1441098861_dirty-basin.png', 1, 'Y', 'N'),
(28, 'test2', '1441098917_bad-odor-active.png', 1, 'Y', 'N'),
(29, 'test', '1441098917_dirty-basin.png', 1, 'Y', 'N'),
(30, 'Dirty Toilet2', '1441098941_dirty-toilet-active.png', 1, 'Y', 'N'),
(31, 'Dirty Toilet1', '1441098941_dirty-basin.png', 1, 'Y', 'N'),
(32, 'wet floor', '1441697719_wet-floor-active.png', 1, 'Y', 'N'),
(33, 'No Paper', '1441697720_no-paper-active.png', 1, 'Y', 'N'),
(34, 'full trash', '1441697720_full-trash-active.png', 1, 'Y', 'N'),
(35, 'fault equipments', '1441697720_fault-equip-active.png', 1, 'Y', 'N'),
(36, 'dirty toilet', '1441697720_dirty-toilet-active.png', 1, 'Y', 'N'),
(37, 'dirty floor', '1441697720_dirty-floor-active.png', 1, 'Y', 'N'),
(38, 'dirty basin', '1441697720_dirty-basin-active.png', 1, 'Y', 'N'),
(39, 'bad odor', '1441697720_bad-odor-active.png', 1, 'Y', 'N'),
(44, 'Toilets', '1441783535_Winter.jpg', 13, 'N', 'Y'),
(46, 'wrong', '1442561615_100.jpg', 1, 'Y', 'N'),
(47, 'fsddfsf', '1442561909_100.jpg', 1, 'Y', 'N'),
(53, 'dirty basin', '1446529351_dirty-basin.png', 1, 'N', 'Y'),
(54, 'dirty floor', '1446532517_dirty-floor.png', 1, 'N', 'Y'),
(55, 'dirty toilet', '1446532907_dirty-toilet.png', 1, 'N', 'Y'),
(56, 'full trash', '1446628823_full-trash-active.png', 30, 'N', 'Y'),
(57, 'fault equipments', '1446628823_fault-equip-active.png', 30, 'N', 'Y'),
(58, 'bad odor', '1446628823_bad-odor-active.png', 30, 'N', 'Y'),
(59, 'dirty basin', '1446628823_dirty-basin-active.png', 30, 'N', 'Y'),
(60, 'fault equipment', '1448862028_dream.jpg', 34, 'Y', 'N'),
(61, 'dirty basin', '1448862028_dream.jpg', 34, 'Y', 'N'),
(62, 'dirty floor', '1448865252_dream.jpg', 34, 'Y', 'N'),
(63, 'full trash', '1448879684_dream.jpg', 34, 'Y', 'N'),
(64, 'dirty basin', '1449568876_image14.jpg', 40, 'Y', 'N'),
(65, 'bad odor', '1449569364_1441096871_bad-odor.png', 40, 'N', 'Y'),
(66, 'full trash', '1449569406_1441697720_full-trash-active.png', 40, 'Y', 'N'),
(67, 'fault equipment', '1449569459_1441697720_fault-equip-active.png', 40, 'N', 'Y'),
(68, 'dirty floor', '1449569459_1441086538_dirty-floor.png', 40, 'N', 'Y'),
(69, 'Dirty Floor', '1450267366_1441086538_dirty-floor.png', 34, 'N', 'Y'),
(70, 'Full Trash', '1450267392_1441697720_full-trash-active.png', 34, 'N', 'Y'),
(71, 'Dirty Basin', '1450267465_1441086538_dirty-basin.png', 34, 'N', 'Y'),
(72, 'bad odor', '1450267521_1441097759_bad-odor.png', 34, 'N', 'Y'),
(73, 'dirty toilet', '1450267562_1441098437_dirty-toilet-active.png', 34, 'N', 'Y'),
(74, 'Fault Equipment', '1450267589_1441697720_fault-equip-active.png', 34, 'N', 'Y'),
(75, 'No Lights', '1450272757_Chrysanthemum.jpg', 34, 'N', 'Y'),
(76, 'Dirty Floor', '1450437307_dirty floor.jpg', 0, 'N', 'Y'),
(77, 'Dirty Toilet', '1450437307_dirty toilet.jpg', 0, 'N', 'Y'),
(78, 'no device', '1450440387_1.png', 34, 'Y', 'N'),
(79, 'Dirty Toilet', '1450440460_dirty toilet.jpg', 46, 'N', 'Y'),
(80, 'Dirty Floor', '1450440460_dirty floor.jpg', 46, 'N', 'Y'),
(81, 'device 2', '1450441348_3.png', 34, 'N', 'Y'),
(82, 'four', '1450443123_4.png', 34, 'Y', 'N'),
(83, 'enter', '1450443579_8.png', 34, 'Y', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `rating` varchar(200) DEFAULT NULL,
  `feedback_status` varchar(200) DEFAULT NULL,
  `user_id` varchar(250) DEFAULT NULL,
  `creation_date` varchar(200) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `fault_id` int(11) DEFAULT NULL,
  `modified_date` varchar(200) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `assigned_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `feedback_fk` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=122 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`feedback_id`, `company_id`, `service_id`, `device_id`, `rating`, `feedback_status`, `user_id`, `creation_date`, `is_deleted`, `fault_id`, `modified_date`, `is_active`, `assigned_to`) VALUES
(1, 10, 0, 19, 'Average', '0', '4', '09/01/2015', 'Y', 1, '09/01/2015', 'N', 33),
(2, 10, 0, 19, 'Good', '0', '4', '09/01/2015', 'Y', 2, '09/01/2015', 'N', 29),
(3, 10, 0, 19, 'Good', '0', '4', '09/01/2015', 'Y', 2, NULL, 'N', 29),
(4, 10, 0, 6, 'Average', '1', '8', '09/01/2015', 'Y', 2, '09/02/2015', 'N', 35),
(5, 10, 0, 29, 'Average', '0', '4', '09/01/2015', 'Y', 3, '09/01/2015', 'N', 33),
(6, 10, 1, 1, 'Average', '1', '4', '09/02/2015', 'Y', 2, '09/02/2015', 'N', 8),
(7, 10, 1, 1, 'Average', '1', '4', '09/02/2015', 'Y', 4, '09/02/2015', 'N', 8),
(8, 10, 1, 1, 'Poor', '0', '4', '09/02/2015', 'Y', 3, NULL, 'N', 8),
(9, 10, 1, 2, 'Poor', '0', '4', '09/02/2015', 'Y', 1, NULL, 'N', 8),
(10, 10, 1, 2, 'Poor', '0', '4', '09/02/2015', 'Y', 1, NULL, 'N', 8),
(11, 10, 1, 2, 'Poor', '0', NULL, '09/02/2015', 'Y', 1, NULL, 'N', NULL),
(12, 10, 1, 2, 'Poor', '0', '4', '09/02/2015', 'Y', 3, NULL, 'N', 8),
(13, 10, 1, 1, 'Average', '1', '8', '09/02/2015', 'Y', 2, '09/16/2015', 'N', 11),
(14, 10, 1, 1, 'Poor', '0', '4', '09/03/2015', 'Y', 9, NULL, 'N', NULL),
(15, 10, 0, 19, 'Good', '0', '4', '09/07/2015', 'Y', 1, NULL, 'N', 29),
(16, 10, 0, 28, 'Average', '0', '4', '09/07/2015', 'Y', 3, NULL, 'N', 29),
(17, 10, 0, 6, 'Good', '0', '4', '09/07/2015', 'Y', 1, NULL, 'N', 29),
(18, 10, 0, 19, 'Good', '0', '4', '09/07/2015', 'Y', 3, NULL, 'N', 33),
(19, 10, 0, 6, 'Average', '0', '4', '09/07/2015', 'Y', 4, NULL, 'N', 29),
(20, 10, 0, 19, 'Average', '0', '4', '09/07/2015', 'Y', 1, NULL, 'N', 8),
(21, 10, 0, 6, 'Good', '0', '4', '09/07/2015', 'Y', 2, NULL, 'N', 29),
(22, 10, 0, 19, 'Average', '0', '4', '09/07/2015', 'Y', 4, NULL, 'N', 33),
(23, 10, 0, 19, 'Poor', '0', '4', '09/07/2015', 'Y', 9, NULL, 'N', 29),
(24, 34, 0, 39, 'Excellent', '1', '76', '09/08/2015', 'Y', 1, '09/08/2015', 'N', 80),
(25, 34, 0, 48, 'Average', '1', '76', '09/08/2015', 'Y', 1, '09/08/2015', 'N', 80),
(26, 34, 0, 39, 'Average', '0', '76', '09/08/2015', 'Y', 34, '09/08/2015', 'N', 80),
(27, 34, 0, 40, 'Good', '1', '76', '09/08/2015', 'Y', 1, '09/08/2015', 'N', 80),
(30, 10, 1, 29, 'Average', '0', '4', '09/08/2015', 'Y', 0, NULL, 'N', 29),
(31, 10, 1, 29, 'Poor', '0', '4', '09/08/2015', 'Y', 0, NULL, 'N', 29),
(32, 10, 1, 29, 'Poor', '0', '4', '09/08/2015', 'Y', 0, NULL, 'N', 29),
(33, 10, 1, 29, 'Good', '0', '4', '09/08/2015', 'Y', 1, NULL, 'N', 29),
(34, 10, 1, 22, 'Good', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(35, 10, 1, 22, 'poor', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(36, 10, 1, 22, 'poor', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(37, 10, 1, 22, 'ee', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(38, 10, 1, 22, '', '0', '4', '09/09/2015', 'Y', 5, NULL, 'N', 29),
(39, 10, 1, 22, '', '0', '4', '09/09/2015', 'Y', 5, NULL, 'N', 29),
(40, 10, 1, 22, '1', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(41, 10, 1, 22, 'Poor', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(42, 10, 1, 22, 'Poor', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(43, 10, 1, 22, '1', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(44, 10, 1, 22, 'Poor', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(45, 10, 1, 22, 'Poor', '0', '4', '09/09/2015', 'Y', 0, NULL, 'N', 29),
(46, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 81),
(47, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 35),
(48, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 7),
(49, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 14),
(50, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 35),
(51, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 82),
(52, 10, 1, 22, 'poor', '0', '29', '09/10/2015', 'Y', 0, NULL, 'N', 14),
(53, 10, 1, 22, '', '0', '29', '09/11/2015', 'Y', 5, NULL, 'N', 12),
(54, 10, 1, 1, 'Excellent', '1', '8', '09/11/2015', 'Y', 34, '09/14/2015', 'N', 81),
(55, 10, 1, 28, 'Poor', '0', '33', '09/11/2015', 'Y', 9, NULL, 'N', 12),
(56, 10, 1, 46, 'Good', '1', '29', '09/11/2015', 'Y', 4, '09/19/2015', 'N', 82),
(57, 10, 1, 46, 'Average', '0', '29', '09/11/2015', 'Y', 1, NULL, 'N', 34),
(58, 10, 1, 28, 'Good', '1', '33', '09/11/2015', 'Y', 32, '09/14/2015', 'N', 11),
(59, 10, 1, 1, 'Average', '1', '8', '09/11/2015', 'Y', 35, '09/11/2015', 'N', 11),
(60, 10, 1, 2, 'Good', '0', '8', '09/11/2015', 'Y', 34, '09/11/2015', 'N', 11),
(61, 32, 0, 52, 'Average', '1', '55', '09/11/2015', 'Y', 43, NULL, 'N', 61),
(62, 32, 0, 64, 'Good', '0', '55', '09/11/2015', 'Y', 44, '09/11/2015', 'N', 61),
(63, 32, 0, 71, 'Good', '1', '55', '09/11/2015', 'Y', 3, '09/11/2015', 'N', 60),
(64, 10, 0, 28, 'Average', '0', '4', '09/16/2015', 'Y', 34, NULL, 'N', 29),
(65, 10, 0, 51, 'Average', '0', '4', '09/16/2015', 'Y', 32, NULL, 'N', 8),
(66, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 14),
(67, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 14),
(68, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 7),
(69, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 35),
(70, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 14),
(71, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 81),
(72, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 35),
(73, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 82),
(74, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 12),
(75, 10, 1, 22, 'Poor', '0', '29', '09/16/2015', 'Y', 0, NULL, 'N', 11),
(76, 39, 0, 77, 'Good', '1', '102', '09/16/2015', 'Y', 1, NULL, 'N', 110),
(77, 39, 0, 78, 'Average', '0', '102', '09/16/2015', 'Y', 39, '09/16/2015', 'N', 110),
(78, 39, 0, 80, 'Poor', '1', '102', '09/16/2015', 'Y', 32, NULL, 'N', 110),
(79, 10, 0, 28, 'Average', '0', '4', '09/17/2015', 'Y', 33, NULL, 'N', 8),
(80, 10, 1, 22, 'Poor', '0', '29', '09/18/2015', 'Y', 0, NULL, 'N', 14),
(81, 10, 1, 22, 'Very Poor', '0', '29', '09/18/2015', 'Y', 0, NULL, 'N', 12),
(82, 10, 1, 22, 'Average', '0', '29', '09/18/2015', 'Y', 0, NULL, 'N', 11),
(83, 10, 1, 22, 'Poor', '0', '29', '09/18/2015', 'Y', 0, NULL, 'N', 14),
(84, 10, 1, 22, 'Average', '0', '29', '09/19/2015', 'Y', 0, NULL, 'N', 82),
(85, 10, 1, 22, 'Poor', '0', '29', '09/19/2015', 'Y', 0, NULL, 'N', 35),
(86, 10, 1, 22, 'Very Poor', '0', '29', '09/22/2015', 'N', 0, NULL, 'N', 0),
(87, 10, 1, 22, 'Poor', '0', '29', '09/22/2015', 'N', 0, NULL, 'N', 0),
(90, 10, 0, 87, 'Good', '0', '4', '11/03/2015', 'N', 53, '11/03/2015', 'Y', 8),
(91, 10, 0, 87, 'Excellent', '0', '4', '11/03/2015', 'N', 54, '11/03/2015', 'Y', 8),
(92, 10, 0, 88, 'Very Poor', '0', '4', '11/03/2015', 'N', 55, '11/04/2015', 'Y', 11),
(93, 50, 0, 90, 'Average', '0', '135', '11/04/2015', 'N', 56, NULL, 'Y', 136),
(94, 50, 0, 90, 'Average', '0', '135', '11/04/2015', 'N', 57, NULL, 'Y', 136),
(95, 50, 0, 91, 'Poor', '0', '135', '11/04/2015', 'Y', 58, NULL, 'N', 138),
(96, 50, 0, 91, 'Poor', '0', '135', '11/04/2015', 'Y', 59, NULL, 'N', 138),
(97, 50, 0, 91, 'Good', '0', '135', '11/04/2015', 'Y', 56, NULL, 'N', 141),
(98, 54, 34, 95, 'Good', '0', '147', '11/30/2015', 'Y', 60, NULL, 'N', 149),
(99, 54, 34, 96, 'Excellent', '1', '147', '11/30/2015', 'N', 61, '12/16/2015', 'Y', 150),
(100, 54, 34, 96, 'Poor', '0', '147', '11/30/2015', 'N', 62, NULL, 'Y', 151),
(101, 54, 34, 95, 'Average', '0', '147', '11/30/2015', 'N', 63, NULL, 'Y', 149),
(102, 54, 34, 95, 'Poor', '0', '147', '11/30/2015', 'N', 60, NULL, 'Y', 150),
(103, 60, 0, 97, 'Average', '0', '172', '12/08/2015', 'Y', 65, '12/08/2015', 'N', 175),
(104, 60, 40, 98, 'Good', '0', '172', '12/08/2015', 'N', 67, NULL, 'Y', 178),
(105, 60, 0, 98, 'Good', '0', '172', '12/09/2015', 'N', 65, '12/10/2015', 'Y', 177),
(106, 10, 1, 22, 'Poor', '0', '29', '12/16/2015', 'N', 0, NULL, 'Y', 82),
(107, 10, 1, 22, 'Very Poor', '0', '29', '12/16/2015', 'N', 0, NULL, 'Y', 11),
(108, 10, 1, 22, 'Average', '0', '29', '12/16/2015', 'N', 0, NULL, 'Y', 82),
(109, 10, 1, 22, 'Poor', '0', '29', '12/16/2015', 'N', 0, NULL, 'Y', 81),
(110, 54, 34, 96, 'Good', '1', '148', '12/16/2015', 'N', 68, '12/16/2015', 'Y', 81),
(111, 10, 1, 22, 'Average', '0', '29', '12/16/2015', 'N', 70, NULL, 'Y', 81),
(112, 10, 1, 22, 'Poor', '0', '29', '12/16/2015', 'N', 70, NULL, 'Y', 11),
(113, 54, 34, 95, 'Average', '1', '148', '12/16/2015', 'N', 70, '12/17/2015', 'Y', 171),
(114, 10, 1, 22, 'Poor', '0', '29', '12/16/2015', 'N', 71, NULL, 'Y', 81),
(115, 10, 1, 22, 'Poor', '0', '29', '12/16/2015', 'N', 70, NULL, 'Y', 81),
(116, 54, 34, 95, 'Very Poor', '0', '148', '12/16/2015', 'N', 73, NULL, 'Y', 171),
(117, 54, 34, 96, 'Poor', '0', '148', '12/16/2015', 'N', 74, NULL, 'Y', 171),
(118, 54, 34, 96, 'Poor', '1', '148', '12/16/2015', 'N', 75, '12/16/2015', 'Y', 149),
(119, 54, 34, 95, 'Poor', '0', '148', '12/17/2015', 'N', 75, NULL, 'Y', 171),
(120, 54, 34, 95, 'Poor', '0', '148', '12/18/2015', 'N', 70, NULL, 'Y', 171),
(121, 54, 0, 95, 'Very Poor', '1', '148', '12/18/2015', 'N', 75, '01/08/2016', 'N', 149);

-- --------------------------------------------------------

--
-- Table structure for table `login_detail`
--

CREATE TABLE IF NOT EXISTS `login_detail` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `access_key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

--
-- Dumping data for table `login_detail`
--

INSERT INTO `login_detail` (`login_id`, `user_id`, `access_key`) VALUES
(1, 6, 'd4cdbe35a52e05ff56e0a38c6d130d18'),
(2, 4, 'h91c341fe0309961ca3d0f6fe40f5e94'),
(3, 11, 'e90223c43673f50a6153ef73bdd13234'),
(4, 10, 'e8e3f0baebc15cbfb7295838aca5166a'),
(5, 12, 'i4bd44b8526cdf0fc07425625b20d710'),
(6, 0, 'c5c995d9436138a3bc49e4982d0c8192'),
(7, 8, 'bc55c83fab346ed6a11317c47eaaf547'),
(8, 14, 'j2da3bfa0988949834f36b30d46e0881'),
(9, 0, 'b87a5f5af5273eb7bb7a6782896ca83c'),
(10, 0, 'kb89948948e76c4a2cf066b40dcf7b4e'),
(11, 0, 'ce4fb648091fac1a1899d85c1812e13a'),
(12, 0, 'g8ce0d7b56c43ffdf775cb7b0c0d0eb5'),
(13, 0, 'c3d8c36248394819bab263652570be29'),
(14, 0, 'kff7895b9df68dff172415d93f854d7c'),
(15, 0, 'g5b0339e909c61865cc3b51f3451877f'),
(16, 0, 'c2ccff1b96d4f9b3dec2c79538355e80'),
(17, 0, 'kf297d795ca4ec2ac18b74815c5c5a91'),
(18, 0, 'h27dd747870296c6827d6b75026c85cc'),
(19, 0, 'g65dfef1420df8deff84f3fe8d82e48f'),
(20, 0, 'bcc18af043c889358392a3dda7665fbd'),
(21, 0, 'k377bc3c8b11f942e858d6175c4b1dae'),
(22, 0, 'a506081db1eef5f912bba9a1d08dbbd0'),
(23, 0, 'g21f6e1435bae872c7f693f1841f54d9'),
(24, 0, 'baa6f6f4e18bb329dea036860f17d670'),
(25, 0, 'b13908177772cf0f2df84c6993c3af24'),
(26, 0, 'g59eb8c9d9c070ce3203e40912cc110a'),
(27, 0, 'k4c28a7bfb2543626377fb138b982b4c'),
(28, 0, 'd090ec19e904dec2705d5fb36359ec0f'),
(29, 0, 'g090ec19e904dec2705d5fb36359ec0f'),
(30, 0, 'k309c8478f0d53ebbcbfa4afb34ca283'),
(31, 1, 'fb6e7fe1bbe11db073f7f561fd59928b'),
(32, 15, 'ab4859708f2c6984a0d775901d67a068'),
(33, 0, 'i14c26e235e3c1f94b5fa2d6242f9718'),
(34, 0, 'b69cf71fe0f13d61e780c3a92e3c627c'),
(35, 0, 'd81170395ea0fedb8171ae2dec7f8860'),
(36, 0, 'i18a479e3ad814961cc495ef358ba520'),
(37, 18, 'ff9ea56b88b1e4dcb1ce2b1147011126'),
(38, 19, 'ccfc894d492c5cea99800da4fa02666d'),
(39, 27, 'a51dbd6620768e82a4701f541278c1b7'),
(40, 28, 'ea29fa924b1ba097a47503a607e2575f'),
(41, 32, 'f8a2b601a0ece7daca5de91624a275bd'),
(42, 30, 'bf3772a2ff653ed6c35d4db039cde987'),
(43, 35, 'f1495c8cb270b77c1be6c30edbf8a09b'),
(44, 0, 'g7edef271cda07b9f58776e9ee8d8bb7'),
(45, 0, 'j75beaddf5eda782507057f55e51010f'),
(46, 0, 'c08653209b4d57d7775e41c2441d6200'),
(47, 0, 'k2dc33eb01bf86d36aea7d7d4e018272'),
(48, 0, 'i7cebd39d0da05130b0a6acf56451c08'),
(49, 0, 'h05d26406e20aaa69ca5cbf08ef0e799'),
(50, 0, 'f0ede8df59da8baa755b399a39d98ecb'),
(51, 0, 'g27e9ea4b1de2011fdbe3c029f40b3ed'),
(52, 55, 'hab8f64d9fabd4b07c283e586e124a11'),
(53, 68, 'c7e92b2b131983f2d36fd872b51110bc'),
(54, 0, 'k044990126bae306e4b54352764202a9'),
(55, 79, 'k9878aa8b43e72e61c3082199b4d99ba'),
(56, 76, 'i184da041e88be0049798fd53d919eed'),
(57, 0, 'c5f835ce5345a39c6546f57fdba907a1'),
(58, 0, 'kca5f41d37fcb39525cb804805611fc5'),
(59, 50, 'fe194edccbaf9027b01c7d566cc9dd6b'),
(60, 0, 'f7752f4759ceea285faec9c514939ba6'),
(61, 0, 'db11bac3f64f03324ef6a2ba8a811a05'),
(62, 0, 'f5a9ad57770cae091b4feb071d4fb553'),
(63, 0, 'g1dfc98045ad86ead87496a1c52b386c'),
(64, 0, 'af655edf0a83fa9be4d327fadf663a08'),
(65, 0, 'c44ad2429c9bd0e157adca779cbfc9de'),
(66, 0, 'a2066cd961746e5065beb15b852cc0dd'),
(67, 0, 'c97566c61ba2e72fa2a1ee94b40f7334'),
(68, 0, 'aa195a0af3d2dc20fb38b90e6d093e17'),
(69, 0, 'j1a10e5d4714a9fb5bcc5b9673641f58'),
(70, 0, 'fd2270987ec257db51e5803be9d773e4'),
(71, 0, 'a4d93b22623702e8311f146babb49c93'),
(72, 0, 'ada019921b23ac48d4295a264a2c3b4f'),
(73, 0, 'j5e9b368cf12d47f5fe8325ec0862ca3'),
(74, 53, 'b2920770995f9d3167fd4b2b361a5ad9'),
(75, 61, 'i43bb5e65ac94990c9b64dd7829528f7'),
(76, 59, 'hcf5318c52c3139dd23626fe36da52d1'),
(77, 0, 'hf5f0afec4007eade2d74b02d0e0bc07'),
(78, 0, 'jad59b278c296cec02b9d03340b732ef'),
(79, 0, 'bf99a5d96f18eb12017e0877d19f74cb'),
(80, 0, 'f7ecdae570818bcb72dd6f1061155018'),
(81, 0, 'kef90c76c9485e4638746a23a2d25856'),
(82, 0, 'j8b3a1cd3d14a101eb6db4f2d58474b9'),
(83, 0, 'f5eb0bd6bd1f8b93d0dd833d3afa80cb'),
(84, 0, 'b8d039bd47769ff4dbbfb9e9e7a21c8b'),
(85, 82, 'je5b6095932e9695ebcfa9ebc5f07e8f'),
(86, 81, 'b702a1eadb97a2bff66f75e8e54b10d5'),
(87, 29, 'c0f0d4125884447e680bab60e1f4ddf6'),
(88, 0, 'j9feda35de1ae3fff6a3670f0b7b934b'),
(89, 0, 'b8549a7b963e8c5a5b411910df0e82c3'),
(90, 0, 'e0e55b43980b4c75f4eee090e40602d8'),
(91, 0, 'd7935f345fc89f0b18ad8f5d6455b205'),
(92, 0, 'b93a614b53d48d1749fea291cbba9b5a'),
(93, 0, 'f684b269795dd088d8d79ae7af4afbbb'),
(94, 98, 'e87ce9a41a38485ef5bbc968f39be08c'),
(95, 49, 'ab8c876372df37fdedaf4d62cba34ace'),
(96, 101, 'c9d22b4f730f465e09bee3ea2c526f9b'),
(97, 102, 'g3003614ece562badf274592c8e36574'),
(98, 0, 'b0bafa104dd3f99131f6d17ffa01f7a9'),
(99, 0, 'ka98a98325aa4351ac61b9902278e260'),
(100, 0, 'gc0d766e0daae903a95831e9d97b38ef'),
(101, 118, 'ac2126d3980eb995d0dc8cb0ff324b34'),
(102, 122, 'gd99bd2fa1574d7d077bd1e8389dd5f5'),
(103, 129, 'g7640d21eb46a93f68530b3e5435b2e5'),
(104, 0, 'e076b61e6f077d3be7b79c0e4b7863a2'),
(105, 0, 'b04006f646c55400f0c8978c8fea93db'),
(106, 130, 'dd3deacf6d6d2efff90fad8764a46334'),
(107, 0, 'c810b6809ffbae021e97788030378882'),
(108, 133, 'j53391cfda54eab63b6b1c969386b73b'),
(109, 135, 'dc38ea455cb1c21f163b5ebeac1d304b'),
(110, 144, 'jda180d5638456db4d80d74c5bfccc1e'),
(111, 147, 'a5727732ac6e2a66ea0d7cdf78043b9e'),
(112, 148, 'b3be0f6835cb3d06937766eb31195104'),
(113, 149, 'f5bc740cc65a8044c2db00f41b50430f'),
(114, 150, 'c0a4eeb268a4fbe597bf1d3772d6712b'),
(115, 136, 'a64fe8661af1422828170915c196b362'),
(116, 157, 'h55f8163801e74e34ab831d6fefa62f9'),
(117, 158, 'k35315c071f0d82d9aa904861f110e09'),
(118, 160, 'cbb423a2e84f0adfd5fde60a18fc550d'),
(119, 162, 'i8fc978706c1f961ebc97c77972c63c6'),
(120, 164, 'k4fbfa7e0f04fee1d9f2124ac47f0bf5'),
(121, 172, 'jb78045b0cd2a3bf698ca6fe6f14f6d4'),
(122, 174, 'b3c16258d2597f4a95ad3eae86843adc'),
(123, 173, 'bd17cd0f0ba033b61124323cfd2c9c89'),
(124, 177, 'idc6f4010a9f77453ded30db33c4063a'),
(125, 151, 'a22e50d6a80d07310ab47faf46634378'),
(126, 183, 'c7c0d661c30425aca20810aaaaa1b111'),
(127, 184, 'b9e9656b546b5d25a44a47e3692fbae0'),
(128, 185, 'h60ecea33df0e8545b8b6d6a6770b6c3'),
(129, 188, 'a21d519abda68b3bd6ba2b1737edea37'),
(130, 189, 'i82c2906ebdb3d595cff29f83915dd36'),
(131, 194, 'd9ff7fe27d7e3d8eda1958d96e4299fd'),
(132, 197, 'j287d918b91114d0f69a1738cad7295f');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(200) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `module_fk` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `module_name`, `company_id`) VALUES
(1, 'dashboard', NULL),
(2, 'company', NULL),
(3, 'user', NULL),
(4, 'feedback', NULL),
(5, 'jobs', NULL),
(6, 'device', NULL),
(7, 'role', NULL),
(8, 'report', NULL),
(10, 'services', NULL),
(11, 'faults', NULL),
(12, 'permission', NULL),
(13, 'device locations', NULL),
(14, 'plan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `permission_name`) VALUES
(1, 'R'),
(2, 'A'),
(3, 'E'),
(4, 'D'),
(5, 'ED'),
(6, 'L');

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE IF NOT EXISTS `plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(250) DEFAULT NULL,
  `plan_description` varchar(500) DEFAULT NULL,
  `no_manager` int(11) DEFAULT NULL,
  `no_officer` int(11) DEFAULT NULL,
  `plan_price` varchar(200) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  `paid` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`plan_id`, `plan_name`, `plan_description`, `no_manager`, `no_officer`, `plan_price`, `is_deleted`, `is_active`, `paid`) VALUES
(1, 'Basic', 'In this plan you can add 1 manager and 5 officers.', 1, 5, '', 'N', 'Y', 'N'),
(2, 'Optimum', 'added 5 managers and 50 officers.', 5, 50, '20000', 'N', 'Y', 'Y'),
(3, 'Premium', 'In this plan you can add 20 managers and 200 officers.', 20, 200, '50000', 'N', 'Y', 'Y'),
(4, 'demo', 'testing', 3, 15, '9000', 'Y', 'N', 'Y'),
(5, 'Gold', 'AB', 0, 0, '', 'Y', 'N', 'N'),
(6, 'Silver', 'a', 2, 2, '', 'Y', 'N', 'N'),
(7, 'Silver', 'A', 2, 2, '', 'Y', 'N', 'N'),
(8, 'Demo', 'test', 3, 15, '9000', 'Y', 'N', 'Y'),
(9, 'Gold', 'test', 3, 9, '', 'Y', 'N', 'N'),
(10, 'Gold', 'test', 3, 9, '', 'Y', 'N', 'N'),
(11, 'Golden', 'test', 3, 9, '', 'N', 'N', 'N'),
(12, 'Gold', 'test', 3, 9, '', 'Y', 'N', 'N'),
(13, 'Gold', 'test', 3, 9, '', 'Y', 'N', 'N'),
(14, 'Gold', 'test', 3, 9, '', 'Y', 'N', 'N'),
(15, 'Gold', 'Test', 2, 6, '500', 'N', 'Y', 'Y'),
(16, 'test plan1', 'plan1', 100, 4, '40000', 'Y', 'N', 'Y'),
(17, 'Premium', 'This plan is premium and unlimited users can be created', 100, 100, '800', 'N', 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(200) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`report_id`),
  KEY `reports_fk` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rolepermission`
--

CREATE TABLE IF NOT EXISTS `rolepermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rolepermission_fk` (`role_id`),
  KEY `rolepermission_fk1` (`permission_id`),
  KEY `rolepermission_fk2` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2056 ;

--
-- Dumping data for table `rolepermission`
--

INSERT INTO `rolepermission` (`id`, `role_id`, `permission_id`, `module_id`) VALUES
(276, 0, 1, 3),
(278, 0, 2, 4),
(279, 0, 3, 4),
(281, 0, 1, 4),
(283, 0, 2, 5),
(284, 0, 3, 5),
(286, 0, 1, 5),
(288, 0, 2, 6),
(289, 0, 3, 6),
(291, 0, 1, 6),
(293, 0, 2, 10),
(294, 0, 3, 10),
(296, 0, 1, 10),
(1902, 1, 2, 2),
(1903, 1, 3, 2),
(1904, 1, 4, 2),
(1905, 1, 1, 2),
(1906, 1, 6, 2),
(1907, 1, 3, 3),
(1908, 1, 1, 3),
(1909, 1, 6, 3),
(1910, 1, 4, 5),
(1911, 1, 1, 5),
(1912, 1, 6, 5),
(1913, 1, 2, 7),
(1914, 1, 3, 7),
(1915, 1, 4, 7),
(1916, 1, 1, 7),
(1917, 1, 6, 7),
(1918, 1, 4, 8),
(1919, 1, 1, 8),
(1920, 1, 6, 8),
(1921, 1, 4, 10),
(1922, 1, 1, 10),
(1923, 1, 6, 10),
(1924, 1, 4, 12),
(1925, 1, 1, 12),
(1926, 1, 6, 12),
(1927, 1, 2, 14),
(1928, 1, 3, 14),
(1929, 1, 4, 14),
(1930, 1, 1, 14),
(1931, 1, 6, 14),
(1965, 2, 1, 2),
(1966, 2, 2, 3),
(1967, 2, 3, 3),
(1968, 2, 4, 3),
(1969, 2, 1, 3),
(1970, 2, 6, 3),
(1971, 2, 2, 4),
(1972, 2, 3, 4),
(1973, 2, 4, 4),
(1974, 2, 1, 4),
(1975, 2, 6, 4),
(1976, 2, 2, 6),
(1977, 2, 3, 6),
(1978, 2, 4, 6),
(1979, 2, 1, 6),
(1980, 2, 6, 6),
(1981, 2, 2, 8),
(1982, 2, 3, 8),
(1983, 2, 1, 8),
(1984, 2, 5, 8),
(1985, 2, 1, 10),
(1986, 2, 2, 11),
(1987, 2, 3, 11),
(1988, 2, 4, 11),
(1989, 2, 1, 11),
(1990, 2, 6, 11),
(1991, 2, 2, 12),
(1992, 2, 3, 12),
(1993, 2, 1, 12),
(1994, 2, 2, 13),
(1995, 2, 3, 13),
(1996, 2, 4, 13),
(1997, 2, 1, 13),
(1998, 2, 6, 13),
(2024, 4, 1, 2),
(2025, 4, 2, 3),
(2026, 4, 3, 3),
(2027, 4, 1, 3),
(2028, 4, 6, 3),
(2029, 4, 2, 4),
(2030, 4, 3, 4),
(2031, 4, 4, 4),
(2032, 4, 1, 4),
(2033, 4, 6, 4),
(2034, 4, 2, 6),
(2035, 4, 3, 6),
(2036, 4, 4, 6),
(2037, 4, 1, 6),
(2038, 4, 6, 6),
(2039, 4, 1, 12),
(2040, 4, 2, 13),
(2041, 4, 3, 13),
(2042, 4, 4, 13),
(2043, 4, 1, 13),
(2044, 4, 6, 13),
(2050, 3, 1, 2),
(2051, 3, 1, 3),
(2052, 3, 6, 3),
(2053, 3, 3, 4),
(2054, 3, 1, 4),
(2055, 3, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `is_deleted`, `is_active`) VALUES
(1, 'Super Admin', 'N', 'Y'),
(2, 'Company Admin', 'N', 'Y'),
(3, 'Cleaner', 'N', 'Y'),
(4, 'Supervisor', 'N', 'Y'),
(5, 'Cleaner', 'Y', 'N'),
(6, '123', 'Y', 'N'),
(7, '     ', 'Y', 'N'),
(8, 'Supervisor', 'Y', 'N'),
(9, '    ', 'Y', 'N'),
(10, '    ', 'Y', 'N'),
(11, '     ', 'Y', 'N'),
(12, '     ', 'Y', 'N'),
(13, '   Cleaner', 'Y', 'N'),
(14, 'Super Visit', 'Y', 'N'),
(15, 'Super', 'Y', 'N'),
(16, 'Super', 'Y', 'N'),
(17, 'Cleaner', 'Y', 'N'),
(18, 'Super', 'Y', 'N'),
(19, 'Cleaner', 'Y', 'N'),
(20, '    Cleaner', 'Y', 'N'),
(21, '    test role', 'Y', 'N'),
(22, 'test cleaner role', 'Y', 'N'),
(23, ' cleaner', 'Y', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(200) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `service_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `is_active` enum('Y','N') DEFAULT 'Y',
  PRIMARY KEY (`service_id`),
  KEY `services_fk` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `company_id`, `service_description`, `image`, `is_deleted`, `is_active`) VALUES
(1, 'Washrooms', 10, '		 Washroom feedbacks ', '1439531693_Sunset.jpg  ', 'N', 'Y'),
(2, 'Washroom', 12, NULL, NULL, 'Y', 'N'),
(3, 'Washroom', 22, 'Washroom feedbacks ', NULL, 'N', 'Y'),
(4, 'Washroom', 23, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(5, 'Washroom', 24, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(6, 'Washroom', 25, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(7, 'Washroom', 26, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(8, 'Washroom', 27, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(9, 'Washroom', 28, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(10, 'Washroom', 29, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(11, 'Washroom', 30, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(12, 'Washroom', 31, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(13, 'Washroom', 32, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(14, 'Washroom', 33, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(15, 'Washroom', 34, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(16, 'Washroom', 35, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(17, 'Washroom', 36, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(18, 'Washroom', 37, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(19, 'Washroom', 38, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(20, 'Washroom', 39, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(21, 'Washroom', 40, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(22, 'Washroom', 42, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(23, 'Washroom', 43, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(24, 'Washroom', 44, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(25, 'Washroom', 45, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(26, 'Washroom', 46, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(27, 'Washroom', 47, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(28, 'Washroom', 48, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(29, 'Washroom', 49, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(30, 'Washroom', 50, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(31, 'Washroom', 51, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(32, 'Washroom', 52, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(33, 'Washroom', 53, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(34, 'Washroom', 54, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(35, 'Washroom', 55, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(36, 'Washroom', 56, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(37, 'Washroom', 57, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(38, 'Washroom', 58, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(39, 'Washroom', 59, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(40, 'Washroom', 60, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(41, 'Washroom', 61, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(42, 'Washroom', 62, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(43, 'Washroom', 63, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(44, 'Washroom', 64, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(45, 'Washroom', 65, 'Washroom Feedbacks', NULL, 'Y', 'N'),
(46, 'Washroom', 66, 'Washroom Feedbacks', NULL, 'N', 'Y'),
(47, 'Washroom', 67, 'Washroom Feedbacks', NULL, 'N', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `service_faults`
--

CREATE TABLE IF NOT EXISTS `service_faults` (
  `service_id` int(11) DEFAULT NULL,
  `fault_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`fault_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `user_email` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `creation_date` varchar(50) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `profile_image` varchar(250) DEFAULT NULL,
  `is_deleted` enum('Y','N') DEFAULT 'N',
  `user_phone` varchar(20) DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `users_fk` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=198 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `user_email`, `password`, `creation_date`, `company_id`, `role_id`, `is_active`, `profile_image`, `is_deleted`, `user_phone`, `assigned_to`) VALUES
(1, 'Jemes', 'John', 'branding@gmail.com', 'obq0tauVZg==', '2015-08-01', 4, 2, 'N', ' ', 'Y', '741852963', NULL),
(3, 'John', 'Ross', 'techno12@gmail.com', 'obq0tauVZg==', '2015-08-05', 2, 2, 'N', NULL, 'Y', NULL, NULL),
(4, 'kevin', 'Thomas', 'spicejet@gmail.com', 'obq0tauVZg==', '2015-08-06', 10, 2, 'Y', ' ', 'N', '2147483647', 4),
(6, 'Adam', 'Smith', 'super_admin@gmail.com', 'nrm0r+iUZZc=', '2015-08-06', 1, 1, 'Y', '1450437601_cute boy.jpg     ', 'N', '3456129980', 0),
(7, 'Michel', 'Smith', 'nick23@gmail.com', 'obq0tauVZg==', '2015-08-07', 10, 3, 'N', '1439379717_Sunset.jpg   ', 'Y', '2156893622', 29),
(8, 'Smith', 'Crew', 'smith@gmail.com', 'obq0tauVZg==', '2015-08-07', 10, 4, 'Y', '1442656640_JPEG_20150123_072457_.jpg', 'N', '123456783', 4),
(9, 'sun', NULL, 'rajneesh.vyas@gmail.com', 'wp5Mg7', '2015-08-11', 11, 2, 'N', NULL, 'Y', NULL, NULL),
(11, 'frenk', 'thomas', 'frenk12@gmail.com', 'o8estOWUZZc=', '2015-08-12', 10, 3, 'Y', '1442231799_JPEG_20150123_074500_.jpg  ', 'N', '7597416291', 8),
(12, 'Devid ', 'Johnson', 'devid@gmail.com', 'obq0tauVZg==', '2015-08-13', 10, 3, 'N', '1439469054_Water lilies.jpg  ', 'Y', '748596321', 8),
(13, 'sunarc', 'sunarc', 'sun@gmail.com', 'obq0tauVZg==', '2015-08-19', 13, 2, 'N', ' ', 'Y', '2147483647', NULL),
(14, 'frenkin', 'frozen', 'frenk.frozen@gmail.com', 'obq0tauVZg==', '2015-08-11', 10, 3, 'N', '1440405872_Tulips.jpg    ', 'Y', '7418529632', 29),
(15, 'John Cena', NULL, 'johnc@gmail.com', 'obq0tauVZg==', '2015-08-25', 14, 2, 'N', NULL, 'Y', NULL, NULL),
(16, 'Kevin John', NULL, 'kevin@gmail.com', 'obq0tauVZg==', '2015-08-26', 15, 2, 'N', NULL, 'Y', NULL, NULL),
(17, 'Mack Thomas', NULL, 'mack@gmail.com', 'obq0tauVZg==', '2015-08-26', 16, 2, 'N', NULL, 'Y', NULL, NULL),
(18, 'John', 'Mete', 'mete@gmail.com', 'obq0tauVZg==', '2015-08-26', 12, 2, 'N', NULL, 'Y', '7418529632', NULL),
(19, 'Mete', 'Thomas', 'thomas@gmail.com', 'obq0tauVZg==', '2015-08-26', 12, 4, 'N', NULL, 'Y', '7418529632', NULL),
(20, 'Kim', 'Mete', 'kim@gmail.com', 'obq0tauVZg==', '2015-08-26', 12, 3, 'N', NULL, 'Y', '7418529632', NULL),
(21, 'hyy', 'Mete', 'hyy@gmail.com', 'obq0tauVZg==', '2015-08-26', 12, 3, 'N', NULL, 'Y', '7418529632', NULL),
(22, 'sunny', NULL, 'sunny@vmail.com', '7rYPLQ', '2015-08-27', 17, 2, 'N', NULL, 'Y', NULL, NULL),
(23, 'rony', NULL, 'rony@vmail.com', 'E3UXm4', '2015-08-27', 18, 2, 'N', NULL, 'Y', NULL, NULL),
(24, 'rony', NULL, 'rony@vmail.com', 'LIPPOJ', '2015-08-27', 19, 2, 'N', NULL, 'Y', NULL, NULL),
(25, 'jony', NULL, 'cyber@gmail.com', 'fuAFjX', '2015-08-27', 20, 2, 'N', NULL, 'Y', NULL, NULL),
(26, 'Tect COmpany', 'Test', 'testcompany@gmail.com', 'obq0tauVZg==', '2015-08-28', 21, 2, 'N', ' ', 'Y', '8529637414', NULL),
(27, 'zumbino', 'devid', 'zumbino@gmail.com', 'obq0tauVZg==', '2015-08-28', 22, 2, 'N', '1440744815_screenshot-{domain} {date} {time}.png    ', 'Y', '7418529632', 6),
(28, 'Devil', 'Gorge', 'devil@gmail.com', 'sMq1p+zGZJap', '2015-08-28', 22, 4, 'N', '1440746712_Screen Shot 2015-08-13 at 2.27.20 pm.png', 'Y', '2343253465645', NULL),
(29, 'jack', 'sparow', 'sprow@gmail.com', 'obq0tauVZg==', '2015-08-28', 10, 4, 'N', ' ', 'Y', '7418529632', 4),
(30, 'Jonh', 'Seeker', 'john@gmail.com', 'sMq1p+zGZJap', '2015-08-28', 22, 3, 'N', '1440767819_Next Tournaments   Events  .png  ', 'Y', '234324345', NULL),
(31, 'Marc', 'devid', 'marc@gmail.com', 'i8O3kt3GpJg=', '2015-08-28', 22, 4, 'N', '', 'Y', '243234324', NULL),
(32, 'Sunarc Admin', NULL, 'sunarc@gmail.com', 'obq0tauVZg==', '2015-08-31', 23, 2, 'N', NULL, 'Y', NULL, NULL),
(33, 'John ', 'Cruze', 'cruze@gmail.com', 'tpqUtPHElpg=', '2015-08-31', 10, 4, 'N', '', 'Y', '7418529632', 4),
(34, 'Michel', 'Jorden', 'michel@gmail.com', 'hpp9l8HWiZg=', '2015-08-31', 10, 3, 'N', '', 'Y', '8529637412', 33),
(35, 'jordan', 'james', 'jordan@gmail.com', 'p8S5qtvRZJap', '2015-08-31', 10, 3, 'N', '1446444590_I_Power_Logo_2_rgb.jpg', 'Y', '2345678901', 0),
(36, 'cfs', 'cleaner', 'cleaner@gmail.com', 'lc61krypZ5U=', '2015-09-01', 10, 3, 'N', '', 'Y', '7418529632', 29),
(37, 'cfs', 'supervisior', 'super@gmail.com', 'q6OMeMPce5g=', '2015-09-01', 10, 4, 'N', '', 'Y', '7418529632', 4),
(38, 'cfs', 'cleaner', 'test@gmail.com', 'hoy7t/Swrd4=', '2015-09-01', 10, 3, 'N', '', 'Y', '7418529632', 37),
(39, 'cfs', 'supervisior5', 'super5@gmail.com', 'hoyTfMavfJo=', '2015-09-01', 10, 4, 'N', '', 'Y', '7418529632', 4),
(40, 'cfs', 'supervisior6', 'super6@gmail.com', 'dMOdnOvXraU=', '2015-09-01', 10, 4, 'N', '', 'Y', '7418529632', 4),
(41, 'cfs', 'supervisior', 'super1@gmail.com', 'lZqTvcbXp9o=', '2015-09-01', 10, 4, 'N', '', 'Y', '7418529632', 4),
(42, 'cfs', 'cleaner1', 'cleaner1@gmail.com', 'tozBva7Lico=', '2015-09-01', 10, 3, 'N', '', 'Y', '7418529632', 33),
(43, 'cfs', 'cleaner2', 'cleaner2@gmail.com', 'q5qQePOXecc=', '2015-09-01', 10, 3, 'N', '', 'Y', '7418529632', 8),
(44, 'cfs', 'cleaner3', 'cleaner3@gmail.com', 'lYyffL+5aNE=', '2015-09-01', 10, 3, 'N', '1441188041_cloth_2.jpg', 'Y', '7418529632', 8),
(45, 'Sunny', NULL, 'sunny@vmail.com', 'ntrCid', '2015-09-04', 24, 2, 'N', NULL, 'Y', NULL, NULL),
(46, 'Rony', NULL, 'rony@vmail.com', 'XEqoW1', '2015-09-04', 25, 2, 'N', NULL, 'Y', NULL, NULL),
(47, 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW', NULL, 'sunny@vmail.com', 'tXHs7v', '2015-09-04', 26, 2, 'N', NULL, 'Y', NULL, NULL),
(48, 'Jony', NULL, 'jony@vmail.com', 'IINIpX', '2015-09-04', 27, 2, 'N', NULL, 'Y', NULL, NULL),
(49, 'Farhan', 'seikh', 'farhan@vmail.com', 'bod6eq+Z', '2015-09-04', 28, 2, 'N', ' ', 'Y', '7878787823', 0),
(50, 'Vinod', 'kumar', 'vinod@vmail.com', 'obq0tauVZg==', '2015-09-04', 29, 2, 'N', ' ', 'Y', '1234567890', 0),
(51, 'Sunarc', ' Admin', 'sunarc@gmail.com', 'obq0tauVZg==', '2015-09-05', 30, 2, 'N', ' ', 'Y', '7418529632', 0),
(52, 'jony', NULL, 'jony@vmail.com', 'I7MBNo', '2015-09-05', 31, 2, 'N', NULL, 'Y', NULL, NULL),
(53, 'mical', 'jeson', '', 'lcOynuWplJc=', '2015-09-05', 22, 4, 'N', '', 'Y', '1234567896', 27),
(54, 'denial', 'woolmer', 'denial@vmail.com', 'i8OtfPDLe80=', '2015-09-05', 22, 4, 'N', '', 'Y', '3265499665', 27),
(55, 'Preet', 'singh', 'preet@vmail.com', 'XXVnZpqD', '2015-09-07', 32, 2, 'N', ' ', 'Y', '8520147963', 0),
(56, 'denial', 'headin', 'denial@vmail.com', 'i8Ozp8zYn5g=', '2015-09-07', 32, 4, 'N', '', 'Y', '8521212112', 55),
(57, 'peter', 'woolmer', 'peter@vmail.com', 'iaO5ne2ZgZg=', '2015-09-07', 32, 4, 'N', '', 'Y', '8521212112', 55),
(58, 'Andy', 'flowerr', 'andy@vmail.com', 'hs6Pp9PUfZg=', '2015-09-07', 32, 4, 'N', '', 'Y', '7410235896', 55),
(59, 'Steve', 'waug', 'steve@vmail.com', 'bod6eq+Z', '2015-09-07', 32, 4, 'N', ' ', 'Y', '9630124587', 55),
(60, 'Henry', 'disuza', 'henry@vmail.com', 'iZ6KkcCqZpg=', '2015-09-07', 32, 4, 'N', '', 'Y', '9865320147', 55),
(61, 'cory', 'andersen', 'cory@vmail.com', 'bod6eq+Z', '2015-09-07', 32, 4, 'N', ' ', 'Y', '3201456987', 55),
(62, 'Shane', 'Watsen', 'shane@vmail.com', 'gq2PfM+uZ5U=', '2015-09-07', 32, 3, 'N', '', 'Y', '2014639875', 61),
(63, 'Mark', 'waug', 'mark@vmail.com', 'i8mIfeLNp5g=', '2015-09-07', 32, 3, 'N', ' ', 'Y', '5023146987', 59),
(64, 'andrew', 'finch', 'andrew@vmail.com', 'tpqzksHNdZg=', '2015-09-07', 32, 3, 'N', '', 'Y', '5102364789', 61),
(65, 'Paul', 'ray', 'paul@vmail.com', 'ks6+qt2oZ5U=', '2015-09-07', 32, 3, 'N', '', 'Y', '5410236987', 61),
(66, 'Hancy', 'cronze', 'hancy@vmail.com', 'i5q6qrDGgpg=', '2015-09-07', 32, 3, 'N', ' ', 'Y', '5203614789', 59),
(67, 'ishan', 'benja', 'ishan@vmail.com', 'toyQeODapts=', '2015-09-07', 32, 3, 'N', '     ', 'Y', '6023145789', 60),
(68, 'Stephen', 'flemig', 'stephen@vmail.com', 'bod6eq+Z', '2015-09-07', 33, 2, 'N', ' ', 'N', '8025461379', 0),
(69, 'Alex', 'core', 'alex@vmail.com', 'i62WfO/MqbI=', '2015-09-07', 33, 4, 'N', '', 'Y', '2580316497', 68),
(70, 'alexendra', 'paul', 'alexa@vmail.com', 'gqGWfMG3Z5g=', '2015-09-07', 33, 3, 'N', '', 'Y', '7014653298', 69),
(71, 'Alex', 'paul', 'alex@vmail.com', 'gqG5fOmolJs=', '2015-09-07', 33, 4, 'N', '', 'Y', '8053614297', 68),
(72, 'Alexendra', 'paul', 'alexa@vmail.com', 'tq2+vcC5ddM=', '2015-09-07', 33, 3, 'N', '', 'Y', '8063259147', 69),
(73, 'Alex', 'pal', 'alex@vmail.com', 'dKOzfNHSopg=', '2015-09-07', 33, 4, 'N', '', 'Y', '8023651479', 68),
(74, 'Alex', 'pal', 'alex@vmail.com', 'i5qUwNunh5U=', '2015-09-07', 33, 4, 'N', '', 'Y', '8520134679', 68),
(75, 'Alex', 'pal', 'alex@vmail.com', 'la2KfdK9d5Y=', '2015-09-07', 33, 4, 'N', '', 'Y', '8520134679', 68),
(76, 'grace', 'tomer', 'grace@vmail.com', 'obq0tauVZg==', '2015-09-07', 34, 2, 'N', ' ', 'Y', '8520146398', 0),
(77, 'denial', 'headin', 'denial@vmail.com', 'rsmruerPe5s=', '2015-09-07', 33, 3, 'Y', '', 'N', '8521212112', 69),
(78, 'Andy', 'disuza', 'andy@vmail.com', 'tsOtu+rHq7o=', '2015-09-07', 33, 4, 'Y', '', 'N', '7410235896', 68),
(79, 'venu', 'gopal', 'venu@vmail.com', 'bod6eq+Z', '2015-09-08', 35, 2, 'N', '  ', 'Y', '95123047643', 0),
(80, 'jack', 'chain', 'jack@vmail.com', 'obq0tauVZg==', '2015-09-08', 34, 4, 'N', ' ', 'Y', '4012369875', 76),
(81, 'james', 'thomas', 'james@gmail.com', 'p7a0q+2UZZc=', '2015-09-10', 10, 3, 'Y', '1442233706_JPEG_20150123_063049_.jpg ', 'N', '2356897413', 8),
(82, 'Harendar singh', 'rawat', 'hari@vmail.com', 'pba5r6uVZg==', '2015-09-10', 10, 3, 'Y', '1442232714_JPEG_20150123_074500_.jpg     ', 'N', '1234567809', 8),
(83, 'Testing', 'Test', 'testing@gmail.com', 'i7uzsN7WZ5Y=', '2015-09-14', 10, 4, 'N', '', 'Y', '7418529632', 4),
(84, 'Jack', 'Njill', 'jill@gmail.com', 'i5aoieCoZ5Y=', '2015-09-15', 34, 4, 'N', '', 'Y', '7894561233', 76),
(85, 'humpty', 'dumpty', 'dumpty@gmail.com', 'goq6nt3KnZg=', '2015-09-15', 34, 3, 'N', '', 'Y', '7418529632', 84),
(86, 'John', 'Sparrow', 'sparow@gmail.com', 'goi6nsfGat0=', '2015-09-15', 34, 4, 'Y', '', 'N', '7418529632', 76),
(87, 'Sunarc', 'Test', 'sun@gmail.com', 'ib+5wMDJet4=', '2015-09-15', 34, 4, 'N', '', 'Y', '7418529632', 76),
(88, 'Sunarc', 'Test', 'sun@g.com', 'o4i+wMDdZ5Y=', '2015-09-15', 34, 3, 'N', '', 'Y', '7418529632', 84),
(89, 'Sand', 'Denis', 'denis@gmail.com', 'rruOeM+wrJg=', '2015-09-15', 34, 3, 'N', '', 'Y', '7418529632', 84),
(90, 'Johny', 'Bravo', 'bravo@gmail.com', 'iYqPwOKvo6s=', '2015-09-15', 34, 3, 'N', '', 'Y', '8529637414', 84),
(91, 'Wills', 'Smith', 'smithwill@gmail.com', 'q7u4ueq3d5g=', '2015-09-15', 34, 3, 'N', '', 'Y', '8529637414', 84),
(92, 'trrer', 'rtertertre', 'johnc@gmail.com', 'scGov+2VnZg=', '2015-09-15', 34, 3, 'N', '', 'Y', '8529637414', 84),
(93, 'trrer', 'Test', 'super_admin@gmail.com', 'i4a4qby6o7o=', '2015-09-15', 34, 4, 'N', '', 'Y', '8529637414', 76),
(94, 'Sunarc', 'rtertertre', 'super_admin@gmail.com', 'goqVidHHl9s=', '2015-09-15', 34, 3, 'N', '', 'Y', '8529637414', 84),
(95, 'Sunarc', 'Cleaner', 'cleaner@gmail.com', 'lbuVjeDSf6o=', '2015-09-15', 34, 3, 'N', '', 'Y', '7418529632', 84),
(96, 'jonty', 'rodes', 'jonty@vmail.com', 'lbuQm87KhLY=', '2015-09-16', 32, 3, 'N', ' ', 'Y', '9889896565', 59),
(97, 'mark', 'minja', 'mark@vmail.com', 'obq0tauVZg==', '2015-09-16', 36, 2, 'N', '    ', 'Y', '980987654', 0),
(98, 'goldy', 'viz', 'goldy@vmail.com', 'bod6eq+Z', '2015-09-16', 37, 2, 'N', ' ', 'Y', '9870654321', 0),
(99, 'wilson', 'ray', 'wilson@vmail.com', 'hoi6vubMZpg=', '2015-09-16', 28, 4, 'N', '', 'Y', '9886765641', 49),
(100, 'nilson', 'rey', 'nilson@vmail.com', 'tpafq9/XmZg=', '2015-09-16', 28, 3, 'N', '', 'Y', '8790654321', 99),
(101, 'super ', 'company', 'super@vmail.com', 'bod6eq+Z', '2015-09-16', 38, 2, 'N', ' ', 'N', '7418529632', 0),
(102, 'Dev', 'gor', 'dev@vmail.com', 'bod6eq+Z', '2015-09-16', 39, 2, 'N', ' ', 'Y', '9866553341', 0),
(103, 'sam', 'patoda', 'sam@vmail.com', 'i4h4iOLKZ5Y=', '2015-09-16', 39, 4, 'N', '', 'Y', '8976598764', 102),
(104, 'nick', 'mida', 'nick@vmail.com', 'toiPl9HLhJg=', '2015-09-16', 39, 3, 'N', ' ', 'Y', '5678901234', 0),
(105, 'vicky', 'waugh', 'vicky@vmail.com', 'sbu4uurEqpg=', '2015-09-16', 39, 3, 'N', '', 'Y', '4646745623', 103),
(106, 'jonty', 'mida', 'jonty1@vmail.com', 'sb+ftNGun5g=', '2015-09-16', 39, 3, 'N', '', 'Y', '3434545466', 103),
(107, 'brian', 'para', 'brian@vmail.com', 'tqqQp67Ylq0=', '2015-09-16', 39, 3, 'N', '', 'Y', '3743984793', 103),
(108, 'wave', 'tarang', 'wave@vmail.com', 'i5a5iOqtapg=', '2015-09-16', 39, 3, 'N', '', 'Y', '6776899000', 103),
(109, 'nick', 'mida', 'nick@vmail.com', 'hsG4uvHNf7s=', '2015-09-16', 39, 3, 'N', ' ', 'Y', '4646745622', 110),
(110, 'peter', 'vidal', 'peter@vmail.com', 'bod6eq+Z', '2015-09-16', 39, 4, 'N', ' ', 'Y', '8137434730', 102),
(111, 'Mack', NULL, 'mack@vmail.com', 'k3qaBX', '2015-09-16', 40, 2, 'N', NULL, 'Y', NULL, NULL),
(112, 'John', 'Cena', 'johnc@gmail.com', 'bod6eq+Z', '2015-09-16', 41, 2, 'N', ' ', 'Y', '7418529632', 6),
(113, 'John Cena', NULL, 'johnc@gmail.com', 'N3EOoj', '2015-09-16', 42, 2, 'N', NULL, 'Y', NULL, 6),
(114, 'Sunarc', 'Test', 'test@gmail.com', 'gs69u9LWass=', '2015-09-17', 10, 3, 'N', '', 'Y', '7418529632', 83),
(115, 'Preet', NULL, 'preet@vmail.com', 't5Pnmj', '2015-09-21', 43, 2, 'N', NULL, 'Y', NULL, 6),
(116, 'Preet', NULL, 'preet@vmail.com', 'yljFif', '2015-09-21', 44, 2, 'N', NULL, 'Y', NULL, 6),
(117, 'Preet', NULL, 'preet@vmail.com', 'yAWFhm', '2015-09-21', 45, 2, 'N', NULL, 'Y', NULL, 6),
(118, 'Preet', 'jon', 'preet@vmail.com', 'bod6eq+Z', '2015-09-21', 46, 2, 'Y', ' ', 'N', '9870123456', 110),
(119, 'john', 'mario', 'john@vmail.com', 'dIaUusXQhto=', '2015-09-21', 46, 4, 'Y', '', 'N', '7530124589', 118),
(120, 'jack', 'mario', 'jack@vmail.com', 'laqojN6qo5g=', '2015-09-21', 46, 4, 'Y', '', 'N', '2013584796', 118),
(121, 'jenny', 'dsuza', 'jenny@vmail.com', 'q6qxl+2sobs=', '2015-09-21', 46, 4, 'Y', '', 'N', '8520316497', 118),
(122, 'rex', 'jon', 'rex@vmail.com', 'bod6eq+Z', '2015-09-21', 47, 2, 'Y', '  ', 'N', '1023654789', 29),
(123, 'Den', 'zen', 'den@vmail.com', 'hsGXr8uwq5s=', '2015-09-21', 47, 4, 'N', '', 'Y', '4025136879', 122),
(124, 'ken', 'zing', 'ken@vmail.com', 'ts6UrbzadJg=', '2015-09-21', 47, 4, 'Y', '', 'N', '7532014698', 122),
(125, 'sam', 'ming', 'sam@vmail.com', 'i86etO6Xdpg=', '2015-09-21', 47, 4, 'Y', '', 'N', '5412036987', 122),
(126, 'Ben', 'ten', 'ben@vmail.com', 'i8Geqt22eZg=', '2015-09-21', 47, 4, 'Y', '', 'N', '9032145687', 122),
(127, 'rock', 'martin', 'rock@vmail.com', 'dL+euunXfq4=', '2015-09-21', 47, 4, 'Y', '', 'N', '7089654123', 122),
(128, 'den', 'zen', 'den@vmail.com', 'dL9+m+Koppo=', '2015-09-21', 47, 4, 'Y', '    ', 'N', '6540123987', 122),
(129, 'Sarah', 'Smith', 'new1@xyz.com', 'bod6eq+Z', '2015-09-22', 48, 2, 'Y', ' ', 'N', '1234567890', 6),
(130, 'Micheal', 'smith', 'micheal@xyz.com', 'bod6eq+Z', '2015-09-22', 48, 4, 'Y', ' ', 'N', '1234567890', 129),
(131, 'David', 'lucas', 'david@xyz.com', 'hqq3ub+wm6o=', '2015-09-22', 48, 3, 'Y', '', 'N', '3214567890', 130),
(132, 'robert', 'junior', 'robert@xyz.com', 'hs6yia/Woc8=', '2015-09-22', 48, 3, 'Y', '', 'N', '9871234560', 130),
(133, 'Ranveer', 'Kapoor', 'ranveer@gmail.com', 'r7a1vN/IpZWolg==', '2015-11-03', 10, 4, 'Y', ' ', 'N', '3456789043', 4),
(134, 'Daniel', 'james', 'cfs@gmail.com', 'sMq1p+zGZJap', '2015-11-03', 49, 2, 'N', '1446620653_logo-standing.png', 'Y', '1234567890', 6),
(135, 'Robert', 'lee', 'mindtree@gmail.com', 'sMq1p+zGZJap', '2015-11-03', 50, 2, 'Y', '1446620596_good-active.png', 'N', '3456789043', 6),
(136, 'Mayank', 'Vyas', 'mayank@gmail.com', 'bod6eq+Z', '2015-11-04', 50, 4, 'Y', '  ', 'N', '1234567890', 135),
(137, 'Mihika', 'Kapoor', 'mihika@gmail.com', 'sMq1p+zGZJap', '2015-11-04', 50, 3, 'Y', '  ', 'N', '1234567809', 136),
(138, 'sarad', 'Singh', 'sarad@gmail.com', 'sMq1p+zGZJap', '2015-11-04', 50, 4, 'Y', '  ', 'N', '3456789043', 135),
(139, 'Neel', 'tomar', 'neel@gmail.com', 'sMq1p+zGZJap', '2015-11-04', 50, 3, 'Y', ' ', 'N', '4567890123', 138),
(140, 'abhijeet', 'chura', 'jeet@gmail.com', 'sMq1p+zGZJap', '2015-11-04', 50, 3, 'Y', ' ', 'N', '1234567890', 136),
(141, 'vishal', 'sharma', 'vishal@gmail.com', 'sMq1p+zGZJap', '2015-11-04', 50, 3, 'Y', '   ', 'N', '2345678901', 138),
(142, 'Yash', 'Raj', 'dhrma@gmail.com', 'sMq1p+zGZJap', '2015-11-05', 51, 2, 'N', '    ', 'Y', '3456789012', 0),
(143, 'Yash', 'Kapoor', 'dhrma@gmail.com', 'bod6eq+Z', '2015-11-05', 52, 2, 'N', ' ', 'Y', '1234567890', 6),
(144, 'Dhruv', 'Test', 'sunarc@gmail.com', 'bod6eq+Z', '2015-11-05', 53, 2, 'Y', '1446729334_Innomaid 2015-11-04 12-17-07.png', 'N', '12345678', 1),
(145, 'Pradeep', 'Singh', 'prageep@gmail.com', 'bod6eq+Z', '2015-11-05', 53, 4, 'Y', ' ', 'N', '1234567890', 144),
(146, 'John ', 'Sunarc', 'john@gmail.com', 'bod6eq+Z', '2015-11-05', 53, 3, 'Y', ' ', 'N', '1242354356', 145),
(147, 'Virat', 'Singh', 'tcs@gmail.com', 'bod6eq+Z', '2015-11-09', 54, 2, 'Y', '1448628326_dream.jpg    ', 'N', '3456789044', 6),
(148, 'Ranveer', 'Goyal', 'goyal@gmail.com', 'bod6eq+Z', '2015-11-09', 54, 4, 'Y', '  ', 'N', '4567890123', 147),
(149, 'james', 'Lee', 'jameslee@gmail.com', 'bod6eq+Z', '2015-11-09', 54, 3, 'Y', '  ', 'N', '3456789043', 148),
(150, 'Jatin', 'Malhotra', 'jatin@gmail.com', 'bod6eq+Z', '2015-11-30', 54, 3, 'Y', '1448861538_dream.jpg ', 'N', '1234568909', 148),
(151, 'Mohit', 'Chouhan', 'mohit@gmail.com', 'bod6eq+Z', '2015-11-30', 54, 3, 'N', '1448861662_perfectshot.jpg ', 'N', '3456129980', 148),
(152, 'Devendra', 'Singh', 'dev@gmail.com', 'lb+8d/Stfcc=', '2015-12-01', 54, 3, 'N', '', 'Y', '1234568909', 148),
(153, 'Virat', 'kumar', 'virat@gmail.com', 'lbayndC6qso=', '2015-12-01', 54, 3, 'N', '', 'Y', '1234568909', 148),
(154, 'tushar', 'mishar', 'tushar@gmail.com', 'grZ4l+TRfZg=', '2015-12-01', 54, 3, 'N', '', 'Y', '3456129980', 148),
(155, 'tarun', 'aggarwal', 'tarun@gmail.com', 's6Ktmu7Sprw=', '2015-12-01', 54, 3, 'N', '', 'Y', '3456789033', 148),
(156, 'murad', 'bhati', 'murad@gmail.com', 'q6yVuN+zgKc=', '2015-12-01', 54, 3, 'N', '', 'Y', '3456129980', 148),
(157, 'tushar', 'Malhotra', 'tushar@gmail.com', 'bod6eq+Z', '2015-12-01', 54, 3, 'N', '1449474426_dream.jpg', 'Y', '3456129980', 148),
(158, 'Devendra', 'bhati', 'dev@gmail.com', 'iax4uNvJ', '2015-12-01', 54, 3, 'N', '', 'Y', '1234568909', 148),
(159, 'dyan', NULL, 'friendzone@gmail.com', 'dLZ6nsCo', '2015-12-01', 55, 2, 'N', NULL, 'Y', NULL, 6),
(160, 'dyan', 'Smith', 'fzi@gmail.com', 'sMq1d6yW', '2015-12-01', 56, 2, 'N', ' ', 'Y', '3456129980', 6),
(161, 'tushar', 'bhati', 'bhati@gmail.com', 'bod6eq+Z', '2015-12-01', 56, 4, 'N', ' ', 'Y', '3456129980', 160),
(162, 'Dion', 'ten', 'dion@gmail.com', 'sMq1p+zGZJap', '2015-12-02', 57, 2, 'Y', ' ', 'Y', '2342453565', 6),
(163, 'Uren', 'chen', 'uren@gmail.com', 'icqbffGzaZg=', '2015-12-02', 57, 4, 'Y', '', 'N', '2134432533', 162),
(164, 'Joy', 'Smith', 'fzi@gmail.com', 'bod6eq+Z', '2015-12-02', 58, 2, 'Y', ' ', 'N', '3456129980', 6),
(165, 'Mohit', 'verma', 'verma@gmail.com', 'bod6eq+Z', '2015-12-02', 58, 4, 'Y', '1449057967_dream.jpg', 'N', '3456129980', 164),
(166, 'ajay', 'singh', 'ajay@gmail.com', 'iZ29l8XbhZg=', '2015-12-02', 58, 3, 'Y', '1449058379_dream.jpg', 'N', '3456129980', 165),
(167, 'Devendra', 'Smith', 'tcs1@gmail.com', 'q8p7s7uvgpg=', '2015-12-02', 58, 4, 'Y', '1449059616_dream.jpg', 'N', '3456129980', 164),
(168, 'sumant', 'bhati', 'bhati1@gmail.com', 'hqKpvfSsZ50=', '2015-12-02', 58, 3, 'Y', '1449060373_dream.jpg', 'N', '3456129980', 167),
(169, 'Jeet', 'vyas', 'jeet@gmail.com', 'lZ23sMOXbJo=', '2015-12-02', 58, 3, 'Y', '', 'N', '3456129980', 167),
(170, 'test admin', 'Smith', 'test@gmail.com', 'bod6eq+Z', '2015-12-02', 59, 2, 'N', '1449062102_dream.jpg', 'Y', '1234568909', 6),
(171, 'Tushar', 'Malhotra', 'tushar@gmail.com', 'bod6eq+Z', '2015-12-07', 54, 3, 'Y', '       ', 'N', '345-612-9927', 148),
(172, 'Tejas', 'Gulmohar', 'gulmohar@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 2, 'Y', '1449554291_image3.jpg ', 'N', '(123) 456 7890', 6),
(173, 'Rounak', 'Vyas', 'rounak@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 4, 'N', '1449554686_image5.jpg ', 'Y', '123-456-8945', 172),
(174, 'Vijay', 'Kumar', 'vj@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 4, 'Y', '1449554784_image9.jpg ', 'N', '1234568909', 172),
(175, 'Mohit', 'Kapoor', 'mohit@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 3, 'N', '1449554892_image11.jpg   ', 'Y', '1234568555', 173),
(176, 'Rajat', 'Bhati', 'rajat@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 3, 'N', '1449555308_image8.jpg ', 'Y', '3456789033', 173),
(177, 'Manshu', 'Maan', 'manshu@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 3, 'Y', '1449555381_image12.jpg ', 'N', '3456129980', 174),
(178, 'Narpen', 'Joshi', 'narpen@gmail.com', 'bod6eq+Z', '2015-12-08', 60, 3, 'Y', '1449555523_image15.png ', 'N', '1234568908', 174),
(179, 'Ashwini', 'Tanwar', 'ashu@gmail.com', 'hoiver+XrJY=', '2015-12-08', 60, 3, 'N', '', 'Y', '3456129980', 173),
(180, 'Amit', 'Kumar', 'amit@gmail.com', 'i8Sev/TVoa0=', '2015-12-08', 60, 3, 'N', '', 'Y', '1234567777', 174),
(181, 'vipin', 'kanojiya', 'vipin@gmail.com', 'tqKUk8avgJc=', '2015-12-09', 60, 4, 'N', '', 'Y', '3456129980', 172),
(182, 'vasim', NULL, 'newtype@xyz.com', 'qKahkMa3', '2015-12-14', 61, 2, 'Y', NULL, 'Y', NULL, 6),
(183, 'Jaydeep', 'vyas', 'grand@abc.com', 'c5d5l+TM', '2015-12-14', 62, 2, 'Y', ' ', 'N', '3456789024', 6),
(184, 'smith', 'john', 'smith@xyz.com', 'bod6eq+Z', '2015-12-15', 63, 2, 'Y', '1450177453_101_1.jpg   ', 'N', '345-612-9925', 6),
(185, 'sarah', 'smith', 'sarah@xyz.com', 'bod6eq+Z', '2015-12-15', 63, 4, 'Y', '  ', 'N', '1234567890', 184),
(186, 'test', 'testing', 'test@xyz.com', 'q8Kwkuq1aJU=', '2015-12-15', 63, 4, 'N', '', 'Y', '1234567890', 184),
(187, 'Devendra', 'Pandit', 'devendra@abc.com', 'gMl7rMbNgpk=', '2015-12-16', 63, 3, 'Y', '', 'N', '3456129980', 185),
(188, 'Joy', 'Smith', 'nq@gmail.com', 'bod6eq+Z', '2015-12-16', 64, 2, 'Y', '1450260397_1446543987_avg-active.png', 'N', '(345) 612-9926', 6),
(189, 'Daniel', 'Redcliffe', 'itc@gmail.com', 'bod6eq+Z', '2015-12-16', 65, 2, 'Y', '     ', 'N', '123-456-8943', 6),
(190, 'michal', 'clark', 'clark@gmail.com', 'bod6eq+Z', '2015-12-16', 65, 4, 'Y', ' ', 'N', '4567890234', 189),
(191, 'dyan', 'warn', 'dyan@gmail.com', 'bod6eq+Z', '2015-12-16', 65, 3, 'Y', ' ', 'N', '4567892288', 190),
(192, 'Adward', 'swan', 'adward@gmail.com', 'bod6eq+Z', '2015-12-16', 65, 4, 'Y', ' ', 'N', '4537899283', 189),
(193, 'Henrry', 'potter', 'henrry@gmail.com', 'bod6eq+Z', '2015-12-16', 65, 3, 'Y', ' ', 'N', '6754321900', 192),
(194, 'Mike', 'Silvester', 'vineet.jain@sunarctechnologies.com', 'bod6eq+Z', '2015-12-18', 66, 2, 'Y', '1450440522_cute boy.jpg', 'N', '5555555555', 6),
(195, 'Candice', 'Alva', 'vineet.jain@sunarctechnologies.com', 'f529ku23oZk=', '2015-12-18', 66, 4, 'Y', '1450439578_cute boy.jpg', 'N', '9999999999', 194),
(196, 'Neil', 'Silvester', 'neil@gmail.com', 'bod6eq+Z', '2015-12-18', 66, 3, 'Y', '1450440021_cute boy.jpg ', 'N', '9999999999', 195),
(197, 'Roddyrexx', NULL, 'roddy@gmail.com', 't863m8vZ', '2016-01-08', 67, 2, 'Y', NULL, 'N', NULL, 6);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `device`
--
ALTER TABLE `device`
  ADD CONSTRAINT `device_fk` FOREIGN KEY (`company_id`) REFERENCES `company_detail` (`company_id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_fk` FOREIGN KEY (`company_id`) REFERENCES `company_detail` (`company_id`);

--
-- Constraints for table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `module_fk` FOREIGN KEY (`company_id`) REFERENCES `company_detail` (`company_id`);

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_fk` FOREIGN KEY (`company_id`) REFERENCES `company_detail` (`company_id`);

--
-- Constraints for table `rolepermission`
--
ALTER TABLE `rolepermission`
  ADD CONSTRAINT `rolepermission_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  ADD CONSTRAINT `rolepermission_fk1` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`),
  ADD CONSTRAINT `rolepermission_fk2` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`);

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_fk` FOREIGN KEY (`company_id`) REFERENCES `company_detail` (`company_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
