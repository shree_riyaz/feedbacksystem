<center>
<br />
<table width="90%" align='center'>
	<tr>
		<td class="mainhead">
			<?php echo ucfirst($Row->event_name); ?>
		</td>
	</tr>
	<?php
		$date=date_create($Row->event_date);
		$date=date_format($date,'d-M-Y');
	?>
	<tr>
		<td align="right"><?php echo $date; ?></td>
	</tr>
	<tr>
		<td>
		<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="tablecss">
		<tr class="tblheading">
		<th class="anth"height="36" width="10%" align="center">S. No.</th>
		<th  align="center" class="anth"> 
		<a  style="color:#ffffff; text-decoration:underline;cursor:pointer">Documents </a>
		</th>
		<th  align="center" class="anth" style="color:#ffffff">&nbsp;Action </th>
	   </tr>	
		<?php 
		$folderpath = ROOT."uploads/";
		$urlpath=ROOTUSERURL."uploads/";
		//print_r($Doc);
		if($Doc)
		{
				$x=1;
				$c=count($Doc);
				for($i=0;$i<$c;$i++)
				{
			?>
					<?php $str=substr($Doc[$i]->name,0,strrpos($Doc[$i]->name,"."));?>
					<tr height="20">
					<td align="center"><?php echo $x++; ?></td>
					<td align='center'>
					<input style='display:none;' type='checkbox' name='documents[]' id='document_<?php echo $Doc[$i]->id; ?>' value='<?php echo $Doc[$i]->id; ?>' />
					<a href="<?php echo $urlpath.$Doc[$i]->name; ?>" target='_blank' >
					<span id="span_<?php echo $Doc[$i]->id; ?>" ><?php echo substr($str,stripos($str,"_")+1); ?>
					</span></a>
					</td>
					<td style="cursor:pointer" align='center'>
					<a target='_blank' href="<?php echo $urlpath.$Doc[$i]->name; ?>" title="Download">Download</a>
					</td>
			<?php
				}
		
		}
        else
		{
		?>
		<tr>
		 <td colspan='3' align='center'> No document found.</td>
	    </tr>		
	   <?php 		
		}		
		
		if($_SESSION['usertype'] == 'admin')
		{
		?>
		<tr>
		 <td colspan='3' align='center'> <input type='button' onclick="location.href='index.php?mode=event';" value='Back'/></td>
	    </tr>		
	   <?php 
	   } 
	   ?>
		</table>
		</td>
	</tr>
</table>	
<br />	
<table align="left" style="margin-left:50px;">
	<tr>
		<td align="left" style="font-weight:bold">Upcoming Events:</td>
	</tr>
	<tr><td align="left"><a href="http://www.theopriskpractice.com/orp-indonesia" target="_blank">Essential Advanced Strategic & Execution Practices to Ensure Your Business is Fit-for-Growth </a></td></tr>	
	<tr><td align="left">13-14 May, 2013</td></tr>
	<tr><td align="left">Jakarta, Indonesia</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td align="left"><a href="http://www.theopriskpractice.com/business-process-excellence" target="_blank">Business Process Excellence for Financial Institutions </a></td></tr>
	<tr><td align="left">27th-28th May, 2013</td></tr>
	<tr><td align="left">Singapore</td></tr>
</table>	



<br>



</div>	

</center>