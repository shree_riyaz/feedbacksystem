<?php

if (isset($frmdata['show_chart_submit'])) {
    $_SESSION['selected_date_range_session'] = $_POST['selected_date_range'];
    $selected_date = $_POST['selected_date_range'];


    $selected_date_interval = $_POST['selected_date_range_type_hidden'];
    if ($selected_date_interval == 'Yesterday') {
        $_SESSION['start_date_by_interval'] = "moment().subtract(1, 'days')";
        $_SESSION['end_date_by_interval'] = "moment().subtract(1, 'days')";
    } elseif ($selected_date_interval == 'Today') {
        $_SESSION['start_date_by_interval'] = "moment()";
        $_SESSION['end_date_by_interval'] = "moment()";
    } elseif ($selected_date_interval == 'Last 7 Days') {
        $_SESSION['start_date_by_interval'] = "moment().subtract(6, 'days')";
        $_SESSION['end_date_by_interval'] = "moment()";
    } elseif ($selected_date_interval == 'Last 30 Days') {
        $_SESSION['start_date_by_interval'] = "moment().subtract(30, 'days')";
        $_SESSION['end_date_by_interval'] = "moment()";
    } elseif ($selected_date_interval == 'This Month') {
        $_SESSION['start_date_by_interval'] = "moment().startOf('month')";
        $_SESSION['end_date_by_interval'] = "moment().endOf('month')";
    } elseif ($selected_date_interval == 'Last Month') {
        $_SESSION['start_date_by_interval'] = "moment().subtract(1, 'month').startOf('month')";
        $_SESSION['end_date_by_interval'] = "moment().subtract(1, 'month').endOf('month')";
    }/*elseif ($selected_date_interval == 'Custom Range'){
                $_SESSION['start_date_by_interval'] = "8/8/18";
                $_SESSION['end_date_by_interval'] = "8/15/18";
            }*//*else{
                $_SESSION['start_date_by_interval'] = "moment().subtract(1, 'month').startOf('month')";
                $_SESSION['end_date_by_interval'] = "moment().subtract(1, 'month').endOf('month')";
            }*/

//            print_r($_POST['selected_date_range_type_hidden']);
//            echo '<br>';
//            print_r($_SESSION['start_date_by_interval']);
//            echo '<br>';
//            print_r($_SESSION['end_date_by_interval']);

//            exit;
//            $CandidateMasterOBJ->chart_function($nameID);
    $split_date = explode('-', $selected_date);
    $get_start_date = trim($split_date[0]);
    $get_end_date = trim($split_date[1]);
    $start_date = date('Y-m-d', strtotime($get_start_date));
    $end_date = date('Y-m-d', strtotime($get_end_date));
    $_SESSION['start_date'] = $start_date;
    $_SESSION['end_date'] = $end_date;
} else {
    $_SESSION['start_date'];
    $_SESSION['end_date'];
//            $end_date = date('Y-m-d');
//            $start_date = date('Y-m-d', strtotime('today - 30 days'));
}

//echo $_SESSION['start_date'];
//echo '<br>';
//echo $_SESSION['end_date'];
//exit;

if ($_REQUEST['call'] != 'API') {
    $permission = $_SESSION['permission'];
    $do = (isset($_REQUEST['do']) ? $_REQUEST['do'] : 'list');
    $id = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
    if ($do == 'del' && $_SESSION['usertype'] == 'company_admin')
        $permission = 'del';
    $userAlowedActions = array($permission);
    //print_r($userAlowedActions);
    if (!in_array($do, $userAlowedActions)) {
        ?>
        <!--	<script type="text/javascript">-->
        <!--	alert('Your are not allowed to perform this action');-->
        <!--		document.location.href="--><?php ///*echo 'index.php?mod=company&do=view&id='.$_SESSION['company_id']; */
        ?>
        <!--//	</script>-->
        <?php
//	   exit;
    }
}
$company = $DBFilter->SelectRecords('company_detail');
$admin_company = $DBFilter->SelectRecord('company_detail', "is_deleted='N' and is_active='Y' and company_id=" . $_SESSION['company_id']);
$loc = $DBFilter->SelectRecords('device_locations', "is_deleted='N' and is_active='Y' and  company_id=" . $_SESSION['company_id']);
$users = $DBFilter->SelectRecords('users', "is_deleted='N' and is_active='Y' and role_id = '4' and company_id=" . $_SESSION['company_id']);
include_once('function.php');
//echo $do; exit;
/**
 * @return string
 */
function check_date_today_yesterday_or_other()
{
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');
    if ($_SESSION['start_date'] == $today) {

        $between_date = "(DATE(fd.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } elseif ($_SESSION['start_date'] == $yesterday) {
        $between_date = "(DATE(fd.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } else {
        $between_date = "(fd.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')";
        return $between_date;
    }
}
function check_date_today_yesterday_or_other_without_alias()
{
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $today = date('Y-m-d');
    if ($_SESSION['start_date'] == $today) {

        $between_date = "(DATE(feedback.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } elseif ($_SESSION['start_date'] == $yesterday) {
        $between_date = "(DATE(feedback.created_at) ='" . $_SESSION['start_date'] . "')";
        return $between_date;
    } else {
        $between_date = "(feedback.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "')";
        return $between_date;
    }
}


switch ($do) {

    case 'logout':
        unset($_SESSION['usertype']);
        $CFG->template = "device/logout.php";
        break;
    case 'interval':
        $Row = interval_based();
        $CFG->template = "reports/interval.php";
        break;
    case 'location_wise_report':
        $Row = location_based_report();
        $CFG->template = "reports/location_based_feedback.php";
        break;
    case 'fault_wise_report':
        $Row = fault_based_report();

        $CFG->template = "reports/fault_based_feedback.php";
        break;
    case 'cleaner_wise_report':
        $Row = cleaner_based_report();
        $graph_cleaner = array_values($Row);

        foreach ($graph_cleaner as $data_list_fault_key=>$data_list_fault) {
            $get_data_for_pie_data_cleaner[] = [ucfirst($data_list_fault['name']), (int)$data_list_fault['total_feedback']];
        }

        $CFG->template = "reports/cleaner_based_feedback.php";
        break;
    case 'genuine_or_anonymous_wise_report':
        $Row = genuine_or_anonymous_based_report();
        $count_data = count_report();

        $CFG->template = "reports/genuine_anonymous_based_feedback.php";
        break;
    case 'genuine_wise_report':
        $Row = genuine_based_report();
        $count_data = count_report();
        $CFG->template = "reports/genuine_wise_report.php";
        break;
    case 'anonymous_wise_report':
        $Row = anonymous_based_report();
        $count_data = count_report();
        $CFG->template = "reports/anonymous_wise_report.php";
        break;
    case 'month_wise_report':
        $Row = month_on_month_based_report();
        $CFG->template = "reports/month_on_month_based_feedback.php";
        break;

    case 'weekly_interval':
        $Row = weekly_interval_based_report();
        $CFG->template = "reports/weekly_interval_based_feedback.php";
        break;
    case 'xss':
//        echo $_GET['page_no']; exit;
        $page_no = $_GET['page_no'];

        if (isset($page_no)) {
            $Row = weekly_interval_based_report_load_more($page_no);
//            $json = include(ROOT . "/append_more_data_new.php");

            $CFG->template = "reports/weekly_interval_based_feedback.php";

//            echo '<pre>'; print_r($json); exit;

//            return json_encode($json);
        }
        break;

    case 'daily_wise_report':
        /*Overall column chart feedback start*/

        $between_date = check_date_today_yesterday_or_other();


        $overall_feedback = $DBFilter->RunSelectQuery("select fd.created_at ,count(fd.rating) as feedback_count from feedback as fd
					where fd.is_deleted = 'N' and fd.is_active= 'Y' AND $between_date  AND fd.rating != '' and fd.company_id='" . $_SESSION['company_id'] . "' GROUP BY fd.created_at");


        $data_overall = array_values($overall_feedback[0]);
        foreach ($data_overall as $data_list_ovellall) {
            $get_count_overall_feedback[] = $data_list_ovellall->feedback_count;
            $get_count_overall_createtion_date[] =date("d-M-Y", strtotime($data_list_ovellall->created_at));

//            $get_count_overall_feedback[] = $data_list_ovellall->feedback_count;
//            $get_count_overall_createtion_date[] = $data_list_ovellall->creation_date;
        }

        $overall_feedback_count = json_encode($get_count_overall_feedback, JSON_NUMERIC_CHECK);
        $overall_feedback_by_date = json_encode($get_count_overall_createtion_date, JSON_NUMERIC_CHECK);

        /*Overall column chart feedback end*/
        $CFG->template = "reports/daily_based_feedback.php";
        break;

    case 'rating_wise_report':

        $between_date = check_date_today_yesterday_or_other();

        /*Feedback Type  start*/
        $poor_feedback = $DBFilter->RunSelectQuery("select rating,count(rating) as feedback_count from feedback as fd where fd.is_deleted = 'N' and fd.is_active= 'Y' AND $between_date and fd.rating != ''  and fd.company_id='" . $_SESSION['company_id'] . "' GROUP BY rating ");

        $data = array_values($poor_feedback[0]);
        foreach ($data as $data_list) {
            $get_data_for_pie_feedback_type[] = [$data_list->rating, (int)$data_list->feedback_count];
        }
        /*Feedback Type end*/

        $CFG->template = "reports/rating_based_feedback.php";
        break;
}

/*==================================================Chart graph for location wise====================================================================================*/

$between_date = check_date_today_yesterday_or_other();

$query_location_based_for_chart = "select dl.location_id, location_name,(select count(*) from device
where device.location_id = dl.location_id and device.is_active='Y'
and device.is_deleted='N') devices, count(fd.feedback_id) as feedbacks from device_locations dl

left join device d on d.location_id = dl.location_id
left join feedback fd on fd.device_id = d.device_id
where dl.company_id= '".$_SESSION['company_id']."' AND $between_date  group by dl.location_id";

$location_based_result = $DBFilter->RunSelectQuery($query_location_based_for_chart);
$data_location = array_values($location_based_result[0]);
foreach ($data_location as $data_list_fault) {
    $get_data_for_pie_data_location[] = [ucfirst($data_list_fault->location_name), (int)$data_list_fault->feedbacks];
}

/*===========================================================================================================================================================================================================*/

/*=============================================Chart graph for fault wise=============================================================================================*/

$between_date = check_date_today_yesterday_or_other();

$query_fault_based_for_chart = "select flt.fault_name,count(rating) as feedback_count from feedback as fd INNER JOIN faults flt on fd.fault_id = flt.fault_id where fd.is_deleted = 'N' and fd.is_active= 'Y' AND $between_date and fd.company_id='".$_SESSION['company_id']."' GROUP BY flt.fault_name";

$fault_based_result = $DBFilter->RunSelectQuery($query_fault_based_for_chart);
$data_fault = array_values($fault_based_result[0]);

foreach ($data_fault as $data_list_fault) {
    $get_data_for_pie_data_fault[] = [ucfirst($data_list_fault->fault_name), (int)$data_list_fault->feedback_count];
}

/*===========================================================================================================================================================================================================*/

/*===========================================Month on Month column chart feedback start==================================================================================*/

$between_date = check_date_today_yesterday_or_other_without_alias();

$month_format = "%b_%y";

$month_on_feedback =   $DBFilter->RunSelectQuery("SELECT COUNT(*) as number_of_feedback,  DATE_FORMAT(created_at, '$month_format') as month, 
YEAR(created_at) as year,MONTH(created_at) as month_desc from feedback where created_at !='' and is_deleted = 'N' and is_active ='Y'and company_id ='".$_SESSION['company_id']."' and $between_date GROUP BY  DATE_FORMAT(created_at, '$month_format'),YEAR(created_at),MONTH(created_at)
order by YEAR(created_at) DESC, MONTH(created_at) DESC");


$data_fault_month_on = array_values($month_on_feedback[0]);

$replace_with= "-";
foreach ($data_fault_month_on as $data_list_fault) {

    $get_data_for_pie_data_list_fault_month_on[] = [ucfirst(str_replace('_',$replace_with,$data_list_fault->month)), (int)$data_list_fault->number_of_feedback];
}
$month_on_array = [];

foreach ($get_data_for_pie_data_list_fault_month_on as $get_data_for_pie_data_list_fault_month_on_list){
    $month_on_array[] = "['$get_data_for_pie_data_list_fault_month_on_list[0]',$get_data_for_pie_data_list_fault_month_on_list[1]]";
}

/*===========================================================================================================================================================================================================*/
/*=============================================Chart graph for Cleaner Wise =====================================================================================*/

$query_cleaner_based = "select count(f.feedback_id) as total_feedback,f.created_at,f.assigned_to,concat(u1.first_name,' ',u1.last_name) as name 
from feedback as f left join company_detail as cd on f.company_id = cd.company_id left join device as d on f.device_id = d.device_id 
left join services as s on f.service_id = s.service_id left join device_locations as dl on d.location_id = dl.location_id 
left join users as u on f.user_id = u.user_id left join users as u1 on f.assigned_to = u1.user_id left join faults on f.fault_id = faults.fault_id 
where f.is_deleted = 'N' and f.is_active ='Y' and u.is_deleted='N' and u.is_active ='Y' and u1.is_deleted='N' 
and f.company_id='".$_SESSION['company_id']."' and s.company_id='".$_SESSION['company_id']."' and f.created_at !='' and u.is_deleted ='N' and f.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'
group by f.assigned_to,f.created_at 
order by f.created_at desc";

$cleaner_based_result = $DBFilter->RunSelectQuery($query_cleaner_based);

foreach ($cleaner_based_result[0] as $cleaner_based_result_data) {

    $query_cleaner_based_rating = " SELECT count(rating) as count, rating,assigned_to FROM `feedback` WHERE `assigned_to` = $cleaner_based_result_data->assigned_to  AND `company_id` = '".$_SESSION['company_id']."' AND CONVERT(`is_deleted` USING utf8mb4) = 'N' AND CONVERT(`is_active` USING utf8mb4) = 'Y' AND `created_at` != '' AND created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  group by rating ";

    $query_cleaner_based_rating_single[$cleaner_based_result_data->assigned_to] = $DBFilter->RunSelectQuery($query_cleaner_based_rating);
    $get_all_cleabner_id[] = $cleaner_based_result_data->assigned_to;
    $get_all_cleabner_ids = array_unique($get_all_cleabner_id);
}

$all_cleaners = implode(',',$get_all_cleabner_ids);

foreach ($cleaner_based_result[0] as $cleaner_based_result_data) {
    $query_cleaner_based_rating_chart = " SELECT count(*) as total_feedback,feedback.assigned_to as assi,concat(users.first_name,' ',users.last_name) as full_name
 FROM feedback INNER JOIN users ON feedback.assigned_to = users.user_id WHERE feedback.assigned_to IN($all_cleaners) AND feedback.company_id='".$_SESSION['company_id']."' AND feedback.is_deleted = 'N' AND feedback.is_active= 'Y' AND feedback.created_at != '' AND feedback.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  group by assi,first_name ";

    $get_data_for_pie_data_cleaner_array = $DBFilter->RunSelectQuery($query_cleaner_based_rating_chart);
}

foreach ($get_data_for_pie_data_cleaner_array[0] as $get_data_for_pie_data_cleaner_val){
    $get_data_for_pie_data_cleaner_pie[] = [$get_data_for_pie_data_cleaner_val->full_name, (int)$get_data_for_pie_data_cleaner_val->total_feedback];
}

/*==============================================================================================================================================================================*/

function get_list_of_all_week($date_for_week){

    $week_list =[];
    $explode_date = explode('-',$date_for_week);
    $first_date = $explode_date[0]."-".$explode_date[1]."-".$explode_date[2];
    $last_date = date("Y-m-t",strtotime($first_date));
    $first_week = date("W",strtotime($first_date));
    $last_week = date("W",strtotime($last_date));
    $k = 1;
    for($i=$first_week;$i<=$last_week;$i++)
    {
        $week_list[$k]= $i;
//        echo "Week #".$i.", ";
        $k++;
    }
//            echo '<pre>'; print_r($week_list); //exit;

    return $week_list;
}
function weekly_interval_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];

        $page_no = $_GET['page_no'];

        if (isset($page_no)){
            $page_no = $page_no;
        }else{
            $page_no = 1;
        }

        $total_rows = get_total_rows_weekly();
        $no_of_records_per_page = 5;
        $offset = ($page_no-1) * $no_of_records_per_page;
        $total_pages = ceil($total_rows / $no_of_records_per_page);
        $_SESSION['total_page_weekly'] = $total_pages;

        $between_date = check_date_today_yesterday_or_other_without_alias();

        $query_interval_based_for_weekly_chart = "select date_format(created_at,'%Y-%m-%v') as years, date_format(created_at,'%M-%Y') as month_name,
date_format(created_at,'%m') as only_month, date_format(created_at,'%Y') as only_year, date_format(created_at,'%Y-%m') as month_unique, count(*) as number_of_feedback,

        (select count(*) as weekly_wise from feedback where rating='Excellent' and years = date_format(created_at,'%Y-%m-%v')
        and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
        and $between_date) as excellent,

        (select count(*) as weekly_wise from feedback where rating='Good' and years = date_format(created_at,'%Y-%m-%v')
        and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
        and $between_date) as good,

        (select count(*) as weekly_wise from feedback where rating='average' and years = date_format(created_at,'%Y-%m-%v')
        and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
        and $between_date) as avergae,

        (select count(*) as weekly_wise from feedback where rating='Poor' and years = date_format(created_at,'%Y-%m-%v')
        and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
        and $between_date) as poor,

        (select count(*) as weekly_wise from feedback where rating='Very Poor' and years = date_format(created_at,'%Y-%m-%v')
        and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
        and $between_date) as very_poor, company_id
        from feedback
        where is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
        and $between_date and created_at !=''
        group by years,month_name,month_unique,only_month, only_year order by years DESC LIMIT $offset,$no_of_records_per_page ";

        $interval_based_result = $DBFilter->RunSelectQuery($query_interval_based_for_weekly_chart);
//        echo '<pre>';print_r($interval_based_result) ;exit;


foreach ($interval_based_result[0] as $interval_based_result_key => $interval_based_result_value){
    $get_similar_month[] = $interval_based_result_value->month_unique ;
    $get_similar_month_unique = array_unique($get_similar_month);
    if (in_array($interval_based_result_value->month_unique,$get_similar_month)){
        $final_list[$interval_based_result_value->month_unique][]= $interval_based_result_value;
    }
}

//        echo '<pre>'; print_r($final_list); exit;

return $final_list;
//$data_fault = array_values($fault_based_result[0]);


  /*==============================================================================================================================================================================*/

    } catch (Exception $e) {

    }
}

function weekly_interval_based_report_load_more($page_no){

    global $DB, $DBFilter;



    $between_date = check_date_today_yesterday_or_other_without_alias();

        $query_interval_based_for_weekly_chart_loadmore = "select date_format(created_at,'%Y-%m-%v') as years, date_format(created_at,'%M-%Y') as month_name, date_format(created_at,'%m') as only_month, date_format(created_at,'%Y') as only_year, date_format(created_at,'%Y-%m') as month_unique, count(*) as number_of_feedback,

(select count(*) as weekly_wise from feedback where rating='Excellent' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as excellent,

(select count(*) as weekly_wise from feedback where rating='Good' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as good,

(select count(*) as weekly_wise from feedback where rating='average' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as avergae,

(select count(*) as weekly_wise from feedback where rating='Poor' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as poor,

(select count(*) as weekly_wise from feedback where rating='Very Poor' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as very_poor,

(select count(*) as weekly_wise from feedback where date_format(created_at,'%Y-%m-%v') =years
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as count_week, company_id


from feedback
 
where is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date and created_at !=''
group by years,month_name,month_unique,only_month, only_year order by years DESC ";

    $interval_based_result_more = $DBFilter->RunSelectQuery($query_interval_based_for_weekly_chart_loadmore);



        foreach ($interval_based_result_more[0] as $interval_based_result_key => $interval_based_result_value){
            $get_similar_month[] = $interval_based_result_value->month_unique ;
            $get_similar_month_unique = array_unique($get_similar_month);
            if (in_array($interval_based_result_value->month_unique,$get_similar_month)){
                $final_list_more[$interval_based_result_value->month_unique][]= $interval_based_result_value;
            }
        }
//        $json = include(ROOT."/templates/default/reports/append_more_data.php");

//    echo '<pre>'; print_r($interval_based_result_more[0]); exit;

    return $final_list_more;


}
function interval_based()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];
        $interval_list = [''];


        $page_no = $_GET['page_no'];

        if (isset($page_no)){
            $page_no = $page_no;
        }else{
            $page_no = 1;
        }

        $total_rows = get_total_rows_interval();
//        echo $total_rows; exit;

        $no_of_records_per_page = 10;
        $offset = ($page_no-1) * $no_of_records_per_page;
        $total_pages = ceil($total_rows / $no_of_records_per_page);
        $_SESSION['total_page_interval'] = $total_pages;

        $between_date = check_date_today_yesterday_or_other_without_alias();

        $query_interval = "select interval_time,count(*) as count_feedback,created_at from feedback where is_active='Y' and is_deleted ='N' and company_id = '$company_id'
	and interval_time != '' AND $between_date group by interval_time,created_at order by created_at DESC LIMIT $offset,$no_of_records_per_page";

        $interval_result = $DBFilter->RunSelectQuery($query_interval);
//        echo '<pre>'; print_r($query_interval); exit;

        $get_final_date_array = [];
        $get_all_interval_array_new = [];
        $get_all_interval_array = [];
        $interval_array = ['12-03', '03-06', '06-09', '09-12', '12-15', '15-18', '18-21', '21-23'];
        foreach (array_filter($interval_result[0]) as $interval_result_list) {
//					foreach($interval_array as $interval_array_list){

            $explode_date = explode(' ', $interval_result_list->interval_time);
            $explode_date = array_values(array_filter($explode_date));

//					$concat_date_and_interval = $explode_date[0].' '.$interval_array_list;
            $concat_date_and_interval = $explode_date[0];
            $concat_interval = $explode_date[1];

            $get_final_date_array[] = $concat_interval;
            $get_final_date_array = array_filter(array_unique($get_final_date_array));

//					}2nd foreach
            foreach ($get_final_date_array as $key => $val) {
                $get_all_interval_array[$val] = array_flip($interval_array);
            }

            foreach ($get_all_interval_array as $key1 => $val1) {
                foreach ($val1 as $key2 => $val2) {
                    if ($key1 == $concat_interval) {
                        if (strpos($interval_result_list->interval_time, $key2) !== false) {
//									echo $val1[$key2].'*'.$key1 .'*'.$key2; echo '<br>';
                            $get_all_interval_array_new[$key1][$key2] = $interval_result_list->count_feedback ? $interval_result_list->count_feedback : 0;
                        }
//								else {
//									$get_all_interval_array_new[$key1][$key2] = 0;
//									echo '<pre>'; print_r($get_all_interval_array_new);
//								}
                    }
                }
            }
        }
        $sort_date = [];
        $understandable_structure_array = [];
        foreach ($get_all_interval_array_new as $key3 => $val4) {
            foreach ($val4 as $key5 => $val5) {

                $sort_date['date'] = $key3;
                $sort_date['time_interval'] = $val4;
            }
            $understandable_structure_array[] = $sort_date;

        }

        return $understandable_structure_array;
    } catch (Exception $e) {

    }
}

function location_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];

        $between_date = check_date_today_yesterday_or_other();
        $query_location_based = "select dl.location_id, location_name,(select count(*) from device 
where device.location_id = dl.location_id and device.is_active='Y' 
and device.is_deleted='N') as devices, count(fd.feedback_id) as feedbacks,fd.rating,count(fd.rating) as count_single_rating from device_locations dl 
left join device d on d.location_id = dl.location_id left join feedback fd on fd.device_id = d.device_id
where dl.company_id= '$company_id' AND fd.created_at != '' AND $between_date group by dl.location_id, fd.rating";

        $location_based_result = $DBFilter->RunSelectQuery($query_location_based);

        $arr_loc = []; $loc_id_arr = []; $ids_arr = [];
        foreach ($location_based_result[0] as $location_based_result_list) {

            $loc_id_arr[] = $location_based_result_list->location_id;
            $loc_id_arr = array_unique($loc_id_arr);
            $arr_loc['location_id'] = $location_based_result_list->location_id;
            $arr_loc['location_name'] = $location_based_result_list->location_name;
            $arr_loc['devices'] = $location_based_result_list->devices;
            $arr_loc['total_feedback'] += $location_based_result_list->feedbacks;
            $arr_loc['feedback_detail'][$location_based_result_list->location_id][$location_based_result_list->location_id][$location_based_result_list->rating] = $location_based_result_list->rating . ',' . $location_based_result_list->count_single_rating;
            $ids_arr[$location_based_result_list->location_id] = $arr_loc;
        }

        return $ids_arr;

    } catch (Exception $e) {

    }
}

function fault_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];

        /*  $query_fault_based = "select flt.fault_name,count(rating) as feedback_count from feedback as fd INNER JOIN faults flt on fd.fault_id = flt.fault_id where fd.is_deleted = 'N' and fd.is_active= 'Y' AND (fd.creation_date BETWEEN '".$_SESSION['start_date']."' AND '".$_SESSION['end_date']."')  and fd.company_id='" . $company_id . "' GROUP BY flt.fault_name";*/
        $between_date = check_date_today_yesterday_or_other();
        $between_date_alias = check_date_today_yesterday_or_other_without_alias();

        $query_fault_based = "select flt.fault_name,flt.fault_id, count(rating) as feedback_count from feedback as fd INNER JOIN faults flt on fd.fault_id = flt.fault_id where fd.is_deleted = 'N' and fd.is_active= 'Y' AND fd.company_id='" . $company_id . "' and $between_date GROUP BY flt.fault_name,flt.fault_id";

        $fault_based_result = $DBFilter->RunSelectQuery($query_fault_based);
//        echo '<pre>'; print_r($query_fault_based); exit;
        foreach ($fault_based_result[0] as $fault_based_result_data) {

            $query_fault_based_rating = " SELECT count(rating) as count, rating,fault_id FROM `feedback` WHERE `fault_id` = $fault_based_result_data->fault_id  AND `company_id` = '" . $_SESSION['company_id'] . "' AND CONVERT(`is_deleted` USING utf8mb4) = 'N' AND CONVERT(`is_active` USING utf8mb4) = 'Y' AND `created_at` != '' AND $between_date_alias group by rating ";

            $query_fault_based_rating_single[$fault_based_result_data->fault_id] = $DBFilter->RunSelectQuery($query_fault_based_rating);

        }

        foreach ($fault_based_result[0] as $fault_based_result_data_key => $fault_based_result_data) {
            $fault_id = $fault_based_result_data->fault_id;
            $data_faults[$fault_based_result_data->fault_id] = $fault_based_result_data;
            $result[$fault_based_result_data->fault_id] = isset($query_fault_based_rating_single[$fault_id]) ? $query_fault_based_rating_single[$fault_id] : null;
        }

        $merged = [];
        foreach ($data_faults as $data_faults_key => $data_faults_val) {
            foreach ($result as $result_key => $result_val) {
                if ($result_key == $data_faults_key){
                    $merged[$data_faults_key] = array_merge((array)$data_faults_val,$result[$result_key]);
                }
            }
        }


        return $merged;

    } catch (Exception $e) {

    }
}

function cleaner_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];


        $query_cleaner_based = "select count(f.feedback_id) as total_feedback,f.created_at,f.assigned_to,concat(u1.first_name,' ',u1.last_name) as name 
from feedback as f left join company_detail as cd on f.company_id = cd.company_id left join device as d on f.device_id = d.device_id 
left join services as s on f.service_id = s.service_id left join device_locations as dl on d.location_id = dl.location_id 
left join users as u on f.user_id = u.user_id left join users as u1 on f.assigned_to = u1.user_id left join faults on f.fault_id = faults.fault_id 
where f.is_deleted = 'N' and f.is_active ='Y' and u.is_deleted='N' and u.is_active ='Y' and u1.is_deleted='N' 
and f.company_id='$company_id' and s.company_id='$company_id' and f.created_at !='' and u.is_deleted ='N' and f.created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'
group by f.assigned_to,f.created_at 
order by f.created_at desc";

        $cleaner_based_result = $DBFilter->RunSelectQuery($query_cleaner_based);


        foreach ($cleaner_based_result[0] as $cleaner_based_result_data) {

            $query_cleaner_based_rating = " SELECT count(rating) as count, rating,assigned_to FROM `feedback` WHERE `assigned_to` = $cleaner_based_result_data->assigned_to  AND `company_id` = '$company_id' AND CONVERT(`is_deleted` USING utf8mb4) = 'N' AND CONVERT(`is_active` USING utf8mb4) = 'Y' AND `created_at` != '' AND created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  group by rating ";

            $query_cleaner_based_rating_single[$cleaner_based_result_data->assigned_to] = $DBFilter->RunSelectQuery($query_cleaner_based_rating);
        }


        foreach ($cleaner_based_result[0] as $cleaner_based_result_data_key => $cleaner_based_result_data) {
            $cleaner_id = $cleaner_based_result_data->assigned_to;
            $cleaner_total[$cleaner_based_result_data->assigned_to] = $cleaner_based_result_data;
            $result[$cleaner_based_result_data->assigned_to] = isset($query_cleaner_based_rating_single[$cleaner_id]) ? $query_cleaner_based_rating_single[$cleaner_id] : null;
        }

        $merged_cleaner_info = [];
        foreach ($cleaner_total as $cleaner_total_key => $cleaner_total_val) {
            foreach ($result as $result_key => $result_val) {
                if ($result_key == $cleaner_total_key){
                    $merged_cleaner_info[$cleaner_total_key] = array_merge((array)$cleaner_total_val,$result[$result_key]);
                }
            }
        }
//        echo '<pre>'; print_r($merged_cleaner_info); exit;

        return $merged_cleaner_info;

    } catch (Exception $e) {

    }
}

function genuine_or_anonymous_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];


        $query_genuine_based = "select count(feedback_id) as genuine_total_feedback,created_at  from feedback where email !='' 
and phone_number !='' and  is_deleted = 'N' and is_active ='Y' GROUP BY created_at ORDER BY count(feedback_id) desc ";

        $genuine_based_result = $DBFilter->RunSelectQuery($query_genuine_based);

        $query_anonymous_based = "select count(feedback_id) as anonymous_total_feedback,created_at from feedback where is_deleted = 'N' and is_active ='Y' GROUP BY created_at ORDER BY count(feedback_id) desc ";

        $anonymous_based_result = $DBFilter->RunSelectQuery($query_anonymous_based);

        $merged = array_merge($genuine_based_result, $anonymous_based_result);

//                     return $merged;

//                    echo "<pre>"; print_r($genuine_based_result); exit;

    } catch (Exception $e) {

    }
}

function genuine_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];


//        $query_genuine_based = "select count(feedback_id) as genuine_total_feedback,created_at  from feedback where and email !=''
//and phone_number !='' and  is_deleted = 'N' and is_active ='Y' and company_id ='$company_id' and created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' GROUP BY created_at ORDER BY count(feedback_id) desc ";

        $between_date_alias = check_date_today_yesterday_or_other_without_alias();

        $query_genuine_based = "select date_format(created_at,'%Y-%m') as month_data, concat(date_format(created_at,'%M'),'-',date_format(created_at,'%Y')) as month_name, count(*) as number_of_feedback, 

(select count(*) as weekly_wise from feedback where rating='Excellent' and month_data = date_format(created_at,'%Y-%m')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id' and email !='' and phone_number !=''   
and $between_date_alias and created_at !='') as excellent,

(select count(*) as weekly_wise from feedback where rating='Good' and month_data = date_format(created_at,'%Y-%m') and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id' and email !='' and phone_number !='' and $between_date_alias and created_at !='') as good, 

(select count(*) as weekly_wise from feedback where rating='average' and month_data = date_format(created_at,'%Y-%m') and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id' and email !='' and phone_number !='' 
and $between_date_alias and created_at !='') as avergae,

(select count(*) as weekly_wise from feedback where rating='Poor' and month_data = date_format(created_at,'%Y-%m')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id' and email !='' and phone_number !='' 
and $between_date_alias and created_at !='') as poor,

(select count(*) as weekly_wise from feedback where rating='Very Poor' and month_data = date_format(created_at,'%Y-%m')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id' and email !='' and phone_number !='' 
and $between_date_alias and created_at !='') as very_poor

from feedback
where is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'  and email !='' and phone_number !='' and $between_date_alias
group by month_data,month_name order by month_data DESC";

        $genuine_based_result = $DBFilter->RunSelectQuery($query_genuine_based);
//        echo '<pre>'; print_r($query_genuine_based); exit;

        return $genuine_based_result;

    } catch (Exception $e) {

    }
}

function anonymous_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];

        $query_anonymous_based = "select count(feedback_id) as anonymous_total_feedback,created_at from feedback where is_deleted = 'N' and is_active ='Y'and company_id ='$company_id' and created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' GROUP BY created_at ORDER BY count(feedback_id) desc ";

        $anonymous_based_result = $DBFilter->RunSelectQuery($query_anonymous_based);

        return $anonymous_based_result;

//                    echo "<pre>"; print_r($genuine_based_result); exit;

    } catch (Exception $e) {

    }
}

function month_on_month_based_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];
        $month_format = "%b_%y";
        $query_month_on_month_based_rating_single = [];
        $query_month_on_month_based = "select date_format(created_at,'%Y-%m') as month_data, concat(date_format(created_at,'%M'),'-',date_format(created_at,'%Y')) as month_name, count(*) as number_of_feedback, 
(select count(*) as weekly_wise from feedback where rating='Excellent' and month_data = date_format(created_at,'%Y-%m')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' and created_at !='') as excellent,

(select count(*) as weekly_wise from feedback where rating='Good' and month_data = date_format(created_at,'%Y-%m') and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id' and created_at BETWEEN  '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  and created_at !='') as good, 

(select count(*) as weekly_wise from feedback where rating='average' and month_data = date_format(created_at,'%Y-%m') and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and created_at BETWEEN  '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  and created_at !='') as avergae,

(select count(*) as weekly_wise from feedback where rating='Poor' and month_data = date_format(created_at,'%Y-%m')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and created_at BETWEEN  '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  and created_at !='') as poor,
(select count(*) as weekly_wise from feedback where rating='Very Poor' and month_data = date_format(created_at,'%Y-%m')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and created_at BETWEEN  '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "'  and created_at !='') as very_poor

from feedback
where is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and created_at BETWEEN '" . $_SESSION['start_date'] . "' AND '" . $_SESSION['end_date'] . "' and created_at !=''
group by month_data,month_name order by month_data DESC";

        $month_on_month_based_result = $DBFilter->RunSelectQuery($query_month_on_month_based);

       /* foreach ($month_on_month_based_result[0] as $month_on_month) {
            $query_month_on_month_based_rating = " SELECT count(rating) as count, rating, DATE_FORMAT(created_at, '$month_format') as month FROM `feedback` WHERE `company_id` = '$company_id' AND CONVERT(`is_deleted` USING utf8mb4) = 'N' AND CONVERT(`is_active` USING utf8mb4) = 'Y' AND `created_at` != '' AND created_at BETWEEN '2018-11-01' AND '" . $_SESSION['end_date'] . "'  group by month,rating order by month ";

            $query_month_on_month_based_rating_single[] = $DBFilter->RunSelectQuery($query_month_on_month_based_rating);
//            $query_month_on_month_based_rating_single[$month_on_month->month] = $DBFilter->RunSelectQuery($query_month_on_month_based_rating);

        }



        $merged_month =[];
        foreach ($month_on_month_based_result[0] as $month_on_month_total_key => $month_on_month_total_val) {
            $merged_month[] = $month_on_month_total_val;
            foreach ($query_month_on_month_based_rating_single as $query_month_on_month_based_rating_single_key => $query_month_on_month_based_rating_single_value) {

                foreach ($query_month_on_month_based_rating_single_value[0] as $query_month_on_month_based_key_two => $query_month_on_month_based_value_two) {
                    if ($month_on_month_total_val->month == $query_month_on_month_based_value_two->month){

                        $aa[] = $query_month_on_month_based_value_two;
                        $merged_month[$month_on_month_total_key] = array_merge((array)$month_on_month_total_val,$query_month_on_month_based_rating_single_value);

                    }
                }
        }
        }*/



        return $month_on_month_based_result;

    } catch (Exception $e) {

    }
}

function count_report()
{
    try {
        global $DB, $DBFilter;
        $company_id = $_SESSION['company_id'];

        $between_date = check_date_today_yesterday_or_other();

        $count_genuine_based = "select count(*) as total_genuine from feedback fd where email !='' 
and phone_number !='' and  is_deleted = 'N' and is_active ='Y' AND $between_date AND company_id='$company_id' ORDER BY count(feedback_id) desc ";
        $count_genuine = $DBFilter->RunSelectQuery($count_genuine_based);

        $count_anonymous_based = "select count(*) as total_anonymous from feedback fd where is_deleted = 'N' and is_active ='Y' and created_at != '' AND $between_date AND company_id='$company_id' ORDER BY count(feedback_id) desc ";
        $count_anonymous = $DBFilter->RunSelectQuery($count_anonymous_based);

        $count_type = array_merge($count_anonymous[0], $count_genuine[0]);
        return $count_type;
    } catch (Exception $e) {

    }
}

function getRatio($num1, $num2)
{
    for ($i = $num2; $i > 1; $i--) {
        if (($num1 % $i) == 0 && ($num2 % $i) == 0) {
            $num1 = $num1 / $i;
            $num2 = $num2 / $i;
        }
    }
    return "$num1:$num2";
}

/* function is used to get device for CMS*/
function getdevice(&$totalCount)
{
    global $DB, $frmdata, $DBFilter;

    $query = " SELECT d.*,u.first_name,u.last_name,dl.location_name from device as d left join users as u on d.user_id = u.user_id left join device_locations as dl on d.location_id = dl.location_id";
    $where = " where d.is_deleted = 'N' and u.role_id = '4' and u.is_deleted='N'";
    //$where = " where d.is_deleted = 'N' and d.is_active = 'Y' and u.role_id = '4' and u.is_deleted='N'";
    if (trim($frmdata['keyword']) != '') {
        $frmdata['keyword'] = trim($frmdata['keyword']);

        $where .= " and (d.device_name like  '%" . $frmdata['keyword'] . "%' or d.device_status like  '%" . $frmdata['keyword'] . "%' or dl.location_name like '%" . $frmdata['keyword'] . "%' or u.first_name like '%" . $frmdata['keyword'] . "%' or concat(u.first_name,' ',u.last_name) like '%" . $frmdata['keyword'] . "%')";
        $_SESSION['keywords'] == 'Y';
    } else {
        $where .= '';
    }

    if ($_SESSION['company_id'] != '' && $_SESSION['usertype'] != 'super_admin') {
        $company_id = $_SESSION['company_id'];
        if ($_SESSION['role_id'] == '4') {
            $where .= " and d.user_id = '" . $_SESSION['user_id'] . "'";

        }
        $where .= " and d.company_id='$company_id'";

    }

    if (isset($frmdata['orderby']) && $frmdata['orderby'] != '') {
        $order .= " order by " . $frmdata['orderby'];
    } else {
        $order .= " order by d.device_id desc";
    }

    $query = $query . $where . $order;
    //echo $query;
    //exit;
    $result = $DBFilter->RunSelectQueryWithPagination($query, $totalCount);
    return $result;
}

function getAllDevices($data)
{
    global $DB, $DBFilter;
    $access_key = $DBFilter->SelectRecord('login_detail', "access_key='" . $data['access_key'] . "'");
    $user = $DBFilter->SelectRecord('users', "user_id=" . $access_key->user_id);
    if ($access_key) {
        $Rows = $DBFilter->SelectRecords('device', "is_deleted='N' and is_active='Y' and company_id=" . $user->company_id);
        if ($Rows != '') {
            $devices = array();
            for ($i = 0; $i < count($Rows[0]); $i++) {
                $devices[$i]['device_id'] = $Rows[0][$i]->device_id;
                $devices[$i]['device_name'] = $Rows[0][$i]->device_name;
            }
            $final_data = array('result' => $devices, 'status' => 1);
            echo json_encode($final_data);
            exit;
        } else {
            $final_data = array('result' => 'No Device Found', 'status' => 0);
            echo json_encode($final_data);
            exit;
        }
    } else {
        $final_data = array('result' => 'Access key is expired.Please login again.', 'status' => 2);
        echo json_encode($final_data);
        exit;
    }
}

function getWeeks($date, $rollover)
{
    $cut = substr($date, 0, 8);

    $daylen = 86400;

    $timestamp = strtotime($date);
    $first = strtotime($cut . "00");
    $elapsed = ($timestamp - $first) / $daylen;

    $weeks = 1;

    for ($i = 1; $i <= $elapsed; $i++)
    {
//        $dayfind = $cut .   (strlen($i) < 2 ? '0' . $i : $i);
        $dayfind = $cut;
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));

        if($day == strtolower($rollover))  $weeks ++;
    }
    echo $weeks;

    return $weeks;
}

function weeks($month, $year){
    // Start of month
//    echo $month.'-'.$year; //exit;

    $start = mktime(0, 0, 0, $month, 1, $year);
    // End of month
    $end = mktime(0, 0, 0, $month, date('t', $start), $year);
    // Start week
    $start_week = date('W', $start);
    // End week
    $end_week = date('W', $end);

    if ($end_week < $start_week) { // Month wraps
        return ((52 + $end_week) - $start_week) + 1;
    }

    return ($end_week - $start_week) + 1;
//    echo $count_weeks;
//    return $count_weeks;
}
function get_total_rows_interval(){


    global $DB, $DBFilter;
    $company_id = $_SESSION['company_id'];
    $between_date = check_date_today_yesterday_or_other_without_alias();

    $query_interval = "select interval_time,count(*) as count_feedback from feedback where is_active='Y' and is_deleted ='N' and company_id = '$company_id'
	and interval_time != '' AND $between_date group by interval_time order by interval_time DESC ";

    $interval_result = $DBFilter->RunSelectQuery($query_interval);


    $total_rows = count($interval_result[0]);


    return $total_rows;
}
function get_total_rows_weekly(){


    global $DB, $DBFilter;
    $company_id = $_SESSION['company_id'];
    $between_date = check_date_today_yesterday_or_other_without_alias();

    $query_interval_based_for_weekly_chart_loadmore = "select date_format(created_at,'%Y-%m-%v') as years, date_format(created_at,'%M-%Y') as month_name, date_format(created_at,'%m') as only_month, date_format(created_at,'%Y') as only_year, date_format(created_at,'%Y-%m') as month_unique, count(*) as number_of_feedback,

(select count(*) as weekly_wise from feedback where rating='Excellent' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as excellent,

(select count(*) as weekly_wise from feedback where rating='Good' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as good,

(select count(*) as weekly_wise from feedback where rating='average' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as avergae,

(select count(*) as weekly_wise from feedback where rating='Poor' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as poor,

(select count(*) as weekly_wise from feedback where rating='Very Poor' and years = date_format(created_at,'%Y-%m-%v')
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as very_poor,

(select count(*) as weekly_wise from feedback where date_format(created_at,'%Y-%m-%v') =years
and is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date) as count_week, company_id from feedback

where is_active='Y' and created_at != '' and is_deleted ='N' and company_id = '$company_id'
and $between_date and created_at !=''
group by years,month_name,month_unique,only_month, only_year order by years ";


    $interval_based_result_more = $DBFilter->RunSelectQuery($query_interval_based_for_weekly_chart_loadmore);

    $total_rows = count($interval_based_result_more[0]);

//    echo '<pre>'; print_r($total_rows);exit;

    return $total_rows;
}

include_once(CURRENTTEMP . "/index.php");
?>