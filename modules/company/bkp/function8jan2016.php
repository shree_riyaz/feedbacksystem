<?php

    if (isset($frmdata['clearsearch']))
    {
		unset($frmdata['keyword']);
		unset($_SESSION['pageNumber']);
        $CFG->template = "company/list.php";
    }
   // print_r($_SESSION);
    if (isset($frmdata['add_company']))
    {
		$err = '';
		/* echo '<pre>';
		 print_r($frmdata);
		 exit;*/
		/******Added By Shivender*********/
		if (trim($frmdata['company_name']) == '')
		{
            $err .= "Please enter company name.<br>";
			$frmdata['company_name']='';
        }
		else
		{
			if(validatename($frmdata['company_name'])== true)
			{
				$frmdata['company_name']= $frmdata['company_name'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for company name.<br>";
			}
		
		}
		if (trim($frmdata['user_name']) == '')
		{
            $err .= "Please enter contact person name.<br>";
			$frmdata['user_name']='';
        }
		else
		{
			if(validatename($frmdata['user_name'])== true)
			{
				$frmdata['user_name']= $frmdata['user_name'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for contact person name.<br>";
			}
		
		}
		if (trim($frmdata['creation_date']) == '')
		{
            $err .= "Please select creation date.<br>";
			$frmdata['creation_date']='';
        }
		
		if (trim($frmdata['expiary_date']) == '')
		{
            $err .= "Please select expiry date.<br>";
			$frmdata['expiary_date']='';
        }
		else
		{
		
		$frmdata['expiary_date']= date("Y-m-d", strtotime($frmdata['expiary_date']));
		
		}
		
		if($frmdata['expiary_date']!='' && $frmdata['creation_date']!='')
		{
			if(strtotime($frmdata['expiary_date']) < strtotime($frmdata['creation_date']))
			{	$frmdata['expiary_date'] = '';
				$err .= "Please select valid expiry date.<br>";
			}
		}
		if (trim($frmdata['plan_id']) == '')
		{
            $err .= "Please select subscription plan .<br>";
			$frmdata['plan_id']='';
        }
		else 
		{
			$frmdata['plan_id']= $frmdata['plan_id'];
		}
		
		if (trim($frmdata['company_email']) != '')
		{
            if ( !filter_var($frmdata['company_email'], FILTER_VALIDATE_EMAIL ) )
			{
				$err.="Please enter a valid email address.<br>";
				$frmdata['company_email']='';
			}
		}
		else
		{
            $err .= "Please enter company email .<br>";
			$frmdata['company_email']='';
        }
		
		if(validateimage($_FILES["company_logo"]["name"])== true)
		{
			if($_FILES['company_logo']['size'] > 2097152)//added by : Neha Pareek. (02/12/2015)
			{
				$err.='Logo size must be less than 2 MB'.'<br/>';
			}
			else
			{
				$_FILES["company_logo"]["name"] = $_FILES["company_logo"]["name"];
			}
		}
		else
		{	//$frmdata['plan_id'] = $frmdata['plan_id'];
			$err .= "Please select only an image.<br>";
			$_FILES["company_logo"]["name"] = '';
		}
		
		if($_FILES["company_logo"]["name"]!='')
		{
			$image_name = $_FILES["company_logo"]["name"];
			$source_image=$_FILES["company_logo"]["tmp_name"];
			$filename= time()."_".$image_name;
			$folderpath = ROOT."images/company_logo/".$filename;
			move_uploaded_file($source_image,$folderpath);
		} 
		if ($err == '')
        {
			$nowdate = date("Y-m-d");
			$updatedate = date("Y-m-d");
			$pass = MakeNewpassword(6);
			$obj = new passEncDec;
			$password = $obj->encrypt_password($pass);
			$company_data = array('company_name'=>$frmdata['company_name'],'plan_id'=>$frmdata['plan_id'],'creation_date'=>$nowdate,'expiary_date'=>$frmdata['expiary_date'],'user_name'=>$frmdata['user_name'],'company_email'=>$frmdata['company_email'],'password'=>$password,'company_logo'=>$filename);
			// echo $pass;
			// echo $password;
			// print_r($company_data);
			// exit;
			if ($id= $DBFilter->InsertRecord('company_detail', $company_data))
            {
				$company_id=$id;
				/* code is for adding company_admin */

				$admin_data = array('company_id'=>$company_id,'role_id'=>'2','creation_date'=>$nowdate,'expiary_date'=>$frmdata['expiary_date'],'first_name'=>$frmdata['user_name'],'user_email'=>$frmdata['company_email'],'password'=>$password,'assigned_to'=>$_SESSION['user_id']);
				
				$company_admin = $DBFilter->InsertRecord('users', $admin_data);				

				/* code is for adding one defualt service */

				$service_data = array('company_id'=>$company_id,'service_name'=>'Washroom','service_description'=>'Washroom Feedbacks');
				$company_services = $DBFilter->InsertRecord('services', $service_data);
				//exit;
				/*if($password != '' && trim($frmdata['company_email']) != ''){
				$name = $frmdata['first_name'].$frmdata['last_name'];
				$userid= $frmdata['user_name'];
				$email = $frmdata['company_email'];
				$passwd = $pass;
					$to = "$email";
					$subject = "Facility System Password";

					$message = "	<table width='90%' align='center' >
					 <tr>
					 <th height='15' style='background-color:#037296;padding:10px;color:#FFFFFF' align='left'>Facility System</th>
					 </tr>
						 <tr >
						 <td style='padding:10px'>
						 Dear $name,
						 </td>
						 </tr>
						 <tr >
						 <td style='padding:10px'>
							Your Email Id = $email
						</td>
						</tr>
						 <tr >
						 <td style='padding:10px'>
							Your Password = $pass
						</td>
						</tr>
						<tr >
						<td style='padding:10px'>
							Thanks,
						</td>
						</tr>
						<tr >
						<td style='padding:10px'>
							The Facility System Team
						</td>
						</tr>
						</table>";

					// Always set content-type when sending HTML email
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

					// More headers
					$headers .= 'From: <harendar.singh@sunarctechnologies.com>' . "\r\n";
					//$headers .= 'Cc: myboss@example.com' . "\r\n";

					mail($to,$subject,$message,$headers);
					
				}*/
				
                $_SESSION['success'] = "Company added successfully, Password for company admin is : <b>$pass</b>";
                Redirect(CreateURL('index.php', 'mod=company'));
                die();
            }
			else
            {
				$_SESSION['error'] = "<font color=red>Please insert correct values for company details.</font>";
            }
        }
        else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
         // echo '<pre>';
		 // print_r($frmdata);
		 // exit;
    }
    elseif (isset($frmdata['update']))
    {
        $err = '';
	
		//print_r($_FILES);		
		if (trim($frmdata['company_name']) == '')
		{
            $err .= "Please enter company name.<br>";
			$frmdata['company_name']='';
        }
		else
		{
			if(validatename($frmdata['company_name'])== true)
			{
				$frmdata['company_name']= $frmdata['company_name'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for company name.<br>";
			}
		
		}
		
		if (trim($frmdata['plan_id']) != '')
		{
            $frmdata['plan_id']= $frmdata['plan_id'];
        }
		else
		{
			$err .= "Please select subscription plan .<br>";
			$frmdata['plan_id']='';
		}
		
		if (trim($frmdata['user_name']) == '')
		{
            $err .= "Please enter contact person name.<br>";
			$frmdata['user_name']='';
        }
		else
		{
			if(validatename($frmdata['user_name'])== true)
			{
				$frmdata['user_name']= $frmdata['user_name'];
			}
			else
			{
				$err .= "Only alpha numeric and white space allowed for contact person name.<br>";
			}
		
		}
		
		if (trim($frmdata['expiary_date']) == '')
		{
            $err .= "Please select expiry date.<br>";
			$frmdata['expiary_date']='';
        }
				
		if (trim($frmdata['creation_date']) == '')
		{
            $err .= "Please select creation date.<br>";
			$frmdata['creation_date']='';
        }
		
		if (trim($frmdata['company_email']) != '')
		{
            if ( !filter_var($frmdata['company_email'], FILTER_VALIDATE_EMAIL ) )
			{
				$err.="Please enter a valid email address.<br>";
				$frmdata['company_email']='';

			}
			else
			{
				$frmdata['company_email']=$frmdata['company_email'];
			}
		}
		else
		{
            $err .= "Please enter company email .<br>";
			$frmdata['company_email']='';
        }
		
		if(strtotime($frmdata['expiary_date']) < strtotime($frmdata['creation_date']))
		{
			$err .= "Please enter valid expiry date.<br>";
		}
			
		if($_FILES["company_logo"]["name"] == '')
		{
			$filename = $frmdata['logo'];	
		}
		else
		{
			if(validateimage($_FILES["company_logo"]["name"])== true)
			{
				if($_FILES['company_logo']['size'] > 2097152)//added by : Neha Pareek. (02/12/2015)
				{
					$err.='Logo size must be less than 2 MB'.'<br/>';
				}
				else
				{
					$_FILES["company_logo"]["name"] = $_FILES["company_logo"]["name"];
				}
			}
			else
			{
				$err .= "Please select only an image.<br>";

			}
		}
		if($_FILES["company_logo"]["name"]!='')
		{
			$image_name = $_FILES["company_logo"]["name"];
			$source_image=$_FILES["company_logo"]["tmp_name"];
			$filename= time()."_".$image_name;
			$folderpath = ROOT."images/company_logo/".$filename;
			$oldfile = ROOT."images/company_logo/".$frmdata['logo'];
			unlink($oldfile);
			move_uploaded_file($source_image,$folderpath);
		}
		else 
		{  
			
			$filename = $frmdata['logo'];
		} 
		if ($err == '')
        {
			$frmdata['expiary_date']= date("Y-m-d", strtotime($frmdata['expiary_date']));	
			$frmdata['creation_date']= date("Y-m-d", strtotime($frmdata['creation_date']));	
			$obj = new passEncDec;
			$password = $obj->encrypt_password($frmdata['password']);
			
			$company_data = array('company_name'=>$frmdata['company_name'],'plan_id'=>$frmdata['plan_id'],'creation_date'=>$frmdata['creation_date'],'expiary_date'=>$frmdata['expiary_date'],'user_name'=>$frmdata['user_name'],'company_email'=>$frmdata['company_email'],'is_active'=>$frmdata['is_active'],'company_logo'=>$filename);
			//	print_r($company_data);exit;
            if($DBFilter->UpdateRecord('company_detail', $company_data, "company_id ='$id'"))
            {
				//$company_id=$id;
				$user_data = $DBFilter->SelectRecord('users',"company_id='$id' and role_id='2'");
				$admin_data = array('company_id'=>$id,'role_id'=>'2','user_email'=>$frmdata['company_email'],'is_active'=>$frmdata['is_active'],'is_deleted'=>'Y');
				/*print_r($company_data);
				 print_r($admin_data);
				 exit;*/
				$company_admin = $DBFilter->UpdateRecord('users', $admin_data, "company_id ='$id' and role_id='2'");
				
				$_SESSION['success'] = "Company updated successfully.";
                Redirect(CreateURL('index.php', 'mod=company'));
                die();
            }
        }
        else
        {	
            $_SESSION['error'] = $err;
        }
        
    }
	//============================================================================
        
    //Delete Multiple or Other Actions added by : Neha Pareek
        
    //============================================================================
	
	if($frmdata['actions'])
	{
		if($frmdata['chkbox']=='')
		{
			$_SESSION['error'] = 'Please select atleast one company.';
		}
		else 
		{
			$ids = $frmdata['chkbox'];
			//print_r($ids);exit;
			$action = $frmdata['actions'];
			$cond = "company_id=";
			multipleAction($ids,'company_detail',$action,$cond);
			$_SESSION['success'] = "Companies has been ".$action."d successfully.";
			Redirect(CreateURL('index.php', 'mod=company'));
			die();
		}
	}
    //============================================================================
        
    //Delete the company
        
    //============================================================================
    elseif ($do == 'del')
    {
		
		//echo 'in'.$id;exit;
		$related_detail = DeleteRelatedData($id,$_SESSION['usertype'],'company_detail');       
		if($related_detail	== true)
		{
			$_SESSION['success'] = 'Company has been deleted successfully.';
			Redirect(CreateURL('index.php', 'mod=company'));
			die();
		}
		else 
		{
			$_SESSION['error'] = 'There is problem in deletion. Try again later.';
			Redirect(CreateURL('index.php', 'mod=company'));
			die();	
			
		}
    }

?>