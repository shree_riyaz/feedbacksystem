<?php
	
	if (isset($frmdata['clearsearch']))
    {
        unset($frmdata['keyword']);
		unset($_SESSION['pageNumber']);
        $CFG->template = "facilityandServices/list.php";
    }


    if (isset($frmdata['add']))
    {
//        echo 777; exit;

//        $get_service_id = $DBFilter->SelectRecord('services',"company_id='".$_SESSION['company_id']."'");

        $get_service_id_query = "select service_id from services where is_active='Y' and is_deleted ='N' and company_id = '".$_SESSION['company_id']."'";
        $get_service_id_result = $DBFilter->RunSelectQuery($get_service_id_query);
        $get_service_id = $get_service_id_result[0][0]->service_id;

        $query_get_fault_name = "select fault_name from faults where is_active='Y' and is_deleted ='N' and service_id = '$get_service_id'";
        $result_fault_name = $DBFilter->RunSelectQuery($query_get_fault_name);
        $query_get_fault_name_aray = [];
        foreach ($result_fault_name[0] as $result_fault_name_val){
            $query_get_fault_name_aray[] = strtolower($result_fault_name_val->fault_name);
        }
//        echo '<pre>';print_r($frmdata['mytextfield']);	 exit;

        $err = '';
		// Image field validation
		$file_count = count($_FILES["myFilesfield"]["name"]);
		//print_r($_FILES["myFilesfield"]["name"]);
		/*if($frmdata['mytextfield']!="")
		{
			if (!preg_match("/^[a-zA-Z]*$/",$frmdata['mytextfield']))
			{
				$err.='Fault name should only contain alphabets'.'<br/>';
			}
		}*/
		// Image Validation

        $c = 0;
		foreach($_FILES['myFilesfield']['name'] as $key => $name )
		{
		    $c++;
			$file_name = $_FILES['myFilesfield']['name'][$key];
			$file_size =$_FILES['myFilesfield']['size'][$key];
		
			if($file_name =='')
			{
				$err.='Please select fault image in row '.$c.'.<br/>';
			}
			if($file_size > 2097152)
			{
				$err.='Image size must be less than 2 MB'.'<br/>';
			}
		
		}
        // Active image Validation
        $a = 0;
        foreach($_FILES['active_image']['name'] as $key => $name )
		{
		    $a++;
			$file_name = $_FILES['active_image']['name'][$key];
			$file_size =$_FILES['active_image']['size'][$key];

			if($file_name =='')
			{
				$err.='Please select active image in row '.$a.'.<br/>';
			}
			if($file_size > 2097152)
			{
				$err.='Active image size must be less than 2 MB.'.'<br/>';
			}

		}


        foreach ($_POST['mytextfield'] as $fault_list){

            $is_fault_avail = in_array(strtolower($fault_list),$query_get_fault_name_aray);

            if ($is_fault_avail == 1){
                $err.= $fault_list.' '.' is already added, please add by different name.'.'<br/>';
            }


//            echo $fault_list;
        }
//        echo '<pre>';print_r($is_fault_avail);	 exit;



        if ($err == '')
        {
			if($_FILES["myFilesfield"]["name"]!='')
			{
				$service_data = $DBFilter->SelectRecord('services', "company_id='".$_SESSION['company_id']."'");
				$service_id = $service_data->service_id;
				//echo $file_count;
				//echo "<br>";
				for($i = 0; $i < $file_count ; $i++)
				{	
					$image_name = $_FILES["myFilesfield"]["name"][$i];
					$source_image = $_FILES["myFilesfield"]["tmp_name"][$i];
					$filename= time()."_".$image_name;
					$folderpath = ROOT."/images/faults_images/".$filename;
					move_uploaded_file($source_image,$folderpath);

                    $image_name_active = $_FILES["active_image"]["name"][$i];
                    $source_image_active = $_FILES["active_image"]["tmp_name"][$i];
                    $filename_active = time()."_active_image_".$image_name_active;
                    $folderpath_active = ROOT."/images/faults_images/".$filename_active;
                    move_uploaded_file($source_image_active,$folderpath_active);



//        echo '<pre>'; print_r($query_interval); exit;
                    $interval_result = $DBFilter->RunSelectQuery($query_interval);
                    $faults_data =array('fault_name'=>trim($frmdata['mytextfield'][$i]),'fault_image'=>$filename,'active_image'=>$filename_active,'service_id'=>$service_id);
//					echo '<pre>'; print_r($faults_data); exit;
					$add_faults = $DBFilter->InsertRecord('faults', $faults_data);
				}
			//exit;
				$_SESSION['success'] = "Faults added successfully.";//exit;
				Redirect(CreateURL('index.php', 'mod=faults&do=view&id=1'));
				exit;
			}
		}
		else
        {
            $_SESSION['error'] = "<font color=red>" . $err . "</font>";
        }
	}
 
        
    //Delete the Fault
        
    //============================================================================
    elseif ($do == 'del')
    {	
		 $data = array('is_deleted'=>'Y','is_active'=>'N');
	     $service_data = $DBFilter->SelectRecord('services', "company_id='".$_SESSION['company_id']."'");
	  	 $service_id = $service_data->service_id;
	//	$services = $DBFilter->Select('faults', $data, "fault_id ='$id'");
        $DBFilter->UpdateRecord('faults', $data, "fault_id ='$id'");
        $_SESSION['success'] = 'Fault has been deleted successfully.';
        Redirect(CreateURL('index.php', "mod=faults&do=view&id=$service_id"));
        die();
    }

?>