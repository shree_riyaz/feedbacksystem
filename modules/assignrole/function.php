<?php
//================ For getting URL variables =============
if(isset($frmdata['deletetrade']))
{	
	if (count($frmdata['delchk']) > 0)
	{	
		//echo "<pre>";
		//var_dump($frmdata['delchk']);
		$count = 0;
		foreach($frmdata['delchk'] as $value)
			{
				$data['role_id'] = '';
				$data['fromDate'] = '';
				$data['toDate'] = '';
				$DB->UpdateRecord('candidate', $data, 'id = '.$value);	
				$count++;
			}	
		//	echo '<pre>';
		//	var_dump($demo);
		$_SESSION['success']= $count." Records has been deleted successfully.";
	}
	$_SESSION['success'] = 'User has been removed from the admin user list.';		
	Redirect(CreateURL('index.php','mod=assignrole'));die();
}
if(isset($getVars['do']))
{
	$do=$getVars['do'];
}
else
{
	$do='';
}

if(isset($getVars['nameID']))
{
	$nameID=$getVars['nameID'];
}
else
{
	$nameID='';
}

if(isset($getVars['mes']))
{
		$frmdata['message']=$getVars['mes'];
}

//=======To clear search when clearsearch button is clicked =====
if(isset($frmdata['clearsearch']))
{
		unset ($frmdata);
		unset($_SESSION['pageNumber']);
		$CFG->template="role/role.php";
}

//========== Work for adding file ===============
//$userFlag=0;


//======================================================================
//Assign Role
//======================================================================

if (isset($frmdata['assignrole']))
{
	//print_r($frmdata);
	$err = '';
	if ($frmdata['role_id'] == '')
	{
		$err .= 'Please select role.<br>';
	}
	if ($frmdata['candidate_id'] == '')
	{
		$err .= 'Please select user.<br>';
	}
	//print_r($frmdata);
	if ($frmdata['fromDate'] != '' && $frmdata['toDate'] != '')
	{
		$frmdata['fromDate'] = str_replace('-','/',$frmdata['fromDate']);
	    $fromDate = strtotime($frmdata['fromDate']);
		$frmdata['toDate'] = str_replace('-','/',$frmdata['toDate']);
		$toDate = strtotime($frmdata['toDate']);
		if ($fromDate > $toDate)
		{
			$err .= "Please enter a valid date range.";
		}
	}
	if ($frmdata['candidate_id'] != '' && $nameID == '')
	{
		$exists = $DB->SelectRecord('candidate','role_id !="" and role_id is not null and candidate_id ='.$frmdata['candidate_id']);
		if ($exists)
		{
			$err .= 'User already assigned a role.';
		}
	}
	if ($err == '')
	{
			
		if ($frmdata['fromDate'] != '')
		{
			 $frmdata['fromDate'] = date('Y-m-d h:i:s',strtotime($frmdata['fromDate']));
		}
		if ($frmdata['toDate'] != '')
		{	
			 $frmdata['toDate'] = date('Y-m-d h:i:s',strtotime($frmdata['toDate']));		
		}	
		$user = $frmdata['candidate_id'];
		unset($frmdata['candidate_id']);
		$DB->UpdateRecord('candidate',$frmdata, 'id = '.$user);
		$_SESSION['success'] = 'Role has been assigned to the selected user.';
		Redirect(CreateURL('index.php','mod=assignrole'));die();
		
	}
	else
	{
		$_SESSION['error'] = $err;	
	}
}

//============================================================================
//Delete the role assignment by updating candidate record data to NULL
//============================================================================
if ($do == 'del')
{
	$data['role_id'] = '';
	$data['fromDate'] = '';
	$data['toDate'] = '';
	$DB->UpdateRecord('candidate', $data, 'id = '.$nameID);
	$_SESSION['success'] = 'User has been removed from the admin user list.';		
	Redirect(CreateURL('index.php','mod=assignrole'));die();
}

$modules = $DB->SelectRecords('module');
?>