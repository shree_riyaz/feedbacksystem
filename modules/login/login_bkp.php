<?php
/*======================================
		Developer	-	Jaishree Sahal
	    Date        -   5 june 2015
		Module      -   login
		view		-   login form
		SunArc Tech. Pvt. Ltd.
======================================
******************************************************/
if($_SESSION['usertype']!= '')
{
?>
	<script type="text/javascript">
		location.href="index.php?mod=company";
	</script>
<?php
}
/*
if(isset($_POST['Reset']))
{
	unset($_SESSION['msg']);
}*/
?>
<script>
function Clear()
{
document.loginform.user_email.value='';
document.loginform.password.value='';
document.loginform.user_email.focus();
	// document.getElementById('user_name').value='';
	// document.getElementById('company_name').value='';
	// document.getElementById('creation_date').value='';
	// document.getElementById('expiary_date').value='';
	// document.getElementById('subscription_plan').value='';
	// document.getElementById('company_email').value='';
	location.href="index.php?mod=login";
	return false;
}
</script>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style type="text/css">
    .bs-example{
    	margin: 20px;
		width:80%;
    }
	/* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
    .form-horizontal .control-label{
        padding-top: 7px;
    }
</style>

</head>
<body>
<center>
   <?php

		$lang = $language->english($lang);
		// print_r($lang);
		if(isset( $_SESSION['msg'] )and trim($_SESSION['msg'])!='')
		{
		?>
		 <table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" >
					<tr>
						<td colspan="2"  align="center">
						<div class="successmsg" style="vertical-align:top; height: 35;">
							<?php echo $_SESSION['msg'] ;
							unset($_SESSION['msg']); ?>
						</div></td>
					</tr>
				</table>
		<?php
		}
		?>
</tbody>
</table>


<h2 align="center"><?php echo $lang['Login']?></h2><br/><br/>
    <div style="width:80%;" align="center">
	 <form class="form-horizontal" name="loginform" action="" method="post" >
         <div class="form-group">
		 <label for="user_email" class="control-label col-xs-4">
		 <?php echo $lang['User Email']?>
		 </label>
		<div class="col-xs-6">
		   	<input type = "text" name="user_email" id="user_email" placeholder="User Email" class="form-control" value="<?php if($_POST['user_email'] != '') echo $_POST['user_email']?>">
		 </div>
		 </div>
         <div class="form-group">
            <label for="password" class="control-label col-xs-4">
			<?php echo $lang['Password']?>
			</label>
			<div class="col-xs-6">
			 <input type="password" class="form-control" id="password" placeholder="Password" name="password">
			</div>
		</div>
		<?php //echo "<pre>";print_r($frmdata); //print_r($_POST);?>
		<div class="form-group">
			<div class="col-xs-6" style="width:90.333%">
			<button type="submit" class="btn btn-primary" name="submit"><?php echo $lang['Login']?></button>
			<button type="reset" class="btn btn-primary" name="Reset" onClick="return Clear()"><?php echo $lang['Reset']?>
			</button>
			</div>
		</div>
    </form>

</div>
</center>
<?php
include_once ROOT."templates/default/footer.php";
?>