<?php 
/*=======================================================================
	@Auther 	Niteen Achray
	@Date   	18/03/2007
	@Company	Sunarc Technologies
//======================================================================= */
defined("ACCESS") or die("Access Restricted");

/*###########################################################################################
@para	: members record object
@return : session object
@Des 	: to set session for a member

//###########################################################################################*/

function CreateSession($session)
{

	global $frmdata,$DBFilter;
	
	//if(isset($_SESSION['auth']))
	$data['sessiondata']=serialize($session);
	$data['sessionid']=session_id();
	$data['sessiondate']=date('Y-m-d');

	InsertRecord('sessiondetails',$data);
	{
	$_SESSION['auth']=serialize($session);
	}
	return SetSession();
}


/*###########################################################################################
@para	: --
@return : sesson object
@Des 	: to set session data

//###########################################################################################*/

function SetSession()
{
$DBFilter= new DBFilter();
	
	global $_SESSION;
	
	$sessionRecord = $DBFilter->SelectRecord('sessiondetails',"sessionid='".session_id()."'");
	
	if($sessionRecord && isset($_SESSION['auth']))
	{
	
		return (object) @unserialize($_SESSION['auth']);
	}
	else
	{
		return false;
	}	
}


/*###########################################################################################
@para	: --
@return : ---
@Des 	: to update session after changeing data

//###########################################################################################*/

function UpdateSession()
{
	global $_SESSION;
	global $session,$DBFilter;
	//print_r($_SESSION);
	$session=$DBFilter->SelectRecord('members',"memid='".$session->
memid."'");
	
	$DBFilter->UpdateRecord('sessiondetails',array("sessiondata='".serialize($session)."'"),"sessionid='".session_id()."'");
	$_SESSION['auth']=serialize($session);
		
}

function getLoginCredentials()
{
	global $frmdata,$DBFilter;
	
	$result=$DBFilter->SelectRecord('users',"is_deleted='N' and user_email='".$frmdata['user_email']."'"); //Added by : Neha Pareek. Dated : 15 Dec 2015
	
	//$result=$DBFilter->SelectRecord('users',"is_deleted='N' and is_active='Y' and user_email='".$frmdata['user_email']."'");
	//print_r($result);exit;
	$result= json_decode(json_encode($result), true);
	return $result;
}
/*###########################################################################################
 function is used to get access-key 
 Devloper - Jaishree Sahal 
 date  -13-Aug-2015
//###########################################################################################*/
function getAccessKey($id)
{
    global $frmdata,$DBFilter;
	$result=$DBFilter->SelectRecord('login_detail',"user_id=".$id);
	//print_r($result);
	$result= json_decode(json_encode($result), true);
	return $result;
}

### for getting access key using both Id and Password for Login Module
function getAccessKeylog($id,$pwd)
{
    global $frmdata,$DBFilter;
	$result=$DBFilter->SelectRecord('login_detail','"user_id=".$id and "password=".$pwd');
	//print_r($result);
	$result= json_decode(json_encode($result), true);
	return $result;
}
/*###########################################################################################
 function is used to insert access-key 
 Devloper - Jaishree Sahal 
 date  -13-Aug-2015
//###########################################################################################*/
function insertAccessKey($table,$data)
{
	global $frmdata,$DBFilter;
	
	$get_accesskey = $DBFilter->SelectRecord('login_detail',"user_id=".$data['user_id']);
	//print_r($get_accesskey);exit;
	if($get_accesskey!='')
	{
		
		$update_result = $DBFilter->UpdateRecord($table,$data,"user_id=".$data['user_id']);
		return $update_result;
	}
	else
	{
		$insert_result=$DBFilter->InsertRecord($table,$data);
	}
	$insert_result= json_decode(json_encode($result), true);
	return $insert_result;
}
function getDevices(&$totalCount,$orderby='')
{

global $frmdata,$DBFilter;
$flag=0;
/*$query="select d.id id ,
       d.device_id, 
       d.make,
       d.model,
       m.id m_id,
       m.company_name,
       mdl.id mdl_id,
       mdl.model_name,
       count(dc.component_id) as totalComponent
from device d
     left join manufacturers m on m.id = d.make and (m.manufacturer_for='device' or m.manufacturer_for='all')
     left join models mdl on mdl.id = d.model  
     left join device_components dc on dc.device_id = d.id";*/

//Query changed by Bajrang.
$query="select d.id id,
       d.device_id as device_name,
       d.device_id,
       d.make,
       d.model,
       m.id m_id,
       m.company_name,
       mdl.id mdl_id,
       mdl.model_name,
       count(distinct dc.component_id) as totalComponent,
       
       /*sum(IF(ckf.key_factor_field_type = 'drop down',kv.kg_co2*dckf.quantity,
       dckf.key_factor_value
       )) as totalCo2 */
       
      ((IF(sum(( (IF (ckf.key_factor_field_type = 'drop down', kv.kg_co2 *
         dckf.quantity ,
         dckf.key_factor_value * ckf.kg_co2)) ) ) > 0,
         
         sum(( (IF (ckf.key_factor_field_type = 'drop down', kv.kg_co2 *
         dckf.quantity ,
         dckf.key_factor_value * ckf.kg_co2)) ) ), 0)
         +
        IF(sum(add_co2.Additional_co2) > 0,sum(add_co2.Additional_co2),0)
         
           )* dc.component_quantity)  as totalCo2     
       
from device d
     left join manufacturers m on m.id = d.make 
     left join models mdl on mdl.id = d.model and (mdl.model_for = 'device' or mdl.model_for = 'all')
     left join device_components dc on dc.device_id = d.id
     
     left join component_key_factor ckf on ckf.component_id = dc.component_id and ckf.status='A'
     left join device_component_key_factors dckf on dckf.component_id = dc.component_id 
 		    and dckf.key_factor_id = ckf.id and dckf.device_id = d.id
     
     left join kf_values kv on kv.id = dckf.key_factor_value 
	     	and ckf.key_factor_field_type = 'drop down' and kv.status='A'
	     	
     left join (select sum(a.Additional_KGCO2) as  Additional_co2, a.*  from  additional_kgco2 a group by a.dc_Id ) add_co2 on add_co2.dc_Id = dc.id
/* group by d.id , d.id,m.id,mdl.id,dc.id,  ckf.id,dckf.id, kv.id */";

if(isset($frmdata['make']) && $frmdata['make']!='ps' && $frmdata['make'] !='' || isset($frmdata['model']) && $frmdata['model']!='ps' && $frmdata['model'] != '')
{
	$query.=" where 1 ";
	if(isset($frmdata['make']) && $frmdata['make']!='ps' and trim($frmdata['make'])!='')
	{	
		 $query.=" and d.make = '".$frmdata['make']."' ";
		$flag=1;
	
	}
	if(isset($frmdata['model']) && $frmdata['model']!='ps' and trim($frmdata['model']) != '')
	{	
		$query.=" and d.model = '".$frmdata['model']."' ";
		$flag=1;
	
	}
}

$query.=" group by d.id ";

	if($orderby!='')
		$query.=" order by ".$orderby;
	else 
	    $query.=" order by id desc ";
	    
  //echo $query;
	$result=$DBFilter->RunSelectQueryWithPagination($query,$totalCount);
	return $result;
}

?>
