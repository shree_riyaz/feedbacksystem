<?php
/* to send post data*/
function ChangeData()
{
	global $_POST;
	return  $_POST;
}
/*###########################################################################################
@Auther : Niteen Acharya
@Des 	: to make a link
//###########################################################################################*/
function CreateURL($url,$querystring='',$encode=false,$redirect=false)
{
	//if($url)
	{

		//$querystring.="&osCsid=".$_SESSION['sessionid'];

		if($querystring)
		{
			//$querystring.='sessionid='.$_SESSION['sessionid'];  // changed by pukhraj
			
			if($encode)
			{
				return $url.'?'.Encode($querystring);		
			}
			else
			{
				return $url.'?'.$querystring;		
			}
		}
		else
		{
			return $url;
		}
	}
	return false;
	
}
//################################################################################################
/*###########################################################################################
@Auther : Niteen Acharya
@para	: any value
@return : decoded value
@Des 	: to decode value
//###########################################################################################*/
function Decode($value)
{
	return base64_decode($value);
}

?>