<?php




/*======================================
		Developer	-	Seema
	    Date        -   22 june 2012
		Module      -   index.php
		SunArc Tech. Pvt. Ltd.
======================================		
******************************************************/	

/*@define("ROOT",$_SERVER['DOCUMENT_ROOT']);
@define("ROOTADMINURL","https://".$_SERVER['HTTP_HOST']);
@define("ADMINROOT",$_SERVER['DOCUMENT_ROOT']);
@define("ROOTUSERURL","https://".$_SERVER['HTTP_HOST']);
@define("ADMINSCRIPTROOT","https://".$_SERVER['HTTP_HOST']);
@define("SITEURL","https://".$_SERVER['HTTP_HOST']);
@define("IMAGEURL","https://".$_SERVER['HTTP_HOST']."/images/");*/

@define("ROOT",$_SERVER['DOCUMENT_ROOT']."/feedbacksystem");
@define("ROOTADMINURL","https://".$_SERVER['HTTP_HOST']."/feedbacksystem");
@define("ADMINROOT",$_SERVER['DOCUMENT_ROOT']."/feedbacksystem");
@define("ROOTUSERURL","https://".$_SERVER['HTTP_HOST']."/feedbacksystem");
@define("ADMINSCRIPTROOT","https://".$_SERVER['HTTP_HOST']."/feedbacksystem");
@define("SITEURL","https://".$_SERVER['HTTP_HOST']."/feedbacksystem");
@define("IMAGEURL","https://".$_SERVER['HTTP_HOST']."/feedbacksystem/images/");
@define("LANGUAGEURL","https://".$_SERVER['HTTP_HOST']."/feedbacksystem/language/");
@define("LANG",ADMINROOT."/language");

@define('SHOW_QUERY_FAILURE',true); //Added by bajrang to print the query failure massage.
@define('ACCESS',1);
@define('PREFIX','');
@define('CROPIMG','cropImg_');

@define('HOST','localhost');
@define('USER','root');
//@define('PASSWORD','SunArc@123');
@define('PASSWORD','$unArc@!#');
//@define('PASSWORD','');
//@define('DATABASE','feedbacksystem_uat');
@define('DATABASE','feedbacksystem');

/*@define('USER','pukhraj');
@define('PASSWORD','pukhraj123');*/
/*@define('USER','cleanerm_wfsys');
@define('PASSWORD','U-H8-Zo)=W6+');
@define('DATABASE','cleanerm_feedbacksystem');*/

@define("BACKUP_DIR",ADMINROOT."/modules/backup/db_backups/");
@define("BACKUP_URL",ROOTADMINURL."/modules/backup/db_backups/");
@define("MY_SQLDUMP_COMMAND","mysqldump");
@define("MY_SQL_COMMAND","mysql");

@define("MOD","modules");
@define("FILEPATH",ADMINROOT."/banimg/");
@define("TEMPLETPATH","templates");
@define("STYLE",ADMINROOT."/style");
@define("JS",ADMINROOT."/js");
@define("CURRENTTEMP",ADMINROOT."/templates/default");
define("IMAGESIZE",33554432);
define("IMAGEEXT","jpg,gif,png,jpeg,pjpeg");
define("FILESIZE",33554432);
define("FILEEXT","docx,doc,txt,xls,pdf");
@define("MANDATORYNOTE","<div ><strong >Note</strong> : Fields marked by (<font color='#FF0000'><strong>*</strong></font>) are mandatory.</div>"); // for mandatory notes

@define("MANDATORYMARK","  <font color='#FF0000' size='3'><strong>*</strong></font>"); // for mandatory mark
@define("NOSTRDATA"," <font> <i> N/A </i></font>");

//This variable holds the tables name which not require to get backups under backup module.
$CONFIG_SETTING_DONT_BACKUP_TABLES_ARR = array("permission","adminlogin","role","rolepermission","users");

//echo $_SERVER['HOST']; exit;
?>