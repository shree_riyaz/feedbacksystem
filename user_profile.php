
<span class="pull-right admin-select">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
    <?php if ($_SESSION['role_id'] != 3 && $_SESSION['user_id'] != '') { ?>
       <span><?php echo $user_name->first_name; ?>&nbsp;&nbsp;</span>
       <i class="fa fa-user-circle-o" aria-hidden="true"></i>
       </button>
       <ul class="dropdown-menu">
          <li><a href="<?php print CreateURL('index.php', 'mod=user&do=edit&id=' . $_SESSION['user_id']); ?>">Profile</a></li>
          <li><a href="<?php print CreateURL('index.php', 'mod=login&do=logout'); ?>">Logout</a></li>
       </ul>
        <?php
        }
        else if ($_SESSION['role_id'] == 3 && $_SESSION['user_id'] != '') {
            ?>
            <span><?php echo $user_name->first_name; ?></span>
            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu">
<!--          <li><a href="--><?php //print CreateURL('index.php', 'mod=user&do=edit&id=' . $_SESSION['user_id']); ?><!--">Profile</a></li>-->
          <li><a href="<?php print CreateURL('index.php', 'mod=login&do=logout'); ?>">Logout</a></li>
       </ul>
    <?php } ?>
</div>
</span>